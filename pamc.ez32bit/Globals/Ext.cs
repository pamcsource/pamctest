// <ppj name="pamc.ez32bit" date="04.09.2018 16:26:42" id="23D03E010660E8A27F254B6E94AF56DEE10839E3"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;
using PPJ.Runtime;
using PPJ.Runtime.Windows;

namespace pamc.ez32bit
{
	
	public static class Ext
	{
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalNumber GetWindowLongA(SalWindowHandle param1, SalNumber param2)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			int _param2 = (int)param2;


			retVal = USER32.GetWindowLongA(_param1, _param2);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalNumber SetWindowLongA(SalWindowHandle param1, SalNumber param2, SalNumber param3)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			int _param2 = (int)param2;
			int _param3 = (int)param3;


			retVal = USER32.SetWindowLongA(_param1, _param2, _param3);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalBoolean ShowWindow(SalWindowHandle param1, SalNumber param2)
		{
			SalBoolean retVal = false;
			System.IntPtr _param1 = (System.IntPtr)param1;
			int _param2 = (int)param2;


			retVal = USER32.ShowWindow(_param1, _param2);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalWindowHandle GetWindow(SalWindowHandle param1, SalNumber param2)
		{
			SalWindowHandle retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			uint _param2 = (uint)param2;


			retVal = USER32.GetWindow(_param1, _param2);

			return retVal;
		}
		
		/// <summary>
		/// Function: GetNextQueueWindow
		/// Description:
		/// Export Ordinal: 0
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Window Handle: HWND
		/// Number: UINT
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalWindowHandle GetParent(SalWindowHandle param1)
		{
			SalWindowHandle retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;


			retVal = USER32.GetParent(_param1);

			return retVal;
		}
		
		/// <summary>
		/// Function: SetWindowPos
		/// Description:
		/// Export Ordinal: 0
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: INT
		/// Number: INT
		/// Number: INT
		/// Number: INT
		/// Number: INT
		/// Enables, disables, or grays a menu item. hMenu is
		/// the menu item, nID is the menu item to be checked.
		/// It can specify pop-up menu items as well as menu
		/// items. nEnable specifies the action to take place.
		/// Uses MF_* flags Disabled, Enabled and Grayed, as
		/// well as ByCommand and ByPosition. Note: pop-ups
		/// must have ByPosition ORed in nEnable; nID specifies
		/// the position in hMenu.
		/// nPrevState = EnableMenuItem( hMenu, nID, nEnable )
		/// JL - per Peter, changed Export Ordinal from 155 to 0.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalNumber EnableMenuItem(SalWindowHandle param1, SalNumber param2, SalNumber param3)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			uint _param2 = (uint)param2;
			uint _param3 = (uint)param3;


			retVal = USER32.EnableMenuItem(_param1, _param2, _param3);

			return retVal;
		}
		
		/// <summary>
		/// determines the number of items in the menu
		/// identified by hMenu. Returns the number of items
		/// in the menu.
		/// nCount = GetMenuItemCount( hMenu )
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GetMenuItemCount(SalWindowHandle param1)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;


			retVal = USER32.GetMenuItemCount(_param1);

			return retVal;
		}
		
		/// <summary>
		/// obtains the menu item identified by hMenu for a
		/// menu item located at nPos. Returns the item ID for
		/// the menu item. nPos is a multiple of 320.
		/// nID = GetMenuItemID( hMenu, nPos )
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalNumber GetMenuItemID(SalWindowHandle param1, SalNumber param2)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			int _param2 = (int)param2;


			retVal = USER32.GetMenuItemID(_param1, _param2);

			return retVal;
		}
		
		/// <summary>
		/// copies the label of the specified menu item into
		/// sLabel. nMax is the max length of the label to be
		/// copied. Returns the number of bytes copied.
		/// nSize = GetMenuString( hMenu, nID, sLabel, nMax, nFlags )
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <param name="param4"></param>
		/// <param name="param5"></param>
		/// <returns></returns>
		public static SalNumber GetMenuStringA(SalWindowHandle param1, SalNumber param2, ref SalString param3, SalNumber param4, SalNumber param5)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			uint _param2 = (uint)param2;
			System.Text.StringBuilder _param3 = new System.Text.StringBuilder(param3.Value);
			int _param4 = (int)param4;
			uint _param5 = (uint)param5;


			retVal = USER32.GetMenuStringA(_param1, _param2, _param3, _param4, _param5);

			param3 = _param3.ToString();
			return retVal;
		}
		
		/// <summary>
		/// Allows the application to access the system
		/// menu for copying and modification. hWndOwner is
		/// the windows that will own a copy of the system
		/// menu; bRevert specifies the action to be taken.
		/// hWndReturn = GetSystemMenu( hWndOwner, bRevert )
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalWindowHandle GetSystemMenu(SalWindowHandle param1, SalBoolean param2)
		{
			SalWindowHandle retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;
			bool _param2 = (bool)param2;


			retVal = USER32.GetSystemMenu(_param1, _param2);

			return retVal;
		}
		
		/// <summary>
		/// Function: GetCursorPos
		/// Description:
		/// Export Ordinal: 0
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Receive Number: LPLONG
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GetSystemMetrics(SalNumber param1)
		{
			SalNumber retVal = 0;
			int _param1 = (int)param1;


			retVal = USER32.GetSystemMetrics(_param1);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GetKeyState(SalNumber param1)
		{
			SalNumber retVal = 0;
			int _param1 = (int)param1;


			retVal = USER32.GetKeyState(_param1);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <param name="param4"></param>
		/// <param name="param5"></param>
		/// <returns></returns>
		public static SalBoolean ModifyMenuA(SalWindowHandle param1, SalNumber param2, SalNumber param3, SalNumber param4, SalString param5)
		{
			SalBoolean retVal = false;
			System.IntPtr _param1 = (System.IntPtr)param1;
			uint _param2 = (uint)param2;
			uint _param3 = (uint)param3;
			uint _param4 = (uint)param4;
			string _param5 = param5.Value;


			retVal = USER32.ModifyMenuA(_param1, _param2, _param3, _param4, _param5);

			return retVal;
		}
		
		/// <summary>
		/// Function: WindowFromPoint
		/// Description:
		/// Export Ordinal: 30
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Number: LONG
		/// Function: FindWindow
		/// Description:
		/// Export Ordinal: 50
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// String: LPSTR
		/// String: LPSTR
		/// Function: SetActiveWindow
		/// Description:
		/// Export Ordinal: 59
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Window Handle: HWND
		/// Function: GetActiveWindow
		/// Description:
		/// Export Ordinal: 60
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Function: GetWindowWord
		/// Description:
		/// Export Ordinal: 133
		/// Returns
		/// Number: WORD
		/// Parameters
		/// Window Handle: HWND
		/// Number: INT
		/// Function: GetClientRect
		/// Description:
		/// Export Ordinal: 33
		/// Returns
		/// Parameters
		/// Window Handle: HWND
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Function: GetWindowRect
		/// Description:
		/// Export Ordinal: 32
		/// Returns
		/// Parameters
		/// Window Handle: HWND
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Receive Number: LPINT
		/// Function: GetClassName
		/// Description:
		/// Export Ordinal: 58
		/// Returns
		/// Number: INT
		/// Parameters
		/// Window Handle: HWND
		/// Receive String: LPSTR
		/// Number: INT
		/// Function: GetTickCount
		/// Description:
		/// Export Ordinal: 13
		/// Returns
		/// Number: DWORD
		/// Parameters
		/// Function: GetFreeSystemResources
		/// Description: Returns the percentage of free space for system resources
		/// Export Ordinal: 284
		/// Returns
		/// Number: INT
		/// Parameters
		/// Number: INT
		/// GFSR_SYSTEMRESOURCES (0x0000) Free space for System resources
		/// GFSR_GDIRESOURCES (0x0001) Free space for GDI resources
		/// GFSR_USERRESOURCES (0x0002) Free space for User resources
		/// SDK Menu Functions
		/// Function: AppendMenu
		/// Description: Appends a new menu item to the end of a menu.  Initial state of the
		/// menu in the wFlags parameter.
		/// bOk = AppendMenu( hMenu, wFlags, wIDNewItem, sItemName )
		/// Export Ordinal: 411
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// String: LPSTR
		/// Function: ChangeMenu
		/// Description: Appends, inserts, deletes or modifies a menu item
		/// in the menu given by hMenu. Other parms. define
		/// which item change and how to change it: nIDChange
		/// is the item to be changed; sLabel is the string
		/// for change/append. nFlags must have MF_STRING;
		/// nIDNew specifies the menu item to replace; nFlags
		/// is the MF_* flags associated with the change.
		/// bOK = ChangeMenu( hMenu, nIDChange, sItem, nIDNew, nFlags )
		/// Export Ordinal: 153
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// String: LPSTR
		/// Number: WORD
		/// Number: WORD
		/// Function: CheckMenuItem
		/// Description:
		/// Export Ordinal: 154
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// Function: CreateMenu
		/// Description: Creates a menu. The menu is initially empty, but can
		/// be filled with menu items by using the ChangeMenu
		/// function.
		/// hMenu = CreateMenu()
		/// Export Ordinal: 151
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Function: CreatePopupMenu
		/// Description: Creates and returns a handle to an empty pop-up menu.
		/// hMenu = CreatePopupMenu()
		/// Export Ordinal: 415
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Function: DestroyMenu
		/// Description: Destroys the menu specifed by the hMenu parameter
		/// and frees any memory that the memory occupied.
		/// bOk = DestroyMenu( hMenu )
		/// Export Ordinal: 152
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Function: GetMenu
		/// Description: retrieves a handle to the menu of hWnd.
		/// hMenu = GetMenu( hWnd )
		/// Export Ordinal: 157
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Window Handle: HWND
		/// Function: GetMenuState
		/// Description: obtains the number of items in the pop-up menu
		/// associated with hMenu specified by nID if the
		/// menu it top-level; if it is pop-up, it obtains the
		/// status of the menu item associated with nFlags.
		/// nFlags can be the MF_* flags ByCommand or ByPosition.
		/// Return as follows: if nID a pop-up menu ID, nReturn
		/// is the number of items in the pop-up menu; else,
		/// nReturn is a mask of the values of the MF_* flags
		/// Checked to Unchecked.
		/// nReturn = GetMenuState( hMenu, nID, nFlags)
		/// Export Ordinal: 250
		/// Returns
		/// Number: WORD
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// Function: GetSubMenu
		/// Description: retrieves the menu handle of a pop-up menu.
		/// hMenuSub = GetSubMenu( hMenuTop, nPos )
		/// Export Ordinal: 159
		/// Returns
		/// Window Handle: HWND
		/// Parameters
		/// Window Handle: HWND
		/// Number: INT
		/// Function: HiliteMenuItem
		/// Description: highlights or removes the highlighting from a
		/// top-level (menu-bar) menu item. hWnd is the window
		/// containing hMenu. wID specified the identifier of
		/// the menu item or the offset of the menu item in the
		/// menu, depending on wHilite. wHilite specifies
		/// whether the menu item is highlighted or the
		/// highlight is removed. Uses MF_* flags ByCommand,
		/// ByPosition, Hilite and Unhilite.
		/// bIsHighlighted = HiliteMenuItem( hWnd, hMenu, wID, wHilite )
		/// Export Ordinal: 162
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// Function: InsertMenu
		/// Description:
		/// Export Ordinal: 410
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// Number: WORD
		/// String: LPSTR
		/// Function: IsWindowEnabled
		/// Description:
		/// Export Ordinal: 35
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Function: IsIconic
		/// Description:
		/// Export Ordinal: 31
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Function: RemoveMenu
		/// Description:
		/// Export Ordinal: 412
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: WORD
		/// Function: SetMenu
		/// Description: Sets the given window's menu to the menu specifed
		/// by the hMenu paramter. If hMenu is NULL, the
		/// windows's current menu is removed. SetMenu causes
		/// the windows to be redrawn to reflect the menu change.
		/// bOk = SetMenu( hWnd, hMenu )
		/// Export Ordinal: 158
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Window Handle: HWND
		/// Function: TrackPopupMenu
		/// Description: Displays a "floating" pop-up menu at the specified location and tracks
		/// the selection of items on the menu. nReserved and sReserved are null.
		/// bOk = TrackPopupMenu( hMenu, wFlags, x, y, nReserved, hWnd, sReserved )
		/// Export Ordinal: 416
		/// Returns
		/// Boolean: BOOL
		/// Parameters
		/// Window Handle: HWND
		/// Number: WORD
		/// Number: INT
		/// Number: INT
		/// Number: INT
		/// Window Handle: HWND
		/// String: LPVOID
		/// Function: ClientToScreen
		/// Description: Converts client coordinates of a given point on the display to
		/// screen coordinates.
		/// ClientToScreen( hWnd, lpPoint )
		/// Export Ordinal: 28
		/// Returns
		/// Parameters
		/// Window Handle: HWND
		/// Receive Number: LPLONG
		/// void GetCursorPos(lppt)
		/// 
		/// POINT FAR* lppt;	/* address of structure for cursor position	*/
		/// 
		/// The GetCursorPos function retrieves the screen coordinates of the cursor's current position.
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalBoolean GetCursorPos(ref SalNumber param1, ref SalNumber param2)
		{
			SalBoolean retVal = false;

			Structures.STRUCT_1 _struct1 = new Structures.STRUCT_1();
			_struct1.Member1 = (int)param1;
			_struct1.Member2 = (int)param2;

			retVal = USER32.GetCursorPos(ref _struct1);

			param1 = (SalNumber)_struct1.Member1;
			param2 = (SalNumber)_struct1.Member2;
			return retVal;
		}
		
		/// <summary>
		/// Checks free System Resources and free GPI and USER heaps.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalBoolean CheckSysRes(SalNumber param1, SalNumber param2)
		{
			SalBoolean retVal = false;
			int _param1 = (int)param1;
			int _param2 = (int)param2;


            //PPJ:TODO:AG: res always sufficient
            //retVal = EZCAP32.CheckSysRes(_param1, _param2);
            retVal = true;

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public static SalNumber GetProcessCount()
		{
			SalNumber retVal = 0;


			retVal = EZCAP32.GetProcessCount();

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalWindowHandle GetModuleHandleW(SalString param1)
		{
			SalWindowHandle retVal = 0;
			string _param1 = param1.Value;


			retVal = KERNEL32.GetModuleHandleW(_param1);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GlobalAddAtomW(SalString param1)
		{
			SalNumber retVal = 0;
			string _param1 = param1.Value;


			retVal = KERNEL32.GlobalAddAtomW(_param1);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GlobalFindAtomW(SalString param1)
		{
			SalNumber retVal = 0;
			string _param1 = param1.Value;


			retVal = KERNEL32.GlobalFindAtomW(_param1);

			return retVal;
		}
		
		/// <summary>
		/// </summary>
		/// <param name="param1"></param>
		/// <returns></returns>
		public static SalNumber GlobalDeleteAtom(SalNumber param1)
		{
			SalNumber retVal = 0;
			System.IntPtr _param1 = (System.IntPtr)param1;


			retVal = KERNEL32.GlobalDeleteAtom(_param1);

			return retVal;
		}
		
		#region user32.dll
		
		/// <summary>
		/// Interop declarations for the external module user32.dll
		/// </summary>
		private static class USER32
		{
			private const string LibraryFileName = "user32.dll";
			[DllImport(LibraryFileName, EntryPoint="GetWindowLongA", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int GetWindowLongA(System.IntPtr param1, int param2);
			[DllImport(LibraryFileName, EntryPoint="SetWindowLongA", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int SetWindowLongA(System.IntPtr param1, int param2, int param3);
			[DllImport(LibraryFileName, EntryPoint="ShowWindow", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern bool ShowWindow(System.IntPtr param1, int param2);
			[DllImport(LibraryFileName, EntryPoint="GetWindow", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GetWindow(System.IntPtr param1, uint param2);
			[DllImport(LibraryFileName, EntryPoint="GetParent", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GetParent(System.IntPtr param1);
			[DllImport(LibraryFileName, EntryPoint="EnableMenuItem", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern ushort EnableMenuItem(System.IntPtr param1, uint param2, uint param3);
			[DllImport(LibraryFileName, EntryPoint="GetMenuItemCount", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int GetMenuItemCount(System.IntPtr param1);
			[DllImport(LibraryFileName, EntryPoint="GetMenuItemID", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern uint GetMenuItemID(System.IntPtr param1, int param2);
			[DllImport(LibraryFileName, EntryPoint="GetMenuStringA", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int GetMenuStringA(System.IntPtr param1, uint param2, System.Text.StringBuilder param3, int param4, uint param5);
			[DllImport(LibraryFileName, EntryPoint="GetSystemMenu", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GetSystemMenu(System.IntPtr param1, bool param2);
			[DllImport(LibraryFileName, EntryPoint="GetSystemMetrics", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int GetSystemMetrics(int param1);
			[DllImport(LibraryFileName, EntryPoint="GetKeyState", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern short GetKeyState(int param1);
			[DllImport(LibraryFileName, EntryPoint="ModifyMenuA", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern bool ModifyMenuA(System.IntPtr param1, uint param2, uint param3, uint param4, string param5);
			[DllImport(LibraryFileName, EntryPoint="GetCursorPos", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern bool GetCursorPos(ref Structures.STRUCT_1 param1);
		}
		#endregion
		
		/// <summary>
		/// External functions structure declarations.
		/// </summary>
		private class Structures
		{
			
			[StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
			public struct STRUCT_1
			{
				public Int32 Member1;
				public Int32 Member2;
			}
		}
		
		#region ezcap32.dll
		
		/// <summary>
		/// Interop declarations for the external module ezcap32.dll
		/// </summary>
		private static class EZCAP32
		{
			private const string LibraryFileName = "ezcap32.dll";
            //PPJ:FINAL:MHO - CA:0036 Use function names as Entry Point
            //[DllImport(LibraryFileName, EntryPoint="#1", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
            [DllImport(LibraryFileName, EntryPoint = "CheckSysRes", CharSet = System.Runtime.InteropServices.CharSet.Ansi)]
            internal static extern bool CheckSysRes(int param1, int param2);
			[DllImport(LibraryFileName, EntryPoint="GetProcessCount", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern int GetProcessCount();
		}
		#endregion
		
		#region kernel32.dll
		
		/// <summary>
		/// Interop declarations for the external module kernel32.dll
		/// </summary>
		private static class KERNEL32
		{
			private const string LibraryFileName = "kernel32.dll";
			[DllImport(LibraryFileName, EntryPoint="GetModuleHandleW", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GetModuleHandleW(string param1);
			[DllImport(LibraryFileName, EntryPoint="GlobalAddAtomW", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GlobalAddAtomW(string param1);
			[DllImport(LibraryFileName, EntryPoint="GlobalFindAtomW", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GlobalFindAtomW(string param1);
			[DllImport(LibraryFileName, EntryPoint="GlobalDeleteAtom", CharSet=System.Runtime.InteropServices.CharSet.Ansi)]
			internal static extern System.IntPtr GlobalDeleteAtom(System.IntPtr param1);
		}
		#endregion
	}
}
