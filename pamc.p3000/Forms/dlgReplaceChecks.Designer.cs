// <ppj name="pamc.p3000" date="07.09.2018 10:25:08" id="D70F323C7DBEADEF74807E910A15A3912A8A74F8"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.p3000
{
	
	public partial class dlgReplaceChecks
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditN dfPrefix;
		public clsDataFieldEditN dfReplacementPrefix;
		public clsDataFieldEditN dfCheckNo;
		public clsDataFieldEditN dfReplacementCheckNo;
		protected clsText label2;
		protected clsText label3;
		protected clsText label4;
		// If nPaymentCode = 1
		// Set MyValue = strClaimMsg
		// Else If (nPaymentCode= 3 or nPaymentCode = 4)
		// Set MyValue = strDisbMsg
		// Else If nPaymentCode = 2
		// Set MyValue = strCapMsg
		public clsMLNoEdit mlVoidMsg;
		protected clsGroupBox groupBox1;
		protected clsText label5;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfPrefix = new pamc.common.clsDataFieldEditN();
			this.dfReplacementPrefix = new pamc.common.clsDataFieldEditN();
			this.dfCheckNo = new pamc.common.clsDataFieldEditN();
			this.dfReplacementCheckNo = new pamc.common.clsDataFieldEditN();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.label4 = new pamc.common.clsText();
			this.mlVoidMsg = new pamc.price.clsMLNoEdit();
			this.groupBox1 = new pamc.common.clsGroupBox();
			this.label5 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfPrefix
			// 
			this.dfPrefix.Name = "dfPrefix";
			this.dfPrefix.MaxLength = 8;
			this.dfPrefix.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPrefix.Format = "#0";
			this.dfPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfPrefix.Location = new System.Drawing.Point(131, 57);
			this.dfPrefix.Size = new System.Drawing.Size(66, 20);
			this.dfPrefix.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPrefix_WindowActions);
			this.dfPrefix.TabIndex = 0;
			// 
			// dfReplacementPrefix
			// 
			this.dfReplacementPrefix.Name = "dfReplacementPrefix";
			this.dfReplacementPrefix.MaxLength = 8;
			this.dfReplacementPrefix.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfReplacementPrefix.Format = "#0";
			this.dfReplacementPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfReplacementPrefix.Location = new System.Drawing.Point(347, 57);
			this.dfReplacementPrefix.Size = new System.Drawing.Size(68, 20);
			this.dfReplacementPrefix.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfReplacementPrefix_WindowActions);
			this.dfReplacementPrefix.TabIndex = 1;
			// 
			// dfCheckNo
			// 
			this.dfCheckNo.Name = "dfCheckNo";
			this.dfCheckNo.MaxLength = 8;
			this.dfCheckNo.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCheckNo.Format = "#0";
			this.dfCheckNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfCheckNo.Location = new System.Drawing.Point(130, 83);
			this.dfCheckNo.Size = new System.Drawing.Size(66, 20);
			this.dfCheckNo.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCheckNo_WindowActions);
			this.dfCheckNo.TabIndex = 2;
			// 
			// dfReplacementCheckNo
			// 
			this.dfReplacementCheckNo.Name = "dfReplacementCheckNo";
			this.dfReplacementCheckNo.MaxLength = 8;
			this.dfReplacementCheckNo.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfReplacementCheckNo.Format = "#0";
			this.dfReplacementCheckNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfReplacementCheckNo.Location = new System.Drawing.Point(347, 83);
			this.dfReplacementCheckNo.Size = new System.Drawing.Size(66, 20);
			this.dfReplacementCheckNo.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfReplacementCheckNo_WindowActions);
			this.dfReplacementCheckNo.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&R";
			this.label1.TabIndex = 4;
			// 
			// pbExecute
			// 
			this.pbExecute.Name = "pbExecute";
			this.pbExecute.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbExecute_WindowActions);
			this.pbExecute.TabIndex = 5;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.TabIndex = 6;
			// 
			// mlOutput
			// 
			this.mlOutput.Name = "mlOutput";
			this.mlOutput.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.mlOutput.TabIndex = 7;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 8;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Check Prefix:";
			this.label2.Location = new System.Drawing.Point(41, 60);
			this.label2.Size = new System.Drawing.Size(84, 16);
			this.label2.TabIndex = 9;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Check #:";
			this.label3.Location = new System.Drawing.Point(41, 87);
			this.label3.Size = new System.Drawing.Size(86, 16);
			this.label3.TabIndex = 10;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Replacement Check #:";
			this.label4.Location = new System.Drawing.Point(205, 86);
			this.label4.Size = new System.Drawing.Size(138, 16);
			this.label4.TabIndex = 11;
			// 
			// mlVoidMsg
			// 
			this.mlVoidMsg.Name = "mlVoidMsg";
			this.mlVoidMsg.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.mlVoidMsg.Location = new System.Drawing.Point(42, 116);
			this.mlVoidMsg.Size = new System.Drawing.Size(415, 46);
			this.mlVoidMsg.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.mlVoidMsg_WindowActions);
			this.mlVoidMsg.TabIndex = 12;
			// 
			// groupBox1
			// 
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox1.Text = "Replace Check :";
			this.groupBox1.Location = new System.Drawing.Point(9, 38);
			this.groupBox1.Size = new System.Drawing.Size(469, 134);
			this.groupBox1.TabIndex = 13;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "Replacement Prefix:";
			this.label5.Location = new System.Drawing.Point(205, 60);
			this.label5.Size = new System.Drawing.Size(130, 16);
			this.label5.TabIndex = 14;
			// 
			// dlgReplaceChecks
			// 
			this.Controls.Add(this.mlVoidMsg);
			this.Controls.Add(this.mlOutput);
			this.Controls.Add(this.pbClose);
			this.Controls.Add(this.pbExecute);
			this.Controls.Add(this.dfReplacementCheckNo);
			this.Controls.Add(this.dfCheckNo);
			this.Controls.Add(this.dfReplacementPrefix);
			this.Controls.Add(this.dfPrefix);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.frame1);
			this.Name = "dlgReplaceChecks";
			this.Text = "Replace Checks";
			this.Location = new System.Drawing.Point(87, 126);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgReplaceChecks_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgReplaceChecks == this) 
			{
				App.dlgReplaceChecks = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
