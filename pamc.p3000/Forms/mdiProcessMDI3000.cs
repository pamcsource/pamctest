// <ppj name="pamc.p3000" date="07.09.2018 10:25:08" id="D70F323C7DBEADEF74807E910A15A3912A8A74F8"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.p3000
{
	
	/// <summary>
	/// </summary>
	/// <param name="hWndThisForm"></param>
	public partial class mdiProcessMDI3000 : clsMDIMain
	{
		#region Window Parameters
		public SalWindowHandle hWndThisForm;
		#endregion
		
		#region Window Variables
		public SalString strOutputTypeA = "";
		public SalString strLibraryNameA = "";
		public SalString strRptLibPathA = "";
		public SalString strRptLibNameA = "";
		public SalString strRptLibraryA = "";
		public SalString strDeviceA = "";
		public SalString strPortA = "";
		public SalString strDriverA = "";
		public SalString strDefaultLibA = "";
		public SalString strReportTitleA = "";
		public SalString strDefaultDirA = "";
		#endregion
		
		#region MDI Child Forms
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public mdiProcessMDI3000(SalWindowHandle hWndThisForm)
		{
			// Assign global reference.
			App.mdiProcessMDI3000 = this;
			// Window Parameters initialization.
			this.hWndThisForm = hWndThisForm;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Automatic forms creation.
		/// </summary>
		protected override void OnMdiCreateAutomaticForms()
		{
		}
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static mdiProcessMDI3000 CreateWindow(Control owner, ref SalWindowHandle hWndThisForm)
		{
			mdiProcessMDI3000 frm = new mdiProcessMDI3000(hWndThisForm);
			frm.Show(owner);
			hWndThisForm = frm.hWndThisForm;
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static mdiProcessMDI3000 FromHandle(SalWindowHandle handle)
		{
			return ((mdiProcessMDI3000)SalWindow.FromHandle(handle, typeof(mdiProcessMDI3000)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// mdiProcessMDI3000 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mdiProcessMDI3000_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.mdiProcessMDI3000_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Sys.SAM_CreateComplete:
					this.mdiProcessMDI3000_OnSAM_CreateComplete(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mdiProcessMDI3000_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.strModuleName = pamc.common.Const.MODULE_Proc;
			#endregion
		}
		
		/// <summary>
		/// SAM_CreateComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mdiProcessMDI3000_OnSAM_CreateComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_CreateComplete, 0, 0);
			Sal.PrtGetDefault(ref this.strDeviceA, ref this.strDriverA, ref this.strPortA);
			this.strOutputTypeA = "D";
			this.strDefaultDirA = pamc.price.Var.ClientConfig.GetReportDir();
			this.strDefaultLibA = pamc.price.Var.ClientConfig.GetReportLib();
			this.strRptLibPathA = this.strDefaultDirA;
			this.strRptLibNameA = this.strDefaultLibA;
			this.strLibraryNameA = this.strRptLibPathA + "\\" + this.strRptLibNameA;
			this.strRptLibraryA = this.strRptLibPathA + "\\" + this.strRptLibNameA;
			#endregion
		}
		#endregion
		
		#region Menu Classes
		
		/// <summary>
		/// Named menus definitions.
		/// </summary>
		public class NamedMenus
		{
			
			/// <summary>
			/// </summary>
			public class menuFileProcess : SalPopupMenu
			{
				private SalPopupMenu popupMenu__Open;
				private SalMenuItem menuItem_Check;
				private SalMenuSeparator menuSeparator1;
				private SalMenuSeparator menuSeparator2;
				private SalMenuItem menuItem__Login;
				private SalMenuItem menuItem_Logout;
				private SalMenuSeparator menuSeparator3;
				private SalMenuItem menuItem_View;
				private SalMenuSeparator menuSeparator4;
				private SalMenuItem menuItem_Exit;
				
				/// <summary>
				/// Named Menu initialization.
				/// </summary>
				protected override void OnInitializeMenu()
				{
					this.popupMenu__Open = new SalPopupMenu();
					this.menuItem_Check = new SalMenuItem();
					this.menuSeparator1 = new SalMenuSeparator();
					this.menuSeparator2 = new SalMenuSeparator();
					this.menuItem__Login = new SalMenuItem();
					this.menuItem_Logout = new SalMenuItem();
					this.menuSeparator3 = new SalMenuSeparator();
					this.menuItem_View = new SalMenuItem();
					this.menuSeparator4 = new SalMenuSeparator();
					this.menuItem_Exit = new SalMenuItem();
					// 
					// menuFileProcess
					// 
					this.Text = "&File";
					this.StatusText = "Open EZ-CAP data entry screens";
					this.MenuItems.Add(this.popupMenu__Open);
					this.MenuItems.Add(this.menuSeparator2);
					this.MenuItems.Add(this.menuItem__Login);
					this.MenuItems.Add(this.menuItem_Logout);
					this.MenuItems.Add(this.menuSeparator3);
					this.MenuItems.Add(this.menuItem_View);
					this.MenuItems.Add(this.menuSeparator4);
					this.MenuItems.Add(this.menuItem_Exit);
					// 
					// popupMenu__Open
					// 
					this.popupMenu__Open.Text = "&Open";
					this.popupMenu__Open.MenuItems.Add(this.menuItem_Check);
					this.popupMenu__Open.MenuItems.Add(this.menuSeparator1);
					this.popupMenu__Open.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.popupMenu__Open_EnabledWhen);
					// 
					// menuItem_Check
					// 
					this.menuItem_Check.Text = "Check &Register";
					this.menuItem_Check.CheckedWhen += new PPJ.Runtime.Windows.SalMenuCheckedWhenHandler(this.menuItem_Check_CheckedWhen);
					this.menuItem_Check.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem_Check_EnabledWhen);
					this.menuItem_Check.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Check_MenuActions);
					// 
					// menuItem__Login
					// 
					this.menuItem__Login.Text = "&Login";
					this.menuItem__Login.StatusText = "Login to EZ-CAP database";
					this.menuItem__Login.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Login_EnabledWhen);
					this.menuItem__Login.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Login_MenuActions);
					// 
					// menuItem_Logout
					// 
					this.menuItem_Logout.Text = "Lo&gout";
					this.menuItem_Logout.StatusText = "Logout of EZ-CAP database";
					this.menuItem_Logout.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem_Logout_EnabledWhen);
					this.menuItem_Logout.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Logout_MenuActions);
					// 
					// menuItem_View
					// 
					this.menuItem_View.Text = "View &Table";
					this.menuItem_View.StatusText = "Show or hide main table";
					this.menuItem_View.Shortcut = Shortcut.CtrlT;
					this.menuItem_View.CheckedWhen += new PPJ.Runtime.Windows.SalMenuCheckedWhenHandler(this.menuItem_View_CheckedWhen);
					this.menuItem_View.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem_View_EnabledWhen);
					this.menuItem_View.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_View_MenuActions);
					// 
					// menuItem_Exit
					// 
					this.menuItem_Exit.Text = "E&xit";
					this.menuItem_Exit.StatusText = "Exit this screen. Return to Module Main Menu.";
					this.menuItem_Exit.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Exit_MenuActions);
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool popupMenu__Open_EnabledWhen()
				{
					return pamc.common.Var.bConnect && ((bool)pamc.common.Var.nLic[1]);
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_Check_EnabledWhen()
				{
					return pamc.common.Var.hWndFormArray[82] == SalWindowHandle.Null && pamc.common.Var.nSecurityLevel[82] > 0;
				}
				
				/// <summary>
				/// Menu CheckedWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_Check_CheckedWhen()
				{
					return !(pamc.common.Var.hWndFormArray[82] == SalWindowHandle.Null);
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_Check_MenuActions()
				{
					if (!(pamc.ez32bit.Int.CheckRes())) 
					{
						return false;
					}
					Sal.WaitCursor(true);
					pamc.common.Var.nGScreen = 82;
					frmInitialize.CreateWindow(Sys.hWndNULL);
					tblCHECK_MASTERS.CreateWindow(Sys.hWndMDI);
					frmCHECK_MASTERS.CreateWindow(Sys.hWndMDI);
					pamc.common.App.frmInitialize.DestroyWindow();
					Sal.WaitCursor(false);
					return 0;
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem__Login_EnabledWhen()
				{
					return !(pamc.common.Var.bConnect);
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Login_MenuActions()
				{
					return Sys.hWndMDI.SendMessage(pamc.common.Const.CM_DoLogin, 0, 0);
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_Logout_EnabledWhen()
				{
					return pamc.common.Var.bConnect;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_Logout_MenuActions()
				{
					return Sys.hWndMDI.SendMessage(pamc.common.Const.CM_DoLogout, 0, 0);
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_View_EnabledWhen()
				{
					return clsMDI.FromHandle(Sys.hWndMDI).pbTable.IsEnabled();
				}
				
				/// <summary>
				/// Menu CheckedWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_View_CheckedWhen()
				{
					return pamc.common.Var.hWndMainTable.IsVisible();
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_View_MenuActions()
				{
					clsMDI.FromHandle(Sys.hWndMDI).pbTable.SendMessage(Sys.SAM_Click, 0, 0);
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_Exit_MenuActions()
				{
					clsMDI.FromHandle(Sys.hWndMDI).pbExit.SendMessage(Sys.SAM_Click, 0, 0);
					return 0;
				}
			}
			
			/// <summary>
			/// </summary>
			public class menuProcessing : SalPopupMenu
			{
				private SalPopupMenu popupMenu__Claims;
				private SalMenuItem menuItem__Release;
				private SalMenuItem menuItem__Select;
				private SalMenuSeparator menuSeparator5;
				private SalMenuItem menuItem__Generate;
				private SalMenuItem menuItem__Print;
				private SalMenuItem menuItem_Post;
				private SalMenuSeparator menuSeparator6;
				private SalMenuItem menuItem__Replace;
				private SalMenuSeparator menuSeparator7;
				private SalPopupMenu popupMenu__Error;
				private SalMenuItem menuItem_DeSelect;
				private SalMenuItem menuItem_RePrint;
				private SalMenuItem menuItem_Cancel;
				private SalMenuItem menuItem__Void;
				private SalMenuItem menuItem__Clear;
				
				/// <summary>
				/// Named Menu initialization.
				/// </summary>
				protected override void OnInitializeMenu()
				{
					this.popupMenu__Claims = new SalPopupMenu();
					this.menuItem__Release = new SalMenuItem();
					this.menuItem__Select = new SalMenuItem();
					this.menuSeparator5 = new SalMenuSeparator();
					this.menuItem__Generate = new SalMenuItem();
					this.menuItem__Print = new SalMenuItem();
					this.menuItem_Post = new SalMenuItem();
					this.menuSeparator6 = new SalMenuSeparator();
					this.menuItem__Replace = new SalMenuItem();
					this.menuSeparator7 = new SalMenuSeparator();
					this.popupMenu__Error = new SalPopupMenu();
					this.menuItem_DeSelect = new SalMenuItem();
					this.menuItem_RePrint = new SalMenuItem();
					this.menuItem_Cancel = new SalMenuItem();
					this.menuItem__Void = new SalMenuItem();
					this.menuItem__Clear = new SalMenuItem();
					// 
					// menuProcessing
					// 
					this.Text = "&Process";
					this.MenuItems.Add(this.popupMenu__Claims);
					// 
					// popupMenu__Claims
					// 
					this.popupMenu__Claims.Text = "&Claims Payment";
					this.popupMenu__Claims.StatusText = "Claims payment process";
					this.popupMenu__Claims.MenuItems.Add(this.menuItem__Release);
					this.popupMenu__Claims.MenuItems.Add(this.menuItem__Select);
					this.popupMenu__Claims.MenuItems.Add(this.menuSeparator5);
					this.popupMenu__Claims.MenuItems.Add(this.menuItem__Generate);
					this.popupMenu__Claims.MenuItems.Add(this.menuItem__Print);
					this.popupMenu__Claims.MenuItems.Add(this.menuItem_Post);
					this.popupMenu__Claims.MenuItems.Add(this.menuSeparator6);
					this.popupMenu__Claims.MenuItems.Add(this.menuItem__Replace);
					this.popupMenu__Claims.MenuItems.Add(this.menuSeparator7);
					this.popupMenu__Claims.MenuItems.Add(this.popupMenu__Error);
					this.popupMenu__Claims.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.popupMenu__Claims_EnabledWhen);
					// 
					// menuItem__Release
					// 
					this.menuItem__Release.Text = "&Release Claims to A/P";
					this.menuItem__Release.StatusText = "Change claims from status 1(Release to A/P) to status 5(A/P Hold)";
					this.menuItem__Release.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Release_MenuActions);
					// 
					// menuItem__Select
					// 
					this.menuItem__Select.Text = "&Select Claims to Pay";
					this.menuItem__Select.StatusText = "Change claims from status 5(A/P Hold) to 6(A/P Pay)";
					this.menuItem__Select.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Select_MenuActions);
					// 
					// menuItem__Generate
					// 
					this.menuItem__Generate.Text = "&Generate Check Run";
					this.menuItem__Generate.StatusText = "Create a batch of claims to be paid by check";
					this.menuItem__Generate.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Generate_MenuActions);
					// 
					// menuItem__Print
					// 
					this.menuItem__Print.Text = "&Print Check Run";
					this.menuItem__Print.StatusText = "Print checks";
					this.menuItem__Print.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Print_MenuActions);
					// 
					// menuItem_Post
					// 
					this.menuItem_Post.Text = "Post &Check Run";
					this.menuItem_Post.StatusText = "Updates claim status, check number, and payment date";
					this.menuItem_Post.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Post_MenuActions);
					// 
					// menuItem__Replace
					// 
					this.menuItem__Replace.Text = "&Replace Check";
					this.menuItem__Replace.StatusText = "Replace checks";
					this.menuItem__Replace.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Replace_EnabledWhen);
					this.menuItem__Replace.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Replace_MenuActions);
					// 
					// popupMenu__Error
					// 
					this.popupMenu__Error.Text = "&Error Recovery";
					this.popupMenu__Error.MenuItems.Add(this.menuItem_DeSelect);
					this.popupMenu__Error.MenuItems.Add(this.menuItem_RePrint);
					this.popupMenu__Error.MenuItems.Add(this.menuItem_Cancel);
					this.popupMenu__Error.MenuItems.Add(this.menuItem__Void);
					this.popupMenu__Error.MenuItems.Add(this.menuItem__Clear);
					// 
					// menuItem_DeSelect
					// 
					this.menuItem_DeSelect.Text = "De-Se&lect";
					this.menuItem_DeSelect.StatusText = "Change claims from status 6(A/P Pay) to status 5(A/P Hold)";
					this.menuItem_DeSelect.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem_DeSelect_EnabledWhen);
					this.menuItem_DeSelect.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_DeSelect_MenuActions);
					// 
					// menuItem_RePrint
					// 
					this.menuItem_RePrint.Text = "R&e-Print Check Run";
					this.menuItem_RePrint.StatusText = "Re-print the current check run";
					this.menuItem_RePrint.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_RePrint_MenuActions);
					// 
					// menuItem_Cancel
					// 
					this.menuItem_Cancel.Text = "C&ancel Check Run";
					this.menuItem_Cancel.StatusText = "Cancel the current check run";
					this.menuItem_Cancel.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Cancel_MenuActions);
					// 
					// menuItem__Void
					// 
					this.menuItem__Void.Text = "&Void Posted Checks";
					this.menuItem__Void.StatusText = "Void claim checks that have already been posted";
					this.menuItem__Void.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Void_EnabledWhen);
					this.menuItem__Void.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Void_MenuActions);
					// 
					// menuItem__Clear
					// 
					this.menuItem__Clear.Text = "&Clear Posted Checks";
					this.menuItem__Clear.StatusText = "Clear claim checks that have already been posted";
					this.menuItem__Clear.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Clear_EnabledWhen);
					this.menuItem__Clear.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Clear_MenuActions);
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool popupMenu__Claims_EnabledWhen()
				{
					return pamc.common.Var.nSecurityLevel[210] > 3 && pamc.common.Var.bConnect && ((bool)pamc.common.Var.nLic[1]);
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Release_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						pamc.common.Var.nGScreen = 102;
						// Call ChangeStatus( 'Release Claim To A/P', '1', '5' )
						dlgReleaseClaims.ModalDialog(Sys.hWndMDI, "Release Claims To A/P", "1", "5");
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Select_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						pamc.common.Var.nGScreen = 103;
						// Call ChangeStatus( 'Payment Selection', '5', '6' )
						dlgReleaseClaims.ModalDialog(Sys.hWndMDI, "Payment Selection", "5", "6");
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Generate_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						pamc.common.Var.nGScreen = 103;
						// Call SalModalDialog( dlgP3003g, hWndMDI )
						dlgClaimGenChks.ModalDialog(Sys.hWndMDI);
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Print_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						dlgPrintCheckRun.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPOST", "CLAIM_CHKPRNT", "VEND_MASTERS", "CLAIMCHK.RTF", 1);
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_Post_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						dlgPostChecks.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPOST", "CLAIM_CHKPRNT", "CLAIM_MASTERS", 1);
					}
					return 0;
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem__Replace_EnabledWhen()
				{
					return pamc.common.Var.nSecurityLevel[446] > 3;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Replace_MenuActions()
				{
					dlgReplaceChecks.ModalDialog(Sys.hWndMDI);
					return 0;
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem_DeSelect_EnabledWhen()
				{
					return pamc.common.Var.nSecurityLevel[210] > 3;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_DeSelect_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						pamc.common.Var.nGScreen = 105;
						// Call ChangeStatus( 'Payment Selection', '5', '6' )
						dlgReleaseClaims.ModalDialog(Sys.hWndMDI, "De-Select", "6", "5");
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_RePrint_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (pamc.common.Int.DoSqlCnt(pamc.price.Var.hSql, " from CLAIM_CHKPOST\r\n" +
							"                  where NET != 0", "", "")) 
						{
							if (!(pamc.ez32bit.Int.CheckRes())) 
							{
								return false;
							}
							dlgRePrintCheckRun.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPOST", "CLAIM_CHKPRNT", "VEND_MASTERS", "CLAIMCHK.RTF", 1);
						}
						else
						{
							Sal.MessageBox("Nothing to print", "No Checks", (Sys.MB_Ok | Sys.MB_IconStop));
						}
					}
					return 0;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem_Cancel_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						dlgCancelCheckRun.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPOST", "CLAIM_CHKPRNT", 1);
					}
					return 0;
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem__Void_EnabledWhen()
				{
					return pamc.common.Var.nSecurityLevel[445] > 3;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Void_MenuActions()
				{
					if (Sal.SendValidateMsg()) 
					{
						if (!(pamc.ez32bit.Int.CheckRes())) 
						{
							return false;
						}
						dlgVoidPostedChecks.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPRNT", "CLAIM_MASTERS", 1);
					}
					return 0;
				}
				
				/// <summary>
				/// Menu EnabledWhen Expression
				/// </summary>
				/// <returns></returns>
				private bool menuItem__Clear_EnabledWhen()
				{
					return pamc.common.Var.nSecurityLevel[450] > 3;
				}
				
				/// <summary>
				/// Menu Actions
				/// </summary>
				/// <returns></returns>
				private SalNumber menuItem__Clear_MenuActions()
				{
					dlgClearPostedChecks.ModalDialog(Sys.hWndMDI, "CLAIM_CHKPRNT", "CLAIM_MASTERS", 1);
					return 0;
				}
			}
		}
		#endregion
	}
}
