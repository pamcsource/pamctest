// <ppj name="pamc.p3000" date="07.09.2018 10:25:08" id="D70F323C7DBEADEF74807E910A15A3912A8A74F8"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.p3000
{
	
	/// <summary>
	/// This dialog box is used for Release Claims to A/P, Payment Selection and De-Select.
	/// </summary>
	/// <param name="strTitle"></param>
	/// <param name="strStatusFrom"></param>
	/// <param name="strStatusTo"></param>
	public partial class dlgReleaseClaims : clsDlgChkProcess
	{
		#region Window Parameters
		public SalString strTitle;
		public SalString strStatusFrom;
		public SalString strStatusTo;
		#endregion
		
		#region Window Variables
		public SalString strErrorMsg = "";
		public SalString strWhere = "";
		public SalNumber nTotalStatusFrom = 0;
		public SalNumber nTotalStatusTo = 0;
		public SalNumber nTotalSelected = 0;
		public SalNumber nTotalTempTable = 0;
		public SalWindowHandle hWndSearchBox = SalWindowHandle.Null;
		public SalNumber nRet = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgReleaseClaims(SalString strTitle, SalString strStatusFrom, SalString strStatusTo)
		{
			// Assign global reference.
			App.dlgReleaseClaims = this;
			// Window Parameters initialization.
			this.strTitle = strTitle;
			this.strStatusFrom = strStatusFrom;
			this.strStatusTo = strStatusTo;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalString strTitle, SalString strStatusFrom, SalString strStatusTo)
		{
			dlgReleaseClaims dlg = new dlgReleaseClaims(strTitle, strStatusFrom, strStatusTo);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgReleaseClaims FromHandle(SalWindowHandle handle)
		{
			return ((dlgReleaseClaims)SalWindow.FromHandle(handle, typeof(dlgReleaseClaims)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function checks to see how many records exist in the CLAIM_TEMP table.
		/// </summary>
		/// <param name="nTotalTempTable"></param>
		/// <returns></returns>
		public SalBoolean CheckTempTable(ref SalNumber nTotalTempTable)
		{
			#region Local Variables
			SqlLocals.CheckTempTableLocals locals = new SqlLocals.CheckTempTableLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.nTotalTempTable = nTotalTempTable;

					Sal.WaitCursor(true);
					locals.nTotalTempTable = 0;
					// Set strSql = 'Select count(*) into :nTotalTempTable from CLAIM_TEMP'
					locals.strSql = "Select count(*)  from CLAIM_TEMP";
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler332 = delegate(SalSqlHandle hSql)
					{
						Sal.WaitCursor(false);
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error checking current state of claims payment process", "Could not select rows from CLAIM_TEMP table.");
						return false;
					};
					#endregion

					if (!(pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, locals.strSql, ":nTotalTempTable"))) 
					{
						return false;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler332);
					Sal.WaitCursor(false);
					return true;
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					nTotalTempTable = locals.nTotalTempTable;

				}
			}
			#endregion
		}
		
		/// <summary>
		/// This function updates the CLAIM_MASTERS status based on the records in the CLAIM_TEMP table.
		/// Sets nRecs to be the number of records updated.
		/// </summary>
		/// <param name="nRecs"></param>
		/// <returns></returns>
		public SalNumber UpdateFromTempTable(ref SalNumber nRecs)
		{
			#region Local Variables
			SqlLocals.UpdateFromTempTableLocals locals = new SqlLocals.UpdateFromTempTableLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.nRecs = nRecs;

					Sal.WaitCursor(true);
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler333 = delegate(SalSqlHandle hSql)
					{
						Sal.WaitCursor(false);
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error Changing Claim Status", "An error occurred while updating claim status.");
						return false;
					};
					#endregion

					if (!(pamc.price.Var.SqlInst.SqlRetr(pamc.price.Var.hSql, "EZCAP.UpdateClaimStatus", ":nRecs", ":nRecs"))) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler333))) 
					{
						return false;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler333);
					Sal.WaitCursor(false);
					return pamc.price.Var.hSql.Commit(sqlErrorHandler333);
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					nRecs = locals.nRecs;

				}
			}
			#endregion
		}
		
		/// <summary>
		/// Check to see if any claims exist with the given status ( 1 or 5 )
		/// </summary>
		/// <param name="strStatusFrom"></param>
		/// <param name="strErrorMsg"></param>
		/// <returns></returns>
		public SalBoolean CheckClaims(SalString strStatusFrom, ref SalString strErrorMsg)
		{
			#region Local Variables
			SqlLocals.CheckClaimsLocals locals = new SqlLocals.CheckClaimsLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.strStatusFrom = strStatusFrom;
					locals.strErrorMsg = strErrorMsg;

					Sal.WaitCursor(true);
					locals.strSql = "Select 1 from CLAIM_MASTERS Where STATUS = " + "\'" + locals.strStatusFrom + "\'";
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler334 = delegate(SalSqlHandle hSql)
					{
						Sal.WaitCursor(false);
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "", "Could not select rows from the table.");
						return false;
					};
					#endregion

					if (!(pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, locals.strSql, ""))) 
					{
						locals.strErrorMsg = "Process aborted.";
						return false;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler334);
					pamc.price.Var.hSql.Commit(sqlErrorHandler334);
					Sal.WaitCursor(false);
					if (locals.nInd == Sys.FETCH_EOF) 
					{
						Sal.MessageBox("There are no status " + locals.strStatusFrom + " claims to process.", strTitle, (Sys.MB_Ok | Sys.MB_IconStop));
						locals.strErrorMsg = "There are no status " + locals.strStatusFrom + " claims to process..." + pamc.price.Const.strCTRL_ENTER + "Process aborted.";
						return false;
					}
					return true;
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					strErrorMsg = locals.strErrorMsg;

				}
			}
			#endregion
		}
		
		/// <summary>
		/// This function updates claim status to 1 or 5 based on the user query criteria.
		/// Sets nRecs to be the number of records updated.
		/// </summary>
		/// <param name="strStatusTo"></param>
		/// <param name="strWhere"></param>
		/// <param name="nRecs"></param>
		/// <returns></returns>
		public SalNumber UpdateTable(SalString strStatusTo, SalString strWhere, ref SalNumber nRecs)
		{
			#region Local Variables
			SqlLocals.UpdateTableLocals locals = new SqlLocals.UpdateTableLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.strStatusTo = strStatusTo;
					locals.strWhere = strWhere;
					locals.nRecs = nRecs;

					// ! Set strSql = 'Update CLAIM_MASTERS set STATUS = '|| '\'' || strStatusTo || '\'' || ', LASTCHANGEBY = ' ||
					// strOpID || ', LASTCHANGEDATE = SYSDATETIME  Where ' || strWhere
					// *** Change to call Stored Procedures to update claim status in Claim Master table ****
					Sal.WaitCursor(true);
					locals.nRecs = 0;
					locals.nToStatus = locals.strStatusTo.ToNumber();
					locals.strAutoCommit = pamc.price.Var.SqlInst.GetQuery(64);
					locals.nAutoCommit = locals.strAutoCommit.ToNumber();
					if (!(pamc.price.Var.hSql.SetParameter(1, locals.nAutoCommit, pamc.sqllib.Const.strNULL))) 
					{
						return false;
					}
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler335 = delegate(SalSqlHandle hSql)
					{
						if (pamc.sqllib.Int.GetMSErrorNo(pamc.price.Var.hSql) == pamc.price.Var.SqlInst.GetQuery(73).ToNumber()) 
						{
							return true;
						}
						pamc.price.Var.hSql.PrepareAndExecute("ROLLBACK");
						if (!(pamc.price.Var.hSql.SetParameter(1, 1, pamc.sqllib.Const.strNULL))) 
						{
							return false;
						}
						pamc.price.Var.hSql.GetError(ref locals.nErr, ref locals.strErr);
						//CA:Normal:0001:ERROR: Check if error codes match
						Sql.GetErrorText(locals.nErr, ref locals.strErr);
						Sal.WaitCursor(false);
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error Changing Claim Status", "An error occurred while updating claim status.");
						return false;
					};
					#endregion

					locals.strWhere = locals.strWhere;
					locals.strParamWhere = locals.strWhere.Mid(0, (locals.strWhere.Length / 2) - 1);
					locals.strParamWhere2 = locals.strWhere.Mid((locals.strWhere.Length / 2) - 1, (locals.strWhere.Length / 2) + 5);
					if (!(pamc.price.Var.SqlInst.SqlRetr(pamc.price.Var.hSql, "EZCAP.LoadTempClaims", ":nToStatus,:strParamWhere,:strParamWhere2,:nOpID, :nRecs", ":nRecs"))) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler335))) 
					{
						return false;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler335);
					// Call SqlCommit( hSql )
					// Update CLAIM_MASTERS status to strStatusTo ( 1 or 5 )
					// If NOT SqlRetrieve( hSql, 'EZCAP.UpdateClaimStatus', '', ':nRecs' )
					// Return FALSE
					if (locals.nRecs > 0) 
					{
						if (!(pamc.price.Var.SqlInst.SqlRetr(pamc.price.Var.hSql, "EZCAP.UpdateClaimStatus", ":nRecs", ":nRecs"))) 
						{
							return false;
						}
						if (!(pamc.price.Var.hSql.Execute())) 
						{
							return false;
						}
						locals.nInd = pamc.price.Var.hSql.FetchNext();
					}
					pamc.price.Var.hSql.Commit(sqlErrorHandler335);
					if (!(pamc.price.Var.hSql.SetParameter(1, 1, pamc.sqllib.Const.strNULL))) 
					{
						return false;
					}
					Sal.WaitCursor(false);
					return true;
					// !
					// ! *** End of change to call Stored Procedures to update claim status in Claim Master table ****
					// ! If not SqlPrepareAndExecute( hSql, strSql )
					// Return FALSE
					// ! Next line is not active in SQL code
					// Return FALSE
					// ! ! If not SqlInst.SqlPrepAndExec( hSql, strSql )
					// Return FALSE
					// ! Else
					// Call SqlPrepareAndExecute(hSql, 'commit')
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					nRecs = locals.nRecs;

				}
			}
			#endregion
		}
		
		/// <summary>
		/// Count records in CLAIM_MASTERS with given status
		/// </summary>
		/// <param name="strWhere"></param>
		/// <param name="nCount"></param>
		/// <returns></returns>
		public SalBoolean CountStatus(SalString strWhere, ref SalNumber nCount)
		{
			#region Local Variables
			SqlLocals.CountStatusLocals locals = new SqlLocals.CountStatusLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.strWhere = strWhere;
					locals.nCount = nCount;

					Sal.WaitCursor(true);
					locals.nCount = 0;
					// Set strSql = 'Select count(*) into :nCount from CLAIM_MASTERS Where ' || strWhere
					locals.strSql = "Select count(*) from CLAIM_MASTERS Where " + locals.strWhere;
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler336 = delegate(SalSqlHandle hSql)
					{
						Sal.WaitCursor(false);
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error counting rows in table", "Could not count rows in CLAIM_MASTERS Where " + locals.strWhere);
						return false;
					};
					#endregion

					if (!(pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, locals.strSql, ":nCount "))) 
					{
						return pamc.price.Const.SQL_Error;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler336);
					Sal.WaitCursor(false);
					return true;
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					nCount = locals.nCount;

				}
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber GetStatistics()
		{
			#region Actions
			using (new SalContext(this))
			{
				mlOutput.Text = mlOutput.Text + pamc.price.Const.strCTRL_ENTER + "# of status " + strStatusFrom + " Claims Examined: " + nTotalStatusFrom.ToString(0) + ".";
				mlOutput.Text = mlOutput.Text + pamc.price.Const.strCTRL_ENTER + "# of status " + strStatusFrom + " Claims Selected: " + nTotalSelected.ToString(0) + ".";
				mlOutput.Text = mlOutput.Text + pamc.price.Const.strCTRL_ENTER + "Total # of status " + strStatusTo + " Claims: " + nTotalStatusTo.ToString(0) + ".";
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// Invokes the query builder for process
		/// </summary>
		/// <param name="strTitle"></param>
		/// <param name="strStatusFrom"></param>
		/// <param name="strStatusTo"></param>
		/// <param name="strRetWhere"></param>
		/// <returns></returns>
		public SalBoolean ChangeStatus(SalString strTitle, SalString strStatusFrom, SalString strStatusTo, ref SalString strRetWhere)
		{
			#region Local Variables
			SalString strSql = "";
			SalArray<SalString> strSt = new SalArray<SalString>(-1, 5);
			SalArray<SalString> strField = new SalArray<SalString>(-1, 2);
			SalNumber nInd = 0;
			SalNumber nI = 0;
			SalNumber nTotal = 0;
			SalNumber nTotal2 = 0;
			SalNumber nTotal3 = 0;
			SalArray<SalString> strFieldArray = new SalArray<SalString>(-1, 5);
			SalString strOrder = "";
			SalString strInfo = "";
			SalString strWhere = "";
			SalString strWhereStatus = "";
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				if (strStatusFrom == "1") 
				{
					pamc.common.Var.nScreenNo = 102;
				}
				else
				{
					pamc.common.Var.nScreenNo = 103;
				}
				GetArray(strFieldArray);
				strWhere = "STATUS = " + "\'" + strStatusFrom + "\'";
				strWhereStatus = "STATUS = " + "\'" + strStatusFrom + "\'";
				if (!(dlgSearch.ModalDialog(this, pamc.common.Var.nScreenNo, pamc.common.Var.nOpID, "C", 0, strFieldArray, ref strWhere, ref strOrder, "CHECKRUN"))) 
				{
					return false;
				}
				if (strWhere != pamc.sqllib.Const.strNULL) 
				{
					// Set strWhere = '(' || strWhereStatus || ') AND ' || strWhere || ''
					strWhere = strWhere + " AND " + strWhereStatus;
				}
				strRetWhere = strWhere;
				return true;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strFieldArray"></param>
		/// <returns></returns>
		public SalNumber GetArray(SalArray<SalString> strFieldArray)
		{
			#region Local Variables
			SalNumber nI = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldArray[0, 0] = "Claim Type";
				strFieldArray[1, 0] = "Vendor ID";
				strFieldArray[2, 0] = "Date Received";
				strFieldArray[3, 0] = "Healthplan Code";
				strFieldArray[4, 0] = "Benefit Option";
				strFieldArray[5, 0] = "Claim Number";
				strFieldArray[6, 0] = "Provider ID";
				strFieldArray[7, 0] = "Earliest Service Date";
				strFieldArray[8, 0] = "Latest Service Date";
				strFieldArray[9, 0] = "Account Number";
				strFieldArray[10, 0] = "SubAccount Number";
				strFieldArray[11, 0] = "Claim Specialty Code";
				strFieldArray[12, 0] = "Claim Pay Type Code";
				strFieldArray[13, 0] = "Provider Claim ID";
				strFieldArray[14, 0] = "Status";
				strFieldArray[15, 0] = "Vendor Type";
				strFieldArray[16, 0] = "Batch No";
				strFieldArray[17, 0] = "External Switch";
				strFieldArray[18, 0] = "Line off Business";
				strFieldArray[19, 0] = "Electronic Claim";
				strFieldArray[20, 0] = "Provider Contract";
				strFieldArray[21, 0] = "Provider Class";
				strFieldArray[0, 4] = "1";
				strFieldArray[1, 4] = "20";
				strFieldArray[2, 4] = "10";
				strFieldArray[3, 4] = "4";
				strFieldArray[4, 4] = "20";
				strFieldArray[5, 4] = "16";
				strFieldArray[6, 4] = "20";
				strFieldArray[7, 4] = "10";
				strFieldArray[8, 4] = "10";
				strFieldArray[9, 4] = "4";
				strFieldArray[10, 4] = "4";
				strFieldArray[11, 4] = "3";
				strFieldArray[12, 4] = "1";
				strFieldArray[13, 4] = "20";
				strFieldArray[14, 4] = "2";
				strFieldArray[16, 4] = "20";
				strFieldArray[17, 4] = "20";
				strFieldArray[18, 4] = "1";
				strFieldArray[19, 4] = "1";
				strFieldArray[20, 4] = "2";
				strFieldArray[21, 4] = "2";
				strFieldArray[0, 1] = "PHCODE";
				strFieldArray[0, 2] = "S";
				strFieldArray[1, 1] = "VENDOR";
				strFieldArray[1, 2] = "S";
				strFieldArray[2, 1] = "DATERECD";
				strFieldArray[2, 2] = "D";
				strFieldArray[3, 1] = "HPCODE";
				strFieldArray[3, 2] = "S";
				strFieldArray[4, 1] = "OPT";
				strFieldArray[4, 2] = "S";
				strFieldArray[5, 1] = "CLAIMNO";
				strFieldArray[5, 2] = "S";
				strFieldArray[6, 1] = "PROVID";
				strFieldArray[6, 2] = "S";
				strFieldArray[7, 1] = "DATEFROM";
				strFieldArray[7, 2] = "D";
				strFieldArray[8, 1] = "DATETO";
				strFieldArray[8, 2] = "D";
				strFieldArray[9, 1] = "ACCT";
				strFieldArray[9, 2] = "S";
				strFieldArray[10, 1] = "SUBACCT";
				strFieldArray[10, 2] = "S";
				strFieldArray[11, 1] = "SPEC";
				strFieldArray[11, 2] = "S";
				strFieldArray[12, 1] = "CLAIMTYPE";
				strFieldArray[12, 2] = "S";
				strFieldArray[13, 1] = "PROVCLAIM";
				strFieldArray[13, 2] = "S";
				strFieldArray[14, 1] = "STATUS";
				strFieldArray[14, 2] = "S";
				strFieldArray[15, 1] = "VENDTYPE";
				strFieldArray[15, 2] = "S";
				strFieldArray[16, 1] = "BATCH_NO";
				strFieldArray[16, 2] = "S";
				strFieldArray[17, 1] = "EXT_SWITCH";
				strFieldArray[17, 2] = "S";
				strFieldArray[18, 1] = "LOBCODE";
				strFieldArray[18, 2] = "S";
				strFieldArray[19, 1] = "ELECTRONIC";
				strFieldArray[19, 2] = "N";
				strFieldArray[20, 1] = "CONTRACT";
				strFieldArray[20, 2] = "S";
				strFieldArray[21, 1] = "CLASS";
				strFieldArray[21, 2] = "S";
				nI = 0;
				while (true)
				{
					if (nI > 21) 
					{
						break;
					}
					strFieldArray[nI, 3] = "CLAIM_MASTERS";
					nI = nI + 1;
				}
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// Checks the current print process status for Claims Payment if de-selecting
		/// </summary>
		/// <returns></returns>
		public SalBoolean CheckStatus()
		{
			#region Local Variables
			SalString strStatus = "";
			clsProcessStatus ProcessStatus = new clsProcessStatus();
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				if (!(ProcessStatus.GetStatus("P01", ref strStatus))) 
				{
					return false;
				}
				if (strStatus == "PRINT_Aborted") 
				{
					Sal.MessageBox("Check Run was aborted. You should cancel Check Run first.", "De-Select", Sys.MB_IconStop);
					return false;
				}
				else if (strStatus == "PRINT_Done") 
				{
					Sal.MessageBox("A check run currently exists which has not been posted.   \r\n" +
						"You can either cancel the current check run or finish the print cycle before continuing with this process. ", "De-Select", Sys.MB_IconStop);
					return false;
				}
				else if (strStatus == "GENERATE_Done") 
				{
					Sal.MessageBox("A check run currently exists which has not been printed.   \r\n" +
						"You can either cancel the current check run or finish the print cycle before continuing with this process. ", "De-Select", Sys.MB_IconStop);
					return false;
				}
				return true;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgReleaseClaims WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgReleaseClaims_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgReleaseClaims_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_CreateComplete:
					this.dlgReleaseClaims_OnSAM_CreateComplete(sender, e);
					break;
				
				case pamc.common.Const.CM_Interrupt:
					this.dlgReleaseClaims_OnCM_Interrupt(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgReleaseClaims_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
            //PPJ:FINAL:MHO - CA:0035 If it is called from SAM_Create, SalHideWindow can lead to wrong behavior. Set Opacity instead.
            //this.HideWindow();
            this.Opacity = 0;
			this.CenterWindow();
			this.pbExecute.DisableWindow();
			this.SetText(this.strTitle);
			// *** The following verification only applies to De-Select process.
			// It is designed to prevent users from editing  claims that has already been printed or ready to print.
			if (this.strStatusTo < this.strStatusFrom && !(this.CheckStatus())) 
			{
				this.pbClose.SendMessage(Sys.SAM_Click, 0, 0);
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_CreateComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgReleaseClaims_OnSAM_CreateComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
            //PPJ:FINAL:MHO - CA:0035 If it is called from SAM_Create, SalHideWindow can lead to wrong behavior. Set Opacity instead.
            //this.ShowWindow();
            this.Opacity = 1;
			#endregion
		}
		
		/// <summary>
		/// CM_Interrupt event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgReleaseClaims_OnCM_Interrupt(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.hWndSearchBox = Sys.wParam.ToWindowHandle();
			this.hWndSearchBox.SetText("Select Claims Criteria");
			Sal.HideWindow(this.hWndSearchBox.FindControl("pbOrderBy"));
			Vis.WinFreeAccelerator(pamc.common.App.dlgSearch.pbOrderBy.hAcc);
			#endregion
		}
		
		/// <summary>
		/// pbSelect WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbSelect_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbSelect_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbSelect_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Set mlOutput = 'Checking claims status...'
			if (!(this.CheckClaims(this.strStatusFrom, ref this.strErrorMsg))) 
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, this.strErrorMsg);
				this.pbSelect.DisableWindow();
				e.Return = false;
				return;
			}
			else if (this.ChangeStatus(this.strTitle, this.strStatusFrom, this.strStatusTo, ref this.strWhere)) 
			{
				// If strWhere = strNULL
				// Set strWhere = 'STATUS = ' || '\'' || strStatusFrom || '\''
				// Call SalMessageBox('You have selected All Claims with status ' || strStatusFrom || '.  ' , 'Caution',
				// MB_Ok | MB_IconExclamation)
				// Else
				// Set strWhere = strWhere || ' and STATUS = ' || '\'' || strStatusFrom || '\''
				this.mlSelection.Text = this.strWhere;
				this.pbExecute.EnableWindow();
				this.pbExecute.SetFocus();
			}
			else
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, "Selection canceled.");
				this.pbClose.SetFocus();
			}
			#endregion
		}
		
		/// <summary>
		/// pbExecute WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbExecute_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbExecute_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbExecute_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.pbSelect.DisableWindow();
			pamc.common.Int.AppendMsgToML(this.mlOutput, "Checking current state of claims payment process...");
			if (!(this.CheckTempTable(ref this.nTotalTempTable))) 
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, "Process aborted.");
				e.Return = false;
				return;
			}
			this.mlOutput.Text = this.mlOutput.Text + "  Done.";
			if (this.nTotalTempTable > 0) 
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, "Last process was not completed. Updating status from last run...");
				if (!(this.UpdateFromTempTable(ref this.nTotalTempTable))) 
				{
					pamc.common.Int.AppendMsgToML(this.mlOutput, "Process aborted.");
					e.Return = false;
					return;
				}
				this.mlOutput.Text = this.mlOutput.Text + "  Done.";
			}
			pamc.common.Int.AppendMsgToML(this.mlOutput, "Counting total number of available status " + this.strStatusFrom + " claims...");
			if (!(this.CountStatus("  Status = " + "\'" + this.strStatusFrom + "\'", ref this.nTotalStatusFrom))) 
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, "Process aborted.");
				e.Return = false;
				return;
			}
			pamc.common.Int.AppendMsgToML(this.mlOutput, "Updating selected claims from Status = " + this.strStatusFrom + " to Status = " + this.strStatusTo + "...");
			if (!(this.UpdateTable(this.strStatusTo, this.strWhere, ref this.nTotalSelected))) 
			{
				pamc.common.Int.AppendMsgToML(this.mlOutput, "Process aborted.");
				e.Return = false;
				return;
			}
			this.CountStatus("  Status = " + "\'" + this.strStatusTo + "\'", ref this.nTotalStatusTo);
			this.GetStatistics();
			pamc.common.Int.AppendMsgToML(this.mlOutput, "Process completed.");
			this.SendMessageToChildren(pamc.common.Const.AM_DisEnable, 1, 0);
			#endregion
		}
		
		/// <summary>
		/// pbClose WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbClose_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_DisEnable:
					this.pbClose_OnAM_DisEnable(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbClose_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class CheckTempTableLocals
			{
				public SalString strSql = "";
				public SalNumber nInd = 0;
				public SalNumber nTotalTempTable = 0;
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class UpdateFromTempTableLocals
			{
				public SalNumber nInd = 0;
				public SalNumber nRecs = 0;
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class CheckClaimsLocals
			{
				public SalString strSql = "";
				public SalNumber nInd = 0;
				public SalString strStatusFrom = "";
				public SalString strErrorMsg = "";
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class UpdateTableLocals
			{
				public SalString strSql = "";
				public SalNumber nInd = 0;
				public SalNumber nToStatus = 0;
				public SalString strParamWhere = "";
				public SalString strErr = "";
				public SalNumber nErr = 0;
				public SalString strAutoCommit = "";
				public SalNumber nAutoCommit = 0;
				public SalString strParamWhere2 = "";
				public SalString strStatusTo = "";
				public SalString strWhere = "";
				public SalNumber nRecs = 0;
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class CountStatusLocals
			{
				public SalString strSql = "";
				public SalNumber nInd = 0;
				public SalString strWhere = "";
				public SalNumber nCount = 0;
			}
		}
		#endregion
	}
}
