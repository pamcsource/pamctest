// <ppj name="pamc.p3000" date="07.09.2018 10:25:08" id="D70F323C7DBEADEF74807E910A15A3912A8A74F8"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.p3000
{
	
	/// <summary>
	/// Select output for check printing process
	/// </summary>
	/// <param name="strReportParam"></param>
	/// <param name="bFile"></param>
	/// <param name="bAlign"></param>
	public partial class dlgSelectOutput : dlgGeneralBox2
	{
		#region Window Parameters
		public SalArray<SalString> strReportParam;
		public SalBoolean bFile;
		public SalBoolean bAlign;
		#endregion
		
		#region Window Variables
		public SalString strDevice = "";
		public SalArray<SalString> strFilters = new SalArray<SalString>("0:1");
		public SalNumber nFilters = 0;
		public SalNumber nIndex = 0;
		public SalString strPort = "";
		public SalString strDriver = "";
		public SalString strFile = "";
		public SalString strPath = "";
		public SalString strOutputDrive = "";
		public SalString strSaveCurrPath = "";
		public SalString strSaveDrive = "";
		public SalBoolean bToFile = false;
		public SalBoolean bAlignment = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgSelectOutput(SalArray<SalString> strReportParam, SalBoolean bFile, SalBoolean bAlign)
		{
			// Assign global reference.
			App.dlgSelectOutput = this;
			// Window Parameters initialization.
			this.strReportParam = strReportParam;
			this.bFile = bFile;
			this.bAlign = bAlign;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalArray<SalString> strReportParam, ref SalBoolean bFile, ref SalBoolean bAlign)
		{
			dlgSelectOutput dlg = new dlgSelectOutput(strReportParam, bFile, bAlign);
			SalNumber ret = dlg.ShowDialog(owner);
			bFile = dlg.bFile;
			bAlign = dlg.bAlign;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgSelectOutput FromHandle(SalWindowHandle handle)
		{
			return ((dlgSelectOutput)SalWindow.FromHandle(handle, typeof(dlgSelectOutput)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgSelectOutput WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSelectOutput_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgSelectOutput_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_CreateComplete:
					this.dlgSelectOutput_OnSAM_CreateComplete(sender, e);
					break;
				
				case pamc.common.Const.AM_GetValue:
					this.dlgSelectOutput_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSelectOutput_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.CenterWindow();
            //PPJ:FINAL:MHO - CA:0035 If it is called from SAM_Create, SalHideWindow can lead to wrong behavior. Set Opacity instead.
            //this.HideWindow();
            this.Opacity = 0;
			this.strFilters[0] = "Rich Text Format  (*.rtf)";
			this.strFilters[1] = "*.rtf";
			// Set strPath = strReportParam[3]
			pamc.common.Int.GetFilePath(this.strReportParam[3], ref this.strPath, ref this.strFile, ref this.strOutputDrive);
			if (!(pamc.common.Int.DosExists(this.strPath))) 
			{
				Sal.FileGetCurrentDirectory(ref this.strPath);
			}
			this.nFilters = 2;
			if (this.strReportParam[8] == pamc.sqllib.Const.strNULL) 
			{
				Sal.PrtGetDefault(ref this.strDevice, ref this.strDriver, ref this.strPort);
				this.strReportParam[8] = this.strDevice;
				this.strReportParam[9] = this.strPort;
			}
			else
			{
				this.strDevice = this.strReportParam[8];
				this.strPort = this.strReportParam[9];
			}
			this.bToFile = this.bFile;
			this.bAlignment = this.bAlign;
			#endregion
		}
		
		/// <summary>
		/// SAM_CreateComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSelectOutput_OnSAM_CreateComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_Initialize, 0, 0);
            //PPJ:FINAL:MHO - CA:0035 If it is called from SAM_Create, SalHideWindow can lead to wrong behavior. Set Opacity instead.
            //this.ShowWindow();
            this.Opacity = 1;
            #endregion
        }

        /// <summary>
        /// AM_GetValue event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dlgSelectOutput_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_GetValue, 0, 0);
			if (this.bToFile) 
			{
				if (this.dfFile.IsEmpty()) 
				{
					pamc.ez32bit.Int.ValidateErrorMsgBox("Please enter a print-to-file name");
					e.Return = false;
					return;
				}
				if (!(pamc.common.Int.DosExists(this.strPath))) 
				{
					pamc.ez32bit.Int.ValidateErrorMsgBox("You have selected an invalid path for output, please verify the path.");
					e.Return = false;
					return;
				}
				this.strReportParam[3] = this.dfFile.Text;
			}
			this.strReportParam[8] = this.strDevice;
			this.strReportParam[9] = this.strPort;
			this.bFile = this.bToFile;
			this.bAlign = this.bAlignment;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// rbChecks WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbChecks_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.rbChecks_OnAM_Initialize(sender, e);
					break;
				
				case pamc.common.Const.AM_GetValue:
					this.rbChecks_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbChecks_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bAlignment)) 
			{
				this.rbChecks.Checked = true;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbChecks_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.rbChecks.Checked) 
			{
				this.bAlignment = false;
			}
			#endregion
		}
		
		/// <summary>
		/// rbAlignment WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbAlignment_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.rbAlignment_OnAM_Initialize(sender, e);
					break;
				
				case pamc.common.Const.AM_GetValue:
					this.rbAlignment_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbAlignment_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bAlignment) 
			{
				this.rbAlignment.Checked = true;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbAlignment_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.rbAlignment.Checked) 
			{
				this.bAlignment = true;
			}
			#endregion
		}
		
		/// <summary>
		/// cbFile WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbFile_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_GetValue:
					this.cbFile_OnAM_GetValue(sender, e);
					break;
				
				case pamc.common.Const.AM_Initialize:
					this.cbFile_OnAM_Initialize(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.cbFile_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbFile_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.cbFile.Checked) 
			{
				this.bToFile = true;
			}
			else
			{
				this.bToFile = false;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbFile_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bFile) 
			{
				this.cbFile.Checked = true;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbFile_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfFile.Text = pamc.sqllib.Const.strNULL;
			if (this.cbFile.Checked) 
			{
				this.pbFile.EnableWindow();
				this.dfFile.Text = this.strPath + "\\" + this.strFile;
			}
			else
			{
				this.pbFile.DisableWindow();
			}
			#endregion
		}
		
		/// <summary>
		/// pbFile WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbFile_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbFile_OnSAM_Click(sender, e);
					break;
				
				case pamc.common.Const.AM_Initialize:
					this.pbFile_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbFile_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Set strPath = strReportParam[3]
			this.strSaveDrive = Sal.FileGetDrive();
			Sal.FileGetCurrentDirectory(ref this.strSaveCurrPath);
			Sal.FileSetDrive(this.strOutputDrive);
			Sal.FileSetCurrentDirectory(this.strPath);
			if (Sal.DlgSaveFile(this, "Save ouput file as...", this.strFilters, this.nFilters, ref this.nIndex, ref this.strFile, ref this.strPath)) 
			{
				// Set strReportParam[3] = strPath
				pamc.common.Int.GetFilePath(this.strPath, ref this.strPath, ref this.strFile, ref this.strOutputDrive);
				this.dfFile.Text = this.strPath + "\\" + this.strFile;
				// Call SalSendMsg(rbFile, AM_Lookup, 0,0)
			}
			Sal.FileSetDrive(this.strSaveDrive);
			Sal.FileSetCurrentDirectory(this.strSaveCurrPath);
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbFile_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bToFile) 
			{
				this.pbFile.EnableWindow();
			}
			else
			{
				this.pbFile.DisableWindow();
			}
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.SendMessage(pamc.common.Const.AM_GetValue, 0, 0)) 
			{
				this.EndDialog(1);
			}
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		
		/// <summary>
		/// pbPrinters WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrinters_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbPrinters_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrinters_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.PrtSetup(ref this.strDevice, ref this.strDriver, ref this.strPort, true)) 
			{
				this.dfPrinter.SendMessage(pamc.common.Const.AM_Initialize, 0, 0);
			}
			#endregion
		}
		
		/// <summary>
		/// dfPrinter WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfPrinter_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.dfPrinter_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfPrinter_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SalPrtGetDefault(strDevice, strDriver, strPort)
			this.dfPrinter.Text = this.strDevice + " " + this.strPort;
			#endregion
		}
		
		/// <summary>
		/// dfFile WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfFile_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.dfFile_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfFile_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bToFile) 
			{
				this.dfFile.Text = this.strReportParam[3];
			}
			#endregion
		}
		#endregion
	}
}
