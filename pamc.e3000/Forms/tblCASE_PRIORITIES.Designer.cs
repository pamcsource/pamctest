// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class tblCASE_PRIORITIES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private pamc.common.App.NamedMenus.menuFile menu_menuFile;
		private pamc.common.App.NamedMenus.menuRecordDefault menu_menuRecordDefault;
		private pamc.common.App.NamedMenus.menuView menu_menuView;
		private pamc.common.App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private pamc.common.App.NamedMenus.menuHelp menu_menuHelp;
		public clsColQueryKey colCODE;
		public clsColQuery colDESCR;
		public clsCol colDEFAULT_YN;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new pamc.common.App.NamedMenus.menuFile();
			this.menu_menuRecordDefault = new pamc.common.App.NamedMenus.menuRecordDefault();
			this.menu_menuView = new pamc.common.App.NamedMenus.menuView();
			this.menu_menuMDIWindows = new pamc.common.App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new pamc.common.App.NamedMenus.menuHelp();
			this.colCODE = new pamc.common.clsColQueryKey();
			this.colDESCR = new pamc.common.clsColQuery();
			this.colDEFAULT_YN = new pamc.common.clsCol();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecordDefault);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// colROWID
			// 
			this.colROWID.Name = "colROWID";
			this.colROWID.TabIndex = 0;
			// 
			// colCreateBy
			// 
			this.colCreateBy.Name = "colCreateBy";
			this.colCreateBy.TabIndex = 1;
			// 
			// colCreateDate
			// 
			this.colCreateDate.Name = "colCreateDate";
			this.colCreateDate.TabIndex = 2;
			// 
			// colLastChangeBy
			// 
			this.colLastChangeBy.Name = "colLastChangeBy";
			this.colLastChangeBy.TabIndex = 3;
			// 
			// colLastChangeDate
			// 
			this.colLastChangeDate.Name = "colLastChangeDate";
			this.colLastChangeDate.TabIndex = 4;
			// 
			// colCODE
			// 
			this.colCODE.Name = "colCODE";
			this.colCODE.Title = "Priority\r\nCode";
			this.colCODE.Width = 60;
			this.colCODE.MaxLength = 1;
			this.colCODE.TabIndex = 5;
			// 
			// colDESCR
			// 
			this.colDESCR.Name = "colDESCR";
			this.colDESCR.Title = "Priority\r\nDescription";
			this.colDESCR.Width = 227;
			this.colDESCR.MaxLength = 30;
			this.colDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.colDESCR_WindowActions);
			this.colDESCR.TabIndex = 6;
			// 
			// colDEFAULT_YN
			// 
			this.colDEFAULT_YN.Name = "colDEFAULT_YN";
			this.colDEFAULT_YN.Title = "Default";
			this.colDEFAULT_YN.Width = 64;
			this.colDEFAULT_YN.MaxLength = 1;
			this.colDEFAULT_YN.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.colDEFAULT_YN.Format = "";
			this.colDEFAULT_YN.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.colDEFAULT_YN.CellType = PPJ.Runtime.Windows.CellType.CheckBox;
			this.colDEFAULT_YN.CheckBox.CheckedValue = "1";
			this.colDEFAULT_YN.TabIndex = 7;
			// 
			// tblCASE_PRIORITIES
			// 
			this.Controls.Add(this.colROWID);
			this.Controls.Add(this.colCreateBy);
			this.Controls.Add(this.colCreateDate);
			this.Controls.Add(this.colLastChangeBy);
			this.Controls.Add(this.colLastChangeDate);
			this.Controls.Add(this.colCODE);
			this.Controls.Add(this.colDESCR);
			this.Controls.Add(this.colDEFAULT_YN);
			this.Name = "tblCASE_PRIORITIES";
			this.Text = "Priority Codes";
			this.Location = new System.Drawing.Point(3, 6);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblCASE_PRIORITIES_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tblCASE_PRIORITIES == this) 
			{
				App.tblCASE_PRIORITIES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
