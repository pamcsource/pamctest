// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmMODIF_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditKey dfMODIF;
		public clsDataFieldEdit dfMODDESC;
		protected clsText label1;
		protected clsText label2;
		protected clsText label3;
		public clsDataFieldDisabled dfEDITABLE;
		public clsDataFieldDisabled dfSELECTABLE;
		// On SAM_Click
		// If cbCUR_CONV = 1
		// Set dfCUR_CONV = 1
		// Else
		// Set dfCUR_CONV = 0
		// Set MyValue = 1
		public clsCheckBoxAuto cbCUR_CONV;
		public clsDataFieldEditN dfMIN_VALUE;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfMODIF = new pamc.common.clsDataFieldEditKey();
			this.dfMODDESC = new pamc.common.clsDataFieldEdit();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.dfEDITABLE = new pamc.common.clsDataFieldDisabled();
			this.dfSELECTABLE = new pamc.common.clsDataFieldDisabled();
			this.cbCUR_CONV = new pamc.common.clsCheckBoxAuto();
			this.dfMIN_VALUE = new pamc.common.clsDataFieldEditN();
			this.SuspendLayout();
			// 
			// dfMODIF
			// 
			this.dfMODIF.Name = "dfMODIF";
			this.dfMODIF.MaxLength = 7;
			this.dfMODIF.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMODIF.Location = new System.Drawing.Point(146, 9);
			this.dfMODIF.Size = new System.Drawing.Size(61, 20);
			this.dfMODIF.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfMODIF_WindowActions);
			this.dfMODIF.TabIndex = 0;
			// 
			// dfMODDESC
			// 
			this.dfMODDESC.Name = "dfMODDESC";
			this.dfMODDESC.MaxLength = 30;
			this.dfMODDESC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMODDESC.Location = new System.Drawing.Point(146, 69);
			this.dfMODDESC.Size = new System.Drawing.Size(224, 20);
			this.dfMODDESC.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfMODDESC_WindowActions);
			this.dfMODDESC.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(10, 11);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Minimum Value:";
			this.label2.Location = new System.Drawing.Point(10, 43);
			this.label2.Size = new System.Drawing.Size(109, 16);
			this.label2.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Description:";
			this.label3.Location = new System.Drawing.Point(10, 73);
			this.label3.Size = new System.Drawing.Size(89, 16);
			this.label3.TabIndex = 4;
			// 
			// dfEDITABLE
			// 
			this.dfEDITABLE.Name = "dfEDITABLE";
			this.dfEDITABLE.Visible = false;
			this.dfEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfEDITABLE.Location = new System.Drawing.Point(143, 63);
			this.dfEDITABLE.Size = new System.Drawing.Size(66, 19);
			this.dfEDITABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfEDITABLE_WindowActions);
			this.dfEDITABLE.TabIndex = 5;
			// 
			// dfSELECTABLE
			// 
			this.dfSELECTABLE.Name = "dfSELECTABLE";
			this.dfSELECTABLE.Visible = false;
			this.dfSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSELECTABLE.Location = new System.Drawing.Point(239, 63);
			this.dfSELECTABLE.Size = new System.Drawing.Size(66, 19);
			this.dfSELECTABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSELECTABLE_WindowActions);
			this.dfSELECTABLE.TabIndex = 6;
			// 
			// cbCUR_CONV
			// 
			this.cbCUR_CONV.Name = "cbCUR_CONV";
			this.cbCUR_CONV.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbCUR_CONV.Text = "Currency Converter";
			this.cbCUR_CONV.Location = new System.Drawing.Point(239, 7);
			this.cbCUR_CONV.Size = new System.Drawing.Size(136, 24);
			this.cbCUR_CONV.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbCUR_CONV_WindowActions);
			this.cbCUR_CONV.TabIndex = 7;
			// 
			// dfMIN_VALUE
			// 
			this.dfMIN_VALUE.Name = "dfMIN_VALUE";
			this.dfMIN_VALUE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMIN_VALUE.Location = new System.Drawing.Point(146, 39);
			this.dfMIN_VALUE.Size = new System.Drawing.Size(61, 20);
			this.dfMIN_VALUE.TabIndex = 8;
			// 
			// frmMODIF_CODES
			// 
			this.Controls.Add(this.dfMIN_VALUE);
			this.Controls.Add(this.cbCUR_CONV);
			this.Controls.Add(this.dfSELECTABLE);
			this.Controls.Add(this.dfEDITABLE);
			this.Controls.Add(this.dfMODDESC);
			this.Controls.Add(this.dfMODIF);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmMODIF_CODES";
			this.ClientSize = new System.Drawing.Size(417, 118);
			this.Text = "Service Modifier Code";
			this.Location = new System.Drawing.Point(97, 179);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmMODIF_CODES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmMODIF_CODES == this) 
			{
				App.frmMODIF_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
