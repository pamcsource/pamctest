// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgSubscribPmts
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public dlgSubscribPmts.tbl1TableWindow tbl1;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbl1 = (dlgSubscribPmts.tbl1TableWindow)CreateTableWindow(typeof(dlgSubscribPmts.tbl1TableWindow));
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.tbl1);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Size = new System.Drawing.Size(570, 38);
			this.frame1.TabIndex = 2;
			// 
			// tbl1
			// 
			this.tbl1.Name = "tbl1";
			this.tbl1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.tbl1.Location = new System.Drawing.Point(7, 41);
			this.tbl1.Size = new System.Drawing.Size(447, 268);
			this.tbl1.UseVisualStyles = true;
			// 
			// tbl1.colSUBSCRIBERID
			// 
			this.tbl1.colSUBSCRIBERID.Name = "colSUBSCRIBERID";
			this.tbl1.colSUBSCRIBERID.Visible = false;
			this.tbl1.colSUBSCRIBERID.Title = "Member ID";
			this.tbl1.colSUBSCRIBERID.Width = 145;
			this.tbl1.colSUBSCRIBERID.MaxLength = 20;
			this.tbl1.colSUBSCRIBERID.Position = 1;
			// 
			// tbl1.colBENYEAR
			// 
			this.tbl1.colBENYEAR.Name = "colBENYEAR";
			this.tbl1.colBENYEAR.Title = "Benefit\r\nYear";
			this.tbl1.colBENYEAR.Width = 55;
			this.tbl1.colBENYEAR.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tbl1.colBENYEAR.Format = "";
			this.tbl1.colBENYEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.tbl1.colBENYEAR.Position = 2;
			// 
			// tbl1.colHPCODE
			// 
			this.tbl1.colHPCODE.Name = "colHPCODE";
			this.tbl1.colHPCODE.Title = "HP\r\nCode";
			this.tbl1.colHPCODE.Width = 49;
			this.tbl1.colHPCODE.MaxLength = 4;
			this.tbl1.colHPCODE.Position = 3;
			// 
			// tbl1.colCOPAY
			// 
			this.tbl1.colCOPAY.Name = "colCOPAY";
			this.tbl1.colCOPAY.Title = "Copay";
			this.tbl1.colCOPAY.Width = 102;
			this.tbl1.colCOPAY.MaxLength = 11;
			this.tbl1.colCOPAY.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tbl1.colCOPAY.Format = "#,##0.00";
			this.tbl1.colCOPAY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tbl1.colCOPAY.Position = 4;
			// 
			// tbl1.colOOP
			// 
			this.tbl1.colOOP.Name = "colOOP";
			this.tbl1.colOOP.Title = "OOP";
			this.tbl1.colOOP.Width = 102;
			this.tbl1.colOOP.MaxLength = 11;
			this.tbl1.colOOP.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tbl1.colOOP.Format = "#,##0.00";
			this.tbl1.colOOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tbl1.colOOP.Position = 5;
			// 
			// tbl1.colDEDUCT
			// 
			this.tbl1.colDEDUCT.Name = "colDEDUCT";
			this.tbl1.colDEDUCT.Title = "Deductible";
			this.tbl1.colDEDUCT.Width = 102;
			this.tbl1.colDEDUCT.MaxLength = 11;
			this.tbl1.colDEDUCT.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tbl1.colDEDUCT.Format = "#,##0.00";
			this.tbl1.colDEDUCT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tbl1.colDEDUCT.Position = 6;
			this.tbl1.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tbl1_WindowActions);
			this.tbl1.TabIndex = 3;
			// 
			// dlgSubscribPmts
			// 
			this.Name = "dlgSubscribPmts";
			this.ClientSize = new System.Drawing.Size(463, 335);
			this.Text = "Subscriber Payments";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgSubscribPmts_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgSubscribPmts == this) 
			{
				App.dlgSubscribPmts = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tbl1
		
		public partial class tbl1TableWindow
		{
			#region Window Controls
			public clsCol colSUBSCRIBERID;
			public clsCol colBENYEAR;
			public clsCol colHPCODE;
			public clsCol colCOPAY;
			public clsCol colOOP;
			public clsCol colDEDUCT;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colSUBSCRIBERID = new pamc.common.clsCol();
				this.colBENYEAR = new pamc.common.clsCol();
				this.colHPCODE = new pamc.common.clsCol();
				this.colCOPAY = new pamc.common.clsCol();
				this.colOOP = new pamc.common.clsCol();
				this.colDEDUCT = new pamc.common.clsCol();
				this.SuspendLayout();
				// 
				// colSUBSCRIBERID
				// 
				this.colSUBSCRIBERID.Name = "colSUBSCRIBERID";
				// 
				// colBENYEAR
				// 
				this.colBENYEAR.Name = "colBENYEAR";
				// 
				// colHPCODE
				// 
				this.colHPCODE.Name = "colHPCODE";
				// 
				// colCOPAY
				// 
				this.colCOPAY.Name = "colCOPAY";
				// 
				// colOOP
				// 
				this.colOOP.Name = "colOOP";
				// 
				// colDEDUCT
				// 
				this.colDEDUCT.Name = "colDEDUCT";
				// 
				// tbl1
				// 
				this.Controls.Add(this.colSUBSCRIBERID);
				this.Controls.Add(this.colBENYEAR);
				this.Controls.Add(this.colHPCODE);
				this.Controls.Add(this.colCOPAY);
				this.Controls.Add(this.colOOP);
				this.Controls.Add(this.colDEDUCT);
				this.Name = "tbl1";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
