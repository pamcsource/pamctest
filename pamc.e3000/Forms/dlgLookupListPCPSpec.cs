// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// Dialog box which displays list of valid entries for the provider's specialty code field.
	/// </summary>
	/// <param name="strSqlSelect"></param>
	/// <param name="strSelected"></param>
	/// <param name="strTitle"></param>
	/// <param name="strSearchItem"></param>
	/// <param name="nSelectableCol"></param>
	public partial class dlgLookupListPCPSpec : SalDialogBox
	{
		#region Window Parameters
		public SalString strSqlSelect;
		public SalString strSelected;
		public SalString strTitle;
		public SalString strSearchItem;
		public SalNumber nSelectableCol;
		#endregion
		
		#region Window Variables
		public SalNumber nIndex = 0;
		public SalWindowHandle hWndDlg = SalWindowHandle.Null;
		public SalString strSql = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgLookupListPCPSpec(SalString strSqlSelect, SalString strSelected, SalString strTitle, SalString strSearchItem, SalNumber nSelectableCol)
		{
			// Assign global reference.
			App.dlgLookupListPCPSpec = this;
			// Window Parameters initialization.
			this.strSqlSelect = strSqlSelect;
			this.strSelected = strSelected;
			this.strTitle = strTitle;
			this.strSearchItem = strSearchItem;
			this.nSelectableCol = nSelectableCol;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalString strSqlSelect, ref SalString strSelected, SalString strTitle, SalString strSearchItem, SalNumber nSelectableCol)
		{
			dlgLookupListPCPSpec dlg = new dlgLookupListPCPSpec(strSqlSelect, strSelected, strTitle, strSearchItem, nSelectableCol);
			SalNumber ret = dlg.ShowDialog(owner);
			strSelected = dlg.strSelected;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static dlgLookupListPCPSpec FromHandle(SalWindowHandle handle)
		{
			return ((dlgLookupListPCPSpec)SalWindow.FromHandle(handle, typeof(dlgLookupListPCPSpec)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgLookupListPCPSpec WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgLookupListPCPSpec_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgLookupListPCPSpec_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Destroy:
					this.dlgLookupListPCPSpec_OnSAM_Destroy(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgLookupListPCPSpec_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			pamc.common.Int.SetPopupLoc(this);
			this.SetText(this.strTitle);
			Sys.hWndMDI.SetStatusBarText("Select valid entry from list");
			this.hWndDlg = this;
			#endregion
		}
		
		/// <summary>
		/// SAM_Destroy event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgLookupListPCPSpec_OnSAM_Destroy(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sys.hWndMDI.SetStatusBarText(pamc.sqllib.Const.strNULL);
			#endregion
		}
		
		/// <summary>
		/// tbl1 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.tbl1_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_DoubleClick:
					this.tbl1_OnSAM_DoubleClick(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.tbl1_OnSAM_Click(sender, e);
					break;
				
				case Sys.SAM_FetchRowDone:
					this.tbl1_OnSAM_FetchRowDone(sender, e);
					break;
				
				// On SAM_FetchRow
				
				// Return GetDiags( lParam )
				
				case pamc.common.Const.CM_Populate:
					this.tbl1_OnCM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SalSetFocus( hWndForm )
			this.tbl1.SetTableFlags(((Sys.TBL_Flag_SuppressRowLines | Sys.TBL_Flag_SingleSelection) | Sys.TBL_Flag_SuppressLastColLine), true);
			this.tbl1.SetTableFlags((Sys.TBL_Flag_MovableCols | Sys.TBL_Flag_SelectableCols), false);
			this.tbl1.DefineRowHeader("", 0, 0, Sys.hWndNULL);
			// Call PopulateAllRows( hWndForm )
			this.tbl1.SendMessage(pamc.common.Const.CM_Populate, 0, 0);
			pamc.common.Int.MakeColsNoEdit(this.tbl1);
			if (this.strSearchItem != pamc.sqllib.Const.strNULL && this.strSearchItem != "0" && ((bool)this.tbl1.TableScan(this.tbl1, this.strSearchItem, ref this.nIndex))) 
			{
				this.tbl1.SetRowFlags(this.nIndex, Sys.ROW_Selected, true);
				this.tbl1.SetFocusRow(this.nIndex);
			}
			else
			{
				this.tbl1.SetContextRow(0);
				this.tbl1.SetRowFlags(0, Sys.ROW_Selected, true);
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_DoubleClick event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_OnSAM_DoubleClick(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.tbl1.SendMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam)) 
			{
				this.strSelected = this.tbl1.CopyRowToStr(this.tbl1);
				this.hWndDlg.EndDialog(1);
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (pamc.common.Int.TblGetColumnTextX(this.tbl1, this.nSelectableCol) == "0") 
			{
				this.tbl1.ClearSelection();
				this.tbl1.KillFocusRow();
				e.Return = false;
				return;
			}
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// SAM_FetchRowDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_OnSAM_FetchRowDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.nSelectableCol > 0 && pamc.common.Int.TblGetColumnTextX(this.tbl1, this.nSelectableCol) == "0") 
			{
				Vis.TblSetRowColor(this.tbl1, Sys.lParam, Sys.COLOR_DarkGray);
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbl1_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// 03/98 ES added VisStrSubstitute
			this.strSql = " select sequence, code, descr from prov_speccodes, prov_specinfo " + " into  :dlgLookupListPCPSpec.tbl1.colSequence, :dlgLookupListPCPSpec.tbl1.colCode, :dlgLookupListPCPSpec.tbl1.colDescr where provid = " + "\'" + Vis.StrSubstitute(
				this.strSearchItem, "\'", "\'\'") + "\'" + " and speccode = code order by sequence ";
			// Call SqlInst.SqlPrep(hSql, strSql,'')
			// If SqlInst.SqlTblPopulate(hWndForm, hSql, ' select sequence, code, descr from prov_speccodes, prov_specinfo ' ||
			// ' into  :colSequence, :colCode, :colDescr where provid = '
			//  || '\'' || VisStrSubstitute(strSearchItem,'\'','\'\'') || '\'' || ' and speccode = code order by sequence ', TBL_FillAll )
			// If SqlInst.SqlTblPopulate(hWndForm, hSql, strSql, TBL_FillAll )
			if (pamc.price.Var.SqlInst.SqlTblPopulate(this.tbl1, pamc.price.Var.hSql, this.strSql, Sys.TBL_FillAll)) 
			{
			}
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tbl1.SendMessage(Sys.SAM_DoubleClick, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		#endregion
		
		#region tbl1
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public partial class tbl1TableWindow : SalTableWindow
		{
			// reference to the container form.
			private dlgLookupListPCPSpec _dlgLookupListPCPSpec = null;
			
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tbl1TableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private dlgLookupListPCPSpec dlgLookupListPCPSpec
			{
				[DebuggerStepThrough]
				get
				{
					if (_dlgLookupListPCPSpec == null) 
					{
						_dlgLookupListPCPSpec = (dlgLookupListPCPSpec)this.FindForm();
					}
					return _dlgLookupListPCPSpec;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public static tbl1TableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tbl1TableWindow)SalWindow.FromHandle(handle, typeof(tbl1TableWindow)));
			}
			#endregion
			
			#region Methods
			
			/// <summary>
			/// Searches col1 of the table for a given search string.
			/// </summary>
			/// <param name="hTbl"></param>
			/// <param name="strSearch"></param>
			/// <param name="nRowMatch"></param>
			/// <returns></returns>
			public SalNumber TableScan(SalWindowHandle hTbl, SalString strSearch, ref SalNumber nRowMatch)
			{
				#region Local Variables
				SalNumber nRow = 0;
				SalString strText = "";
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					nRow = Sys.TBL_MinRow;
					while (hTbl.FindNextRow(ref nRow, 0, 0)) 
					{
						hTbl.SetContextRow(nRow);
						strText = hTbl.GetColumnText(1);
						if (strText == strSearch) 
						{
							nRowMatch = nRow;
							return 1;
						}
					}
					return 0;
				}
				#endregion
			}
			
			/// <summary>
			/// </summary>
			/// <param name="hWndTbl"></param>
			/// <returns></returns>
			public SalString CopyRowToStr(SalWindowHandle hWndTbl)
			{
				#region Local Variables
				SalWindowHandle hWndChildCol = SalWindowHandle.Null;
				SalString strText = "";
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					hWndChildCol = hWndTbl.GetFirstChild(Sys.TYPE_TableColumn);
					while (hWndChildCol != SalWindowHandle.Null) 
					{
						strText = strText + pamc.common.Int.GetWindowTextX(hWndChildCol, 254) + pamc.common.Const.strTAB;
						hWndChildCol = hWndChildCol.GetNextChild(Sys.TYPE_TableColumn);
					}
					return strText;
				}
				#endregion
			}
			
			/// <summary>
			/// </summary>
			/// <param name="hWndTbl"></param>
			/// <returns></returns>
			public SalNumber ColTitleCleanup(SalWindowHandle hWndTbl)
			{
				#region Local Variables
				SalWindowHandle hWndChild = SalWindowHandle.Null;
				SalString strColTitle = "";
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					hWndChild = hWndTbl.GetFirstChild(Sys.TYPE_TableColumn);
					while (hWndChild != SalWindowHandle.Null) 
					{
						strColTitle = hWndChild.GetColumnTitle(100);
						hWndChild.SetColumnTitle(Vis.StrSubstitute(strColTitle, "_", " "));
						hWndChild = hWndChild.GetNextChild(Sys.TYPE_TableColumn);
					}
				}

				return 0;
				#endregion
			}
			#endregion
		}
		#endregion
	}
}
