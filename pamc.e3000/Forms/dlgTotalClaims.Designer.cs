// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgTotalClaims
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public dlgTotalClaims.tblCLAIM_MAstersTableWindow tblCLAIM_MAsters;
		protected SalFrame frame2;
		public clsDataFieldDisabled dfTotalNet;
		public clsDataFieldDisabled dfTotalUnits;
		protected clsText label1;
		public clsDlgClose pbClose;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tblCLAIM_MAsters = (dlgTotalClaims.tblCLAIM_MAstersTableWindow)CreateTableWindow(typeof(dlgTotalClaims.tblCLAIM_MAstersTableWindow));
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.dfTotalNet = new pamc.common.clsDataFieldDisabled();
			this.dfTotalUnits = new pamc.common.clsDataFieldDisabled();
			this.label1 = new pamc.common.clsText();
			this.pbClose = new pamc.common.clsDlgClose();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.pbClose);
			this.ClientArea.Controls.Add(this.dfTotalUnits);
			this.ClientArea.Controls.Add(this.dfTotalNet);
			this.ClientArea.Controls.Add(this.tblCLAIM_MAsters);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame2);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Visible = false;
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.Location = new System.Drawing.Point(361, 1);
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Size = new System.Drawing.Size(419, 38);
			this.frame1.TabIndex = 2;
			// 
			// tblCLAIM_MAsters
			// 
			this.tblCLAIM_MAsters.Name = "tblCLAIM_MAsters";
			this.tblCLAIM_MAsters.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.tblCLAIM_MAsters.Location = new System.Drawing.Point(20, 41);
			this.tblCLAIM_MAsters.Size = new System.Drawing.Size(361, 196);
			// 
			// tblCLAIM_MAsters.colSEQUENCE
			// 
			this.tblCLAIM_MAsters.colSEQUENCE.Name = "colSEQUENCE";
			this.tblCLAIM_MAsters.colSEQUENCE.Position = 1;
			// 
			// tblCLAIM_MAsters.colROWID
			// 
			this.tblCLAIM_MAsters.colROWID.Name = "colROWID";
			this.tblCLAIM_MAsters.colROWID.Position = 2;
			// 
			// tblCLAIM_MAsters.colPhCode
			// 
			this.tblCLAIM_MAsters.colPhCode.Name = "colPhCode";
			this.tblCLAIM_MAsters.colPhCode.Title = "Claim\r\nType";
			this.tblCLAIM_MAsters.colPhCode.Width = 34;
			this.tblCLAIM_MAsters.colPhCode.MaxLength = 1;
			this.tblCLAIM_MAsters.colPhCode.Position = 3;
			// 
			// tblCLAIM_MAsters.colClaimNo
			// 
			this.tblCLAIM_MAsters.colClaimNo.Name = "colClaimNo";
			this.tblCLAIM_MAsters.colClaimNo.Title = "Claim No.";
			this.tblCLAIM_MAsters.colClaimNo.Width = 122;
			this.tblCLAIM_MAsters.colClaimNo.MaxLength = 16;
			this.tblCLAIM_MAsters.colClaimNo.Position = 4;
			// 
			// tblCLAIM_MAsters.colNet
			// 
			this.tblCLAIM_MAsters.colNet.Name = "colNet";
			this.tblCLAIM_MAsters.colNet.Title = "Net";
			this.tblCLAIM_MAsters.colNet.Width = 123;
			this.tblCLAIM_MAsters.colNet.MaxLength = 15;
			this.tblCLAIM_MAsters.colNet.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblCLAIM_MAsters.colNet.Format = "#,##0.00";
			this.tblCLAIM_MAsters.colNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tblCLAIM_MAsters.colNet.Position = 5;
			// 
			// tblCLAIM_MAsters.colNBRUnits
			// 
			this.tblCLAIM_MAsters.colNBRUnits.Name = "colNBRUnits";
			this.tblCLAIM_MAsters.colNBRUnits.Title = "Units";
			this.tblCLAIM_MAsters.colNBRUnits.Width = 41;
			this.tblCLAIM_MAsters.colNBRUnits.MaxLength = 4;
			this.tblCLAIM_MAsters.colNBRUnits.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblCLAIM_MAsters.colNBRUnits.Format = "#0";
			this.tblCLAIM_MAsters.colNBRUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tblCLAIM_MAsters.colNBRUnits.Position = 6;
			// 
			// tblCLAIM_MAsters.col1Filler1
			// 
			this.tblCLAIM_MAsters.col1Filler1.Name = "col1Filler1";
			this.tblCLAIM_MAsters.col1Filler1.Visible = false;
			this.tblCLAIM_MAsters.col1Filler1.Position = 7;
			// 
			// tblCLAIM_MAsters.col1Filler2
			// 
			this.tblCLAIM_MAsters.col1Filler2.Name = "col1Filler2";
			this.tblCLAIM_MAsters.col1Filler2.Visible = false;
			this.tblCLAIM_MAsters.col1Filler2.Position = 8;
			// 
			// tblCLAIM_MAsters.col1Filler3
			// 
			this.tblCLAIM_MAsters.col1Filler3.Name = "col1Filler3";
			this.tblCLAIM_MAsters.col1Filler3.Visible = false;
			this.tblCLAIM_MAsters.col1Filler3.Position = 9;
			// 
			// tblCLAIM_MAsters.colCreateBy
			// 
			this.tblCLAIM_MAsters.colCreateBy.Name = "colCreateBy";
			this.tblCLAIM_MAsters.colCreateBy.Visible = false;
			this.tblCLAIM_MAsters.colCreateBy.Position = 10;
			// 
			// tblCLAIM_MAsters.colCreateDate
			// 
			this.tblCLAIM_MAsters.colCreateDate.Name = "colCreateDate";
			this.tblCLAIM_MAsters.colCreateDate.Visible = false;
			this.tblCLAIM_MAsters.colCreateDate.Position = 11;
			// 
			// tblCLAIM_MAsters.colLastChangeBy
			// 
			this.tblCLAIM_MAsters.colLastChangeBy.Name = "colLastChangeBy";
			this.tblCLAIM_MAsters.colLastChangeBy.Visible = false;
			this.tblCLAIM_MAsters.colLastChangeBy.Position = 12;
			// 
			// tblCLAIM_MAsters.colLastChangeDate
			// 
			this.tblCLAIM_MAsters.colLastChangeDate.Name = "colLastChangeDate";
			this.tblCLAIM_MAsters.colLastChangeDate.Visible = false;
			this.tblCLAIM_MAsters.colLastChangeDate.Position = 13;
			this.tblCLAIM_MAsters.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblCLAIM_MAsters_WindowActions);
			this.tblCLAIM_MAsters.TabIndex = 3;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.Color.Silver;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.DropShadow;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(19, 239);
			this.frame2.Size = new System.Drawing.Size(363, 35);
			this.frame2.TabIndex = 4;
			// 
			// dfTotalNet
			// 
			this.dfTotalNet.Name = "dfTotalNet";
			this.dfTotalNet.MaxLength = 18;
			this.dfTotalNet.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfTotalNet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTotalNet.Format = "#,##0.00";
			this.dfTotalNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.dfTotalNet.Location = new System.Drawing.Point(174, 252);
			this.dfTotalNet.Size = new System.Drawing.Size(145, 19);
			this.dfTotalNet.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfTotalNet_WindowActions);
			this.dfTotalNet.TabIndex = 5;
			// 
			// dfTotalUnits
			// 
			this.dfTotalUnits.Name = "dfTotalUnits";
			this.dfTotalUnits.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfTotalUnits.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTotalUnits.Format = "#0";
			this.dfTotalUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.dfTotalUnits.Location = new System.Drawing.Point(330, 252);
			this.dfTotalUnits.Size = new System.Drawing.Size(31, 19);
			this.dfTotalUnits.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfTotalUnits_WindowActions);
			this.dfTotalUnits.TabIndex = 6;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Totals:";
			this.label1.Location = new System.Drawing.Point(121, 251);
			this.label1.Size = new System.Drawing.Size(46, 16);
			this.label1.TabIndex = 7;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Location = new System.Drawing.Point(320, 1);
			this.pbClose.Size = new System.Drawing.Size(78, 31);
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 8;
			// 
			// dlgTotalClaims
			// 
			this.Name = "dlgTotalClaims";
			this.ClientSize = new System.Drawing.Size(404, 304);
			this.Text = "Total Claims";
			this.Location = new System.Drawing.Point(200, 110);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgTotalClaims_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgTotalClaims == this) 
			{
				App.dlgTotalClaims = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tblCLAIM_MAsters
		
		public partial class tblCLAIM_MAstersTableWindow
		{
			#region Window Controls
			public clsCol colPhCode;
			public clsCol colClaimNo;
			public clsCol colNet;
			public clsCol colNBRUnits;
			public clsColIgnore col1Filler1;
			public clsColIgnore col1Filler2;
			public clsColIgnore col1Filler3;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colPhCode = new pamc.common.clsCol();
				this.colClaimNo = new pamc.common.clsCol();
				this.colNet = new pamc.common.clsCol();
				this.colNBRUnits = new pamc.common.clsCol();
				this.col1Filler1 = new pamc.common.clsColIgnore();
				this.col1Filler2 = new pamc.common.clsColIgnore();
				this.col1Filler3 = new pamc.common.clsColIgnore();
				this.SuspendLayout();
				// 
				// colSEQUENCE
				// 
				this.colSEQUENCE.Name = "colSEQUENCE";
				this.colSEQUENCE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.colSEQUENCE_WindowActions);
				// 
				// colROWID
				// 
				this.colROWID.Name = "colROWID";
				// 
				// colPhCode
				// 
				this.colPhCode.Name = "colPhCode";
				// 
				// colClaimNo
				// 
				this.colClaimNo.Name = "colClaimNo";
				// 
				// colNet
				// 
				this.colNet.Name = "colNet";
				// 
				// colNBRUnits
				// 
				this.colNBRUnits.Name = "colNBRUnits";
				// 
				// col1Filler1
				// 
				this.col1Filler1.Name = "col1Filler1";
				// 
				// col1Filler2
				// 
				this.col1Filler2.Name = "col1Filler2";
				// 
				// col1Filler3
				// 
				this.col1Filler3.Name = "col1Filler3";
				// 
				// colCreateBy
				// 
				this.colCreateBy.Name = "colCreateBy";
				// 
				// colCreateDate
				// 
				this.colCreateDate.Name = "colCreateDate";
				// 
				// colLastChangeBy
				// 
				this.colLastChangeBy.Name = "colLastChangeBy";
				// 
				// colLastChangeDate
				// 
				this.colLastChangeDate.Name = "colLastChangeDate";
				// 
				// tblCLAIM_MAsters
				// 
				this.Controls.Add(this.colSEQUENCE);
				this.Controls.Add(this.colROWID);
				this.Controls.Add(this.colPhCode);
				this.Controls.Add(this.colClaimNo);
				this.Controls.Add(this.colNet);
				this.Controls.Add(this.colNBRUnits);
				this.Controls.Add(this.col1Filler1);
				this.Controls.Add(this.col1Filler2);
				this.Controls.Add(this.col1Filler3);
				this.Controls.Add(this.colCreateBy);
				this.Controls.Add(this.colCreateDate);
				this.Controls.Add(this.colLastChangeBy);
				this.Controls.Add(this.colLastChangeDate);
				this.Name = "tblCLAIM_MAsters";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
