// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class tdlgCASE_UDF1VALUES : clsDlgInfoTable
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public tdlgCASE_UDF1VALUES()
		{
			// Assign global reference.
			App.tdlgCASE_UDF1VALUES = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			tdlgCASE_UDF1VALUES dlg = new tdlgCASE_UDF1VALUES();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static tdlgCASE_UDF1VALUES FromHandle(SalWindowHandle handle)
		{
			return ((tdlgCASE_UDF1VALUES)SalWindow.FromHandle(handle, typeof(tdlgCASE_UDF1VALUES)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// tdlgCASE_UDF1VALUES WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgCASE_UDF1VALUES_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_Edit:
					this.tdlgCASE_UDF1VALUES_OnCM_Edit(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.tdlgCASE_UDF1VALUES_OnSAM_Create(sender, e);
					break;
				
				case pamc.common.Const.CM_New:
					this.tdlgCASE_UDF1VALUES_OnCM_New(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Edit event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgCASE_UDF1VALUES_OnCM_Edit(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgCASE_UDF1VALUES.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_Edit);
			return;
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgCASE_UDF1VALUES_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Create, 0, 0);
			this.SetText("Valid Values for " + App.frmCASE_TYPES.dfUDF1LABEL.Text);
			#endregion
		}
		
		/// <summary>
		/// CM_New event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgCASE_UDF1VALUES_OnCM_New(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgCASE_UDF1VALUES.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_New);
			return;
			#endregion
		}
		
		/// <summary>
		/// tblCASE_UDF1VALUES WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblCASE_UDF1VALUES_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On CM_MoveTblCols
				
				// Call MoveTblCols( hWndForm )
				
				case pamc.common.Const.CM_SetInstanceVars:
					this.tblCASE_UDF1VALUES_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblCASE_UDF1VALUES_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SetMinRows( 1 )
			// 04/21/98 ES. Use SetRowLimit function to restrict Window Row Limit to 2000 rows.The same value
			// we have in clsChildTable -  "Max Rows in Memory" property.
			this.tblCASE_UDF1VALUES.SetRowLimit(pamc.common.Const.ChildTbl_MaxRows);
			this.tblCASE_UDF1VALUES.SetOrder(" Order by CASETYPE ASC, VALUE ASC");
			this.tblCASE_UDF1VALUES.SetWhere(" CASE_UDF1VALUES.CASETYPE = :hWndMainForm.dfCASETYPE");
			#endregion
		}
		#endregion
		
		#region tblCASE_UDF1VALUES
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public new partial class tbl1TableWindow : clsDlgInfoTable.tbl1TableWindow
		{
			// reference to the container form.
			private tdlgCASE_UDF1VALUES _tdlgCASE_UDF1VALUES = null;
			
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tbl1TableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private tdlgCASE_UDF1VALUES tdlgCASE_UDF1VALUES
			{
				[DebuggerStepThrough]
				get
				{
					if (_tdlgCASE_UDF1VALUES == null) 
					{
						_tdlgCASE_UDF1VALUES = (tdlgCASE_UDF1VALUES)this.FindForm();
					}
					return _tdlgCASE_UDF1VALUES;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public new static tbl1TableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tbl1TableWindow)SalWindow.FromHandle(handle, typeof(tbl1TableWindow)));
			}
			#endregion
			
			#region Window Actions
			
			/// <summary>
			/// colSEQUENCE WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colSEQUENCE_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Sys.SAM_Create:
						this.colSEQUENCE_OnSAM_Create(sender, e);
						break;
					
					case pamc.common.Const.CM_NotSelect:
						this.colSEQUENCE_OnCM_NotSelect(sender, e);
						break;
					
					case pamc.common.Const.CM_NotInsert:
						this.colSEQUENCE_OnCM_NotInsert(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// SAM_Create event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colSEQUENCE_OnSAM_Create(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colSEQUENCE.HideWindow();
				#endregion
			}
			
			/// <summary>
			/// CM_NotSelect event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colSEQUENCE_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// CM_NotInsert event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colSEQUENCE_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colCASETYPE WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCASETYPE_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case pamc.common.Const.AM_Initialize:
						this.colCASETYPE_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCASETYPE_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				if (Sys.wParam == 0) 
				{
					this.colCASETYPE.Text = pamc.common.Var.hWndMainForm.FindControl("dfCASETYPE").Text;
				}
				e.Return = true;
				return;
				#endregion
			}
			#endregion
		}
		#endregion
	}
}
