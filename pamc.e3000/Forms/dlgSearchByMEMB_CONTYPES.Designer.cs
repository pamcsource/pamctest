// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgSearchByMEMB_CONTYPES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldSearch dfTYPE;
		public clsDataFieldSearch dfDESCR;
		protected clsText label1;
		protected clsText label2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfTYPE = new pamc.common.clsDataFieldSearch();
			this.dfDESCR = new pamc.common.clsDataFieldSearch();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfTYPE
			// 
			this.dfTYPE.Name = "dfTYPE";
			this.dfTYPE.MaxLength = 2;
			this.dfTYPE.Location = new System.Drawing.Point(153, 56);
			this.dfTYPE.Size = new System.Drawing.Size(43, 24);
			this.dfTYPE.TabIndex = 0;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 30;
			this.dfDESCR.Location = new System.Drawing.Point(151, 92);
			this.dfDESCR.Size = new System.Drawing.Size(228, 24);
			this.dfDESCR.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Contract Type Code:";
			this.label1.Location = new System.Drawing.Point(13, 60);
			this.label1.Size = new System.Drawing.Size(132, 16);
			this.label1.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(13, 96);
			this.label2.Size = new System.Drawing.Size(117, 16);
			this.label2.TabIndex = 3;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 4;
			// 
			// pbOrderBy
			// 
			this.pbOrderBy.Name = "pbOrderBy";
			this.pbOrderBy.TabIndex = 5;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 6;
			// 
			// pbCount
			// 
			this.pbCount.Name = "pbCount";
			this.pbCount.TabIndex = 7;
			// 
			// dfCOUNT
			// 
			this.dfCOUNT.Name = "dfCOUNT";
			this.dfCOUNT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfCOUNT.TabIndex = 8;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.TabIndex = 9;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.TabIndex = 10;
			// 
			// dlgSearchByMEMB_CONTYPES
			// 
			this.Controls.Add(this.pbClose);
			this.Controls.Add(this.dfCOUNT);
			this.Controls.Add(this.pbCount);
			this.Controls.Add(this.pbOrderBy);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfTYPE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame2);
			this.Controls.Add(this.frame1);
			this.Name = "dlgSearchByMEMB_CONTYPES";
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgSearchByMEMB_CONTYPES == this) 
			{
				App.dlgSearchByMEMB_CONTYPES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
