// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class frmMEMB_STATUS : clsFormWindow
	{
		#region Window Variables
		public SalNumber nInd = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public frmMEMB_STATUS()
		{
			// Assign global reference.
			App.frmMEMB_STATUS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static frmMEMB_STATUS CreateWindow(Control owner)
		{
			frmMEMB_STATUS frm = new frmMEMB_STATUS();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static frmMEMB_STATUS FromHandle(SalWindowHandle handle)
		{
			return ((frmMEMB_STATUS)SalWindow.FromHandle(handle, typeof(frmMEMB_STATUS)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean ValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strFieldName = "";
			SalArray<SalString> strData = new SalArray<SalString>();
			SalNumber nInd = 0;
			SalNumber nItem = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				return true;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// frmMEMB_STATUS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.frmMEMB_STATUS_OnAM_Initialize(sender, e);
					break;
				
				case pamc.common.Const.CM_Populate:
					this.frmMEMB_STATUS_OnCM_Populate(sender, e);
					break;
				
				case pamc.common.Const.AM_SetDefault:
					this.frmMEMB_STATUS_OnAM_SetDefault(sender, e);
					break;
				
				case pamc.common.Const.CM_Edit:
					this.frmMEMB_STATUS_OnCM_Edit(sender, e);
					break;
				
				case pamc.common.Const.CM_Delete:
					this.frmMEMB_STATUS_OnCM_Delete(sender, e);
					break;
				
				case pamc.common.Const.CM_New:
					this.frmMEMB_STATUS_OnCM_New(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_Initialize, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.CM_DoLookup, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// AM_SetDefault event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnAM_SetDefault(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 1) 
			{
				if (this.GetScreenMode() != pamc.price.Const.MODE_New && this.GetScreenMode() != pamc.price.Const.MODE_Edit) 
				{
					e.Return = true;
					return;
				}
			}
			else
			{
				if (dlgDefault.ModalDialog(this, "TYPE", "DESCR", "")) 
				{
					this.SendMessage(pamc.common.Const.CM_Refresh, 0, 0);
				}
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Edit event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnCM_Edit(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.MessageBox("This is a EZ-CAP System code.  Unable to edit.", "Edit", (Sys.MB_Ok | Sys.MB_IconStop));
			e.Return = false;
			return;
			// If NOT dfEDITABLE
			// Call SalMessageBox( 'This is a EZ-CAP System code.  Unable to edit.', 'Edit', MB_Ok | MB_IconStop )
			// Return FALSE
			// Return SalSendClassMessage( CM_Edit, wParam, lParam )
			#endregion
		}
		
		/// <summary>
		/// CM_Delete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnCM_Delete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.MessageBox("This is a EZ-CAP System code.  Unable to delete.", "Delete", (Sys.MB_Ok | Sys.MB_IconStop));
			e.Return = false;
			return;
			// If NOT dfEDITABLE
			// Call SalMessageBox( 'This is a EZ-CAP System code.  Unable to delete.', 'Delete', MB_Ok | MB_IconStop )
			// Return FALSE
			// Return SalSendClassMessage( CM_Delete, wParam, lParam )
			#endregion
		}
		
		/// <summary>
		/// CM_New event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMEMB_STATUS_OnCM_New(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.MessageBox("These are EZ-CAP System codes.  Unable to add.", "Add", (Sys.MB_Ok | Sys.MB_IconStop));
			e.Return = false;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfTYPE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTYPE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfTYPE_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTYPE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (pamc.common.Int.SqlLookup("Select TYPE From MEMB_STATUS Where TYPE = :dfTYPE", "", ref this.nInd)) 
			{
				Sal.MessageBox("Member Status Type Code already exists, try again.", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfDESCR WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDESCR_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfDESCR_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDESCR_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfDESCR.bRequired = true;
			#endregion
		}
		#endregion
	}
}
