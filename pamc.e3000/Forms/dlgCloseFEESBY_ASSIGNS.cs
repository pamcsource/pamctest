// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// Dialog box to delete  "fee_schedsets" from the database.
	/// </summary>
	public partial class dlgCloseFEESBY_ASSIGNS : dlgGeneralBox2
	{
		#region Window Variables
		public SalString strType = "";
		public SalString strWhere = "";
		public SalBoolean bOk = false;
		public SalNumber nCount = 0;
		public SalNumber nInd = 0;
		public SalString strErrMsg = "";
		public SalString strGlobalRecordCount = "";
		public SalString strAutoCommit = "";
		public SalNumber nAutoCommit = 0;
		public SalNumber nNewRow = 0;
		public SalNumber nGlobalRecordCount = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgCloseFEESBY_ASSIGNS()
		{
			// Assign global reference.
			App.dlgCloseFEESBY_ASSIGNS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			dlgCloseFEESBY_ASSIGNS dlg = new dlgCloseFEESBY_ASSIGNS();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgCloseFEESBY_ASSIGNS FromHandle(SalWindowHandle handle)
		{
			return ((dlgCloseFEESBY_ASSIGNS)SalWindow.FromHandle(handle, typeof(dlgCloseFEESBY_ASSIGNS)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber ClearFeeSetList()
		{
			#region Local Variables
			SqlLocals.ClearFeeSetListLocals locals = new SqlLocals.ClearFeeSetListLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler823 = delegate(SalSqlHandle hSql)
				{
					pamc.price.Var.hSql.GetError(ref locals.nErr, ref locals.strErr);
					dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error writing FEE_SETS records to the database", "A SQL Database error occurred while inserting records in the FEE_SETS table.");
					return false;
				};
				#endregion

				//PPJ:FINAL:BB: adjust syntax of procedure call
				//if (!(pamc.sqllib.Ext.OdrPrepareProc(pamc.price.Var.hSql, "{ Call ClearFeesetList}", ""))) 
				if (!(pamc.sqllib.Ext.OdrPrepareProc(pamc.price.Var.hSql, "ClearFeesetList", ""))) 
				{
					return false;
				}
				if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler823))) 
				{
					return false;
				}
				// If NOT SqlFetchNext( hSql, nInd )
				// Return FALSE

				return 0;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber UpdateFeeset()
		{
			#region Local Variables
			SqlLocals.UpdateFeesetLocals locals = new SqlLocals.UpdateFeesetLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				locals.nRow = Sys.TBL_MinRow;
				while (tblFEESET.FindNextRow(ref locals.nRow, 0, 0)) 
				{
					if (locals.nRow >= 0) 
					{
						locals.nSeq = locals.nRow + 1;
						tblFEESET.SetContextRow(locals.nRow);
						locals.nFEESET = tblFEESET.colFEESET.Number;
						//PPJ:FINAL:BB: adjust syntax of procedure call
						//if (!(pamc.sqllib.Ext.OdrPrepareProc(pamc.price.Var.hSql, "{ Call CreateFeesetList(:nFEESET, :nOpID )}", ""))) 
						if (!(pamc.sqllib.Ext.OdrPrepareProc(pamc.price.Var.hSql, "CreateFeesetList :nFEESET, :nOpID ", ""))) 
						{
							return false;
						}
						
						#region WhenSqlError
						WhenSqlErrorHandler sqlErrorHandler824 = delegate(SalSqlHandle hSql)
						{
							pamc.price.Var.hSql.GetError(ref locals.nErr, ref locals.strErr);
							dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error writing FEE_SETS records to the database", "A SQL Database error occurred while inserting records in the FEE_SETS table.");
							return false;
						};
						#endregion

						if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler824))) 
						{
							return false;
						}
					}
				}

				return 0;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber CloseFeeSet()
		{
			#region Local Variables
			SqlLocals.CloseFeeSetLocals locals = new SqlLocals.CloseFeeSetLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				if (!(locals.bConnectClose)) 
				{
					locals.bConnectClose = locals.hSqlClose.Connect();
				}
				if (dfHPCODE.Text == pamc.sqllib.Const.strNULL) 
				{
					dfHPCODE.Text = "XXX";
				}
				Sal.DateToStr(dfCLOSEDATE.DateTime, ref locals.strCLOSEDATE);
				locals.strCLOSEDATE = locals.strCLOSEDATE.Left(10);
				Sal.DateToStr(dfFROMDATE1.DateTime, ref locals.strFROMDATE1);
				locals.strFROMDATE1 = locals.strFROMDATE1.Left(10);
				Sal.DateToStr(dfFROMDATE2.DateTime, ref locals.strFROMDATE2);
				locals.strFROMDATE2 = locals.strFROMDATE2.Left(10);
				locals.hSqlClose.SetParameter(1, 0, pamc.sqllib.Const.strNULL);
				//PPJ:FINAL:BB: adjust syntax of procedure call
				//if (!(pamc.sqllib.Ext.OdrPrepareProc(locals.hSqlClose, "{ Call CloseFeeSet(:dfCLOSEDATE,:dfPHCODE,:dfHPCODE,:dfFROMDATE1,:dfFROMDATE2,:nOpID )}", ":nRecordCount"))) 
				if (!(pamc.sqllib.Ext.OdrPrepareProc(locals.hSqlClose, "CloseFeeSet :dfCLOSEDATE,:dfPHCODE,:dfHPCODE,:dfFROMDATE1,:dfFROMDATE2,:nOpID ", ":nRecordCount"))) 
				{
					return false;
				}
				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler825 = delegate(SalSqlHandle hSql)
				{
					locals.hSqlClose.GetError(ref locals.nErr, ref locals.strErr);
					dlgError.ModalDialog(this, locals.hSqlClose, "Error writing FEE_SETS records to the database", "A SQL Database error occurred while inserting records in the FEE_SETS table.");
					return false;
				};
				#endregion

				if (!(locals.hSqlClose.Execute(sqlErrorHandler825))) 
				{
					return false;
				}
				if (!(locals.hSqlClose.FetchNext(ref locals.nInd, sqlErrorHandler825))) 
				{
					return false;
				}
				// Call SqlPrepareAndExecute( hSql, 'ROLLBACK' )
				nGlobalRecordCount = locals.nRecordCount;
				locals.strRecordCount = locals.nRecordCount.ToString(0);
				if (Sys.IDNO == Sal.MessageBox("Close " + locals.strRecordCount + " Feesby Assign Records? ", "Close Feesby Assign", Sys.MB_YesNo)) 
				{
					locals.hSqlClose.PrepareAndExecute("ROLLBACK");
					nGlobalRecordCount = 0;
					locals.nRecordCount = 0;
				}
				else
				{
					locals.hSqlClose.Commit();
					nGlobalRecordCount = locals.nRecordCount;
				}
				// Call SqlPrepareAndExecute( hSql, 'ROLLBACK' )
				// If SqlCommit( hSql )
				if (locals.bConnectClose) 
				{
					locals.hSqlClose.Disconnect();
					locals.bConnectClose = false;
				}
				// If SqlCommit( hSql )

				return 0;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <returns></returns>
		public SalNumber AddRow(SalWindowHandle hWndTbl)
		{
			#region Local Variables
			SalNumber nRow = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				nRow = Sys.TBL_MinRow;
				// If no row selected, append row to end
				if (hWndTbl.FindNextRow(ref nRow, Sys.ROW_Selected, 0)) 
				{
					nRow = nRow + 1;
				}
				else
				{
					nRow = Sys.TBL_MaxRow;
				}
				nRow = hWndTbl.InsertRow(nRow);
				SetCurrentTblRow(hWndTbl, nRow);
				// Set nRowCount = nRowCount + 1
				return nRow;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <returns></returns>
		public SalNumber DeleteCurrentRow(SalWindowHandle hWndTbl)
		{
			#region Local Variables
			SalNumber nRow = 0;
			SalNumber nNextRow = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				nRow = hWndTbl.GetContextRow();
				nNextRow = nRow;
				hWndTbl.FindPrevRow(ref nNextRow, 0, 0);
				hWndTbl.DeleteRow(nRow, Sys.TBL_NoAdjust);
				if (hWndTbl.AnyRows(0, 0)) 
				{
					if (nNextRow == Sys.TBL_MinSplitRow) 
					{
						if (!(hWndTbl.FindNextRow(ref nNextRow, 0, 0))) 
						{
							hWndTbl.KillFocusRow();
						}
						else
						{
							SetCurrentTblRow(hWndTbl, nNextRow);
						}
					}
					else
					{
						SetCurrentTblRow(hWndTbl, nNextRow);
					}
				}
				else
				{
					hWndTbl.KillFocusRow();
				}
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// This function sets the specified row to be the context and row selected.
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <param name="nRow"></param>
		/// <returns></returns>
		public SalNumber SetCurrentTblRow(SalWindowHandle hWndTbl, SalNumber nRow)
		{
			#region Actions
			using (new SalContext(this))
			{
				hWndTbl.ClearSelection();
				hWndTbl.KillFocusRow();
				hWndTbl.ScrollRow(nRow, Sys.hWndNULL, Sys.TBL_AutoScroll);
				// Call SalTblSetRowFlags( hWndTbl, nRow, ROW_Selected, TRUE )
				hWndTbl.SetContextRow(nRow);
				// Call SalTblSetFocusRow( hWndTbl, nRow )
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalBoolean Validate()
		{
			#region Actions
			using (new SalContext(this))
			{
				if (Sal.IsNull(dfCLOSEDATE) || Sal.IsNull(dfHPCODE) || Sal.IsNull(dfPHCODE) || Sal.IsNull(dfFROMDATE1) || Sal.IsNull(dfFROMDATE2)) 
				{
					Sal.MessageBox("Please enter required fields! ", "Close Feesby Assign", Sys.MB_Ok);
					return false;
				}
				return true;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgCloseFEESBY_ASSIGNS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgCloseFEESBY_ASSIGNS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgCloseFEESBY_ASSIGNS_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgCloseFEESBY_ASSIGNS_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SalStatusSetText( hWndForm, 'Enter a set # or a range of sets to delete')
			// If bPH
			// Set dfTypeLabel = 'Service Type:'
			// Set dfTypeDesc= '(P)rof, (H)osp,(M)edicine,A,B,C,D and (X) for all'
			// Call SalShowWindow( dfTypeLabel )
			// Call SalShowWindow( dfTypeDesc )
			// Call SalShowWindow( dfType )
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.Validate()) 
			{
				this.ClearFeeSetList();
				this.UpdateFeeset();
				this.CloseFeeSet();
				this.strGlobalRecordCount = this.nGlobalRecordCount.ToString(0);
				Sal.MessageBox("Feesby Assign Records Closed: " + this.strGlobalRecordCount + ". ", "Close Feesby Assign", Sys.MB_Ok);
			}
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		
		/// <summary>
		/// pbADD WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbADD_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbADD_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbADD_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfFEESET.IsEmpty()) 
			{
				Sal.MessageBox("Fee Set is blank. Please enter a value", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = false;
				return;
			}
			this.nNewRow = this.AddRow(this.tblFEESET);
			this.tblFEESET.colFEESET.Number = this.dfFEESET.Number;
			this.dfFEESET.Number = Sys.NUMBER_Null;
			this.tblFEESET.SetRowFlags(this.nNewRow, (Sys.ROW_New | Sys.ROW_Edited), false);
			#endregion
		}
		
		/// <summary>
		/// pbDELETE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbDELETE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbDELETE_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbDELETE_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.DeleteCurrentRow(this.tblFEESET);
			#endregion
		}
		#endregion
		
		#region tblFEESET
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public partial class tblFEESETTableWindow : SalTableWindow
		{
			// reference to the container form.
			private dlgCloseFEESBY_ASSIGNS _dlgCloseFEESBY_ASSIGNS = null;
			
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tblFEESETTableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private dlgCloseFEESBY_ASSIGNS dlgCloseFEESBY_ASSIGNS
			{
				[DebuggerStepThrough]
				get
				{
					if (_dlgCloseFEESBY_ASSIGNS == null) 
					{
						_dlgCloseFEESBY_ASSIGNS = (dlgCloseFEESBY_ASSIGNS)this.FindForm();
					}
					return _dlgCloseFEESBY_ASSIGNS;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public static tblFEESETTableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tblFEESETTableWindow)SalWindow.FromHandle(handle, typeof(tblFEESETTableWindow)));
			}
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class ClearFeeSetListLocals
			{
				public SalString strCLOSEDATE = "";
				public SalString strFROMDATE1 = "";
				public SalString strFROMDATE2 = "";
				public SalNumber nRecordCount = 0;
				public SalNumber nErr = 0;
				public SalNumber nInd = 0;
				public SalString strErr = "";
				public SalString strRecordCount = "";
				public SalBoolean bConnectClose = false;
				public SalSqlHandle hSqlClose = SalSqlHandle.Null;
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class UpdateFeesetLocals
			{
				public SalNumber nFEESET = 0;
				public SalNumber nRow = 0;
				public SalNumber nSeq = 0;
				public SalNumber nErr = 0;
				public SalString strErr = "";
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class CloseFeeSetLocals
			{
				public SalString strCLOSEDATE = "";
				public SalString strFROMDATE1 = "";
				public SalString strFROMDATE2 = "";
				public SalNumber nRecordCount = 0;
				public SalNumber nErr = 0;
				public SalNumber nInd = 0;
				public SalString strErr = "";
				public SalString strRecordCount = "";
				public SalBoolean bConnectClose = false;
				public SalSqlHandle hSqlClose = SalSqlHandle.Null;
			}
		}
		#endregion
	}
}
