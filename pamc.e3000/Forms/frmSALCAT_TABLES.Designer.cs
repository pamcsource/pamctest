// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmSALCAT_TABLES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditNKey dfSALCATSET;
		public clsDataFieldDisabled dfSALCATSETDESC;
		// clsDataFieldEditNKey: dfSALCAT
		// Message Actions
		// On SAM_Validate
		// On SAM_Validate verhoed dat  die class SAM_Validate geroep word en range valideer
		// On SAM_Validate verhoed dat  die class SAM_Validate geroep word en range valideer
		public clsDataFieldEditKey dfSALCAT;
		protected clsText label1;
		protected clsText label2;
		protected clsText label3;
		protected clsText label4;
		public clsDataFieldEditDKey dfFROMDATE;
		// clsDataFieldEditD: dfTODATE
		// Message Actions
		// On AM_Initialize
		// Set MyValue = DATETIME_Null
		// clsDataFieldEditDKey: dfTODATE
		// Message Actions
		// On AM_Initialize
		// Set MyValue = DATETIME_Null
		// On AM_DisEnable
		// EDIT
		// If wParam = 0  AND lParam = 2
		// If dfCURRHIST = 'H'
		// Call SalDisableWindow( dfTODATE )
		// Else
		// Call SalEnableWindow( dfTODATE )
		// NEW
		// If wParam = 0  AND lParam = 0
		// Call SalEnableWindow( dfTODATE )
		// MOVE
		// If wParam = 1  AND lParam = 0
		// Call SalDisableWindow( dfTODATE )
		// On SAM_Create
		// Set bRequired = FALSE
		// On SAM_Validate
		// If SalSendClassMessage(SAM_Validate,wParam, lParam) = VALIDATE_Cancel
		// Return VALIDATE_Cancel
		// Else
		// If NOT SalIsNull(hWndItem)
		// Call SalSendMsg(dfPCPTHRUDT, AM_Initialize,0,0)
		// Return VALIDATE_Ok
		// Else
		// If ValidateToDate(dfFROMDATE, dfTODATE) = VALIDATE_Cancel
		// Call SalClearField(hWndItem)
		// Return VALIDATE_Cancel
		// Else
		// Call SalSendMsg(dfTODATE, AM_Initialize,0,0)
		// Return VALIDATE_Ok
		// Set MyValue = DATETIME_Null
		public clsDataFieldEditD dfTODATE;
		public clsDataFieldDisabled dfCURRHIST;
		public clsDataFieldEdit dfDESCR;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfSALCATSET = new pamc.common.clsDataFieldEditNKey();
			this.dfSALCATSETDESC = new pamc.common.clsDataFieldDisabled();
			this.dfSALCAT = new pamc.common.clsDataFieldEditKey();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.label4 = new pamc.common.clsText();
			this.dfFROMDATE = new pamc.common.clsDataFieldEditDKey();
			this.dfTODATE = new pamc.common.clsDataFieldEditD();
			this.dfCURRHIST = new pamc.common.clsDataFieldDisabled();
			this.dfDESCR = new pamc.common.clsDataFieldEdit();
			this.SuspendLayout();
			// 
			// dfSALCATSET
			// 
			this.dfSALCATSET.Name = "dfSALCATSET";
			this.dfSALCATSET.MaxLength = 6;
			this.dfSALCATSET.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSALCATSET.Location = new System.Drawing.Point(139, 6);
			this.dfSALCATSET.Size = new System.Drawing.Size(68, 20);
			this.dfSALCATSET.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSALCATSET_WindowActions);
			this.dfSALCATSET.TabIndex = 0;
			// 
			// dfSALCATSETDESC
			// 
			this.dfSALCATSETDESC.Name = "dfSALCATSETDESC";
			this.dfSALCATSETDESC.MaxLength = 30;
			this.dfSALCATSETDESC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSALCATSETDESC.Location = new System.Drawing.Point(211, 8);
			this.dfSALCATSETDESC.Size = new System.Drawing.Size(194, 16);
			this.dfSALCATSETDESC.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSALCATSETDESC_WindowActions);
			this.dfSALCATSETDESC.TabIndex = 1;
			// 
			// dfSALCAT
			// 
			this.dfSALCAT.Name = "dfSALCAT";
			this.dfSALCAT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSALCAT.Location = new System.Drawing.Point(139, 31);
			this.dfSALCAT.Size = new System.Drawing.Size(68, 20);
			this.dfSALCAT.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSALCAT_WindowActions);
			this.dfSALCAT.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Salary Category Set:";
			this.label1.Location = new System.Drawing.Point(7, 9);
			this.label1.Size = new System.Drawing.Size(120, 16);
			this.label1.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Salary Category:";
			this.label2.Location = new System.Drawing.Point(7, 33);
			this.label2.Size = new System.Drawing.Size(120, 16);
			this.label2.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "From Date:";
			this.label3.Location = new System.Drawing.Point(7, 65);
			this.label3.Size = new System.Drawing.Size(112, 16);
			this.label3.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "To Date:";
			this.label4.Location = new System.Drawing.Point(215, 65);
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 6;
			// 
			// dfFROMDATE
			// 
			this.dfFROMDATE.Name = "dfFROMDATE";
			this.dfFROMDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFROMDATE.Location = new System.Drawing.Point(139, 63);
			this.dfFROMDATE.Size = new System.Drawing.Size(68, 20);
			this.dfFROMDATE.TabIndex = 7;
			// 
			// dfTODATE
			// 
			this.dfTODATE.Name = "dfTODATE";
			this.dfTODATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTODATE.Location = new System.Drawing.Point(279, 63);
			this.dfTODATE.Size = new System.Drawing.Size(68, 20);
			this.dfTODATE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfTODATE_WindowActions);
			this.dfTODATE.TabIndex = 8;
			// 
			// dfCURRHIST
			// 
			this.dfCURRHIST.Name = "dfCURRHIST";
			this.dfCURRHIST.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCURRHIST.Location = new System.Drawing.Point(367, 63);
			this.dfCURRHIST.Size = new System.Drawing.Size(48, 19);
			this.dfCURRHIST.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCURRHIST_WindowActions);
			this.dfCURRHIST.TabIndex = 9;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 100;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(223, 31);
			this.dfDESCR.Size = new System.Drawing.Size(360, 20);
			this.dfDESCR.TabIndex = 10;
			// 
			// frmSALCAT_TABLES
			// 
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfCURRHIST);
			this.Controls.Add(this.dfTODATE);
			this.Controls.Add(this.dfFROMDATE);
			this.Controls.Add(this.dfSALCAT);
			this.Controls.Add(this.dfSALCATSETDESC);
			this.Controls.Add(this.dfSALCATSET);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmSALCAT_TABLES";
			this.ClientSize = new System.Drawing.Size(618, 111);
			this.Text = "Salary Category Table";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmSALCAT_TABLES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmSALCAT_TABLES == this) 
			{
				App.frmSALCAT_TABLES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
