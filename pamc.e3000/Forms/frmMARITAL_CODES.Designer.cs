// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmMARITAL_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldDisabled dfCODE;
		public clsDataFieldDisabled dfDESCR;
		public clsDataFieldDisabled dfSELECTABLE;
		public clsDataFieldDisabled dfEDITABLE;
		protected clsText label1;
		protected clsText label2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfCODE = new pamc.common.clsDataFieldDisabled();
			this.dfDESCR = new pamc.common.clsDataFieldDisabled();
			this.dfSELECTABLE = new pamc.common.clsDataFieldDisabled();
			this.dfEDITABLE = new pamc.common.clsDataFieldDisabled();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfCODE
			// 
			this.dfCODE.Name = "dfCODE";
			this.dfCODE.MaxLength = 1;
			this.dfCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCODE.Location = new System.Drawing.Point(99, 12);
			this.dfCODE.Size = new System.Drawing.Size(28, 19);
			this.dfCODE.TabIndex = 0;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 30;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(99, 43);
			this.dfDESCR.Size = new System.Drawing.Size(276, 19);
			this.dfDESCR.TabIndex = 1;
			// 
			// dfSELECTABLE
			// 
			this.dfSELECTABLE.Name = "dfSELECTABLE";
			this.dfSELECTABLE.Visible = false;
			this.dfSELECTABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSELECTABLE.Format = "";
			this.dfSELECTABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfSELECTABLE.Location = new System.Drawing.Point(0, 0);
			this.dfSELECTABLE.Size = new System.Drawing.Size(66, 19);
			this.dfSELECTABLE.TabIndex = 2;
			// 
			// dfEDITABLE
			// 
			this.dfEDITABLE.Name = "dfEDITABLE";
			this.dfEDITABLE.Visible = false;
			this.dfEDITABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfEDITABLE.Format = "";
			this.dfEDITABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfEDITABLE.Location = new System.Drawing.Point(0, 0);
			this.dfEDITABLE.Size = new System.Drawing.Size(66, 19);
			this.dfEDITABLE.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(5, 12);
			this.label1.Size = new System.Drawing.Size(82, 16);
			this.label1.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(5, 41);
			this.label2.Size = new System.Drawing.Size(82, 16);
			this.label2.TabIndex = 5;
			// 
			// frmMARITAL_CODES
			// 
			this.Controls.Add(this.dfEDITABLE);
			this.Controls.Add(this.dfSELECTABLE);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfCODE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmMARITAL_CODES";
			this.ClientSize = new System.Drawing.Size(440, 66);
			this.Text = "Member Marital Status Code";
			this.Location = new System.Drawing.Point(0, 1);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmMARITAL_CODES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmMARITAL_CODES == this) 
			{
				App.frmMARITAL_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
