// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgSearchCLAIM_MASTERSw
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldSearchLookup dfSUBSSN;
		public clsDataFieldSearchLookup dfPROVID;
		public clsDataFieldSearchLookup dfMEMBID;
		public clsDataFieldSearchLookup dfSPEC;
		public clsDataFieldSearchLookup dfSTATUS;
		public clsDataFieldEditN dfYear;
		public clsDataFieldEditN dfMonth;
		public clsDataFieldSearchLookup dfAUTHNO;
		public clsDataFieldSearch2 dfCLAIMNO;
		protected clsText label1;
		protected clsText label2;
		protected clsText label3;
		protected clsText label4;
		protected clsText label5;
		protected clsText label6;
		protected clsTextRt label7;
		protected FCSalBackgroundText label8;
		protected FCSalBackgroundText label9;
		protected FCSalBackgroundText label10;
		public clsDataFieldDisabled dfProviderName;
		protected FCSalBackgroundText label11;
		protected FCSalBackgroundText label12;
		protected clsText label13;
		protected clsTextRt label14;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfSUBSSN = new pamc.common.clsDataFieldSearchLookup();
			this.dfPROVID = new pamc.common.clsDataFieldSearchLookup();
			this.dfMEMBID = new pamc.common.clsDataFieldSearchLookup();
			this.dfSPEC = new pamc.common.clsDataFieldSearchLookup();
			this.dfSTATUS = new pamc.common.clsDataFieldSearchLookup();
			this.dfYear = new pamc.common.clsDataFieldEditN();
			this.dfMonth = new pamc.common.clsDataFieldEditN();
			this.dfAUTHNO = new pamc.common.clsDataFieldSearchLookup();
			this.dfCLAIMNO = new pamc.common.clsDataFieldSearch2();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.label4 = new pamc.common.clsText();
			this.label5 = new pamc.common.clsText();
			this.label6 = new pamc.common.clsText();
			this.label7 = new pamc.common.clsTextRt();
			this.label8 = new FCSalBackgroundText();
			this.label9 = new FCSalBackgroundText();
			this.label10 = new FCSalBackgroundText();
			this.dfProviderName = new pamc.common.clsDataFieldDisabled();
			this.label11 = new FCSalBackgroundText();
			this.label12 = new FCSalBackgroundText();
			this.label13 = new pamc.common.clsText();
			this.label14 = new pamc.common.clsTextRt();
			this.SuspendLayout();
			// 
			// dfSUBSSN
			// 
			this.dfSUBSSN.Name = "dfSUBSSN";
			this.dfSUBSSN.Visible = false;
			this.dfSUBSSN.MaxLength = 20;
			this.dfSUBSSN.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSUBSSN.Location = new System.Drawing.Point(150, 41);
			this.dfSUBSSN.Size = new System.Drawing.Size(173, 24);
			this.dfSUBSSN.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSUBSSN_WindowActions);
			this.dfSUBSSN.TabIndex = 0;
			// 
			// dfPROVID
			// 
			this.dfPROVID.Name = "dfPROVID";
			this.dfPROVID.Visible = false;
			this.dfPROVID.MaxLength = 20;
			this.dfPROVID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPROVID.Location = new System.Drawing.Point(150, 69);
			this.dfPROVID.Size = new System.Drawing.Size(173, 24);
			this.dfPROVID.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPROVID_WindowActions);
			this.dfPROVID.TabIndex = 1;
			// 
			// dfMEMBID
			// 
			this.dfMEMBID.Name = "dfMEMBID";
			this.dfMEMBID.Visible = false;
			this.dfMEMBID.MaxLength = 20;
			this.dfMEMBID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMEMBID.Location = new System.Drawing.Point(150, 117);
			this.dfMEMBID.Size = new System.Drawing.Size(177, 24);
			this.dfMEMBID.TabIndex = 2;
			// 
			// dfSPEC
			// 
			this.dfSPEC.Name = "dfSPEC";
			this.dfSPEC.Visible = false;
			this.dfSPEC.MaxLength = 48;
			this.dfSPEC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSPEC.Location = new System.Drawing.Point(150, 147);
			this.dfSPEC.Size = new System.Drawing.Size(57, 24);
			this.dfSPEC.TabIndex = 3;
			// 
			// dfSTATUS
			// 
			this.dfSTATUS.Name = "dfSTATUS";
			this.dfSTATUS.Visible = false;
			this.dfSTATUS.MaxLength = 1;
			this.dfSTATUS.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTATUS.Location = new System.Drawing.Point(150, 177);
			this.dfSTATUS.Size = new System.Drawing.Size(24, 24);
			this.dfSTATUS.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSTATUS_WindowActions);
			this.dfSTATUS.TabIndex = 4;
			// 
			// dfYear
			// 
			this.dfYear.Name = "dfYear";
			this.dfYear.Visible = false;
			this.dfYear.MaxLength = 4;
			this.dfYear.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfYear.Format = "#0";
			this.dfYear.Location = new System.Drawing.Point(334, 179);
			this.dfYear.Size = new System.Drawing.Size(36, 20);
			this.dfYear.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfYear_WindowActions);
			this.dfYear.TabIndex = 5;
			// 
			// dfMonth
			// 
			this.dfMonth.Name = "dfMonth";
			this.dfMonth.Visible = false;
			this.dfMonth.MaxLength = 2;
			this.dfMonth.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMonth.Format = "#0";
			this.dfMonth.Location = new System.Drawing.Point(387, 179);
			this.dfMonth.Size = new System.Drawing.Size(27, 20);
			this.dfMonth.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfMonth_WindowActions);
			this.dfMonth.TabIndex = 6;
			// 
			// dfAUTHNO
			// 
			this.dfAUTHNO.Name = "dfAUTHNO";
			this.dfAUTHNO.Visible = false;
			this.dfAUTHNO.MaxLength = 16;
			this.dfAUTHNO.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfAUTHNO.Location = new System.Drawing.Point(150, 203);
			this.dfAUTHNO.Size = new System.Drawing.Size(144, 24);
			this.dfAUTHNO.TabIndex = 7;
			// 
			// dfCLAIMNO
			// 
			this.dfCLAIMNO.Name = "dfCLAIMNO";
			this.dfCLAIMNO.MaxLength = 16;
			this.dfCLAIMNO.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCLAIMNO.Location = new System.Drawing.Point(110, 63);
			this.dfCLAIMNO.Size = new System.Drawing.Size(144, 24);
			this.dfCLAIMNO.TabIndex = 8;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 9;
			// 
			// pbCount
			// 
			this.pbCount.Name = "pbCount";
			this.pbCount.Visible = false;
			this.pbCount.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCount_WindowActions);
			this.pbCount.TabIndex = 10;
			// 
			// pbOrderBy
			// 
			this.pbOrderBy.Name = "pbOrderBy";
			this.pbOrderBy.Visible = false;
			this.pbOrderBy.TabIndex = 11;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Location = new System.Drawing.Point(403, 2);
			this.pbClose.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Visible = false;
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Auth No.";
			this.label1.Location = new System.Drawing.Point(34, 207);
			this.label1.Size = new System.Drawing.Size(89, 16);
			this.label1.TabIndex = 13;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Visible = false;
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Claim Status:";
			this.label2.Location = new System.Drawing.Point(34, 181);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 14;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Visible = false;
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Member ID:";
			this.label3.Location = new System.Drawing.Point(34, 123);
			this.label3.Size = new System.Drawing.Size(89, 16);
			this.label3.TabIndex = 15;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Visible = false;
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Speciality Code:";
			this.label4.Location = new System.Drawing.Point(33, 151);
			this.label4.Size = new System.Drawing.Size(110, 16);
			this.label4.TabIndex = 16;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "Claim No:";
			this.label5.Location = new System.Drawing.Point(42, 70);
			this.label5.Size = new System.Drawing.Size(61, 16);
			this.label5.TabIndex = 17;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.Visible = false;
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label6.Text = "Provider ID:";
			this.label6.Location = new System.Drawing.Point(33, 73);
			this.label6.Size = new System.Drawing.Size(89, 16);
			this.label6.TabIndex = 18;
			// 
			// label7
			// 
			this.label7.Name = "label7";
			this.label7.Visible = false;
			this.label7.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label7.Text = "Date Paid Yr/Month:";
			this.label7.Location = new System.Drawing.Point(184, 181);
			this.label7.Size = new System.Drawing.Size(138, 16);
			this.label7.TabIndex = 19;
			// 
			// label8
			// 
			this.label8.Name = "label8";
			this.label8.Visible = false;
			this.label8.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label8.Text = "<F2>";
			this.label8.Location = new System.Drawing.Point(6, 181);
			this.label8.Size = new System.Drawing.Size(25, 16);
			this.label8.TabIndex = 20;
			// 
			// label9
			// 
			this.label9.Name = "label9";
			this.label9.Visible = false;
			this.label9.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label9.Text = "<F2>";
			this.label9.Location = new System.Drawing.Point(6, 207);
			this.label9.Size = new System.Drawing.Size(25, 16);
			this.label9.TabIndex = 21;
			// 
			// label10
			// 
			this.label10.Name = "label10";
			this.label10.Visible = false;
			this.label10.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label10.Text = "<F2>";
			this.label10.Location = new System.Drawing.Point(6, 123);
			this.label10.Size = new System.Drawing.Size(25, 16);
			this.label10.TabIndex = 22;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Size = new System.Drawing.Size(489, 39);
			this.frame1.TabIndex = 23;
			// 
			// dfCOUNT
			// 
			this.dfCOUNT.Name = "dfCOUNT";
			this.dfCOUNT.Visible = false;
			this.dfCOUNT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfCOUNT.Location = new System.Drawing.Point(301, 13);
			this.dfCOUNT.TabIndex = 24;
			// 
			// dfProviderName
			// 
			this.dfProviderName.Name = "dfProviderName";
			this.dfProviderName.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfProviderName.Location = new System.Drawing.Point(150, 95);
			this.dfProviderName.Size = new System.Drawing.Size(321, 16);
			this.dfProviderName.TabIndex = 25;
			// 
			// label11
			// 
			this.label11.Name = "label11";
			this.label11.Visible = false;
			this.label11.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label11.Text = "<F2>";
			this.label11.Location = new System.Drawing.Point(3, 45);
			this.label11.Size = new System.Drawing.Size(25, 16);
			this.label11.TabIndex = 26;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.Visible = false;
			this.frame2.Location = new System.Drawing.Point(299, 9);
			this.frame2.TabIndex = 27;
			// 
			// label12
			// 
			this.label12.Name = "label12";
			this.label12.Visible = false;
			this.label12.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label12.Text = "<F2>";
			this.label12.Location = new System.Drawing.Point(4, 73);
			this.label12.Size = new System.Drawing.Size(25, 16);
			this.label12.TabIndex = 28;
			// 
			// label13
			// 
			this.label13.Name = "label13";
			this.label13.Visible = false;
			this.label13.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label13.Text = "Main Member ID:";
			this.label13.Location = new System.Drawing.Point(33, 45);
			this.label13.Size = new System.Drawing.Size(110, 16);
			this.label13.TabIndex = 29;
			// 
			// label14
			// 
			this.label14.Name = "label14";
			this.label14.Visible = false;
			this.label14.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label14.Text = "/";
			this.label14.Location = new System.Drawing.Point(370, 180);
			this.label14.Size = new System.Drawing.Size(14, 16);
			this.label14.TabIndex = 30;
			// 
			// dlgSearchCLAIM_MASTERSw
			// 
			this.Controls.Add(this.dfProviderName);
			this.Controls.Add(this.dfCOUNT);
			this.Controls.Add(this.pbClose);
			this.Controls.Add(this.pbOrderBy);
			this.Controls.Add(this.pbCount);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.dfCLAIMNO);
			this.Controls.Add(this.dfAUTHNO);
			this.Controls.Add(this.dfMonth);
			this.Controls.Add(this.dfYear);
			this.Controls.Add(this.dfSTATUS);
			this.Controls.Add(this.dfSPEC);
			this.Controls.Add(this.dfMEMBID);
			this.Controls.Add(this.dfPROVID);
			this.Controls.Add(this.dfSUBSSN);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame2);
			this.Controls.Add(this.frame1);
			this.Name = "dlgSearchCLAIM_MASTERSw";
			this.ClientSize = new System.Drawing.Size(471, 115);
			this.Text = "Reverse Claim";
			this.Location = new System.Drawing.Point(91, 136);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgSearchCLAIM_MASTERSw_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgSearchCLAIM_MASTERSw == this) 
			{
				App.dlgSearchCLAIM_MASTERSw = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
