// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmSALCAT_SETS
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditNKey dfSALCATSET;
		protected clsText label1;
		public clsDataFieldEdit dfDESCR;
		protected clsText label2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfSALCATSET = new pamc.common.clsDataFieldEditNKey();
			this.label1 = new pamc.common.clsText();
			this.dfDESCR = new pamc.common.clsDataFieldEdit();
			this.label2 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfSALCATSET
			// 
			this.dfSALCATSET.Name = "dfSALCATSET";
			this.dfSALCATSET.MaxLength = 2;
			this.dfSALCATSET.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSALCATSET.Location = new System.Drawing.Point(151, 23);
			this.dfSALCATSET.Size = new System.Drawing.Size(64, 20);
			this.dfSALCATSET.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Salary Category Set:";
			this.label1.Location = new System.Drawing.Point(23, 25);
			this.label1.Size = new System.Drawing.Size(120, 16);
			this.label1.TabIndex = 1;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 50;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(151, 55);
			this.dfDESCR.Size = new System.Drawing.Size(208, 20);
			this.dfDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDESCR_WindowActions);
			this.dfDESCR.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(23, 57);
			this.label2.Size = new System.Drawing.Size(104, 16);
			this.label2.TabIndex = 3;
			// 
			// frmSALCAT_SETS
			// 
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfSALCATSET);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmSALCAT_SETS";
			this.ClientSize = new System.Drawing.Size(395, 100);
			this.Text = "Salary Category Sets";
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmSALCAT_SETS == this) 
			{
				App.frmSALCAT_SETS = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
