// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmUpdEFTRunNUmber
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditN dfBATCH;
		public clsDataFieldEdit dfRUNNUMBER;
		public SalPushbutton pbPost;
		protected SalFrame frame1;
		protected clsText label1;
		protected clsText label2;
		protected SalFrame frame2;
		protected SalFrame frame3;
		protected clsText label3;
		public clsDataFieldDisabled dfStatus;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfBATCH = new pamc.common.clsDataFieldEditN();
			this.dfRUNNUMBER = new pamc.common.clsDataFieldEdit();
			this.pbPost = new PPJ.Runtime.Windows.SalPushbutton();
			this.frame1 = new PPJ.Runtime.Windows.SalFrame();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.frame3 = new PPJ.Runtime.Windows.SalFrame();
			this.label3 = new pamc.common.clsText();
			this.dfStatus = new pamc.common.clsDataFieldDisabled();
			this.SuspendLayout();
			// 
			// dfBATCH
			// 
			this.dfBATCH.Name = "dfBATCH";
			this.dfBATCH.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfBATCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfBATCH.Location = new System.Drawing.Point(151, 47);
			this.dfBATCH.Size = new System.Drawing.Size(80, 20);
			this.dfBATCH.TabIndex = 0;
			// 
			// dfRUNNUMBER
			// 
			this.dfRUNNUMBER.Name = "dfRUNNUMBER";
			this.dfRUNNUMBER.MaxLength = 20;
			this.dfRUNNUMBER.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfRUNNUMBER.Location = new System.Drawing.Point(151, 71);
			this.dfRUNNUMBER.Size = new System.Drawing.Size(80, 20);
			this.dfRUNNUMBER.TabIndex = 1;
			// 
			// pbPost
			// 
			this.pbPost.Name = "pbPost";
			this.pbPost.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.pbPost.Location = new System.Drawing.Point(5, 4);
			this.pbPost.Size = new System.Drawing.Size(68, 32);
			this.pbPost.ImageName = "apply.bmp";
			this.pbPost.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbPost_WindowActions);
			this.pbPost.TabIndex = 2;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.BackColor = System.Drawing.SystemColors.Control;
			this.frame1.BorderStyle = PPJ.Runtime.Windows.BorderStyle.RaisedShadow;
			this.frame1.BorderSize = 1;
			this.frame1.Location = new System.Drawing.Point(-1, 31);
			this.frame1.Size = new System.Drawing.Size(400, 112);
			this.frame1.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "EFT Run Number:";
			this.label1.Location = new System.Drawing.Point(31, 73);
			this.label1.Size = new System.Drawing.Size(120, 16);
			this.label1.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "EFT Batch:";
			this.label2.Location = new System.Drawing.Point(31, 49);
			this.label2.Size = new System.Drawing.Size(112, 16);
			this.label2.TabIndex = 5;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.SystemColors.Control;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(15, 39);
			this.frame2.Size = new System.Drawing.Size(368, 88);
			this.frame2.TabIndex = 6;
			// 
			// frame3
			// 
			this.frame3.Name = "frame3";
			this.frame3.BackColor = System.Drawing.Color.Gray;
			this.frame3.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame3.BorderSize = 1;
			this.frame3.Location = new System.Drawing.Point(0, 0);
			this.frame3.Size = new System.Drawing.Size(399, 38);
			this.frame3.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Status:";
			this.label3.Location = new System.Drawing.Point(31, 97);
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 8;
			// 
			// dfStatus
			// 
			this.dfStatus.Name = "dfStatus";
			this.dfStatus.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfStatus.Location = new System.Drawing.Point(95, 95);
			this.dfStatus.Size = new System.Drawing.Size(152, 24);
			this.dfStatus.TabIndex = 9;
			// 
			// frmUpdEFTRunNUmber
			// 
			this.Controls.Add(this.dfStatus);
			this.Controls.Add(this.pbPost);
			this.Controls.Add(this.dfRUNNUMBER);
			this.Controls.Add(this.dfBATCH);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame3);
			this.Controls.Add(this.frame2);
			this.Controls.Add(this.frame1);
			this.Name = "frmUpdEFTRunNUmber";
			this.ClientSize = new System.Drawing.Size(410, 144);
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "EFT Run Number Update";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmUpdEFTRunNUmber_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmUpdEFTRunNUmber == this) 
			{
				App.frmUpdEFTRunNUmber = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
