// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmMembExtraNotes
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEdit dfMembid;
		public clsCombo cmbExtaType;
		public SalPushbutton pbOk;
		public SalPushbutton pbCancel;
		protected clsText label1;
		protected SalFrame frame1;
		public clsDataFieldEditN dfExtraType;
		protected clsText label2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfMembid = new pamc.common.clsDataFieldEdit();
			this.cmbExtaType = new pamc.common.clsCombo();
			this.pbOk = new PPJ.Runtime.Windows.SalPushbutton();
			this.pbCancel = new PPJ.Runtime.Windows.SalPushbutton();
			this.label1 = new pamc.common.clsText();
			this.frame1 = new PPJ.Runtime.Windows.SalFrame();
			this.dfExtraType = new pamc.common.clsDataFieldEditN();
			this.label2 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfMembid
			// 
			this.dfMembid.Name = "dfMembid";
			this.dfMembid.MaxLength = 20;
			this.dfMembid.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMembid.Location = new System.Drawing.Point(135, 47);
			this.dfMembid.Size = new System.Drawing.Size(128, 20);
			this.dfMembid.TabIndex = 0;
			// 
			// cmbExtaType
			// 
			this.cmbExtaType.Name = "cmbExtaType";
			this.cmbExtaType.MaxLength = 30;
			this.cmbExtaType.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cmbExtaType.Location = new System.Drawing.Point(135, 79);
			this.cmbExtaType.Size = new System.Drawing.Size(163, 24);
			this.cmbExtaType.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cmbExtaType_WindowActions);
			this.cmbExtaType.TabIndex = 1;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbOk.AcceleratorKey = Keys.Enter;
			this.pbOk.Location = new System.Drawing.Point(4, 5);
			this.pbOk.Size = new System.Drawing.Size(78, 30);
			this.pbOk.TransparentColor = System.Drawing.Color.Silver;
			this.pbOk.ImageName = "ok5.bmp";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 2;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbCancel.AcceleratorKey = Keys.Escape;
			this.pbCancel.Location = new System.Drawing.Point(338, 5);
			this.pbCancel.Size = new System.Drawing.Size(78, 30);
			this.pbCancel.TransparentColor = System.Drawing.Color.Silver;
			this.pbCancel.ImageName = "cancel5.bmp";
			this.pbCancel.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCancel_WindowActions);
			this.pbCancel.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Extra Notes Type:";
			this.label1.Location = new System.Drawing.Point(15, 81);
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 4;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.BackColor = System.Drawing.Color.Gray;
			this.frame1.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame1.BorderSize = 1;
			this.frame1.Location = new System.Drawing.Point(0, 0);
			this.frame1.Size = new System.Drawing.Size(423, 38);
			this.frame1.TabIndex = 5;
			// 
			// dfExtraType
			// 
			this.dfExtraType.Name = "dfExtraType";
			this.dfExtraType.Visible = false;
			this.dfExtraType.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfExtraType.Location = new System.Drawing.Point(311, 79);
			this.dfExtraType.Size = new System.Drawing.Size(66, 20);
			this.dfExtraType.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Member Number:";
			this.label2.Location = new System.Drawing.Point(15, 49);
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 7;
			// 
			// frmMembExtraNotes
			// 
			this.Controls.Add(this.dfExtraType);
			this.Controls.Add(this.pbCancel);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.cmbExtaType);
			this.Controls.Add(this.dfMembid);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame1);
			this.Name = "frmMembExtraNotes";
			this.ClientSize = new System.Drawing.Size(419, 118);
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "Member Extra Notes Search";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmMembExtraNotes_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmMembExtraNotes == this) 
			{
				App.frmMembExtraNotes = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
