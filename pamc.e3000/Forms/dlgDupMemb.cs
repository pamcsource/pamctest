// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// Subscriber payments info, called by frmMEMB_MASTERS and frmMEMB_SUBSCRIBERS
	/// </summary>
	/// <param name="strMembID1"></param>
	/// <param name="strMembID2"></param>
	/// <param name="strMembID3"></param>
	public partial class dlgDupMemb : dlgGeneralBox2
	{
		#region Window Parameters
		public SalString strMembID1;
		public SalString strMembID2;
		public SalString strMembID3;
		#endregion
		
		#region Window Variables
		public SalString strSelect = "";
		public SalString strCurerent = "";
		public SalBoolean bSinced = false;
		public SalBoolean bAdd = false;
		public SalNumber nID = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgDupMemb(SalString strMembID1, SalString strMembID2, SalString strMembID3)
		{
			// Assign global reference.
			App.dlgDupMemb = this;
			// Window Parameters initialization.
			this.strMembID1 = strMembID1;
			this.strMembID2 = strMembID2;
			this.strMembID3 = strMembID3;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalString strMembID1, SalString strMembID2, SalString strMembID3)
		{
			dlgDupMemb dlg = new dlgDupMemb(strMembID1, strMembID2, strMembID3);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgDupMemb FromHandle(SalWindowHandle handle)
		{
			return ((dlgDupMemb)SalWindow.FromHandle(handle, typeof(dlgDupMemb)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalBoolean GetMemb()
		{
			#region Local Variables
			SqlLocals.GetMembLocals locals = new SqlLocals.GetMembLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				locals.strSelect = "SELECT MEMBID,SUBSSN,RLSHIP,SEX,BIRTH,MEMBSSN,PATID,LASTNM,FIRSTNM, MI\r\n" +
				" FROM MEMB_MASTERS WHERE MEMBID = :strMembID1";
				if (!(pamc.price.Var.SqlInst.SqlPrep(pamc.price.Var.hSql, locals.strSelect, ":dfMEMBID, :dfSUBSSN, :dfRL_SHIP,:dfSEX,:dfBIRTH,:dfMEMBSSN,:dfPATID,:dfLASTNM,:dfFIRSTNM, :dfMI"))) 
				{
					return false;
				}
				if (!(pamc.price.Var.hSql.Execute())) 
				{
					return false;
				}
				locals.nInd = pamc.price.Var.hSql.FetchNext();

				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgDupMemb WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgDupMemb_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_CreateComplete:
					this.dlgDupMemb_OnSAM_CreateComplete(sender, e);
					break;
				
				case pamc.common.Const.CM_DoLookup:
					this.dlgDupMemb_OnCM_DoLookup(sender, e);
					break;
				
				case pamc.common.Const.AM_Initialize:
					this.dlgDupMemb_OnAM_Initialize(sender, e);
					break;
				
				case Sys.SAM_Close:
					this.dlgDupMemb_OnSAM_Close(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.CenterWindow();
			#endregion
		}
		
		/// <summary>
		/// SAM_CreateComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_OnSAM_CreateComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.GetMemb();
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_Initialize, 0, 0);
			this.SendMessageToChildren(pamc.common.Const.CM_DoLookup, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// SAM_Close event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgDupMemb_OnSAM_Close(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class GetMembLocals
			{
				public SalString strSelect = "";
				public SalNumber nInd = 0;
			}
		}
		#endregion
	}
}
