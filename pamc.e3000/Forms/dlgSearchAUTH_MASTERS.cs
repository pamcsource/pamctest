// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class dlgSearchAUTH_MASTERS : clsSearchDlgSpecial
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgSearchAUTH_MASTERS()
		{
			// Assign global reference.
			App.dlgSearchAUTH_MASTERS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			dlgSearchAUTH_MASTERS dlg = new dlgSearchAUTH_MASTERS();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgSearchAUTH_MASTERS FromHandle(SalWindowHandle handle)
		{
			return ((dlgSearchAUTH_MASTERS)SalWindow.FromHandle(handle, typeof(dlgSearchAUTH_MASTERS)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="nType"></param>
		/// <returns></returns>
		public SalBoolean ProcessLookup(SalWindowHandle hWnd, SalNumber nType)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strFieldName = "";
			SalString str1 = "";
			SalString str2 = "";
			SalString str3 = "";
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfSTATUS") 
				{
					strQuery = "Select Status=CODE,Description=DESCR from AUTH_STATUS Order by 1 ";
					if (dlgLookupList.ModalDialog(this, strQuery, ref strResult, "Status Codes", Sys.hWndForm.FindControl("dfSTATUS").Text, 0)) 
					{
						strResult.Tokenize("", pamc.common.Const.strTAB, strCols);
						dfSTATUS.Text = strCols[0];
					}
				}
				else if (strFieldName == "dfPSTATUS") 
				{
					strQuery = "Select Status=CODE,Description=DESCR from AUTH_PAYSTATUS Order by 1 ";
					if (dlgLookupList.ModalDialog(this, strQuery, ref strResult, "Status Codes", Sys.hWndForm.FindControl("dfPSTATUS").Text, 0)) 
					{
						strResult.Tokenize("", pamc.common.Const.strTAB, strCols);
						dfPSTATUS.Text = strCols[0];
					}
				}
				else if (strFieldName == "dfMEMBID") 
				{
					str2 = this.dfMEMBID.Text;
					if (pamc.common.Int.ProcessLookupMEMBID(pamc.common.Const.LOOKUP_Edit, ref str2, ref str1)) 
					{
						this.dfMEMBID.Text = str2;
						// Set dlgSearchCLAIM_MASTERS.dfMemberName = str1
					}
				}
				// Else If strFieldName = 'dfMEMBNAME'
				// If ProcessLookupMEMBID( LOOKUP_Edit, str2, str1 )
				// Set dlgSearchAUTH_MASTERS.dfMEMBNAME = str1
				else if (strFieldName == "dfREQPROV") 
				{
					str2 = this.dfREQPROV.Text;
					if (pamc.common.Int.ProcessLookupPROVID(pamc.common.Const.LOOKUP_Edit, ref str2, ref str1)) 
					{
						this.dfREQPROV.Text = str2;
						// Set dlgSearchCLAIM_MASTERS.dfProviderName = str1
					}
				}
				else if (strFieldName == "dfAUTHPCP") 
				{
					str2 = this.dfAUTHPCP.Text;
					if (pamc.common.Int.ProcessLookupPROVID(pamc.common.Const.LOOKUP_Edit, ref str2, ref str1)) 
					{
						this.dfAUTHPCP.Text = str2;
					}
				}
				// Else If strFieldName = 'dfCASENO'
				// Set str2 = dlgSearchAUTH_MASTERS.dfMEMBID
				// Set str3 = dlgSearchAUTH_MASTERS.dfCASENO
				// If ProcessLookupCASENO( LOOKUP_Edit, str2, str3 )
				// Set dlgSearchAUTH_MASTERS.dfCASENO = str3
				else if (strFieldName == "dfSUBSSN") 
				{
					str3 = this.dfSUBSSN.Text;
					if (pamc.common.Int.ProcessLookupSUBSCRIBER(pamc.common.Const.LOOKUP_Edit, ref str2, ref str3)) 
					{
						this.dfSUBSSN.Text = str3;
					}
				}
			}

			return false;
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgSearchAUTH_MASTERS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSearchAUTH_MASTERS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_DoLookup:
					this.dlgSearchAUTH_MASTERS_OnCM_DoLookup(sender, e);
					break;
				
				case pamc.common.Const.CM_SetInstanceVars:
					this.dlgSearchAUTH_MASTERS_OnCM_SetInstanceVars(sender, e);
					break;
				
				case pamc.common.Const.CM_BuildQueryDone:
					this.dlgSearchAUTH_MASTERS_OnCM_BuildQueryDone(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSearchAUTH_MASTERS_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.ProcessLookup(Sal.GetFocus(), pamc.common.Const.LOOKUP_Edit);
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSearchAUTH_MASTERS_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.strDefaultOrder = "AUTH_MASTERS.AUTHNO";
			#endregion
		}
		
		/// <summary>
		/// CM_BuildQueryDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgSearchAUTH_MASTERS_OnCM_BuildQueryDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.strWhere == pamc.sqllib.Const.strNULL) 
			{
				e.Return = pamc.common.Const.MSGRET_False;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// dfSUBSSN WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSUBSSN_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfSUBSSN_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSUBSSN_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfSUBSSN.Text != Sys.STRING_Null) 
			{
				this.dfSUBSSN.Text = "M" + this.dfSUBSSN.Text;
			}
			#endregion
		}
		
		/// <summary>
		/// dfSTATUS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTATUS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.dfSTATUS_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTATUS_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfSTATUS.bSpecial = true;
			#endregion
		}
		
		/// <summary>
		/// dfPSTATUS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfPSTATUS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.dfPSTATUS_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfPSTATUS_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfPSTATUS.bSpecial = true;
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.dfAUTHNO.IsEmpty()) && ((SalString)this.dfAUTHNO.Text).Length < 6) 
			{
				Sal.MessageBox("You must enter at least the first 6 digits of the Authorization Number", "Search", (Sys.MB_Ok | Sys.MB_IconStop));
			}
			else if (!(this.dfREQSPEC.IsEmpty())) 
			{
				if (this.dfSUBSSN.IsEmpty() && this.dfREQPROV.IsEmpty() && this.dfMEMBID.IsEmpty() && this.dfSTATUS.IsEmpty() && this.dfAUTHNO.IsEmpty() && this.dfAUTHPCP.IsEmpty() && this.dfPSTATUS.IsEmpty()) 
				{
					Sal.MessageBox("You must enter at least one more search criterea !", "Search", (Sys.MB_Ok | Sys.MB_IconStop));
				}
				else
				{
					Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
				}
			}
			else
			{
				Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
			}
			#endregion
		}
		
		/// <summary>
		/// pbCount WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCount_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCount_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCount_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.dfAUTHNO.IsEmpty()) && ((SalString)this.dfAUTHNO.Text).Length < 6) 
			{
				Sal.MessageBox("You must enter at least the first 6 digits of the Claim Number", "Search", (Sys.MB_Ok | Sys.MB_IconStop));
			}
			else if (!(this.dfREQSPEC.IsEmpty())) 
			{
				if (this.dfSUBSSN.IsEmpty() && this.dfREQPROV.IsEmpty() && this.dfMEMBID.IsEmpty() && this.dfSTATUS.IsEmpty() && this.dfAUTHNO.IsEmpty() && this.dfAUTHPCP.IsEmpty() && this.dfPSTATUS.IsEmpty()) 
				{
					Sal.MessageBox("You must enter at least one more search criterea !", "Search", (Sys.MB_Ok | Sys.MB_IconStop));
				}
				else
				{
					Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
				}
			}
			else
			{
				Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
			}
			#endregion
		}
		#endregion
	}
}
