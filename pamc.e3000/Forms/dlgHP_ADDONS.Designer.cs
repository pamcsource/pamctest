// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgHP_ADDONS
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		// If ProcessLookupSPECIAL_EXC( nType, str1, str2) and nType = LOOKUP_Edit
		// Set hWndForm.dfSPECEXL_ID = SalStrToNumber(str1)
		// Set hWndForm.dfSPECEXTDESC = str2
		// Call SalSetFieldEdit( hWnd, TRUE )
		// Return TRUE
		public clsDataFieldEditNKey dfHP_ADDON_ID;
		public clsDataFieldEditKey dfHPCODE;
		public clsDataFieldEditD dfFROMDATE;
		public clsDataFieldEditD dfTODATE;
		public clsDlgNext pbAdd;
		public clsDlgPrevious pbRemove;
		protected clsText label1;
		protected clsText label2;
		protected clsText label3;
		public dlgHP_ADDONS.tblEXTRACOVER_CODESTableWindow tblEXTRACOVER_CODES;
		public dlgHP_ADDONS.tblHP_ADDONS_LISTTableWindow tblHP_ADDONS_LIST;
		protected clsText label4;
		protected clsText label5;
		protected SalFrame frame1;
		public SalPushbutton pbCancel;
		public SalPushbutton pbOk;
		public clsDataText dfHP_CODE_DESC;
		protected clsText label6;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfHP_ADDON_ID = new pamc.common.clsDataFieldEditNKey();
			this.dfHPCODE = new pamc.common.clsDataFieldEditKey();
			this.dfFROMDATE = new pamc.common.clsDataFieldEditD();
			this.dfTODATE = new pamc.common.clsDataFieldEditD();
			this.pbAdd = new pamc.common.clsDlgNext();
			this.pbRemove = new pamc.common.clsDlgPrevious();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.tblEXTRACOVER_CODES = (dlgHP_ADDONS.tblEXTRACOVER_CODESTableWindow)CreateTableWindow(typeof(dlgHP_ADDONS.tblEXTRACOVER_CODESTableWindow));
			this.tblHP_ADDONS_LIST = (dlgHP_ADDONS.tblHP_ADDONS_LISTTableWindow)CreateTableWindow(typeof(dlgHP_ADDONS.tblHP_ADDONS_LISTTableWindow));
			this.label4 = new pamc.common.clsText();
			this.label5 = new pamc.common.clsText();
			this.frame1 = new PPJ.Runtime.Windows.SalFrame();
			this.pbCancel = new PPJ.Runtime.Windows.SalPushbutton();
			this.pbOk = new PPJ.Runtime.Windows.SalPushbutton();
			this.dfHP_CODE_DESC = new pamc.common.clsDataText();
			this.label6 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfHP_ADDON_ID
			// 
			this.dfHP_ADDON_ID.Name = "dfHP_ADDON_ID";
			this.dfHP_ADDON_ID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfHP_ADDON_ID.Location = new System.Drawing.Point(159, 55);
			this.dfHP_ADDON_ID.Size = new System.Drawing.Size(64, 20);
			this.dfHP_ADDON_ID.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfHP_ADDON_ID_WindowActions);
			this.dfHP_ADDON_ID.TabIndex = 0;
			// 
			// dfHPCODE
			// 
			this.dfHPCODE.Name = "dfHPCODE";
			this.dfHPCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfHPCODE.Location = new System.Drawing.Point(159, 87);
			this.dfHPCODE.Size = new System.Drawing.Size(64, 20);
			this.dfHPCODE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfHPCODE_WindowActions);
			this.dfHPCODE.TabIndex = 1;
			// 
			// dfFROMDATE
			// 
			this.dfFROMDATE.Name = "dfFROMDATE";
			this.dfFROMDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFROMDATE.Location = new System.Drawing.Point(95, 119);
			this.dfFROMDATE.Size = new System.Drawing.Size(72, 20);
			this.dfFROMDATE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfFROMDATE_WindowActions);
			this.dfFROMDATE.TabIndex = 2;
			// 
			// dfTODATE
			// 
			this.dfTODATE.Name = "dfTODATE";
			this.dfTODATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTODATE.Location = new System.Drawing.Point(271, 119);
			this.dfTODATE.Size = new System.Drawing.Size(72, 20);
			this.dfTODATE.TabIndex = 3;
			// 
			// pbAdd
			// 
			this.pbAdd.Name = "pbAdd";
			this.pbAdd.Location = new System.Drawing.Point(311, 239);
			this.pbAdd.Size = new System.Drawing.Size(32, 30);
			this.pbAdd.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbAdd_WindowActions);
			this.pbAdd.TabIndex = 4;
			// 
			// pbRemove
			// 
			this.pbRemove.Name = "pbRemove";
			this.pbRemove.Location = new System.Drawing.Point(311, 287);
			this.pbRemove.Size = new System.Drawing.Size(32, 30);
			this.pbRemove.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbRemove_WindowActions);
			this.pbRemove.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Health Plan Code";
			this.label1.Location = new System.Drawing.Point(15, 89);
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "From Date";
			this.label2.Location = new System.Drawing.Point(15, 121);
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "To Date";
			this.label3.Location = new System.Drawing.Point(207, 121);
			this.label3.Size = new System.Drawing.Size(56, 16);
			this.label3.TabIndex = 8;
			// 
			// tblEXTRACOVER_CODES
			// 
			this.tblEXTRACOVER_CODES.Name = "tblEXTRACOVER_CODES";
			this.tblEXTRACOVER_CODES.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.tblEXTRACOVER_CODES.Location = new System.Drawing.Point(15, 183);
			this.tblEXTRACOVER_CODES.Size = new System.Drawing.Size(280, 216);
			this.tblEXTRACOVER_CODES.UseVisualStyles = true;
			// 
			// tblEXTRACOVER_CODES.colEXTCODE
			// 
			this.tblEXTRACOVER_CODES.colEXTCODE.Name = "colEXTCODE";
			this.tblEXTRACOVER_CODES.colEXTCODE.Title = "ID";
			this.tblEXTRACOVER_CODES.colEXTCODE.Width = 48;
			this.tblEXTRACOVER_CODES.colEXTCODE.MaxLength = 5;
			this.tblEXTRACOVER_CODES.colEXTCODE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblEXTRACOVER_CODES.colEXTCODE.Enabled = false;
			this.tblEXTRACOVER_CODES.colEXTCODE.Position = 1;
			// 
			// tblEXTRACOVER_CODES.colEXTCODEDESC
			// 
			this.tblEXTRACOVER_CODES.colEXTCODEDESC.Name = "colEXTCODEDESC";
			this.tblEXTRACOVER_CODES.colEXTCODEDESC.Title = "Description";
			this.tblEXTRACOVER_CODES.colEXTCODEDESC.Width = 188;
			this.tblEXTRACOVER_CODES.colEXTCODEDESC.Enabled = false;
			this.tblEXTRACOVER_CODES.colEXTCODEDESC.Position = 2;
			this.tblEXTRACOVER_CODES.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblEXTRACOVER_CODES_WindowActions);
			this.tblEXTRACOVER_CODES.TabIndex = 9;
			// 
			// tblHP_ADDONS_LIST
			// 
			this.tblHP_ADDONS_LIST.Name = "tblHP_ADDONS_LIST";
			this.tblHP_ADDONS_LIST.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.tblHP_ADDONS_LIST.Location = new System.Drawing.Point(367, 183);
			this.tblHP_ADDONS_LIST.Size = new System.Drawing.Size(272, 216);
			this.tblHP_ADDONS_LIST.UseVisualStyles = true;
			// 
			// tblHP_ADDONS_LIST.colEXTCODE
			// 
			this.tblHP_ADDONS_LIST.colEXTCODE.Name = "colEXTCODE";
			this.tblHP_ADDONS_LIST.colEXTCODE.Title = "ID";
			this.tblHP_ADDONS_LIST.colEXTCODE.Width = 48;
			this.tblHP_ADDONS_LIST.colEXTCODE.MaxLength = 5;
			this.tblHP_ADDONS_LIST.colEXTCODE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblHP_ADDONS_LIST.colEXTCODE.Enabled = false;
			this.tblHP_ADDONS_LIST.colEXTCODE.Position = 1;
			// 
			// tblHP_ADDONS_LIST.colEXTCODEDESC
			// 
			this.tblHP_ADDONS_LIST.colEXTCODEDESC.Name = "colEXTCODEDESC";
			this.tblHP_ADDONS_LIST.colEXTCODEDESC.Title = "Description";
			this.tblHP_ADDONS_LIST.colEXTCODEDESC.Width = 181;
			this.tblHP_ADDONS_LIST.colEXTCODEDESC.Enabled = false;
			this.tblHP_ADDONS_LIST.colEXTCODEDESC.Position = 2;
			this.tblHP_ADDONS_LIST.TabIndex = 10;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "List Of All Add On Sets Available";
			this.label4.Location = new System.Drawing.Point(15, 161);
			this.label4.Size = new System.Drawing.Size(192, 16);
			this.label4.TabIndex = 11;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "Health Plan Add On Id";
			this.label5.Location = new System.Drawing.Point(15, 57);
			this.label5.Size = new System.Drawing.Size(128, 16);
			this.label5.TabIndex = 12;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.BackColor = System.Drawing.Color.Gray;
			this.frame1.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame1.BorderSize = 1;
			this.frame1.Location = new System.Drawing.Point(0, 0);
			this.frame1.Size = new System.Drawing.Size(671, 38);
			this.frame1.TabIndex = 13;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbCancel.AcceleratorKey = Keys.Escape;
			this.pbCancel.Location = new System.Drawing.Point(588, 3);
			this.pbCancel.Size = new System.Drawing.Size(81, 30);
			this.pbCancel.TransparentColor = System.Drawing.Color.Silver;
			this.pbCancel.ImageName = "cancel5.bmp";
			this.pbCancel.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCancel_WindowActions);
			this.pbCancel.TabIndex = 14;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbOk.AcceleratorKey = Keys.Enter;
			this.pbOk.Location = new System.Drawing.Point(4, 3);
			this.pbOk.Size = new System.Drawing.Size(80, 30);
			this.pbOk.TransparentColor = System.Drawing.Color.Silver;
			this.pbOk.ImageName = "ok5.bmp";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 15;
			// 
			// dfHP_CODE_DESC
			// 
			this.dfHP_CODE_DESC.Name = "dfHP_CODE_DESC";
			this.dfHP_CODE_DESC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfHP_CODE_DESC.Location = new System.Drawing.Point(247, 87);
			this.dfHP_CODE_DESC.Size = new System.Drawing.Size(240, 19);
			this.dfHP_CODE_DESC.TabIndex = 16;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label6.Text = "List Of Add On Sets For This Health Plan";
			this.label6.Location = new System.Drawing.Point(367, 161);
			this.label6.Size = new System.Drawing.Size(248, 16);
			this.label6.TabIndex = 17;
			// 
			// dlgHP_ADDONS
			// 
			this.Controls.Add(this.dfHP_CODE_DESC);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.pbCancel);
			this.Controls.Add(this.tblHP_ADDONS_LIST);
			this.Controls.Add(this.tblEXTRACOVER_CODES);
			this.Controls.Add(this.pbRemove);
			this.Controls.Add(this.pbAdd);
			this.Controls.Add(this.dfTODATE);
			this.Controls.Add(this.dfFROMDATE);
			this.Controls.Add(this.dfHPCODE);
			this.Controls.Add(this.dfHP_ADDON_ID);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame1);
			this.Name = "dlgHP_ADDONS";
			this.ClientSize = new System.Drawing.Size(670, 421);
			this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
			this.BackColor = System.Drawing.Color.Silver;
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "Health Plan Add On Sets";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgHP_ADDONS_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgHP_ADDONS == this) 
			{
				App.dlgHP_ADDONS = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tblEXTRACOVER_CODES
		
		public partial class tblEXTRACOVER_CODESTableWindow
		{
			#region Window Controls
			public SalTableColumn colEXTCODE;
			public SalTableColumn colEXTCODEDESC;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colEXTCODE = new PPJ.Runtime.Windows.SalTableColumn();
				this.colEXTCODEDESC = new PPJ.Runtime.Windows.SalTableColumn();
				this.SuspendLayout();
				// 
				// colEXTCODE
				// 
				this.colEXTCODE.Name = "colEXTCODE";
				// 
				// colEXTCODEDESC
				// 
				this.colEXTCODEDESC.Name = "colEXTCODEDESC";
				// 
				// tblEXTRACOVER_CODES
				// 
				this.Controls.Add(this.colEXTCODE);
				this.Controls.Add(this.colEXTCODEDESC);
				this.Name = "tblEXTRACOVER_CODES";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
		
		#region tblHP_ADDONS_LIST
		
		public partial class tblHP_ADDONS_LISTTableWindow
		{
			#region Window Controls
			public SalTableColumn colEXTCODE;
			public SalTableColumn colEXTCODEDESC;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colEXTCODE = new PPJ.Runtime.Windows.SalTableColumn();
				this.colEXTCODEDESC = new PPJ.Runtime.Windows.SalTableColumn();
				this.SuspendLayout();
				// 
				// colEXTCODE
				// 
				this.colEXTCODE.Name = "colEXTCODE";
				// 
				// colEXTCODEDESC
				// 
				this.colEXTCODEDESC.Name = "colEXTCODEDESC";
				// 
				// tblHP_ADDONS_LIST
				// 
				this.Controls.Add(this.colEXTCODE);
				this.Controls.Add(this.colEXTCODEDESC);
				this.Name = "tblHP_ADDONS_LIST";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
