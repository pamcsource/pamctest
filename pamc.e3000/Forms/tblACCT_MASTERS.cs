// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class tblACCT_MASTERS : clsMainTable
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public tblACCT_MASTERS()
		{
			// Assign global reference.
			App.tblACCT_MASTERS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static tblACCT_MASTERS CreateWindow(Control owner)
		{
			tblACCT_MASTERS frm = new tblACCT_MASTERS();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static tblACCT_MASTERS FromHandle(SalWindowHandle handle)
		{
			return ((tblACCT_MASTERS)SalWindow.FromHandle(handle, typeof(tblACCT_MASTERS)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// tblACCT_MASTERS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblACCT_MASTERS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_Search:
					this.tblACCT_MASTERS_OnCM_Search(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Search event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblACCT_MASTERS_OnCM_Search(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.strJoin = "ACCT_MASTERS.TYPE = ACCT_TYPES.CODE";
			if (Sys.wParam == pamc.common.Const.QUERY) 
			{
				if (!(this.DoSearch("", ref this.strWhereClause, ref this.strOrderClause))) 
				{
					e.Return = false;
					return;
				}
			}
			else if (Sys.wParam == pamc.common.Const.QUICK) 
			{
				if (!(dlgSearchByACCT_MASTERS.ModalDialog(this))) 
				{
					e.Return = false;
					return;
				}
			}
			if (this.strOrderClause == pamc.sqllib.Const.strNULL) 
			{
				this.strOrderClause = "ACCT_MASTERS.ACCOUNT ASC, ACCT_MASTERS.SUBACCOUNT ASC";
			}
			e.Return = Sal.SendClassMessage(pamc.common.Const.CM_Search, Sys.wParam, Sys.lParam);
			return;
			#endregion
		}
		
		/// <summary>
		/// colACCTSUBACCT WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colACCTSUBACCT_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.colACCTSUBACCT_OnCM_SetInstanceVars(sender, e);
					break;
				
				case pamc.common.Const.CM_NotInsert:
					this.colACCTSUBACCT_OnCM_NotInsert(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colACCTSUBACCT_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.colACCTSUBACCT.SetField("ACCOUNT||\'-\'||SUBACCOUNT");
			#endregion
		}
		
		/// <summary>
		/// CM_NotInsert event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colACCTSUBACCT_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// colDESCR WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colDESCR_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.colDESCR_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colDESCR_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.colDESCR.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// colTYPE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colTYPE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.colTYPE_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colTYPE_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.colTYPE.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// colTYPEDESC WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colTYPEDESC_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.colTYPEDESC_OnCM_SetInstanceVars(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colTYPEDESC_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.colTYPEDESC.Initialize("DESCR", "ACCT_TYPES", "", 0);
			#endregion
		}
		#endregion
	}
}
