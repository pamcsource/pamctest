// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class frmCASE_EVTYPES : clsFormWindow
	{
		#region Window Variables
		public SalNumber nInd = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public frmCASE_EVTYPES()
		{
			// Assign global reference.
			App.frmCASE_EVTYPES = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static frmCASE_EVTYPES CreateWindow(Control owner)
		{
			frmCASE_EVTYPES frm = new frmCASE_EVTYPES();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static frmCASE_EVTYPES FromHandle(SalWindowHandle handle)
		{
			return ((frmCASE_EVTYPES)SalWindow.FromHandle(handle, typeof(frmCASE_EVTYPES)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean ValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strFieldName = "";
			SalArray<SalString> strData = new SalArray<SalString>();
			SalNumber nInd = 0;
			SalNumber nItem = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				strItem = Vis.StrSubstitute(strItem, "\'", "\'\'");
				if (strFieldName == "dfEVENTTYPE") 
				{
					if (strItem == pamc.sqllib.Const.strNULL) 
					{
						return true;
					}
					if (pamc.common.Int.SqlLookup("Select  EVENTTYPE from CASE_EVTYPES\r\n" +
						"	Where EVENTTYPE = " + "\'" + strItem + "\'", "", ref nInd)) 
					{
						return false;
					}
				}
				return true;
			}
			#endregion
		}
		
		/// <summary>
		/// Delete child records of a particular table, used when deleting master record.
		/// </summary>
		/// <param name="strTableName"></param>
		/// <param name="strItemName"></param>
		/// <param name="strItemValue"></param>
		/// <returns></returns>
		public SalBoolean DeleteChildRecords(SalString strTableName, SalString strItemName, SalString strItemValue)
		{
			#region Local Variables
			SqlLocals.DeleteChildRecordsLocals locals = new SqlLocals.DeleteChildRecordsLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.strTableName = strTableName;
				locals.strItemName = strItemName;
				locals.strItemValue = strItemValue;

				locals.strSql = "Delete from " + locals.strTableName + " where  " + locals.strItemName + " = \'" + locals.strItemValue + "\'";
				locals.strSql = "Delete from " + locals.strTableName + " where  " + locals.strItemName + " = :dfEVENTTYPE";
				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler603 = delegate(SalSqlHandle hSql)
				{
					dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error Deleting Child records", "An error occurred while trying to delete child records from the " + locals.strTableName + " table.");
					return false;
				};
				#endregion

				pamc.price.Var.SqlInst.SqlPrep(pamc.price.Var.hSql, locals.strSql, "");
				if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler603))) 
				{
					return false;
				}
				return true;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// frmCASE_EVTYPES WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmCASE_EVTYPES_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.frmCASE_EVTYPES_OnAM_Initialize(sender, e);
					break;
				
				case pamc.common.Const.CM_PreDeleteDone:
					this.frmCASE_EVTYPES_OnCM_PreDeleteDone(sender, e);
					break;
				
				case pamc.common.Const.CM_PreMainSaveDone:
					this.frmCASE_EVTYPES_OnCM_PreMainSaveDone(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmCASE_EVTYPES_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_Initialize, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_PreDeleteDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmCASE_EVTYPES_OnCM_PreDeleteDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.DeleteChildRecords("CASE_EVUDF1VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
			{
				e.Return = pamc.common.Const.DB_FAIL;
				return;
			}
			if (!(this.DeleteChildRecords("CASE_EVUDF2VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
			{
				e.Return = pamc.common.Const.DB_FAIL;
				return;
			}
			if (!(this.DeleteChildRecords("CASE_EVUDF3VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
			{
				e.Return = pamc.common.Const.DB_FAIL;
				return;
			}
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_PreMainSaveDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmCASE_EVTYPES_OnCM_PreMainSaveDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfEVENTUDF1LABEL.IsEmpty()) 
			{
				if (!(this.DeleteChildRecords("CASE_EVUDF1VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
				{
					e.Return = pamc.common.Const.SAVE_Fail;
					return;
				}
			}
			if (this.dfEVENTUDF2LABEL.IsEmpty()) 
			{
				if (!(this.DeleteChildRecords("CASE_EVUDF2VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
				{
					e.Return = pamc.common.Const.SAVE_Fail;
					return;
				}
			}
			if (this.dfEVENTUDF3LABEL.IsEmpty()) 
			{
				if (!(this.DeleteChildRecords("CASE_EVUDF3VALUES", "EVENTTYPE", this.dfEVENTTYPE.Text))) 
				{
					e.Return = pamc.common.Const.SAVE_Fail;
					return;
				}
			}
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfEVENTTYPE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfEVENTTYPE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfEVENTTYPE_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfEVENTTYPE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			if (!(this.ValidateEntry(this.dfEVENTTYPE.Text, this.dfEVENTTYPE))) 
			{
				Sal.MessageBox("Event Type Code already exists, try again.", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfEVENTTYPEDESC WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfEVENTTYPEDESC_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfEVENTTYPEDESC_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfEVENTTYPEDESC_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfEVENTTYPEDESC.bRequired = true;
			#endregion
		}
		
		/// <summary>
		/// pbValOne WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValOne_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_DisEnable:
					this.pbValOne_OnAM_DisEnable(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.pbValOne_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.pbValOne_OnSAM_Click(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_MOUSEMOVE:
					this.pbValOne_OnWM_MOUSEMOVE(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValOne_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 0) 
			{
				if (Sys.lParam != 2) 
				{
					this.pbValOne.DisableWindow();
				}
				else
				{
					this.pbValOne.EnableWindow();
				}
			}
			else if (Sys.lParam == 0) 
			{
				if (!(this.dfEVENTTYPE.IsEmpty())) 
				{
					this.pbValOne.EnableWindow();
				}
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValOne_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.pbValOne.DisableWindow();
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValOne_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfEVENTUDF1LABEL.IsEmpty()) 
			{
				Sal.MessageBox("Enter User Field 1 Name first", "Missing Label", (Sys.MB_Ok | Sys.MB_IconStop));
			}
			else
			{
				this.SetStatusBarText("Getting values...");
				tdlgCASE_EVUDF1VALUES.ModalDialog(this);
				this.SetStatusBarText(" ");
			}
			#endregion
		}
		
		/// <summary>
		/// WM_MOUSEMOVE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValOne_OnWM_MOUSEMOVE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetStatusBarText("View/edit/add values allowed for this field.");
			#endregion
		}
		
		/// <summary>
		/// pbValTwo WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValTwo_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_DisEnable:
					this.pbValTwo_OnAM_DisEnable(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.pbValTwo_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.pbValTwo_OnSAM_Click(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_MOUSEMOVE:
					this.pbValTwo_OnWM_MOUSEMOVE(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValTwo_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 0) 
			{
				if (Sys.lParam != 2) 
				{
					this.pbValTwo.DisableWindow();
				}
				else
				{
					this.pbValTwo.EnableWindow();
				}
			}
			else if (Sys.lParam == 0) 
			{
				if (!(this.dfEVENTTYPE.IsEmpty())) 
				{
					this.pbValTwo.EnableWindow();
				}
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValTwo_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.pbValTwo.DisableWindow();
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValTwo_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfEVENTUDF2LABEL.IsEmpty()) 
			{
				Sal.MessageBox("Enter User Field 2 Name first", "Missing Label", (Sys.MB_Ok | Sys.MB_IconStop));
			}
			else
			{
				this.SetStatusBarText("Getting values...");
				tdlgCASE_EVUDF2VALUES.ModalDialog(this);
				this.SetStatusBarText(" ");
			}
			#endregion
		}
		
		/// <summary>
		/// WM_MOUSEMOVE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValTwo_OnWM_MOUSEMOVE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetStatusBarText("View/edit/add values allowed for this field.");
			#endregion
		}
		
		/// <summary>
		/// pbValThree WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValThree_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_DisEnable:
					this.pbValThree_OnAM_DisEnable(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.pbValThree_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.pbValThree_OnSAM_Click(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_MOUSEMOVE:
					this.pbValThree_OnWM_MOUSEMOVE(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValThree_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 0) 
			{
				if (Sys.lParam != 2) 
				{
					this.pbValThree.DisableWindow();
				}
				else
				{
					this.pbValThree.EnableWindow();
				}
			}
			else if (Sys.lParam == 0) 
			{
				if (!(this.dfEVENTTYPE.IsEmpty())) 
				{
					this.pbValThree.EnableWindow();
				}
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValThree_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.pbValThree.DisableWindow();
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValThree_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfEVENTUDF3LABEL.IsEmpty()) 
			{
				Sal.MessageBox("Enter User Field 3 Name first", "Missing Label", (Sys.MB_Ok | Sys.MB_IconStop));
			}
			else
			{
				this.SetStatusBarText("Getting values...");
				tdlgCASE_EVUDF3VALUES.ModalDialog(this);
				this.SetStatusBarText(" ");
			}
			#endregion
		}
		
		/// <summary>
		/// WM_MOUSEMOVE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbValThree_OnWM_MOUSEMOVE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetStatusBarText("View/edit/add values allowed for this field.");
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class DeleteChildRecordsLocals
			{
				public SalString strSql = "";
				public SalString strTableName = "";
				public SalString strItemName = "";
				public SalString strItemValue = "";
			}
		}
		#endregion
	}
}
