// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgMembUtilizationUpdt
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsRadio rbMember;
		public clsRadio rbSubscriber;
		public clsRadio rbHP;
		public clsRadio rbAll;
		public clsRadio rbPlanYr;
		public clsDataFieldEdit dfMemberId;
		protected clsText label1;
		public clsDataFieldEdit dfSubscriber;
		public clsDataFieldEdit dfHP;
		protected clsTextRt label2;
		protected clsGroupBox groupBox1;
		protected clsTextRt label3;
		protected clsGroupBox groupBox2;
		public clsDataFieldEdit dfYear;
		protected clsTextRt label4;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.rbMember = new pamc.common.clsRadio();
			this.rbSubscriber = new pamc.common.clsRadio();
			this.rbHP = new pamc.common.clsRadio();
			this.rbAll = new pamc.common.clsRadio();
			this.rbPlanYr = new pamc.common.clsRadio();
			this.dfMemberId = new pamc.common.clsDataFieldEdit();
			this.label1 = new pamc.common.clsText();
			this.dfSubscriber = new pamc.common.clsDataFieldEdit();
			this.dfHP = new pamc.common.clsDataFieldEdit();
			this.label2 = new pamc.common.clsTextRt();
			this.groupBox1 = new pamc.common.clsGroupBox();
			this.label3 = new pamc.common.clsTextRt();
			this.groupBox2 = new pamc.common.clsGroupBox();
			this.dfYear = new pamc.common.clsDataFieldEdit();
			this.label4 = new pamc.common.clsTextRt();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.dfYear);
			this.ClientArea.Controls.Add(this.dfHP);
			this.ClientArea.Controls.Add(this.dfSubscriber);
			this.ClientArea.Controls.Add(this.dfMemberId);
			this.ClientArea.Controls.Add(this.rbPlanYr);
			this.ClientArea.Controls.Add(this.rbAll);
			this.ClientArea.Controls.Add(this.rbHP);
			this.ClientArea.Controls.Add(this.rbSubscriber);
			this.ClientArea.Controls.Add(this.rbMember);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.groupBox2);
			this.ClientArea.Controls.Add(this.groupBox1);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Location = new System.Drawing.Point(340, 1);
			this.pbCancel.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCancel_WindowActions);
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 2;
			// 
			// rbMember
			// 
			this.rbMember.Name = "rbMember";
			this.rbMember.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbMember.Text = "&Member";
			this.rbMember.Location = new System.Drawing.Point(31, 63);
			this.rbMember.Size = new System.Drawing.Size(88, 24);
			this.rbMember.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbMember_WindowActions);
			this.rbMember.TabIndex = 3;
			// 
			// rbSubscriber
			// 
			this.rbSubscriber.Name = "rbSubscriber";
			this.rbSubscriber.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbSubscriber.Text = "&Subscriber";
			this.rbSubscriber.Location = new System.Drawing.Point(31, 87);
			this.rbSubscriber.Size = new System.Drawing.Size(96, 24);
			this.rbSubscriber.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbSubscriber_WindowActions);
			this.rbSubscriber.TabIndex = 4;
			// 
			// rbHP
			// 
			this.rbHP.Name = "rbHP";
			this.rbHP.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbHP.Text = "&Healthplan";
			this.rbHP.Location = new System.Drawing.Point(31, 111);
			this.rbHP.Size = new System.Drawing.Size(96, 24);
			this.rbHP.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbHP_WindowActions);
			this.rbHP.TabIndex = 5;
			// 
			// rbAll
			// 
			this.rbAll.Name = "rbAll";
			this.rbAll.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbAll.Text = "&All";
			this.rbAll.Location = new System.Drawing.Point(31, 135);
			this.rbAll.Size = new System.Drawing.Size(80, 24);
			this.rbAll.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbAll_WindowActions);
			this.rbAll.TabIndex = 6;
			// 
			// rbPlanYr
			// 
			this.rbPlanYr.Name = "rbPlanYr";
			this.rbPlanYr.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbPlanYr.Text = "&Plan Years";
			this.rbPlanYr.Location = new System.Drawing.Point(31, 156);
			this.rbPlanYr.Size = new System.Drawing.Size(103, 24);
			this.rbPlanYr.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbPlanYr_WindowActions);
			this.rbPlanYr.TabIndex = 7;
			// 
			// dfMemberId
			// 
			this.dfMemberId.Name = "dfMemberId";
			this.dfMemberId.MaxLength = 20;
			this.dfMemberId.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMemberId.Location = new System.Drawing.Point(247, 63);
			this.dfMemberId.Size = new System.Drawing.Size(152, 20);
			this.dfMemberId.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfMemberId_WindowActions);
			this.dfMemberId.TabIndex = 8;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Member ID:";
			this.label1.Location = new System.Drawing.Point(170, 68);
			this.label1.Size = new System.Drawing.Size(66, 16);
			this.label1.TabIndex = 9;
			// 
			// dfSubscriber
			// 
			this.dfSubscriber.Name = "dfSubscriber";
			this.dfSubscriber.MaxLength = 20;
			this.dfSubscriber.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSubscriber.Location = new System.Drawing.Point(248, 93);
			this.dfSubscriber.Size = new System.Drawing.Size(71, 20);
			this.dfSubscriber.EditMask = "999-99-9999";
			this.dfSubscriber.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSubscriber_WindowActions);
			this.dfSubscriber.TabIndex = 10;
			// 
			// dfHP
			// 
			this.dfHP.Name = "dfHP";
			this.dfHP.MaxLength = 4;
			this.dfHP.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfHP.Location = new System.Drawing.Point(248, 122);
			this.dfHP.Size = new System.Drawing.Size(39, 20);
			this.dfHP.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfHP_WindowActions);
			this.dfHP.TabIndex = 11;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.label2.Text = "HP Code:";
			this.label2.Location = new System.Drawing.Point(171, 124);
			this.label2.Size = new System.Drawing.Size(60, 16);
			this.label2.TabIndex = 12;
			// 
			// groupBox1
			// 
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox1.Text = "Update";
			this.groupBox1.Location = new System.Drawing.Point(15, 47);
			this.groupBox1.Size = new System.Drawing.Size(128, 137);
			this.groupBox1.TabIndex = 13;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.label3.Text = "Subscriber SSN:";
			this.label3.Location = new System.Drawing.Point(170, 91);
			this.label3.Size = new System.Drawing.Size(69, 26);
			this.label3.TabIndex = 14;
			// 
			// groupBox2
			// 
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox2.Text = "Values";
			this.groupBox2.Location = new System.Drawing.Point(160, 47);
			this.groupBox2.Size = new System.Drawing.Size(247, 137);
			this.groupBox2.TabIndex = 15;
			// 
			// dfYear
			// 
			this.dfYear.Name = "dfYear";
			this.dfYear.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfYear.Location = new System.Drawing.Point(247, 151);
			this.dfYear.Size = new System.Drawing.Size(40, 20);
			this.dfYear.EditMask = "9999";
			this.dfYear.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfYear_WindowActions);
			this.dfYear.TabIndex = 16;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.label4.Text = "Year:";
			this.label4.Location = new System.Drawing.Point(175, 153);
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 17;
			// 
			// dlgMembUtilizationUpdt
			// 
			this.Name = "dlgMembUtilizationUpdt";
			this.ClientSize = new System.Drawing.Size(420, 204);
			this.Text = "Member Utilization Update";
			this.Location = new System.Drawing.Point(242, 100);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgMembUtilizationUpdt_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgMembUtilizationUpdt == this) 
			{
				App.dlgMembUtilizationUpdt = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
