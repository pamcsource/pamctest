// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class tblDIAG_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private pamc.common.App.NamedMenus.menuFile menu_menuFile;
		private pamc.common.App.NamedMenus.menuRecord menu_menuRecord;
		private pamc.common.App.NamedMenus.menuView menu_menuView;
		private SalPopupMenu popupMenu__Utilities;
		private SalMenuItem menuItem__Update;
		private pamc.common.App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private pamc.common.App.NamedMenus.menuHelp menu_menuHelp;
		public clsColQueryKey colDIAGCODE;
		public clsColCB colTERMINATE;
		public clsColQuery colDIAGDESC;
		public clsColCB colPRE_MIN_BEN;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new pamc.common.App.NamedMenus.menuFile();
			this.menu_menuRecord = new pamc.common.App.NamedMenus.menuRecord();
			this.menu_menuView = new pamc.common.App.NamedMenus.menuView();
			this.popupMenu__Utilities = new SalPopupMenu();
			this.menuItem__Update = new SalMenuItem();
			this.menu_menuMDIWindows = new pamc.common.App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new pamc.common.App.NamedMenus.menuHelp();
			this.colDIAGCODE = new pamc.common.clsColQueryKey();
			this.colTERMINATE = new pamc.common.clsColCB();
			this.colDIAGDESC = new pamc.common.clsColQuery();
			this.colPRE_MIN_BEN = new pamc.common.clsColCB();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecord);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.popupMenu__Utilities);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// popupMenu__Utilities
			// 
			this.popupMenu__Utilities.Text = "&Utilities";
			this.popupMenu__Utilities.MenuItems.Add(this.menuItem__Update);
			// 
			// menuItem__Update
			// 
			this.menuItem__Update.Text = "&Update Diagnosis Codes";
			this.menuItem__Update.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Update_EnabledWhen);
			this.menuItem__Update.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Update_MenuActions);
			// 
			// colROWID
			// 
			this.colROWID.Name = "colROWID";
			this.colROWID.TabIndex = 0;
			// 
			// colCreateBy
			// 
			this.colCreateBy.Name = "colCreateBy";
			this.colCreateBy.TabIndex = 1;
			// 
			// colCreateDate
			// 
			this.colCreateDate.Name = "colCreateDate";
			this.colCreateDate.TabIndex = 2;
			// 
			// colLastChangeBy
			// 
			this.colLastChangeBy.Name = "colLastChangeBy";
			this.colLastChangeBy.TabIndex = 3;
			// 
			// colLastChangeDate
			// 
			this.colLastChangeDate.Name = "colLastChangeDate";
			this.colLastChangeDate.TabIndex = 4;
			// 
			// colDIAGCODE
			// 
			this.colDIAGCODE.Name = "colDIAGCODE";
			this.colDIAGCODE.Title = "Code";
			this.colDIAGCODE.Width = 58;
			this.colDIAGCODE.MaxLength = 10;
			this.colDIAGCODE.TabIndex = 5;
			// 
			// colTERMINATE
			// 
			this.colTERMINATE.Name = "colTERMINATE";
			this.colTERMINATE.Title = "Terminate";
			this.colTERMINATE.TabIndex = 6;
			// 
			// colDIAGDESC
			// 
			this.colDIAGDESC.Name = "colDIAGDESC";
			this.colDIAGDESC.Title = "Description";
			this.colDIAGDESC.Width = 227;
			this.colDIAGDESC.MaxLength = 100;
			this.colDIAGDESC.TabIndex = 7;
			// 
			// colPRE_MIN_BEN
			// 
			this.colPRE_MIN_BEN.Name = "colPRE_MIN_BEN";
			this.colPRE_MIN_BEN.Title = "Prescribed Minimum Benefit";
			this.colPRE_MIN_BEN.TabIndex = 8;
			// 
			// tblDIAG_CODES
			// 
			this.Controls.Add(this.colROWID);
			this.Controls.Add(this.colCreateBy);
			this.Controls.Add(this.colCreateDate);
			this.Controls.Add(this.colLastChangeBy);
			this.Controls.Add(this.colLastChangeDate);
			this.Controls.Add(this.colDIAGCODE);
			this.Controls.Add(this.colTERMINATE);
			this.Controls.Add(this.colDIAGDESC);
			this.Controls.Add(this.colPRE_MIN_BEN);
			this.Name = "tblDIAG_CODES";
			this.Text = "Diagnosis Codes";
			this.Location = new System.Drawing.Point(3, 6);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblDIAG_CODES_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tblDIAG_CODES == this) 
			{
				App.tblDIAG_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
