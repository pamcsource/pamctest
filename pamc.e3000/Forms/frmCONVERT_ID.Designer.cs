// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmCONVERT_ID
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private pamc.common.App.NamedMenus.menuFile menu_menuFile;
		private pamc.common.App.NamedMenus.menuRecord menu_menuRecord;
		private pamc.common.App.NamedMenus.menuView menu_menuView;
		private SalPopupMenu popupMenu__Utilities;
		private SalMenuItem menuItem__Clear;
		private SalPopupMenu popupMenu__Load;
		private SalMenuItem menuItem__Select;
		private SalMenuItem menuItem__Inload;
		private SalMenuItem menuItem__Outload;
		private SalMenuItem menuItem__Apply;
		private SalMenuSeparator menuSeparator1;
		private SalPopupMenu popupMenu_Review;
		private SalMenuSeparator menuSeparator2;
		private SalMenuSeparator menuSeparator3;
		private SalMenuSeparator menuSeparator4;
		private SalMenuItem menuItem_Convert;
		private SalMenuSeparator menuSeparator5;
		private SalPopupMenu popupMenu_Review_1;
		private pamc.common.App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private pamc.common.App.NamedMenus.menuHelp menu_menuHelp;
		protected clsText label1;
		protected clsGroupBox groupBox1;
		public clsDataFieldEditKey dfMEMBID;
		public clsDataFieldEdit dfSUBSSN;
		public clsDataFieldDisabled dfNAME;
		protected clsText label2;
		protected clsGroupBox groupBox2;
		public clsDataFieldEdit dfNEWID;
		public clsDataFieldEdit dfNEWSUBSSN;
		protected clsText label3;
		protected clsText label4;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new pamc.common.App.NamedMenus.menuFile();
			this.menu_menuRecord = new pamc.common.App.NamedMenus.menuRecord();
			this.menu_menuView = new pamc.common.App.NamedMenus.menuView();
			this.popupMenu__Utilities = new SalPopupMenu();
			this.menuItem__Clear = new SalMenuItem();
			this.popupMenu__Load = new SalPopupMenu();
			this.menuItem__Select = new SalMenuItem();
			this.menuItem__Inload = new SalMenuItem();
			this.menuItem__Outload = new SalMenuItem();
			this.menuItem__Apply = new SalMenuItem();
			this.menuSeparator1 = new SalMenuSeparator();
			this.popupMenu_Review = new SalPopupMenu();
			this.menuSeparator2 = new SalMenuSeparator();
			this.menuSeparator3 = new SalMenuSeparator();
			this.menuSeparator4 = new SalMenuSeparator();
			this.menuItem_Convert = new SalMenuItem();
			this.menuSeparator5 = new SalMenuSeparator();
			this.popupMenu_Review_1 = new SalPopupMenu();
			this.menu_menuMDIWindows = new pamc.common.App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new pamc.common.App.NamedMenus.menuHelp();
			this.label1 = new pamc.common.clsText();
			this.groupBox1 = new pamc.common.clsGroupBox();
			this.dfMEMBID = new pamc.common.clsDataFieldEditKey();
			this.dfSUBSSN = new pamc.common.clsDataFieldEdit();
			this.dfNAME = new pamc.common.clsDataFieldDisabled();
			this.label2 = new pamc.common.clsText();
			this.groupBox2 = new pamc.common.clsGroupBox();
			this.dfNEWID = new pamc.common.clsDataFieldEdit();
			this.dfNEWSUBSSN = new pamc.common.clsDataFieldEdit();
			this.label3 = new pamc.common.clsText();
			this.label4 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecord);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.popupMenu__Utilities);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// popupMenu__Utilities
			// 
			this.popupMenu__Utilities.Text = "&Utilities";
			this.popupMenu__Utilities.MenuItems.Add(this.menuItem__Clear);
			this.popupMenu__Utilities.MenuItems.Add(this.popupMenu__Load);
			this.popupMenu__Utilities.MenuItems.Add(this.menuItem__Outload);
			this.popupMenu__Utilities.MenuItems.Add(this.menuItem__Apply);
			this.popupMenu__Utilities.MenuItems.Add(this.menuSeparator1);
			this.popupMenu__Utilities.MenuItems.Add(this.popupMenu_Review);
			this.popupMenu__Utilities.MenuItems.Add(this.menuSeparator4);
			this.popupMenu__Utilities.MenuItems.Add(this.menuItem_Convert);
			this.popupMenu__Utilities.MenuItems.Add(this.menuSeparator5);
			this.popupMenu__Utilities.MenuItems.Add(this.popupMenu_Review_1);
			// 
			// menuItem__Clear
			// 
			this.menuItem__Clear.Text = "&Clear Cross Reference Table";
			this.menuItem__Clear.StatusText = "Delete all rows from the Cross Reference Table";
			this.menuItem__Clear.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Clear_EnabledWhen);
			this.menuItem__Clear.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Clear_MenuActions);
			// 
			// popupMenu__Load
			// 
			this.popupMenu__Load.Text = "&Load Cross Reference Table";
			this.popupMenu__Load.StatusText = "Fill Cross Reference Table with Member ID's to be converted";
			this.popupMenu__Load.MenuItems.Add(this.menuItem__Select);
			this.popupMenu__Load.MenuItems.Add(this.menuItem__Inload);
			// 
			// menuItem__Select
			// 
			this.menuItem__Select.Text = "&Select EZ-CAP Member IDs";
			this.menuItem__Select.StatusText = "Insert Member IDs into Cross Reference Table by using Search Criteria";
			this.menuItem__Select.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Select_EnabledWhen);
			this.menuItem__Select.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Select_MenuActions);
			// 
			// menuItem__Inload
			// 
			this.menuItem__Inload.Text = "&Inload Member IDs from file";
			this.menuItem__Inload.StatusText = "Inload Member IDs from the text file";
			this.menuItem__Inload.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Inload_EnabledWhen);
			this.menuItem__Inload.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Inload_MenuActions);
			// 
			// menuItem__Outload
			// 
			this.menuItem__Outload.Text = "&Outload Member IDs into file";
			this.menuItem__Outload.StatusText = "Outload Member IDs into the text file from Cross Reference Table";
			this.menuItem__Outload.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Outload_EnabledWhen);
			this.menuItem__Outload.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Outload_MenuActions);
			// 
			// menuItem__Apply
			// 
			this.menuItem__Apply.Text = "&Apply Template to the Member IDs";
			this.menuItem__Apply.StatusText = "Add/Remove character from the Member ID";
			this.menuItem__Apply.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem__Apply_EnabledWhen);
			this.menuItem__Apply.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem__Apply_MenuActions);
			// 
			// popupMenu_Review
			// 
			this.popupMenu_Review.Text = "Review &Data to be Converted";
			this.popupMenu_Review.StatusText = "Reports to be checked prior to conversion";
			this.popupMenu_Review.MenuItems.Add(this.menuSeparator2);
			this.popupMenu_Review.MenuItems.Add(this.menuSeparator3);
			// 
			// menuItem_Convert
			// 
			this.menuItem_Convert.Text = "Conver&t";
			this.menuItem_Convert.StatusText = "Convert old Member IDs to new";
			this.menuItem_Convert.EnabledWhen += new PPJ.Runtime.Windows.SalMenuEnabledWhenHandler(this.menuItem_Convert_EnabledWhen);
			this.menuItem_Convert.MenuActions += new PPJ.Runtime.Windows.SalMenuActionsHandler(this.menuItem_Convert_MenuActions);
			// 
			// popupMenu_Review_1
			// 
			this.popupMenu_Review_1.Text = "R&eview Converted Data";
			this.popupMenu_Review_1.StatusText = "Reports to be checked after completed conversion";
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Member ID:";
			this.label1.Location = new System.Drawing.Point(15, 27);
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox1.Text = "Old";
			this.groupBox1.Location = new System.Drawing.Point(7, 7);
			this.groupBox1.Size = new System.Drawing.Size(544, 72);
			this.groupBox1.TabIndex = 1;
			// 
			// dfMEMBID
			// 
			this.dfMEMBID.Name = "dfMEMBID";
			this.dfMEMBID.MaxLength = 20;
			this.dfMEMBID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfMEMBID.Location = new System.Drawing.Point(111, 23);
			this.dfMEMBID.Size = new System.Drawing.Size(152, 20);
			this.dfMEMBID.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfMEMBID_WindowActions);
			this.dfMEMBID.TabIndex = 2;
			// 
			// dfSUBSSN
			// 
			this.dfSUBSSN.Name = "dfSUBSSN";
			this.dfSUBSSN.MaxLength = 20;
			this.dfSUBSSN.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSUBSSN.Location = new System.Drawing.Point(111, 47);
			this.dfSUBSSN.Size = new System.Drawing.Size(152, 20);
			this.dfSUBSSN.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSUBSSN_WindowActions);
			this.dfSUBSSN.TabIndex = 3;
			// 
			// dfNAME
			// 
			this.dfNAME.Name = "dfNAME";
			this.dfNAME.MaxLength = 40;
			this.dfNAME.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNAME.Location = new System.Drawing.Point(271, 26);
			this.dfNAME.Size = new System.Drawing.Size(264, 16);
			this.dfNAME.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfNAME_WindowActions);
			this.dfNAME.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Member ID:";
			this.label2.Location = new System.Drawing.Point(15, 107);
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 5;
			// 
			// groupBox2
			// 
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox2.Text = "New";
			this.groupBox2.Location = new System.Drawing.Point(7, 87);
			this.groupBox2.Size = new System.Drawing.Size(544, 72);
			this.groupBox2.TabIndex = 6;
			// 
			// dfNEWID
			// 
			this.dfNEWID.Name = "dfNEWID";
			this.dfNEWID.MaxLength = 20;
			this.dfNEWID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNEWID.Location = new System.Drawing.Point(111, 103);
			this.dfNEWID.Size = new System.Drawing.Size(152, 20);
			this.dfNEWID.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfNEWID_WindowActions);
			this.dfNEWID.TabIndex = 7;
			// 
			// dfNEWSUBSSN
			// 
			this.dfNEWSUBSSN.Name = "dfNEWSUBSSN";
			this.dfNEWSUBSSN.MaxLength = 20;
			this.dfNEWSUBSSN.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNEWSUBSSN.Location = new System.Drawing.Point(111, 127);
			this.dfNEWSUBSSN.Size = new System.Drawing.Size(152, 20);
			this.dfNEWSUBSSN.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfNEWSUBSSN_WindowActions);
			this.dfNEWSUBSSN.TabIndex = 8;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Main Member:";
			this.label3.Location = new System.Drawing.Point(15, 51);
			this.label3.Size = new System.Drawing.Size(88, 16);
			this.label3.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Main Member:";
			this.label4.Location = new System.Drawing.Point(15, 131);
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 10;
			// 
			// frmCONVERT_ID
			// 
			this.Controls.Add(this.dfNEWSUBSSN);
			this.Controls.Add(this.dfNEWID);
			this.Controls.Add(this.dfNAME);
			this.Controls.Add(this.dfSUBSSN);
			this.Controls.Add(this.dfMEMBID);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "frmCONVERT_ID";
			this.ClientSize = new System.Drawing.Size(580, 189);
			this.Text = "Member ID Cross Reference Table";
			this.Location = new System.Drawing.Point(47, 78);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmCONVERT_ID_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmCONVERT_ID == this) 
			{
				App.frmCONVERT_ID = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
