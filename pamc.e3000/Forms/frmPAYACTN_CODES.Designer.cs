// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmPAYACTN_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		// On SAM_Validate
		// Call SqlExists( 'Select CODE From _EZCODE_PAYMENT Where CODE = :dfCODE', bGExists )
		// If bGExists
		// Call SalMessageBox( 'Payment Type Code already exists, try again.', 'Validate',MB_Ok | MB_IconStop)
		// Return VALIDATE_Cancel
		// Call SalSendClassMessage(SAM_Validate, wParam, lParam)
		// If SqlLookup( 'Select CODE From _PAYACTN_CODES Where CODE = :dfCODE',nGInd )
		// Call SalMessageBox( 'Payment Type Code already exists, try again.', 'Validate',MB_Ok | MB_IconStop)
		// Return VALIDATE_Cancel
		// If NOT ValidateEntry(MyValue, hWndItem)
		// Call SalMessageBox( 'Payment Type Code already exists, try again.', 'Validate',MB_Ok | MB_IconStop)
		// Return VALIDATE_Cancel
		// Return VALIDATE_Ok
		public clsDataFieldDisabled dfCODE;
		public clsDataFieldDisabled dfDESCR;
		protected clsText label1;
		protected clsText label2;
		// On AM_Initialize
		// Set MyValue=1
		public clsDataFieldDisabled dfEDITABLE;
		// On AM_Initialize
		// Set MyValue=1
		public clsDataFieldDisabled dfSELECTABLE;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfCODE = new pamc.common.clsDataFieldDisabled();
			this.dfDESCR = new pamc.common.clsDataFieldDisabled();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.dfEDITABLE = new pamc.common.clsDataFieldDisabled();
			this.dfSELECTABLE = new pamc.common.clsDataFieldDisabled();
			this.SuspendLayout();
			// 
			// dfCODE
			// 
			this.dfCODE.Name = "dfCODE";
			this.dfCODE.MaxLength = 2;
			this.dfCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCODE.Location = new System.Drawing.Point(146, 9);
			this.dfCODE.Size = new System.Drawing.Size(37, 19);
			this.dfCODE.TabIndex = 0;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 30;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(146, 37);
			this.dfDESCR.Size = new System.Drawing.Size(224, 19);
			this.dfDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDESCR_WindowActions);
			this.dfDESCR.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(10, 11);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(10, 41);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 3;
			// 
			// dfEDITABLE
			// 
			this.dfEDITABLE.Name = "dfEDITABLE";
			this.dfEDITABLE.Visible = false;
			this.dfEDITABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfEDITABLE.Format = "";
			this.dfEDITABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfEDITABLE.Location = new System.Drawing.Point(143, 71);
			this.dfEDITABLE.Size = new System.Drawing.Size(66, 19);
			this.dfEDITABLE.TabIndex = 4;
			// 
			// dfSELECTABLE
			// 
			this.dfSELECTABLE.Name = "dfSELECTABLE";
			this.dfSELECTABLE.Visible = false;
			this.dfSELECTABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSELECTABLE.Format = "";
			this.dfSELECTABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfSELECTABLE.Location = new System.Drawing.Point(239, 71);
			this.dfSELECTABLE.Size = new System.Drawing.Size(66, 19);
			this.dfSELECTABLE.TabIndex = 5;
			// 
			// frmPAYACTN_CODES
			// 
			this.Controls.Add(this.dfSELECTABLE);
			this.Controls.Add(this.dfEDITABLE);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfCODE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmPAYACTN_CODES";
			this.ClientSize = new System.Drawing.Size(440, 117);
			this.Text = "Payment Action Code";
			this.Location = new System.Drawing.Point(106, 118);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmPAYACTN_CODES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmPAYACTN_CODES == this) 
			{
				App.frmPAYACTN_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
