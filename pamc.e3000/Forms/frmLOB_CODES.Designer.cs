// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class frmLOB_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private pamc.common.App.NamedMenus.menuFile menu_menuFile;
		private pamc.common.App.NamedMenus.menuRecordDefault menu_menuRecordDefault;
		private pamc.common.App.NamedMenus.menuView menu_menuView;
		private pamc.common.App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private pamc.common.App.NamedMenus.menuHelp menu_menuHelp;
		public clsDataFieldEditKey dfCODE;
		public clsDataFieldEdit dfDESCR;
		protected clsText label1;
		protected clsText label2;
		public clsDataFieldDisabled dfEDITABLE;
		public clsDataFieldDisabled dfSELECTABLE;
		public clsDefault df1;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new pamc.common.App.NamedMenus.menuFile();
			this.menu_menuRecordDefault = new pamc.common.App.NamedMenus.menuRecordDefault();
			this.menu_menuView = new pamc.common.App.NamedMenus.menuView();
			this.menu_menuMDIWindows = new pamc.common.App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new pamc.common.App.NamedMenus.menuHelp();
			this.dfCODE = new pamc.common.clsDataFieldEditKey();
			this.dfDESCR = new pamc.common.clsDataFieldEdit();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.dfEDITABLE = new pamc.common.clsDataFieldDisabled();
			this.dfSELECTABLE = new pamc.common.clsDataFieldDisabled();
			this.df1 = new pamc.common.clsDefault();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecordDefault);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// dfCODE
			// 
			this.dfCODE.Name = "dfCODE";
			this.dfCODE.MaxLength = 4;
			this.dfCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCODE.Location = new System.Drawing.Point(146, 9);
			this.dfCODE.Size = new System.Drawing.Size(45, 20);
			this.dfCODE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCODE_WindowActions);
			this.dfCODE.TabIndex = 0;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 30;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(146, 37);
			this.dfDESCR.Size = new System.Drawing.Size(224, 20);
			this.dfDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDESCR_WindowActions);
			this.dfDESCR.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(10, 11);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(10, 41);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 3;
			// 
			// dfEDITABLE
			// 
			this.dfEDITABLE.Name = "dfEDITABLE";
			this.dfEDITABLE.Visible = false;
			this.dfEDITABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfEDITABLE.Format = "";
			this.dfEDITABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfEDITABLE.Location = new System.Drawing.Point(143, 71);
			this.dfEDITABLE.Size = new System.Drawing.Size(66, 19);
			this.dfEDITABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfEDITABLE_WindowActions);
			this.dfEDITABLE.TabIndex = 4;
			// 
			// dfSELECTABLE
			// 
			this.dfSELECTABLE.Name = "dfSELECTABLE";
			this.dfSELECTABLE.Visible = false;
			this.dfSELECTABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSELECTABLE.Format = "";
			this.dfSELECTABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfSELECTABLE.Location = new System.Drawing.Point(231, 71);
			this.dfSELECTABLE.Size = new System.Drawing.Size(66, 19);
			this.dfSELECTABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSELECTABLE_WindowActions);
			this.dfSELECTABLE.TabIndex = 5;
			// 
			// df1
			// 
			this.df1.Name = "df1";
			this.df1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.df1.Location = new System.Drawing.Point(215, 12);
			this.df1.Size = new System.Drawing.Size(80, 16);
			this.df1.TabIndex = 6;
			// 
			// frmLOB_CODES
			// 
			this.Controls.Add(this.df1);
			this.Controls.Add(this.dfSELECTABLE);
			this.Controls.Add(this.dfEDITABLE);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfCODE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmLOB_CODES";
			this.ClientSize = new System.Drawing.Size(440, 97);
			this.Text = "Line of Business Code";
			this.Location = new System.Drawing.Point(0, 0);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmLOB_CODES_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmLOB_CODES == this) 
			{
				App.frmLOB_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
