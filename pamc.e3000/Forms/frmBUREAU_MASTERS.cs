// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class frmBUREAU_MASTERS : clsFormWindow
	{
		#region Window Variables
		public SalNumber nBureauID = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public frmBUREAU_MASTERS()
		{
			// Assign global reference.
			App.frmBUREAU_MASTERS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static frmBUREAU_MASTERS CreateWindow(Control owner)
		{
			frmBUREAU_MASTERS frm = new frmBUREAU_MASTERS();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static frmBUREAU_MASTERS FromHandle(SalWindowHandle handle)
		{
			return ((frmBUREAU_MASTERS)SalWindow.FromHandle(handle, typeof(frmBUREAU_MASTERS)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <param name="nBureauID"></param>
		/// <returns></returns>
		public SalBoolean GetNewBureauID(ref SalNumber nBureauID)
		{
			#region Local Variables
			SqlLocals.GetNewBureauIDLocals locals = new SqlLocals.GetNewBureauIDLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				try
				{

					// PPJ: Assign parameters to the locals instance.
					locals.nBureauID = nBureauID;

					if (!(pamc.price.Var.SqlInst.SqlRetr(pamc.price.Var.hSql, "EZCAP.GetNewBureauID", "", ":nBureauID"))) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.Execute())) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.FetchNext(ref locals.nInd))) 
					{
						return false;
					}
					return true;
				}
				finally
				{

					// PPJ: Assign back receive parameters.
					nBureauID = locals.nBureauID;

				}
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// frmBUREAU_MASTERS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmBUREAU_MASTERS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_New:
					this.frmBUREAU_MASTERS_OnCM_New(sender, e);
					break;
				
				// On CM_DoLookup
				
				// Call ProcessLookup( SalGetFocus(),LOOKUP_Edit)
			}
			#endregion
		}
		
		/// <summary>
		/// CM_New event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmBUREAU_MASTERS_OnCM_New(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.GetNewBureauID(ref this.nBureauID))) 
			{
				e.Return = false;
				return;
			}
			this.bOk = Sal.SendClassMessage(pamc.common.Const.CM_New, Sys.wParam, Sys.lParam);
			this.dfBUREAUID.Number = this.nBureauID;
			e.Return = this.bOk;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfCOMPREGNO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCOMPREGNO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfCOMPREGNO_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCOMPREGNO_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfCOMPREGNO.bRequired = true;
			#endregion
		}
		
		/// <summary>
		/// dfNAME WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNAME_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfNAME_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNAME_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfNAME.bRequired = true;
			#endregion
		}
		
		/// <summary>
		/// dfSTREET1 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTREET1_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfSTREET1_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTREET1_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		
		/// <summary>
		/// dfSTREET2 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTREET2_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfSTREET2_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSTREET2_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class GetNewBureauIDLocals
			{
				public SalNumber nInd = 0;
				public SalNumber nBureauID = 0;
			}
		}
		#endregion
	}
}
