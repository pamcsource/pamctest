// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	public partial class tblBENTRACK_TABLES : clsMainTableHistory
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public tblBENTRACK_TABLES()
		{
			// Assign global reference.
			App.tblBENTRACK_TABLES = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static tblBENTRACK_TABLES CreateWindow(Control owner)
		{
			tblBENTRACK_TABLES frm = new tblBENTRACK_TABLES();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static tblBENTRACK_TABLES FromHandle(SalWindowHandle handle)
		{
			return ((tblBENTRACK_TABLES)SalWindow.FromHandle(handle, typeof(tblBENTRACK_TABLES)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// Menu EnabledWhen Expression
		/// </summary>
		/// <returns></returns>
		private bool popupMenu__CopyOutload_EnabledWhen()
		{
			return pamc.common.Var.nSEC > 3;
		}
		
		/// <summary>
		/// Menu Actions
		/// </summary>
		/// <returns></returns>
		private SalNumber menuItem_By_MenuActions()
		{
			if (Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
			{
				dlgCopySet.ModalDialog(this, "BENTRACK_TABLES", "BENTRACK_TBLSETS", "TRACKSET", "Tracking Code Set");
			}
			return 0;
		}
		
		/// <summary>
		/// Menu Actions
		/// </summary>
		/// <returns></returns>
		private SalNumber menuItem_By_1_MenuActions()
		{
			if (Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
			{
				dlgCopySetHP.ModalDialog(this, "BENTRACK_TABLES", "BENTRACK_TBLSETS", "TRACKSET", "Tracking Code Set");
			}
			return 0;
		}
		
		/// <summary>
		/// Menu EnabledWhen Expression
		/// </summary>
		/// <returns></returns>
		private bool menuItem__Inload_EnabledWhen()
		{
			return pamc.common.Var.nSEC > 3;
		}
		
		/// <summary>
		/// Menu Actions
		/// </summary>
		/// <returns></returns>
		private SalNumber menuItem__Inload_MenuActions()
		{
			if (Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
			{
				dlgInloadBENTRACK_TABLES.ModalDialog(this);
			}
			return 0;
		}
		
		/// <summary>
		/// Menu EnabledWhen Expression
		/// </summary>
		/// <returns></returns>
		private bool menuItem__Delete_EnabledWhen()
		{
			return pamc.common.Var.nSEC > 3;
		}
		
		/// <summary>
		/// Menu Actions
		/// </summary>
		/// <returns></returns>
		private SalNumber menuItem__Delete_MenuActions()
		{
			if (Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
			{
				if (dlgDeleteSetsBENTRACK_TABLES.ModalDialog(this, "BENTRACK_TABLES", "BENTRACK_TBLSETS", "TRACKSET", 0)) 
				{
					Sal.MessageBox("Set(s) successfully deleted!", "Delete Set", (Sys.MB_Ok | Sys.MB_IconAsterisk));
				}
			}
			return 0;
		}
		
		/// <summary>
		/// tblBENTRACK_TABLES WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblBENTRACK_TABLES_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_Search:
					this.tblBENTRACK_TABLES_OnCM_Search(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Search event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblBENTRACK_TABLES_OnCM_Search(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == pamc.common.Const.QUERY) 
			{
				if (!(this.DoSearch(pamc.price.Var.strOpID + "BYC00", ref this.strWhereClause, ref this.strOrderClause))) 
				{
					e.Return = false;
					return;
				}
			}
			else if (Sys.wParam == pamc.common.Const.QUICK) 
			{
				if (!(dlgSearchByBENTRACK_TABLES.ModalDialog(this))) 
				{
					e.Return = false;
					return;
				}
			}
			if (this.strOrderClause == pamc.sqllib.Const.strNULL) 
			{
				this.strOrderClause = "TRACKSET ASC,BENTYPE ASC,FROMDATE DESC";
			}
			e.Return = Sal.SendClassMessage(pamc.common.Const.CM_Search, Sys.wParam, Sys.lParam);
			return;
			#endregion
		}
		#endregion
	}
}
