// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// </summary>
	/// <param name="hWndTbl"></param>
	/// <param name="nMode"></param>
	public partial class fdlgMEMB_PENHIST : clsDlgInfoForm
	{
		#region Window Parameters
		public SalWindowHandle hWndTbl;
		public SalNumber nMode;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public fdlgMEMB_PENHIST(SalWindowHandle hWndTbl, SalNumber nMode)
		{
			// Assign global reference.
			App.fdlgMEMB_PENHIST = this;
			// Window Parameters initialization.
			this.hWndTbl = hWndTbl;
			this.nMode = nMode;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalWindowHandle hWndTbl, SalNumber nMode)
		{
			fdlgMEMB_PENHIST dlg = new fdlgMEMB_PENHIST(hWndTbl, nMode);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static fdlgMEMB_PENHIST FromHandle(SalWindowHandle handle)
		{
			return ((fdlgMEMB_PENHIST)SalWindow.FromHandle(handle, typeof(fdlgMEMB_PENHIST)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean IDProcessLookup(SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strFieldName = "";
			SalNumber nSelectableCol = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfTYPE") 
				{
					strQuery = "Select Type=TYPE,Description=DESCR from MEMB_IDTYPES Order by 1 ";
					if (dlgLookupList.ModalDialog(this, strQuery, ref strResult, "ID Types", Sys.hWndForm.FindControl("dfTYPE").Text, nSelectableCol)) 
					{
						strResult.Tokenize("", pamc.common.Const.strTAB, strCols);
						Sys.hWndForm.FindControl("dfTYPE").Text = strCols[0];
						hWnd.SetModified(true);
						return true;
					}
				}
				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// fdlgMEMB_PENHIST WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_SetInstanceVars:
					this.fdlgMEMB_PENHIST_OnCM_SetInstanceVars(sender, e);
					break;
				
				case pamc.common.Const.CM_DoLookup:
					this.fdlgMEMB_PENHIST_OnCM_DoLookup(sender, e);
					break;
				
				case pamc.common.Const.AM_Initialize:
					this.fdlgMEMB_PENHIST_OnAM_Initialize(sender, e);
					break;
				
				case pamc.common.Const.CM_Populate:
					this.fdlgMEMB_PENHIST_OnCM_Populate(sender, e);
					break;
				
				case Sys.SAM_CreateComplete:
					this.fdlgMEMB_PENHIST_OnSAM_CreateComplete(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.hWndTable = this.hWndTbl;
			this.nFormMode = this.nMode;
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.IDProcessLookup(Sal.GetFocus());
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(pamc.common.Const.AM_Initialize, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(pamc.common.Const.CM_Populate, 0, 0);
			this.SendMessageToChildren(pamc.common.Const.AM_GetValue, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// SAM_CreateComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgMEMB_PENHIST_OnSAM_CreateComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_CreateComplete, Sys.wParam, Sys.lParam);
			this.dfCURRHIST.Text = "C";
			this.dfCURRHIST.DisableWindow();
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfCURRHIST.Text == "C") 
			{
				if (App.tdlgMEMB_PENHIST.SendMessage(pamc.common.Const.CM_ProcessUpdate, 0, 0)) 
				{
					Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
				}
			}
			else
			{
				this.dfCURRHIST.Text = pamc.sqllib.Const.strNULL;
				Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
			}
			#endregion
		}
		
		/// <summary>
		/// dfSUBSSN WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSUBSSN_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.AM_Initialize:
					this.dfSUBSSN_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSUBSSN_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfSUBSSN.Text = pamc.common.Var.hWndMainForm.FindControl("dfSUBSSN").Text;
			#endregion
		}
		
		/// <summary>
		/// dfCURRHIST WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCURRHIST_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfCURRHIST_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Validate:
					this.dfCURRHIST_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCURRHIST_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfCURRHIST.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCURRHIST_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		#endregion
	}
}
