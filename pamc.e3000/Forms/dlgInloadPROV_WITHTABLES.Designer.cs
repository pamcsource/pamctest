// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgInloadPROV_WITHTABLES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			this.ToolBar.Create = false;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.pbFile);
			this.ClientArea.Controls.Add(this.dfFile);
			this.ClientArea.Controls.Add(this.dfSet);
			this.ClientArea.Controls.Add(this.pbClose);
			this.ClientArea.Controls.Add(this.pbInload);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.frame2);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Create = false;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&I";
			this.label1.TabIndex = 0;
			// 
			// pbInload
			// 
			this.pbInload.Name = "pbInload";
			this.pbInload.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Text = "&L";
			this.label2.TabIndex = 2;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Start Numbering New Sets at:";
			this.label3.TabIndex = 4;
			// 
			// dfSet
			// 
			this.dfSet.Name = "dfSet";
			this.dfSet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSet.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label4.Text = "Inload From File:";
			this.label4.TabIndex = 6;
			// 
			// dfFile
			// 
			this.dfFile.Name = "dfFile";
			this.dfFile.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFile.TabIndex = 7;
			// 
			// pbFile
			// 
			this.pbFile.Name = "pbFile";
			this.pbFile.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbFile.TabIndex = 8;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.TabIndex = 9;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 10;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 11;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 12;
			// 
			// dlgInloadPROV_WITHTABLES
			// 
			this.Name = "dlgInloadPROV_WITHTABLES";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgInloadPROV_WITHTABLES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgInloadPROV_WITHTABLES == this) 
			{
				App.dlgInloadPROV_WITHTABLES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
