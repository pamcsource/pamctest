// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class tblMEXT_NOTES_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsColQueryKey colEXTNOTECODE;
		public clsColQuery colEXTNOTECODEDESC;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.colEXTNOTECODE = new pamc.common.clsColQueryKey();
			this.colEXTNOTECODEDESC = new pamc.common.clsColQuery();
			this.SuspendLayout();
			// 
			// colROWID
			// 
			this.colROWID.Name = "colROWID";
			this.colROWID.TabIndex = 0;
			// 
			// colCreateBy
			// 
			this.colCreateBy.Name = "colCreateBy";
			this.colCreateBy.TabIndex = 1;
			// 
			// colCreateDate
			// 
			this.colCreateDate.Name = "colCreateDate";
			this.colCreateDate.TabIndex = 2;
			// 
			// colLastChangeBy
			// 
			this.colLastChangeBy.Name = "colLastChangeBy";
			this.colLastChangeBy.TabIndex = 3;
			// 
			// colLastChangeDate
			// 
			this.colLastChangeDate.Name = "colLastChangeDate";
			this.colLastChangeDate.TabIndex = 4;
			// 
			// colEXTNOTECODE
			// 
			this.colEXTNOTECODE.Name = "colEXTNOTECODE";
			this.colEXTNOTECODE.Title = "Member Extra Notes Code";
			this.colEXTNOTECODE.Width = 184;
			this.colEXTNOTECODE.MaxLength = 20;
			this.colEXTNOTECODE.TabIndex = 5;
			// 
			// colEXTNOTECODEDESC
			// 
			this.colEXTNOTECODEDESC.Name = "colEXTNOTECODEDESC";
			this.colEXTNOTECODEDESC.Title = "Description";
			this.colEXTNOTECODEDESC.Width = 275;
			this.colEXTNOTECODEDESC.MaxLength = 30;
			this.colEXTNOTECODEDESC.TabIndex = 6;
			// 
			// tblMEXT_NOTES_CODES
			// 
			this.Controls.Add(this.colROWID);
			this.Controls.Add(this.colCreateBy);
			this.Controls.Add(this.colCreateDate);
			this.Controls.Add(this.colLastChangeBy);
			this.Controls.Add(this.colLastChangeDate);
			this.Controls.Add(this.colEXTNOTECODE);
			this.Controls.Add(this.colEXTNOTECODEDESC);
			this.Name = "tblMEXT_NOTES_CODES";
			this.ClientSize = new System.Drawing.Size(796, 329);
			this.Text = "Member Identification Types";
			this.Location = new System.Drawing.Point(3, 6);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblMEXT_NOTES_CODES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tblMEXT_NOTES_CODES == this) 
			{
				App.tblMEXT_NOTES_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
