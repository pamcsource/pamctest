// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	/// <summary>
	/// Un-Do Last Re-Enrollment
	/// </summary>
	/// <param name="strMEMBID"></param>
	public partial class dlgUnDo_ReEnroll : clsTransDlg
	{
		#region Window Parameters
		public SalString strMEMBID;
		#endregion
		
		#region Window Variables
		public SalString strExtCode = "";
		public SalDateTime dtENROLLDATE = SalDateTime.Null;
		public SalDateTime dtTERMINATIONDATE = SalDateTime.Null;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgUnDo_ReEnroll(SalString strMEMBID)
		{
			// Assign global reference.
			App.dlgUnDo_ReEnroll = this;
			// Window Parameters initialization.
			this.strMEMBID = strMEMBID;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalString strMEMBID)
		{
			dlgUnDo_ReEnroll dlg = new dlgUnDo_ReEnroll(strMEMBID);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgUnDo_ReEnroll FromHandle(SalWindowHandle handle)
		{
			return ((dlgUnDo_ReEnroll)SalWindow.FromHandle(handle, typeof(dlgUnDo_ReEnroll)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <param name="strMEMBID"></param>
		/// <returns></returns>
		public SalBoolean Populate(SalString strMEMBID)
		{
			#region Local Variables
			SalString strQuery = "";
			SalNumber nInd = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				// Get current HP/Option info
				// Set strQuery = 'select  distinct H.HPCODE, H.OPFROMDT, H.OPT, H.OPTHRUDT, B.OPTNAME,
				//  P.PCP, P.PCPFROMDT, P.PCPTHRUDT, P.ADDNOTFY, P.DROPNOTFY,
				// M.LASTNAME||@IF(@isna(M.FIRSTNAME),\'\',\',\')||M.FIRSTNAME
				//   from  MEMB_HPHISTS H , BEN_OPTIONS B, MEMB_PCPHISTS P ' || SqlInst.GetQuery(9)  ||   ' PROV_MASTERS M
				// where H.MEMBID = :strMEMBID ' ||
				// ' and H.MEMBID = P.MEMBID' ||
				// ' and H.CURRHIST = '|| '\'' || 'C' || '\'' ||
				// ' and P.CURRHIST = '|| '\'' || 'C' || '\'' ||
				// ' and P.PCP = M.PROVID(+)' ||
				// ' and H.HPCODE = B.HPCODE and H.OPT = B.OPT '
				strQuery = "select  distinct H.HPCODE, H.OPFROMDT, H.OPT, H.OPTHRUDT, B.OPTNAME,\r\n" +
				" P.PCP, P.PCPFROMDT, P.PCPTHRUDT, P.ADDNOTFY, P.DROPNOTFY, \r\n" +
				"M.LASTNAME||@IF(@isna(M.FIRSTNAME),'',',')||M.FIRSTNAME \r\n" +
				"  from  MEMB_HPHISTS H , BEN_OPTIONS B, \r\n" +
				"MEMB_PCPHISTS P " + pamc.price.Var.SqlInst.GetQuery(9) + " PROV_MASTERS M " + pamc.price.Var.SqlInst.GetQuery(37) + " where H.MEMBID = :strMEMBID " + " and H.MEMBID = P.MEMBID" + " and H.CURRHIST =  'C' " + " and P.CURRHIST =  'C' " + " and P.PCP = M.PROVID  and H.HPCODE = B.HPCODE and H.OPT = B.OPT ";
				if (pamc.common.Int.SqlLookup(strQuery, ":dfHPCODE, :dfOPFROMDT,:dfOPT, :dfOPTHRUDT, :dfOPTNAME,\r\n" +
					":dfPCP, :dfPCPFROMDT, :dfPCPTHRUDT, :dfADDNOTFY, :dfDROPNOTFY,\r\n" +
					":dfPROVIDERNAME", ref nInd)) 
				{
					// Get previous HP/Option  info
					strQuery = "select  P1.HPCODE, P1.OPFROMDT, P1.OPT,  P1.OPTHRUDT, B.OPTNAME\r\n" +
					" from  MEMB_HPHISTS P1 , BEN_OPTIONS B\r\n" +
					"where P1.MEMBID = :strMEMBID and P1.HPCODE = B.HPCODE and P1.OPT = B.OPT" + " and P1.OPFROMDT = (select max(P2.OPFROMDT) from MEMB_HPHISTS P2\r\n" +
					" where P2.MEMBID = :strMEMBID and P2.CURRHIST = " + "\'" + "H" + "\' )";
					if (pamc.common.Int.SqlLookup(strQuery, ":dfOldHPCODE,  :dfOldOPFROMDT,:dfOldOPT, :dfOldOPTHRUDT, :dfOldOPTNAME", ref nInd)) 
					{
						// Get previous PCP  info
						strQuery = "select  P.PCPFROMDT, P.PCP, P.ADDNOTFY, P.DROPNOTFY, P.PCPTHRUDT,\r\n" +
						" M.LASTNAME||@if(@isna(M.FIRSTNAME),\'\',\',\')||M.FIRSTNAME\r\n" +
						" from  MEMB_PCPHISTS P" + pamc.price.Var.SqlInst.GetQuery(9) + " PROV_MASTERS M " + pamc.price.Var.SqlInst.GetQuery(44) + "where P.MEMBID = :strMEMBID " + " and P.PCP = M.PROVID (+)" + " and P.PCPFROMDT = (select max(P2.PCPFROMDT) from MEMB_PCPHISTS P2\r\n" +
						" where P2.MEMBID = :strMEMBID and P2.CURRHIST = " + "\'" + "H" + "\' )";
						if (pamc.common.Int.SqlLookup(strQuery, ":dfOldPCPFROMDT,:dfOldPCP, :dfOldADDNOTFY, :dfOldDROPNOTFY,:dfOldPCPTHRUDT,\r\n" +
							" :dfOldPROVIDERNAME", ref nInd)) 
						{
							return true;
						}
					}
				}
				Sal.MessageBox("No records found !", "Retrieve data", Sys.MB_Ok);
				pbOk.DisableWindow();
				return false;
			}
			#endregion
		}
		// Function: CheckRequired
		// Description: This function traverses the table to determine if any required values are missing.
		// It builds a string of the labels of any missing values and also returns the number of
		// empty fields.
		// Returns
		// Number:
		// Parameters
		// Window Handle: hWndFrm
		// Receive String: strMissing
		// Static Variables
		// Local variables
		// Number: nEmpty
		// Window Handle: hWndChild
		// Window Handle: hWndDataField
		// String: strText
		// Actions
		// Set nEmpty = 0
		// Set strMissing = strNULL
		// Set hWndChild = SalGet( hWndFrm, TYPE_DataField )
		// While hWndChild != hWndNULL
		// If hWndDataField != hWndNULL
		// If SalSendMsg( hWndDataField, AM_Empty, 0, 0 )
		// Set strMissing = strMissing||SalGetWindowLabelText(hWndChild,strText,
		// Set nEmpty = nEmpty + 1
		// Call SalGetWindowText( hWndDataField, strText, 25 )
		// If SalGetType( hWndDataField ) = TYPE_DataField AND hWndDataField.clsDataField.bRequired AND ( SalIsNull( hWndDataField ) OR
		// 	( SalGetDataType( hWndDataField ) = DT_Number AND SalStrToNumber( strText ) = 0 ) )
		// Set strMissing = strMissing||TblGetColumnTitleX( hWndChild )||', '
		// Set nEmpty = nEmpty + 1
		// Set hWndChild = SalGetNextChild( hWndChild, TYPE_TableColumn )
		// Set strMissing = SalStrLeftX( strMissing, SalStrLength( SalStrTrimX( strMissing ) ) -1 )
		// Return nEmpty
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean ValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strFieldName = "";
			SalArray<SalString> strData = new SalArray<SalString>();
			SalNumber nInd = 0;
			SalNumber nItem = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				return true;
			}
			#endregion
		}
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.  Also handles window-to
		/// lookups.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="nType"></param>
		/// <returns></returns>
		public SalBoolean ProcessLookup(SalWindowHandle hWnd, SalNumber nType)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strWhere = "";
			SalString strFieldName = "";
			SalString str2 = "";
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfPCP") 
				{
					strQuery = "Select  PCP_ID = PROVID,Provider=LASTNAME||@if(@isna( FIRSTNAME),\'\',\',\')||FIRSTNAME from PROV_MASTERS" + " Order by 2";
					if (dlgLookupList.ModalDialog(this, strQuery, ref strResult, "Provider", str2, 0)) 
					{
						strResult.Tokenize("", pamc.common.Const.strTAB, strCols);
						Sys.hWndForm.FindControl("dfPCP").Text = strCols[0];
						Sys.hWndForm.FindControl("dfPROVIDERNAME").Text = strCols[1];
						hWnd.SetModified(true);
						return true;
					}
				}
				return false;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="strMEMBID"></param>
		/// <returns></returns>
		public SalBoolean UpdateDetails(SalWindowHandle hWnd, SalString strMEMBID)
		{
			#region Local Variables
			SqlLocals.UpdateDetailsLocals locals = new SqlLocals.UpdateDetailsLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.hWnd = hWnd;
				locals.strMEMBID = strMEMBID;

				locals.strWhere = " MEMBID = :strMEMBID and CURRHIST =  " + "\'" + "C" + "\'";
				locals.strTimeStamp = "LASTCHANGEBY = :nOpID,LASTCHANGEDATE=SYSDATETIME ";
				// Delete current record  in Member HP/Option History table
				locals.strSql = "delete from MEMB_HPHISTS" + " where " + locals.strWhere;
				if (!(pamc.common.Int.DoSql(pamc.price.Var.hSql, locals.hWnd, locals.strSql, "Error deleting  record from database.", " MEMB_HPHISTS table."))) 
				{
					return false;
				}
				// Update the previous record  to 'C' record type in  Member HP/Option History table
				locals.strSql = "update MEMB_HPHISTS set CURRHIST= " + "\'" + "C" + "\'" + ", TRANCODE = " + "\'" + "C" + "\'" + ",TRANDATE = SYSDATETIME," + locals.strTimeStamp + " where MEMBID = :strMEMBID and OPFROMDT = " + "\'" + Sal.FmtFormatDateTime(
					dfOldOPFROMDT.DateTime, "dd/MM/yyyy") + "\'";
				if (!(pamc.common.Int.DoSql(pamc.price.Var.hSql, this, locals.strSql, "Unable to update ", "Member ID = " + locals.strMEMBID + "  into HP/Option history table"))) 
				{
					return false;
				}
				// Delete current record in Member PCP history table
				locals.strSql = "delete from MEMB_PCPHISTS" + " where " + locals.strWhere;
				if (!(pamc.common.Int.DoSql(pamc.price.Var.hSql, locals.hWnd, locals.strSql, "Error deleting  record from database.", " MEMB_PCPHISTS table."))) 
				{
					return false;
				}
				// Update the latest 'H' record to 'C' record type in Member PCP history table
				locals.strSql = "update MEMB_PCPHISTS set CURRHIST= " + "\'" + "C" + "\'" + ", TRANCODE = " + "\'" + "C" + "\'" + ",TRANDATE = SYSDATETIME," + locals.strTimeStamp + " where MEMBID = :strMEMBID and PCPFROMDT = " + "\'" + Sal.FmtFormatDateTime(
					dfOldPCPFROMDT.DateTime, "dd/MM/yyyy") + "\'";
				if (!(pamc.common.Int.DoSql(pamc.price.Var.hSql, this, locals.strSql, "Unable to update ", "Member ID = " + locals.strMEMBID + "  into PCP history table"))) 
				{
					return false;
				}
				pamc.common.Int.Connect(ref locals.SqlGetExtCovHist);
				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler530 = delegate(SalSqlHandle hSql)
				{
					dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error Reading Member Extracover", "Because of SQL database error, EZ-CAP could not properly Read Member Extracover.");
					return false;
				};
				#endregion

				if (!(pamc.price.Var.SqlInst.SqlRetr(locals.SqlGetExtCovHist, "EZCAP.Q171", ":strMEMBID", ":strExtCode,:dtENROLLDATE,:dtTERMINATIONDATE"))) 
				{
					return false;
				}
				if (!(locals.SqlGetExtCovHist.Execute(sqlErrorHandler530))) 
				{
					return false;
				}
				while (locals.SqlGetExtCovHist.FetchNext(ref locals.nInd, sqlErrorHandler530)) 
				{
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler531 = delegate(SalSqlHandle hSql)
					{
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error Inserting Member ExtraCover History", "Because of SQL database error, EZ-CAP could not properly save your Extracover History.");
						return false;
					};
					#endregion

					locals.strSql = "update MEMB_EXTRACOVER set ENROLLDATE = :dtENROLLDATE,TERMINATIONDATE = :dtTERMINATIONDATE,TERMTYPE = " + "\'" + "A" + "\'";
					locals.strSql = locals.strSql + " where MEMBID = :strMEMBID AND TERMTYPE = " + "\'" + "R" + "\' AND EXTCODE = :strExtCode";
					if (!(pamc.common.Int.DoSql(pamc.price.Var.hSql, this, locals.strSql, "Error updating record into the database.", " MEMB_EXTRACOVER table."))) 
					{
						return false;
					}
				}
				locals.SqlGetExtCovHist.Disconnect(sqlErrorHandler530);
				// Update extracover with new enroll date
				Int.GetMembTermCode(locals.strMEMBID, ref locals.strUpdateTermCode);
				App.frmMEMB_MASTERS.dfTERM_CODE.Text = locals.strUpdateTermCode;
				return true;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgUnDo_ReEnroll WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgUnDo_ReEnroll_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.common.Const.CM_Populate:
					this.dlgUnDo_ReEnroll_OnCM_Populate(sender, e);
					break;
				
				case pamc.common.Const.CM_MainSaveDone:
					this.dlgUnDo_ReEnroll_OnCM_MainSaveDone(sender, e);
					break;
				
				case pamc.common.Const.CM_PreMainSaveDone:
					this.dlgUnDo_ReEnroll_OnCM_PreMainSaveDone(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgUnDo_ReEnroll_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.Populate(this.strMEMBID))) 
			{
				this.pbCancel.SendMessage(Sys.SAM_Click, 0, 0);
			}
			#endregion
		}
		
		/// <summary>
		/// CM_MainSaveDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgUnDo_ReEnroll_OnCM_MainSaveDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Update detail tables here
			if (!(this.UpdateDetails(this, this.strMEMBID))) 
			{
				e.Return = pamc.common.Const.SAVE_Fail;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_PreMainSaveDone event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgUnDo_ReEnroll_OnCM_PreMainSaveDone(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Check same HPCode/Option here
			// **** check if  record  has been edited or deleted by another user  --- begin ****
			if (!(clsMainTable.FromHandle(pamc.common.Var.hWndMainTable).CheckROWID("MEMB_MASTERS"))) 
			{
				clsMainTable.FromHandle(pamc.common.Var.hWndMainTable).CheckStatusOnSave("MEMB_MASTERS", clsMainTable.FromHandle(pamc.common.Var.hWndMainTable).strKey, "Save...");
				pamc.ez32bit.Int.ValidateErrorMsgBox("This transaction has been canceled. To view new changes you need to do Refresh List.");
				this.pbCancel.SendMessage(Sys.SAM_Click, 0, 0);
				e.Return = pamc.common.Const.SAVE_Fail;
				return;
			}
			// Copy data to main form here
			((SalDataField)pamc.common.Var.hWndMainForm.FindControl("dfOPFROMDT")).DateTime = this.dfOldOPFROMDT.DateTime;
			((SalDataField)pamc.common.Var.hWndMainForm.FindControl("dfOPTHRUDT")).DateTime = this.dfOldOPTHRUDT.DateTime;
			pamc.common.Var.hWndMainForm.FindControl("dfHPCODE").Text = this.dfOldHPCODE.Text;
			pamc.common.Var.hWndMainForm.FindControl("dfOPT").Text = this.dfOldOPT.Text;
			pamc.common.Var.hWndMainForm.FindControl("dfOPTNAME").Text = this.dfOldOPTNAME.Text;
			pamc.common.Var.hWndMainForm.FindControl("dfPCP").Text = this.dfOldPCP.Text;
			pamc.common.Var.hWndMainForm.FindControl("dfPCPNAME").Text = this.dfOldPROVIDERNAME.Text;
			((SalDataField)pamc.common.Var.hWndMainForm.FindControl("dfPCPFROMDT")).DateTime = this.dfOldPCPFROMDT.DateTime;
			((SalDataField)pamc.common.Var.hWndMainForm.FindControl("dfPCPTHRUDT")).DateTime = this.dfOldPCPTHRUDT.DateTime;
			App.frmMEMB_MASTERS.SetOutLoadStatus();
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.MessageBox("You are about to un-do this member\'s  Last Re-Enrollment.   Are you sure?", "Un-Do Last Re-Enrollment", (Sys.MB_IconExclamation | Sys.MB_YesNo)) == Sys.IDYES) 
			{
				Sal.SendClassMessage(Sys.SAM_Click, Sys.wParam, Sys.lParam);
			}
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class UpdateDetailsLocals
			{
				public SalString strSql = "";
				public SalString strWhere = "";
				public SalString strTimeStamp = "";
				public SalSqlHandle SqlGetExtCovHist = SalSqlHandle.Null;
				public SalNumber nInd = 0;
				public SalString strUpdateTermCode = "";
				public SalWindowHandle hWnd = SalWindowHandle.Null;
				public SalString strMEMBID = "";
			}
		}
		#endregion
	}
}
