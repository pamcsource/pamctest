// <ppj name="pamc.e3000" date="06.09.2018 10:54:11" id="DC9D413B8B3256BCB7EF146CFD10B044F2BED214"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3000
{
	
	public partial class dlgTerminateDiags
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		protected clsText label1;
		public clsDataFieldEditKey dfDIAGCODE;
		protected clsText label2;
		public clsDataFieldEdit dfDIAGDESC;
		public clsCheckBoxAuto cbTERMINATE;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new pamc.common.clsText();
			this.dfDIAGCODE = new pamc.common.clsDataFieldEditKey();
			this.label2 = new pamc.common.clsText();
			this.dfDIAGDESC = new pamc.common.clsDataFieldEdit();
			this.cbTERMINATE = new pamc.common.clsCheckBoxAuto();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.cbTERMINATE);
			this.ClientArea.Controls.Add(this.dfDIAGDESC);
			this.ClientArea.Controls.Add(this.dfDIAGCODE);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCancel_WindowActions);
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(26, 59);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 3;
			// 
			// dfDIAGCODE
			// 
			this.dfDIAGCODE.Name = "dfDIAGCODE";
			this.dfDIAGCODE.MaxLength = 10;
			this.dfDIAGCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDIAGCODE.Location = new System.Drawing.Point(162, 57);
			this.dfDIAGCODE.Size = new System.Drawing.Size(77, 20);
			this.dfDIAGCODE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDIAGCODE_WindowActions);
			this.dfDIAGCODE.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(26, 97);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 5;
			// 
			// dfDIAGDESC
			// 
			this.dfDIAGDESC.Name = "dfDIAGDESC";
			this.dfDIAGDESC.MaxLength = 100;
			this.dfDIAGDESC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDIAGDESC.Location = new System.Drawing.Point(162, 93);
			this.dfDIAGDESC.Size = new System.Drawing.Size(224, 20);
			this.dfDIAGDESC.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDIAGDESC_WindowActions);
			this.dfDIAGDESC.TabIndex = 6;
			// 
			// cbTERMINATE
			// 
			this.cbTERMINATE.Name = "cbTERMINATE";
			this.cbTERMINATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbTERMINATE.Text = "Terminate";
			this.cbTERMINATE.Location = new System.Drawing.Point(271, 55);
			this.cbTERMINATE.Size = new System.Drawing.Size(112, 24);
			this.cbTERMINATE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbTERMINATE_WindowActions);
			this.cbTERMINATE.TabIndex = 7;
			// 
			// dlgTerminateDiags
			// 
			this.Name = "dlgTerminateDiags";
			this.ClientSize = new System.Drawing.Size(420, 148);
			this.Text = "Terminate Diagnosis Code";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgTerminateDiags_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgTerminateDiags == this) 
			{
				App.dlgTerminateDiags = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
