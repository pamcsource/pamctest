// <ppj name="pamc.sqllib" date="04.09.2018 16:38:03" id="36844E8BF536645EEEF11AF9BE268BC3A19C5CCB"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;

namespace pamc.sqllib
{
	
	public static class Int
	{
		
		/// <summary>
		/// This Function Replaces The Sqlretrieve function used in Sqlwindows
		/// It Formats the String to be compatible with whatever SQL Server is being used
		/// </summary>
		/// <param name="cInString"></param>
		/// <param name="cOutString"></param>
		/// <returns></returns>
		public static SalString SQLChange(SalString cInString, ref SalString cOutString)
		{
			#region Local Variables
			SalNumber nCounter = 0;
			SalNumber nArrayLen = 0;
			SalNumber nFirstPos = 0;
			SalNumber nCutoff = 0;
			SalString cVal = "";
			SalString cCheckVal = "";
			SalNumber nLen = 0;
			SalNumber nStringLength = 0;
			SalString strUpper = "";
			SalNumber nCheckCount = 0;
			SalNumber nJoinCheck = 0;
			#endregion
			
			#region Actions
			nArrayLen = Var.aConversions.GetUpperBound(1);
			nCounter = 0;
			// Call SalStrUpper(cInString, strUpper)
			cOutString = cInString;
			while (nCounter <= nArrayLen) 
			{
				nCounter = nCounter + 1;
				nFirstPos = 0;
				nCutoff = 0;
				// Call SalStrUpper(cOutString, strUpper)
				strUpper = cOutString;
				if (Var.aConversions[nCounter, Var.SQLBASE] != SalString.Null) 
				{
					while (nFirstPos != -1) 
					{
						nFirstPos = strUpper.Scan(Var.aConversions[nCounter, Var.SQLBASE]);
						if (nFirstPos != -1) 
						{
							nLen = Var.aConversions[nCounter, Var.nCompany].Length;
							nStringLength = cOutString.Length;
							cVal = strUpper.Mid(nFirstPos - 1, 1);
							if (Var.aConversions[nCounter, Var.SQLBASE] == "(+)" && Var.nCompany != Var.SQLBASE) 
							{
								if (strUpper.Mid(nFirstPos - 4, 3).ToUpper() != "AND" && strUpper.Mid(nFirstPos - 3, 2).ToUpper() != "OR" && strUpper.Mid(nFirstPos - 6, 5).ToUpper() != "WHERE") 
								{
									cCheckVal = "";
									nCheckCount = 1;
									while (cCheckVal != "=") 
									{
										nCheckCount = nCheckCount + 1;
										cCheckVal = strUpper.Mid(nFirstPos - nCheckCount, 1);
										nJoinCheck = strUpper.Scan("JOIN");
										if (cCheckVal == "=" && nJoinCheck <= 0) 
										{
											strUpper = strUpper.Left(nFirstPos - nCheckCount - 1) + " *= " + strUpper.Right((strUpper.Length - (nFirstPos - nCheckCount + 2)));
											cOutString = strUpper.Left(nFirstPos - nCheckCount - 1) + " *= " + strUpper.Right((strUpper.Length - (nFirstPos - nCheckCount + 2)));
											nFirstPos = nFirstPos + 1;
											nStringLength = nStringLength + 1;
										}
									}
								}
								else
								{
									cCheckVal = "";
									nCheckCount = 1;
									while (cCheckVal != "=") 
									{
										nCheckCount = nCheckCount + 1;
										cCheckVal = strUpper.Mid(nFirstPos + nCheckCount, 1);
										nJoinCheck = strUpper.Scan("JOIN");
										if (cCheckVal == "=" && nJoinCheck <= 0) 
										{
											strUpper = strUpper.Left(nFirstPos + nCheckCount - 1) + " =* " + strUpper.Right((strUpper.Length - (nFirstPos + nCheckCount + 2)));
											cOutString = strUpper.Left(nFirstPos + nCheckCount - 1) + " =* " + strUpper.Right((strUpper.Length - (nFirstPos + nCheckCount + 2)));
											nStringLength = nStringLength + 1;
										}
									}
								}
							}
							if (nFirstPos > 0) 
							{
								if (Var.aConversions[nCounter, Var.CHANGE_CRITERIA] == "FIELD") 
								{
									if (cVal == " " || cVal == "," || cVal == "." || cVal == "") 
									{
										if (nFirstPos + Var.aConversions[nCounter, Var.SQLBASE].Length + 1 > nStringLength) 
										{
											cVal = ",";
										}
										else
										{
											cVal = strUpper.Mid(nFirstPos + Var.aConversions[nCounter, Var.SQLBASE].Length, 1);
										}
										if (cVal == " " || cVal == "," || cVal == "." || cVal == "") 
										{
											strUpper = strUpper.Left(nFirstPos) + Var.aConversions[nCounter, Var.nCompany] + strUpper.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
											// Set cOutString = SalStrLeftX(cOutString,nFirstPos+nCutoff) || aConversions[nCounter, nCompany] ||
											// 	SalStrRightX(strUpper, nStringLength-nFirstPos-nCutoff-
											// 		SalStrLength( aConversions[nCounter, SQLBASE] ))
											cOutString = cOutString.Left(nFirstPos + nCutoff) + Var.aConversions[nCounter, Var.nCompany] + cOutString.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
										}
										else
										{
											nCutoff = nCutoff + nFirstPos + Var.aConversions[nCounter, Var.SQLBASE].Length;
											strUpper = strUpper.Right(strUpper.Length - nFirstPos - Var.aConversions[nCounter, Var.SQLBASE].Length);
										}
									}
									else
									{
										nCutoff = nCutoff + nFirstPos + Var.aConversions[nCounter, Var.SQLBASE].Length;
										strUpper = strUpper.Right(strUpper.Length - nFirstPos - Var.aConversions[nCounter, Var.SQLBASE].Length);
									}
								}
								else if (Var.aConversions[nCounter, Var.CHANGE_CRITERIA] != "FIELD") 
								{
									strUpper = strUpper.Left(nFirstPos + nCutoff) + Var.aConversions[nCounter, Var.nCompany] + strUpper.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
									cOutString = cOutString.Left(nFirstPos + nCutoff) + Var.aConversions[nCounter, Var.nCompany] + cOutString.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
								}
							}
							else
							{
								cOutString = Var.aConversions[nCounter, Var.nCompany] + cOutString.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
								strUpper = Var.aConversions[nCounter, Var.nCompany] + strUpper.Right(nStringLength - nFirstPos - nCutoff - Var.aConversions[nCounter, Var.SQLBASE].Length);
							}
						}
					}
				}
			}
			// This Loop replaces all places where there are ,,
			while (true) 
			{
				nFirstPos = cOutString.Scan(",,");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, 2, ",NULL,");
				}
				else
				{
					break;
				}
			}
			Int.SQLChangeComplex(cOutString, ref cOutString);
			while (true) 
			{
				nFirstPos = cOutString.Scan("||");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, 2, "+");
				}
				else
				{
					break;
				}
			}
			nFirstPos = strUpper.Scan("DROP TABLE_NAME ");
			if (nFirstPos != -1) 
			{
				cOutString = cOutString.Replace(nFirstPos, 16, "DROP TABLE ");
			}
			nFirstPos = strUpper.Scan("CREATE TABLE_NAME ");
			if (nFirstPos != -1) 
			{
				cOutString = cOutString.Replace(nFirstPos, 18, "CREATE TABLE ");
			}

			return "";
			#endregion
		}
		
		/// <summary>
		/// Extracts the SQLServer error number from the ODBC error text
		/// </summary>
		/// <param name="hSqlError"></param>
		/// <returns></returns>
		public static SalNumber GetMSErrorNo(SalSqlHandle hSqlError)
		{
			#region Local Variables
			SalNumber nError = 0;
			SalString strError = "";
			SalNumber nPos1 = 0;
			SalNumber nPos2 = 0;
			#endregion
			
			#region Actions
			hSqlError.GetError(ref nError, ref strError);
			if (nError > 20000 && nError < 60000) 
			{
				nPos1 = strError.Scan(":");
				nPos2 = strError.Scan("[");
				return strError.Mid(nPos1 + 1, nPos2 - nPos1 - 1).ToNumber();
			}
			else
			{
				return nError;
			}
			#endregion
		}
		
		/// <summary>
		/// Extracts the SQLServer error textfrom the ODBC error text
		/// </summary>
		/// <param name="hSqlError"></param>
		/// <returns></returns>
		public static SalString GetMSErrorText(SalSqlHandle hSqlError)
		{
			#region Local Variables
			SalNumber nError = 0;
			SalString strError = "";
			SalNumber nPos1 = 0;
			SalNumber nPos2 = 0;
			#endregion
			
			#region Actions
			hSqlError.GetError(ref nError, ref strError);
			if (nError > 20000 && nError < 60000) 
			{
				nPos1 = strError.Scan("[SQL Server]");
				return strError.Mid(nPos1 + 12, 254);
			}
			else
			{
				return strError;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="cInString"></param>
		/// <param name="cOutString"></param>
		/// <returns></returns>
		public static SalString SQLChangeComplex(SalString cInString, ref SalString cOutString)
		{
			#region Local Variables
			SalNumber nFirstPos = 0;
			SalNumber nPos2 = 0;
			SalNumber nStringLength = 0;
			SalString cBuffer = "";
			SalNumber nOpenBrackets = 0;
			SalNumber nCloseBrackets = 0;
			SalString cCondition = "";
			SalString cTrue = "";
			SalString cFalse = "";
			SalString cTemp = "";
			SalNumber I = 0;
			SalBoolean lStart = false;
			SalArray<SalString> cReplaceArray = new SalArray<SalString>(-1, 2);
			SalNumber nElements = 0;
			#endregion
			
			#region Actions
			cOutString = cInString;
			nFirstPos = 1;
			while (true)
			{
				nFirstPos = cOutString.Scan("@ISNA");
				if (nFirstPos != -1) 
				{
					nStringLength = cOutString.Length;
					cBuffer = cOutString.Right(nStringLength - nFirstPos);
					nPos2 = cBuffer.Scan(")");
					cBuffer = cBuffer.Left(nPos2);
					cTemp = cBuffer.Left(nPos2 + 1);
					nPos2 = cBuffer.Scan("(");
					cBuffer = cBuffer.Right(cBuffer.Length - nPos2 - 1);
					cOutString = cOutString.Left(nFirstPos) + " (select case Len( Cast(" + cBuffer + " as VarChar )) when 0 then 1 else 0 end) " + cOutString.Right(nStringLength - nFirstPos - cTemp.Length - 1);
				}
				else
				{
					break;
				}
			}
			// This Function Below Looks For IF(
			nFirstPos = 1;
			while (true)
			{
				nFirstPos = cOutString.Scan("@IF(");
				nCloseBrackets = 0;
				nOpenBrackets = 0;
				I = 0;
				cTrue = Const.strNULL;
				if (nFirstPos != -1) 
				{
					nStringLength = cOutString.Length;
					cBuffer = cOutString.Right(nStringLength - nFirstPos - 4);
					nPos2 = 0;
					// Loop HERE Until ")" is greater than "(" This gives you the End of the Function
					// If No '(' occurs in the Function then Just look for COMMAS 1=CONDITION 2=TRUERESULT 3=FALSERESULT
					// Add the First Part of the string then 'CASE "CONDITION"=TRUE Then "TRUERESULT" ELSE "FALSERESULT" END
					// During Loop If ',' if found and Number of '(' = ')'  then This is the "CONDITION" Same with Others
					while (true)
					{
						if (nCloseBrackets > nOpenBrackets) 
						{
							cFalse = cBuffer.Mid(nPos2 + 1, I - nPos2 - 1);
							nPos2 = I;
							break;
						}
						I = I + 1;
						cTemp = cBuffer.Mid(I, 1);
						if (cTemp == "(") 
						{
							nOpenBrackets = nOpenBrackets + 1;
						}
						if (cTemp == ")") 
						{
							nCloseBrackets = nCloseBrackets + 1;
						}
						if (cTemp == "," && nCloseBrackets == nOpenBrackets) 
						{
							if (nPos2 == 0) 
							{
								cCondition = cBuffer.Left(I);
								nPos2 = I;
							}
							else if (cTrue.Length <= 0) 
							{
								cTrue = cBuffer.Mid(nPos2 + 1, I - nPos2 - 1);
								nPos2 = I;
							}
						}
					}
					cOutString = cOutString.Left(nFirstPos) + "(SELECT CASE " + cCondition + " WHEN 1 THEN " + cTrue + " ELSE " + cFalse + " END) " + cBuffer.Right(cBuffer.Length - I - 1);
				}
				else
				{
					break;
				}
			}
			// this is what it must get converted to -> convert(char(10), DATETIME, 101)
			nFirstPos = 1;
			while (true)
			{
				nFirstPos = cOutString.Scan("@DATETOCHAR(");
				if (nFirstPos != -1) 
				{
					nStringLength = cOutString.Length;
					cBuffer = cOutString.Right(nStringLength - nFirstPos);
					nPos2 = cBuffer.Scan(")");
					cBuffer = cBuffer.Left(nPos2);
					cTemp = cBuffer.Left(nPos2 + 1);
					nPos2 = cBuffer.Scan(",");
					cBuffer = cBuffer.Left(nPos2);
					nPos2 = cBuffer.Scan("(");
					cBuffer = cBuffer.Right(cBuffer.Length - nPos2 - 1);
					cOutString = cOutString.Left(nFirstPos) + " convert(char(10)," + cBuffer + " ,103) " + cOutString.Right(nStringLength - nFirstPos - cTemp.Length - 1);
				}
				else
				{
					break;
				}
			}

			return "";
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="cInString"></param>
		/// <param name="hWndTbl"></param>
		/// <param name="cOutString"></param>
		/// <returns></returns>
		public static SalNumber SQLCol2Values(SalString cInString, SalWindowHandle hWndTbl, ref SalString cOutString)
		{
			#region Local Variables
			SalWindowHandle hWndChildCol = SalWindowHandle.Null;
			SalArray<SalString> cReplaceArray = new SalArray<SalString>(-1, 2);
			SalNumber nColID = 0;
			SalBoolean lOk = false;
			SalString cColName = "";
			SalString cTableName = "";
			SalNumber nDType = 0;
			SalString cColValue = "";
			SalNumber nFirstPos = 0;
			SalNumber nElements = 0;
			SalNumber I = 0;
			#endregion
			
			#region Actions
			cOutString = cInString;
			cTableName = hWndTbl.GetName();
			// Set cTableName = SalStrRightX ( cTableName, SalStrLength(cTableName)-3 )
			hWndChildCol = hWndTbl.GetFirstChild(Sys.TYPE_TableColumn);
			while (hWndChildCol != SalWindowHandle.Null) 
			{
				nColID = hWndChildCol.GetColumnID();
				cColName = hWndChildCol.GetName();
				// Set cColName = SalStrRightX ( cColName, SalStrLength(cColName)-3 )
				cReplaceArray[nColID - 1, 0] = ":" + cTableName + "." + cColName;
				lOk = hWndTbl.GetColumnText(nColID, ref cColValue);
				if (cColValue == SalString.Null) 
				{
					cColValue = "NULL";
				}
				nDType = hWndChildCol.GetDataType();
				if ((nDType == Sys.DT_LongString || nDType == Sys.DT_String || nDType == Sys.DT_DateTime) && cColValue != "NULL") 
				{
					cColValue = "\'" + cColValue + "\'";
					// Set cColValue =  SalStrTrimX( cColValue )  I removed this statement because it gave us problems with right justified columns
				}
				// Strip all Commas out of the String
				while (true) 
				{
					nFirstPos = cColValue.Scan(",");
					// If nFirstPos != -1
					// Call SalStrReplace( cColValue, nFirstPos,2,
					// 	"",cColValue)
					if (nFirstPos != -1) 
					{
						cColValue = cColValue.Replace(nFirstPos, 1, "");
					}
					else
					{
						break;
					}
				}
				cReplaceArray[nColID - 1, 1] = cColValue;
				hWndChildCol = hWndChildCol.GetNextChild(Sys.TYPE_TableColumn);
			}
			// Break for Readabuility
			nElements = cReplaceArray.GetUpperBound(1);
			I = 0;
			while (true)
			{
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + ",");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + ",");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + " ");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + " ");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + ")");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + ")");
				}
				I = I + 1;
				if (I > nElements) 
				{
					break;
				}
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="cInString"></param>
		/// <param name="hWndTbl"></param>
		/// <param name="cOutString"></param>
		/// <returns></returns>
		public static SalNumber SQLDF2Values(SalString cInString, SalWindowHandle hWndTbl, ref SalString cOutString)
		{
			#region Local Variables
			SalWindowHandle hWndChildCol = SalWindowHandle.Null;
			SalArray<SalString> cReplaceArray = new SalArray<SalString>(-1, 3);
			SalNumber nColID = 0;
			SalBoolean lOk = false;
			SalString cColName = "";
			SalString cTableName = "";
			SalNumber nDType = 0;
			SalString cColValue = "";
			SalNumber nFirstPos = 0;
			SalNumber nElements = 0;
			SalNumber I = 0;
			#endregion
			
			#region Actions
			cOutString = cInString;
			cTableName = hWndTbl.GetName();
			// Set cTableName = SalStrRightX ( cTableName, SalStrLength(cTableName)-3 )
			hWndChildCol = hWndTbl.GetFirstChild(Sys.TYPE_DataField);
			I = -1;
			while (hWndChildCol != SalWindowHandle.Null) 
			{
				I = I + 1;
				cColName = hWndChildCol.GetName();
				cReplaceArray[I, 0] = ":" + cTableName + "." + cColName;
				cReplaceArray[I, 2] = ":" + cColName;
				cColValue = hWndChildCol.GetFormattedText(true);
				if (cColValue == SalString.Null) 
				{
					cColValue = "NULL";
				}
				nDType = hWndChildCol.GetDataType();
				if ((nDType == Sys.DT_LongString || nDType == Sys.DT_String || nDType == Sys.DT_DateTime) && cColValue != "NULL") 
				{
					cColValue = "\'" + cColValue + "\'";
				}
				// Strip all Commas out of the String
				while (true) 
				{
					nFirstPos = cColValue.Scan(",");
					if (nFirstPos != -1) 
					{
						cColValue = cColValue.Replace(nFirstPos, 2, "");
					}
					else
					{
						break;
					}
				}
				cReplaceArray[I, 1] = cColValue;
				hWndChildCol = hWndChildCol.GetNextChild(Sys.TYPE_DataField);
			}
			// Break for Readabuility
			nElements = cReplaceArray.GetUpperBound(1);
			I = 0;
			while (true)
			{
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + ",");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + ",");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + " ");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + " ");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 0] + ")");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 0].Length + 1, cReplaceArray[I, 1] + ")");
				}
				I = I + 1;
				if (I > nElements) 
				{
					break;
				}
			}
			I = 0;
			while (true)
			{
				nFirstPos = cOutString.Scan(cReplaceArray[I, 2] + ",");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 2].Length + 1, cReplaceArray[I, 1] + ",");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 2] + " ");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 2].Length + 1, cReplaceArray[I, 1] + " ");
				}
				nFirstPos = cOutString.Scan(cReplaceArray[I, 2] + ")");
				if (nFirstPos != -1) 
				{
					cOutString = cOutString.Replace(nFirstPos, cReplaceArray[I, 2].Length + 1, cReplaceArray[I, 1] + ")");
				}
				I = I + 1;
				if (I > nElements) 
				{
					break;
				}
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strMsg"></param>
		/// <returns></returns>
		public static SalNumber GetErrNo(SalString strMsg)
		{
			#region Local Variables
			SalString strErrCode = "";
			#endregion
			
			#region Actions
			strErrCode = strMsg.Mid(21, 5);
			return strErrCode.ToNumber();
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strVal1"></param>
		/// <param name="strResult"></param>
		/// <returns></returns>
		public static SalNumber FrmDateTime(SalString strVal1, ref SalString strResult)
		{
			#region Local Variables
			SalString strDatePart = "";
			SalString strTimePart = "";
			#endregion
			
			#region Actions
			strDatePart = strVal1.Left(10);
			strTimePart = strVal1.Mid(11, 8);
			strResult = Vis.StrSubstitute(strDatePart + " " + strTimePart, ".", ":").Trim();

			return 0;
			#endregion
		}
		
		/// <summary>
		/// This is done for Sql Server 7 which only except the date in format 'mm/dd/yyyy'
		/// </summary>
		/// <param name="strDate"></param>
		/// <returns></returns>
		public static SalNumber ConstructDMY(ref SalString strDate)
		{
			#region Local Variables
			SalNumber nYear = 0;
			SalNumber nMonth = 0;
			SalNumber nDay = 0;
			SalString strYear = "";
			SalString strMonth = "";
			SalString strDay = "";
			SalString strTime = "";
			#endregion
			
			#region Actions
			strYear = strDate.Left(4);
			strMonth = strDate.Mid(5, 2);
			strDay = strDate.Mid(8, 2);
			strTime = strDate.Right(9);
			if (strMonth.Length == 1) 
			{
				strMonth = "0" + strMonth;
			}
			if (strDay.Length == 1) 
			{
				strDay = "0" + strDay;
			}
			// Set strTODAY = strMonth||'/'||strDay||'/'||strYear
			strDate = strDay + "/" + strMonth + "/" + strYear + strTime;

			return 0;
			#endregion
		}
	}
}
