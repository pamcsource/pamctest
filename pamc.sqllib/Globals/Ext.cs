// <ppj name="pamc.sqllib" date="04.09.2018 16:38:03" id="36844E8BF536645EEEF11AF9BE268BC3A19C5CCB"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;

namespace pamc.sqllib
{
	
	public static class Ext
	{
		
		/// <summary>
		/// Executes a stored procedure. If the stored procedure execution involves bind variables
		/// you should not use OdrExecuteProc. Use OdrPrepareProc instead.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalBoolean OdrExecuteProc(SalSqlHandle param1, SalString param2, SalString param3)
		{
			SalBoolean retVal = false;
			return retVal;
		}
		
		/// <summary>
		/// If the stored procedure executed using OdrExecuteProc returned more than one
		/// result set, use this function to get the second and subsequent result sets.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalBoolean OdrGetNextResults(SalSqlHandle param1, SalString param2, ref SalNumber param3)
		{
			SalBoolean retVal = false;
			return retVal;
		}
		
		/// <summary>
		/// This function gets the return status information for the stored procedure.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <returns></returns>
		public static SalBoolean OdrGetReturnStatus(SalSqlHandle param1, ref SalNumber param2)
		{
			SalBoolean retVal = false;
			return retVal;
		}
		
		/// <summary>
		/// If the stored procedure specified in the call to OdrPrepareProc returns more than
		/// one resut set, use this function followed by explicit execution  (SqlExecute) or
		/// implicit execution to get the next result set.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalBoolean OdrPrepareNextResults(SalSqlHandle param1, SalString param2, ref SalNumber param3)
		{
			SalBoolean retVal = false;
			return retVal;
		}
		
		/// <summary>
		/// This function prepares (compiles) the invocation of a stored procedure for
		/// subsequent execution. If the stored procedure execution involves bind variables
		/// you should use this function and not OdrExecuteProc.
        /// This function is used for preparation of standard sql commands like select and update as well.
        /// In .NET it is checking for standard commands and executing either a standard Sql.Prepare() or 
        /// a Sql.PrepareSP() for stored procedures.
		/// </summary>
		/// <param name="param1"></param>
		/// <param name="param2"></param>
		/// <param name="param3"></param>
		/// <returns></returns>
		public static SalBoolean OdrPrepareProc(SalSqlHandle param1, SalString param2, SalString param3)
		{
            SalBoolean retVal = false;
            SalString sqlCommandType = "";
            SalString sql = "";

            sqlCommandType = ((string)param2).Substring(0, 6).ToLower();

            //for standard sql commands like select, insert, update and delete, just use standard prepare
            if ((sqlCommandType == "select") || (sqlCommandType == "delete")
                || (sqlCommandType == "insert") || (sqlCommandType == "update"))
            {
                //if there are into variables, add INTO clause
                if (!param3.IsEmpty)
                    sql = param2 + " INTO " + param3;
                else
                    sql = param2;

                retVal = param1.Prepare(sql);
                return retVal;
            }

            //otherwise assume a storedprocedure should be prepared
            sql = param2;
            if (!((string)sql).StartsWith("dbo."))
            {
                sql = ((string)sql).Replace("'", "''");

                sql = "EXEC ('" + sql + "')";
            }
            retVal = param1.PrepareSP(sql, param3);
            return retVal;
        }
		
		#region odbsal32.dll
		
		/// <summary>
		/// Interop declarations for the external module odbsal32.dll
		/// </summary>
		private static class ODBSAL32
		{
			private const string LibraryFileName = "odbsal32.dll";
		}
		#endregion
	}
}
