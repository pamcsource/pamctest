// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	/// <summary>
	/// Main Table Window w/ From-To Dates
	/// </summary>
	public partial class clsMainTableHistory : clsMainTable
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsMainTableHistory()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsMainTableHistory(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// clsMainTableHistory WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMainTableHistory_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_AppendToKey:
					this.clsMainTableHistory_OnCM_AppendToKey(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_AppendToKey event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMainTableHistory_OnCM_AppendToKey(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.AppendToKey(" AND FROMDATE = :tbl" + this.strTableName + ".colFROMDATE");
			#endregion
		}
		
		/// <summary>
		/// colFROMDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colFROMDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.AM_Initialize:
					this.colFROMDATE_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void colFROMDATE_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.colFROMDATE.bRequired = true;
			#endregion
		}
		#endregion
		
		#region Multiple Inheritance Operators
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsMainTableHistory to type clsTopWindow.
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsTopWindow(clsMainTableHistory self)
		{
			return self._clsTopWindow;
		}
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsTopWindow to type clsMainTableHistory.
		/// </summary>
		/// <param name="super"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsMainTableHistory(clsTopWindow super)
		{
			return ((clsMainTableHistory)super._derived);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static clsMainTableHistory FromHandle(SalWindowHandle handle)
		{
			return ((clsMainTableHistory)SalWindow.FromHandle(handle, typeof(clsMainTableHistory)));
		}
		#endregion
	}
}
