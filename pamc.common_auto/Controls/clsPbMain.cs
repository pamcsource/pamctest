// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	/// <summary>
	/// Main module menu pushbutton class
	/// </summary>
	public class clsPbMain : SalPushbutton
	{
		#region Fields
		public SalWindowHandle hWndThis = SalWindowHandle.Null;
		public SalNumber nScreen = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsPbMain()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsPbMain(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// clsPbMain
			// 
			this.Name = "clsPbMain";
			this.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.Size = new System.Drawing.Size(85, 52);
			this.TransparentColor = System.Drawing.Color.Silver;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsPbMain_WindowActions);
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber ClearHandle()
		{
			#region Actions
			using (new SalContext(this))
			{
				hWndThis = SalWindowHandle.Null;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public SalNumber SetScreenNo(SalNumber n)
		{
			#region Actions
			using (new SalContext(this))
			{
				nScreen = n;
			}

			return 0;
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// clsPbMain WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsPbMain_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.clsPbMain_OnSAM_Create(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.clsPbMain_OnSAM_Click(sender, e);
					break;
				
				case Const.AM_DisEnable:
					this.clsPbMain_OnAM_DisEnable(sender, e);
					break;
				
				case Const.CM_Exit:
					this.clsPbMain_OnCM_Exit(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsPbMain_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.DisableWindow();
			this.SendMessage(Const.CM_SetScreenNo, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsPbMain_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Var.nSecurityLevel[this.nScreen] < 1) 
			{
				e.Return = false;
				return;
			}
			if (this.hWndThis == SalWindowHandle.Null) 
			{
				if (!(pamc.ez32bit.Int.CheckRes())) 
				{
					e.Return = false;
					return;
				}
				Var.nSEC = Var.nSecurityLevel[this.nScreen];
				// Call SalMessageBox( 'SYSTEM '||SalNumberToStrX( GetFreeSystemResources( GFSR_SYSTEMRESOURCES ),0 )||'
				// GDI '||SalNumberToStrX( GetFreeSystemResources( GFSR_GDIRESOURCES ),0 )||'
				// USER '||SalNumberToStrX( GetFreeSystemResources( GFSR_USERRESOURCES ),0 ), 'Free Resources', MB_Ok )
				Var.nScreenNo = this.nScreen;
				this.SendMessage(Const.CM_CreateWindow, 0, 0);
			}
			else
			{
				this.hWndThis.BringWindowToTop();
				pamc.ez32bit.Ext.ShowWindow(this.hWndThis, pamc.ez32bit.Const.SW_SHOWMAXIMIZED);
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsPbMain_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 1) 
			{
				this.DisableWindow();
				e.Return = true;
				return;
			}
			if (Var.nSecurityLevel[this.nScreen] > 0) 
			{
				this.EnableWindow();
			}
			else
			{
				this.DisableWindow();
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Exit event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsPbMain_OnCM_Exit(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.hWndThis != SalWindowHandle.Null) 
			{
				if (!(this.hWndThis.SendMessage(Const.CM_Exit, Sys.wParam, Sys.lParam))) 
				{
					e.Return = -1;
					return;
				}
			}
			#endregion
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static clsPbMain FromHandle(SalWindowHandle handle)
		{
			return ((clsPbMain)SalWindow.FromHandle(handle, typeof(clsPbMain)));
		}
		#endregion
	}
}
