// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	/// <summary>
	/// Multiline field. Editable. Responds to AM_DisEnable msg.
	/// </summary>
	public class clsMLEdit : SalMultilineField
	{
		// Multiple Inheritance: Base class instance.
		protected clsDataEntry _clsDataEntry;
		
		
		#region Fields
		public SalBoolean bEditable = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsMLEdit()
		{
			// Initialize second-base instances.
			InitializeMultipleInheritance();
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsMLEdit(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// Initialize second-base instances.
			InitializeMultipleInheritance();
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Multiple Inheritance Fields
		[Browsable(false)]
		[DesignerSerializationVisibility(0)]
		public SalWindowHandle hWndTblCol
		{
			get { return _clsDataEntry.hWndTblCol; }
			set { _clsDataEntry.hWndTblCol = value; }
		}
		[Browsable(false)]
		[DesignerSerializationVisibility(0)]
		public SalNumber nMyColor
		{
			get { return _clsDataEntry.nMyColor; }
			set { _clsDataEntry.nMyColor = value; }
		}
		[Browsable(false)]
		[DesignerSerializationVisibility(0)]
		public SalBoolean bRequired
		{
			get { return _clsDataEntry.bRequired; }
			set { _clsDataEntry.bRequired = value; }
		}
		[Browsable(false)]
		[DesignerSerializationVisibility(0)]
		public SalString strLabel
		{
			get { return _clsDataEntry.strLabel; }
			set { _clsDataEntry.strLabel = value; }
		}
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// clsMLEdit
			// 
			this.Name = "clsMLEdit";
			this.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.MaxLength = 32767;
			this.Size = new System.Drawing.Size(72, 21);
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsMLEdit_WindowActions);
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// clsMLEdit WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.AM_DisEnable:
					this.clsMLEdit_OnAM_DisEnable(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_SETFOCUS:
					this.clsMLEdit_OnWM_SETFOCUS(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_KILLFOCUS:
					this.clsMLEdit_OnWM_KILLFOCUS(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_CHAR:
					this.clsMLEdit_OnWM_CHAR(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_KEYDOWN:
					this.clsMLEdit_OnWM_KEYDOWN(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 1) 
			{
				this.bEditable = false;
				this.DisableWindow();
				this.EnableWindow();
			}
			else
			{
				// Call SalEnableWindow( hWndItem )
				this.bEditable = true;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_SETFOCUS event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_OnWM_SETFOCUS(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			else
			{
				this.SetWindowColor(Sys.COLOR_IndexWindow, Sys.COLOR_Yellow);
			}
			#endregion
		}
		
		/// <summary>
		/// WM_KILLFOCUS event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_OnWM_KILLFOCUS(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetWindowColor(Sys.COLOR_IndexWindow, Sys.COLOR_White);
			#endregion
		}
		
		/// <summary>
		/// WM_CHAR event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_OnWM_CHAR(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_KEYDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsMLEdit_OnWM_KEYDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		#endregion
		
		#region Multiple Inheritance Methods
		#endregion
		
		#region Multiple Inheritance Operators
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsMLEdit to type clsDataEntry.
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsDataEntry(clsMLEdit self)
		{
			return self._clsDataEntry;
		}
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsDataEntry to type clsMLEdit.
		/// </summary>
		/// <param name="super"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsMLEdit(clsDataEntry super)
		{
			return ((clsMLEdit)super._derived);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Multiple Inheritance: Initialize delegate instances.
		/// </summary>
		private void InitializeMultipleInheritance()
		{
			this._clsDataEntry = new clsDataEntry(this);
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static clsMLEdit FromHandle(SalWindowHandle handle)
		{
			return ((clsMLEdit)SalWindow.FromHandle(handle, typeof(clsMLEdit)));
		}
		#endregion
	}
}
