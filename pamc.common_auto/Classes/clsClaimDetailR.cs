// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	/// <summary>
	/// Defines a record of type CLAIM_DETAILS
	/// </summary>
	public class clsClaimDetailR : SalFunctionalClass
	{
		#region Fields
		public SalString strClaimNo = "";
		public SalNumber nAllowed = 0;
		public SalString strProcCode = "";
		public SalString strModif = "";
		public SalDateTime dtFromDateSvc = SalDateTime.Null;
		public SalNumber nQty = 0;
		public SalNumber nBilled = 0;
		public SalNumber nContrVal = 0;
		public SalNumber nCopay = 0;
		public SalNumber nWithhold = 0;
		public SalNumber nAdjust = 0;
		public SalString strAdjCode = "";
		public SalNumber nNet = 0;
		public SalString strP0 = "";
		public SalString strP1 = "";
		public SalString strP2 = "";
		public SalString strP3 = "";
		public SalString strP4 = "";
		public SalString strP5 = "";
		public SalString strP6 = "";
		public SalString strP7 = "";
		public SalDateTime dtToDateSvc = SalDateTime.Null;
		public SalString strDiagCode = "";
		public SalNumber nDiagRefNum = 0;
		public SalNumber nTblRowId = 0;
		public SalNumber nCreateBy = 0;
		public SalDateTime dtCreateDate = SalDateTime.Null;
		public SalNumber nLastChangeBy = 0;
		public SalDateTime dtLastChangeDate = SalDateTime.Null;
		public SalNumber nCovered = 0;
		public SalNumber nNotCovered = 0;
		public SalNumber nDeductible = 0;
		public SalNumber nAdjustWh = 0;
		public SalNumber nOOP = 0;
		public SalString strAdjCodeWh = "";
		public SalNumber nCapitated = 0;
		public SalNumber nCoveredYN = 0;
		public SalNumber nCountUtiliz = 0;
		public SalString strFinancialResp = "";
		public SalString strPayAction = "";
		public SalNumber nSequence = 0;
		public SalNumber nBenType = 0;
		public SalNumber nGlobalType = 0;
		public SalString strPHCode = "";
		public SalNumber nPended = 0;
		public SalNumber nOther = 0;
		public SalString strBundler = "";
		public SalString strBundlerTyp = "";
		public SalNumber nSavingsAcc = 0;
		public SalNumber nModAmount = 0;
		public SalNumber nCOPAY_CHANGED = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsClaimDetailR(){ }
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsClaimDetailR(object derived)
		{
			// Attach derived instance.
			this._derived = derived;
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Clears all the instance variables
		/// </summary>
		/// <returns></returns>
		public SalNumber Initialize()
		{
			#region Actions
			strClaimNo = SalString.Null;
			nAllowed = 0;
			strProcCode = SalString.Null;
			strModif = SalString.Null;
			dtFromDateSvc = SalDateTime.Null;
			nQty = 0;
			nBilled = 0;
			nContrVal = 0;
			nCopay = 0;
			nWithhold = 0;
			nAdjust = 0;
			strAdjCode = SalString.Null;
			nNet = 0;
			strP0 = SalString.Null;
			strP1 = SalString.Null;
			strP2 = SalString.Null;
			strP3 = SalString.Null;
			strP4 = SalString.Null;
			strP5 = SalString.Null;
			strP6 = SalString.Null;
			strP7 = SalString.Null;
			dtToDateSvc = SalDateTime.Null;
			strDiagCode = "N";
			nDiagRefNum = 1;
			nTblRowId = SalNumber.Null;
			nCreateBy = SalNumber.Null;
			dtCreateDate = SalDateTime.Null;
			nLastChangeBy = SalNumber.Null;
			dtLastChangeDate = SalDateTime.Null;
			nCovered = 0;
			nNotCovered = 0;
			nDeductible = 0;
			nAdjustWh = 0;
			nOOP = 0;
			strAdjCodeWh = SalString.Null;
			nCapitated = 0;
			nCoveredYN = 1;
			nCountUtiliz = 0;
			strFinancialResp = "EZ-CAP";
			strPayAction = "1";
			nSequence = SalNumber.Null;
			nBenType = SalNumber.Null;
			nGlobalType = SalNumber.Null;
			strPHCode = "P";
			nPended = 0;
			nOther = 0;
			strBundler = SalString.Null;
			strBundlerTyp = SalString.Null;
			nSavingsAcc = 0;
			nModAmount = 0;
			nCOPAY_CHANGED = 0;

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="PendDataDetails"></param>
		/// <returns></returns>
		public SalNumber CopyPendArray(clsPendDataDetails PendDataDetails)
		{
			#region Actions
			strP0 = PendDataDetails.strPendArray[0];
			strP1 = PendDataDetails.strPendArray[1];
			strP2 = PendDataDetails.strPendArray[2];
			strP3 = PendDataDetails.strPendArray[3];
			strP4 = PendDataDetails.strPendArray[4];
			strP5 = PendDataDetails.strPendArray[5];
			strP6 = PendDataDetails.strPendArray[6];
			strP7 = PendDataDetails.strPendArray[7];

			return 0;
			#endregion
		}
		#endregion
	}
}
