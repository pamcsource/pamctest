// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	/// <summary>
	/// Status form to show progress of some action.
	/// </summary>
	/// <param name="strDF1"></param>
	/// <param name="strDF2"></param>
	/// <param name="bCancel"></param>
	public partial class frmStatus : SalFormWindow
	{
		#region Window Parameters
		public SalString strDF1;
		public SalString strDF2;
		public SalBoolean bCancel;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public frmStatus(SalString strDF1, SalString strDF2, SalBoolean bCancel)
		{
			// Assign global reference.
			App.frmStatus = this;
			// Window Parameters initialization.
			this.strDF1 = strDF1;
			this.strDF2 = strDF2;
			this.bCancel = bCancel;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static frmStatus CreateWindow(Control owner, SalString strDF1, SalString strDF2, ref SalBoolean bCancel)
		{
			frmStatus frm = new frmStatus(strDF1, strDF2, bCancel);
			frm.Show(owner);
			bCancel = frm.bCancel;
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static frmStatus FromHandle(SalWindowHandle handle)
		{
			return ((frmStatus)SalWindow.FromHandle(handle, typeof(frmStatus)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <param name="strText"></param>
		/// <param name="nPercent"></param>
		/// <returns></returns>
		public SalNumber Update(SalString strText, SalNumber nPercent)
		{
			#region Actions
			using (new SalContext(this))
			{
				cc1.SetProgress(nPercent.Truncate(3, 0));
				df3.Text = strText;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public SalNumber UpdateText(SalString strText)
		{
			#region Local Variables
			SalString strShow = "";
			SalNumber i = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				df3.Text = strText;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="nPercent"></param>
		/// <returns></returns>
		public SalNumber UpdateMeter(SalNumber nPercent)
		{
			#region Actions
			using (new SalContext(this))
			{
				cc1.SetProgress(nPercent.Truncate(3, 0));
			}

			return 0;
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// frmStatus WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmStatus_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.frmStatus_OnSAM_Create(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_NCCREATE:
					this.frmStatus_OnWM_NCCREATE(sender, e);
					break;
				
				case Sys.SAM_Destroy:
					this.frmStatus_OnSAM_Destroy(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmStatus_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.CenterWindow();
			this.SetText(this.strDF1);
			this.df2.Text = this.strDF2;
            //PPJ:FINAL:MHO - Obsolete functions or constants used: YieldEnable. Replaced with Sal.Yield.
            //Sal.YieldEnable(true);
            //PPJ:FINAL:MHO - Obsolete functions or constants used: YieldStartMessages. Replaced with Sal.Yield.
            //Sal.YieldStartMessages(this.pbCancel);
            Sal.Yield(this.pbCancel);
            #endregion
        }
		
		/// <summary>
		/// WM_NCCREATE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmStatus_OnWM_NCCREATE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			//PPJ:FINAL:BB - CA:0054 SetWindowLong is not working when it is used to change form properties. Replace when possible with .NET properties.
			pamc.ez32bit.Int.SetWindowLong(this, pamc.ez32bit.Const.GWL_STYLE, pamc.ez32bit.Int.GetWindowLong(this, pamc.ez32bit.Const.GWL_STYLE) - pamc.ez32bit.Const.WS_ThickFrame);
			#endregion
		}
		
		/// <summary>
		/// SAM_Destroy event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmStatus_OnSAM_Destroy(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
            //PPJ:FINAL:MHO - Obsolete functions or constants used: YieldStopMessages. Removed YieldStopMessages.
            //Sal.YieldStopMessages();
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SalDestroyWindow( hWndForm )
			this.bCancel = true;
			#endregion
		}
		#endregion
	}
}
