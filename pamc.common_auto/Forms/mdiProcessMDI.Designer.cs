// <ppj name="pamc.common_auto" date="06.09.2018 16:52:40" id="43A7616F4257EA466F0017F06AF36370498B7A2B"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common_auto
{
	
	public partial class mdiProcessMDI
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
        #endregion


        #region Window Controls
        //PPJ:FINAL:AG: private mdiProcessMDI.NamedMenus.menuFile menu_menuFile;
        private App.NamedMenus.menuRecord menu_menuRecord;
        //PPJ:FINAL:AG: private mdiProcessMDI.NamedMenus.menuView menu_menuView;
        //PPJ:FINAL:AG:  private mdiProcessMDI.NamedMenus.menuProcessing menu_menuProcessing;
        private App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
        //PPJ:FINAL:AG: private mdiProcessMDI.NamedMenus.menuHelp menu_menuHelp;
        #endregion

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
            //PPJ:FINAL:AG: this.menu_menuFile = new mdiProcessMDI.NamedMenus.menuFile();
            this.menu_menuRecord = new App.NamedMenus.menuRecord();
            //PPJ:FINAL:AG: 	this.menu_menuView = new mdiProcessMDI.NamedMenus.menuView();
            //PPJ:FINAL:AG:   this.menu_menuProcessing = new mdiProcessMDI.NamedMenus.menuProcessing();
            this.menu_menuMDIWindows = new App.NamedMenus.menuMDIWindows();
            //PPJ:FINAL:AG: this.menu_menuHelp = new mdiProcessMDI.NamedMenus.menuHelp();
            this.ToolBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.BackColor = System.Drawing.Color.Gray;
			this.ToolBar.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.ToolBar.TabStop = true;
			this.ToolBar.Controls.Add(this.pbExit);
			this.ToolBar.Controls.Add(this.pbTable);
			this.ToolBar.Controls.Add(this.pbNotes);
			this.ToolBar.Controls.Add(this.pbMemo);
			this.ToolBar.Controls.Add(this.pbSearch);
			this.ToolBar.Controls.Add(this.pbLast);
			this.ToolBar.Controls.Add(this.pbFirst);
			this.ToolBar.Controls.Add(this.pbPrev);
			this.ToolBar.Controls.Add(this.pbNext);
			this.ToolBar.Controls.Add(this.pbSave);
			this.ToolBar.Controls.Add(this.pbDelete);
			this.ToolBar.Controls.Add(this.pbEdit);
			this.ToolBar.Controls.Add(this.pbNew);
			this.ToolBar.Controls.Add(this.picToolBar);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
            // 
            // MainMenu
            // 
            //PPJ:FINAL:AG: this.MainMenu.MenuItems.Add(this.menu_menuFile);
            this.MainMenu.MenuItems.Add(this.menu_menuRecord);
            //PPJ:FINAL:AG: this.MainMenu.MenuItems.Add(this.menu_menuView);
            //PPJ:FINAL:AG:   this.MainMenu.MenuItems.Add(this.menu_menuProcessing);
            this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
            //PPJ:FINAL:AG: this.MainMenu.MenuItems.Add(this.menu_menuHelp);
            // 
            // pbNew
            // 
            this.pbNew.Name = "pbNew";
			this.pbNew.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbNew.Location = new System.Drawing.Point(4, 5);
			this.pbNew.Size = new System.Drawing.Size(37, 27);
			this.pbNew.TabIndex = 0;
			// 
			// pbEdit
			// 
			this.pbEdit.Name = "pbEdit";
			this.pbEdit.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbEdit.Location = new System.Drawing.Point(41, 5);
			this.pbEdit.Size = new System.Drawing.Size(37, 27);
			this.pbEdit.TabIndex = 1;
			// 
			// pbDelete
			// 
			this.pbDelete.Name = "pbDelete";
			this.pbDelete.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbDelete.Location = new System.Drawing.Point(79, 5);
			this.pbDelete.Size = new System.Drawing.Size(37, 27);
			this.pbDelete.TabIndex = 2;
			// 
			// pbSave
			// 
			this.pbSave.Name = "pbSave";
			this.pbSave.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbSave.Location = new System.Drawing.Point(116, 5);
			this.pbSave.Size = new System.Drawing.Size(37, 27);
			this.pbSave.TabIndex = 3;
			// 
			// pbNext
			// 
			this.pbNext.Name = "pbNext";
			this.pbNext.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbNext.Location = new System.Drawing.Point(255, 5);
			this.pbNext.Size = new System.Drawing.Size(37, 27);
			this.pbNext.TabIndex = 4;
			// 
			// pbPrev
			// 
			this.pbPrev.Name = "pbPrev";
			this.pbPrev.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbPrev.Location = new System.Drawing.Point(217, 5);
			this.pbPrev.Size = new System.Drawing.Size(37, 27);
			this.pbPrev.TabIndex = 5;
			// 
			// pbFirst
			// 
			this.pbFirst.Name = "pbFirst";
			this.pbFirst.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbFirst.Location = new System.Drawing.Point(180, 5);
			this.pbFirst.Size = new System.Drawing.Size(37, 27);
			this.pbFirst.TabIndex = 6;
			// 
			// pbLast
			// 
			this.pbLast.Name = "pbLast";
			this.pbLast.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbLast.Location = new System.Drawing.Point(292, 5);
			this.pbLast.Size = new System.Drawing.Size(37, 27);
			this.pbLast.TabIndex = 7;
			// 
			// pbSearch
			// 
			this.pbSearch.Name = "pbSearch";
			this.pbSearch.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbSearch.Location = new System.Drawing.Point(355, 5);
			this.pbSearch.Size = new System.Drawing.Size(37, 27);
			this.pbSearch.TabIndex = 8;
			// 
			// pbMemo
			// 
			this.pbMemo.Name = "pbMemo";
			this.pbMemo.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbMemo.Location = new System.Drawing.Point(417, 5);
			this.pbMemo.Size = new System.Drawing.Size(37, 27);
			this.pbMemo.TabIndex = 9;
			// 
			// pbNotes
			// 
			this.pbNotes.Name = "pbNotes";
			this.pbNotes.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbNotes.Location = new System.Drawing.Point(456, 5);
			this.pbNotes.Size = new System.Drawing.Size(37, 27);
			this.pbNotes.TabIndex = 10;
			// 
			// pbTable
			// 
			this.pbTable.Name = "pbTable";
			this.pbTable.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbTable.Location = new System.Drawing.Point(517, 5);
			this.pbTable.Size = new System.Drawing.Size(37, 27);
			this.pbTable.TabIndex = 11;
			// 
			// pbExit
			// 
			this.pbExit.Name = "pbExit";
			this.pbExit.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbExit.Location = new System.Drawing.Point(744, 5);
			this.pbExit.Size = new System.Drawing.Size(37, 27);
			this.pbExit.TabIndex = 12;
			// 
			// picToolBar
			// 
			this.picToolBar.Name = "picToolBar";
			this.picToolBar.Location = new System.Drawing.Point(0, 5);
			this.picToolBar.Size = new System.Drawing.Size(793, 27);
			this.picToolBar.TabIndex = 13;
			// 
			// mdiProcessMDI
			// 
			this.Name = "mdiProcessMDI";
			this.ClientSize = new System.Drawing.Size(654, 391);
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Text = "EZ-PROCESS";
			this.Menu = this.MainMenu;
			this.ToolBar.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.mdiProcessMDI == this) 
			{
				App.mdiProcessMDI = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
