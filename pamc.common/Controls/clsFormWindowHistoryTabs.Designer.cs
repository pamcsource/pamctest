// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class clsFormWindowHistoryTabs
	{
		#region Window Controls
		public clsDataFieldEditDKey dfFROMDATE;
		public clsDataFieldEditDKey dfTODATE;
		public clsDataFieldDisabled dfCURRHIST;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// picTabs
			// 
			this.picTabs.Name = "picTabs";
			this.picTabs.TabSetup = "{Name=Name0;Label=Label0}";
			this.picTabs.UseVisualStyleBackColor = true;
			this.picTabs.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picTabs_WindowActions);
			this.picTabs.TabIndex = 0;
			// 
			// clsFormWindowHistoryTabs
			// 
			this.Controls.Add(this.picTabs);
			this.Name = "clsFormWindowHistoryTabs";
			this.ClientSize = new System.Drawing.Size(618, 201);
			this.BackColor = System.Drawing.Color.Silver;
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "(Untitled)";
			this.AutoScroll = false;
			this.ResumeLayout(false);
		}
		
		/// <summary>
		/// Enhanced component initialization method - used to initialize
		/// inherited controls. This method is not supported by the designer.
		/// </summary>
		private void InitializeComponentEx()
		{
			this.dfFROMDATE = this._clsFormWindowHistory.dfFROMDATE;
			this.dfTODATE = this._clsFormWindowHistory.dfTODATE;
			this.dfCURRHIST = this._clsFormWindowHistory.dfCURRHIST;
			this.SuspendLayout();
			// 
			// dfFROMDATE
			// 
			this.dfFROMDATE.Name = "dfFROMDATE";
			this.dfFROMDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFROMDATE.TabIndex = 0;
			// 
			// dfTODATE
			// 
			this.dfTODATE.Name = "dfTODATE";
			this.dfTODATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTODATE.TabIndex = 1;
			// 
			// dfCURRHIST
			// 
			this.dfCURRHIST.Name = "dfCURRHIST";
			this.dfCURRHIST.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCURRHIST.TabIndex = 2;
			// 
			// clsFormWindowHistoryTabs
			// 
			this.Controls.Add(this.dfCURRHIST);
			this.Controls.Add(this.dfTODATE);
			this.Controls.Add(this.dfFROMDATE);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
