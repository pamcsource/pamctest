﻿using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	/// <summary>
	/// Base class for text labels.
	/// </summary>
	public class clsDataFieldDisabled : clsDataField
	{
		#region Constructors/Destructors

		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsDataFieldDisabled()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

            //make the readonly datafield even not selectable
           
            Vis.WinSetStyle(this, 0x8000000, true); 
        }
		#endregion

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            // 
            // clsDataFieldDisabled
            // 
           
            this.ReadOnly = false;
            this.BackColor = System.Drawing.SystemColors.Control;
        }
		#endregion

		#region System Methods/Properties

		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static clsDataFieldDisabled FromHandle(SalWindowHandle handle)
		{
			return ((clsDataFieldDisabled)SalWindow.FromHandle(handle, typeof(clsDataFieldDisabled)));
		}
		#endregion
	}
}
