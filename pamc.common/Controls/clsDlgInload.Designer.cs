// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class clsDlgInload
	{
		#region Window Controls
		protected FCSalBackgroundText label1;
		public SalPushbutton pbInload;
		protected FCSalBackgroundText label2;
		// Pushbutton: pbLog
		// Message Actions
		// On SAM_Click
		// Call SalModalDialog( dlgView, hWndForm, strLogFile, 'View Log File' )
		// Call ViewReport( hWndForm, strLogFile, 'Inload Sets Log File', '' )
		// Call SalDisableWindow( hWndForm )
		// Call PrintLogFile( 'INLD', 'EZ-CAP Version 3.0 Inload Log File' )
		// Call SalBringWindowToTop( hWndForm )
		// Call SalEnableWindow( hWndForm )
		// Call PrintReportText( 'EZ-CAP Log File', 'D', ClientConfig.GetReportDir( )||'\\'||ClientConfig.GetReportLib( ), '_INLD'||strOpID||'.LOG', ClientConfig.GetOutputDir( ), 'EZ-CAP Version 3.0 Inload Log File' )
		public SalPushbutton pbClose;
		protected clsText label3;
		public clsDataFieldEditN dfSet;
		protected clsText label4;
		public clsDataFieldDisabled dfFile;
		public SalPushbutton pbFile;
		protected SalFrame frame2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new FCSalBackgroundText();
			this.pbInload = new PPJ.Runtime.Windows.SalPushbutton();
			this.label2 = new FCSalBackgroundText();
			this.pbClose = new PPJ.Runtime.Windows.SalPushbutton();
			this.label3 = new pamc.common.clsText();
			this.dfSet = new pamc.common.clsDataFieldEditN();
			this.label4 = new pamc.common.clsText();
			this.dfFile = new pamc.common.clsDataFieldDisabled();
			this.pbFile = new PPJ.Runtime.Windows.SalPushbutton();
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			this.ToolBar.Create = false;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.pbFile);
			this.ClientArea.Controls.Add(this.dfFile);
			this.ClientArea.Controls.Add(this.dfSet);
			this.ClientArea.Controls.Add(this.pbClose);
			this.ClientArea.Controls.Add(this.pbInload);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.frame2);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Create = false;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.BackColor = System.Drawing.Color.Gray;
			this.label1.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.label1.ForeColor = System.Drawing.Color.Gray;
			this.label1.Text = "&I";
			this.label1.Location = new System.Drawing.Point(78, 8);
			this.label1.Size = new System.Drawing.Size(10, 16);
			this.label1.TabIndex = 0;
			// 
			// pbInload
			// 
			this.pbInload.Name = "pbInload";
			this.pbInload.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbInload.AcceleratorKey = Keys.Enter;
			this.pbInload.Location = new System.Drawing.Point(4, 0);
			this.pbInload.Size = new System.Drawing.Size(65, 30);
			this.pbInload.TransparentColor = System.Drawing.Color.Silver;
			this.pbInload.ImageName = "inload.bmp";
			this.pbInload.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbInload_WindowActions);
			this.pbInload.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.BackColor = System.Drawing.Color.Gray;
			this.label2.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.label2.ForeColor = System.Drawing.Color.Gray;
			this.label2.Text = "&L";
			this.label2.Location = new System.Drawing.Point(147, 6);
			this.label2.Size = new System.Drawing.Size(21, 16);
			this.label2.TabIndex = 2;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.pbClose.AcceleratorKey = Keys.Escape;
			this.pbClose.Location = new System.Drawing.Point(346, 0);
			this.pbClose.Size = new System.Drawing.Size(65, 30);
			this.pbClose.TransparentColor = System.Drawing.Color.Silver;
			this.pbClose.ImageName = "close3.bmp";
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Start Numbering New Sets at:";
			this.label3.Location = new System.Drawing.Point(40, 94);
			this.label3.Size = new System.Drawing.Size(173, 16);
			this.label3.TabIndex = 4;
			// 
			// dfSet
			// 
			this.dfSet.Name = "dfSet";
			this.dfSet.MaxLength = 6;
			this.dfSet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSet.Format = "#0";
			this.dfSet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfSet.Location = new System.Drawing.Point(319, 92);
			this.dfSet.Size = new System.Drawing.Size(49, 20);
			this.dfSet.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label4.ForeColor = System.Drawing.Color.Navy;
			this.label4.Text = "Inload From File:";
			this.label4.Location = new System.Drawing.Point(10, 173);
			this.label4.Size = new System.Drawing.Size(89, 16);
			this.label4.TabIndex = 6;
			// 
			// dfFile
			// 
			this.dfFile.Name = "dfFile";
			this.dfFile.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFile.Location = new System.Drawing.Point(87, 197);
			this.dfFile.Size = new System.Drawing.Size(317, 16);
			this.dfFile.TabIndex = 7;
			// 
			// pbFile
			// 
			this.pbFile.Name = "pbFile";
			this.pbFile.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbFile.Text = "&File...";
			this.pbFile.Location = new System.Drawing.Point(10, 193);
			this.pbFile.Size = new System.Drawing.Size(73, 25);
			this.pbFile.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbFile_WindowActions);
			this.pbFile.TabIndex = 8;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.Color.Silver;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.DropShadow;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(23, 84);
			this.frame2.Size = new System.Drawing.Size(364, 35);
			this.frame2.TabIndex = 9;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Visible = false;
			this.pbOk.TabIndex = 10;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.TabIndex = 11;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 12;
			// 
			// clsDlgInload
			// 
			this.Name = "clsDlgInload";
			this.ClientSize = new System.Drawing.Size(421, 231);
			this.Text = "Set Inload";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsDlgInload_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
