﻿using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	/// <summary>
	/// Base radio button class
	/// </summary>
	public class FCSalRadioButton : SalRadioButton
	{
		#region Constructors/Destructors

		/// <summary>
		/// Default Constructor.
		/// </summary>
		public FCSalRadioButton()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// FCSalRadioButton
			// 
			this.UseCompatibleTextRendering = true;
		}
		#endregion

		#region System Methods/Properties

		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static FCSalRadioButton FromHandle(SalWindowHandle handle)
		{
			return ((FCSalRadioButton)SalWindow.FromHandle(handle, typeof(FCSalRadioButton)));
		}
		#endregion
	}
}
