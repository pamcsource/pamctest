// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class clsDlgInfoFormProviders
	{
		#region Window Controls
		public clsDataFieldDisabled dfPROVID;
		public clsDataFieldEdit dfTYPE;
		public clsDataFieldDisabled dfVERIFY;
		public clsCheckBox cbVERIFY;
		protected clsText label3;
		public clsDataFieldEditD dfVERIFYDATE;
		protected clsText label4;
		// On AM_GetValue
		// If dfVERIFY = 'Y' AND nSEC > 1
		// Call SalEnableWindowAndLabel(dfVERIFYRESULT)
		public clsDataFieldEdit dfVERIFYRESULT;
		protected clsText label5;
		// On AM_GetValue
		// If dfVERIFY = 'Y' AND nSEC > 1
		// Call SalEnableWindowAndLabel(dfVERIFYNEXT)
		public clsDataFieldEditD dfVERIFYNEXT;
		protected clsText label6;
		public clsMLEdit dfNOTES;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfPROVID = new pamc.common.clsDataFieldDisabled();
			this.dfTYPE = new pamc.common.clsDataFieldEdit();
			this.dfVERIFY = new pamc.common.clsDataFieldDisabled();
			this.cbVERIFY = new pamc.common.clsCheckBox();
			this.label3 = new pamc.common.clsText();
			this.dfVERIFYDATE = new pamc.common.clsDataFieldEditD();
			this.label4 = new pamc.common.clsText();
			this.dfVERIFYRESULT = new pamc.common.clsDataFieldEdit();
			this.label5 = new pamc.common.clsText();
			this.dfVERIFYNEXT = new pamc.common.clsDataFieldEditD();
			this.label6 = new pamc.common.clsText();
			this.dfNOTES = new pamc.common.clsMLEdit();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.dfNOTES);
			this.ClientArea.Controls.Add(this.dfVERIFYNEXT);
			this.ClientArea.Controls.Add(this.dfVERIFYRESULT);
			this.ClientArea.Controls.Add(this.dfVERIFYDATE);
			this.ClientArea.Controls.Add(this.cbVERIFY);
			this.ClientArea.Controls.Add(this.dfVERIFY);
			this.ClientArea.Controls.Add(this.dfTYPE);
			this.ClientArea.Controls.Add(this.dfPROVID);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.dfSEQUENCE);
			this.ClientArea.Controls.Add(this.pbNew);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label6);
			this.ClientArea.Controls.Add(this.label5);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&A";
			this.label1.TabIndex = 1;
			// 
			// pbNew
			// 
			this.pbNew.Name = "pbNew";
			this.pbNew.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbNew.TabIndex = 2;
			// 
			// dfSEQUENCE
			// 
			this.dfSEQUENCE.Name = "dfSEQUENCE";
			this.dfSEQUENCE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfSEQUENCE.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Sequence No:";
			this.label2.TabIndex = 4;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 5;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 6;
			// 
			// dfPROVID
			// 
			this.dfPROVID.Name = "dfPROVID";
			this.dfPROVID.Visible = false;
			this.dfPROVID.MaxLength = 20;
			this.dfPROVID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPROVID.Location = new System.Drawing.Point(63, 51);
			this.dfPROVID.Size = new System.Drawing.Size(124, 19);
			this.dfPROVID.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPROVID_WindowActions);
			this.dfPROVID.TabIndex = 7;
			// 
			// dfTYPE
			// 
			this.dfTYPE.Name = "dfTYPE";
			this.dfTYPE.MaxLength = 10;
			this.dfTYPE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTYPE.Location = new System.Drawing.Point(419, 42);
			this.dfTYPE.Size = new System.Drawing.Size(82, 20);
			this.dfTYPE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfTYPE_WindowActions);
			this.dfTYPE.TabIndex = 8;
			// 
			// dfVERIFY
			// 
			this.dfVERIFY.Name = "dfVERIFY";
			this.dfVERIFY.Visible = false;
			this.dfVERIFY.MaxLength = 1;
			this.dfVERIFY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFY.Location = new System.Drawing.Point(135, 231);
			this.dfVERIFY.Size = new System.Drawing.Size(72, 19);
			this.dfVERIFY.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfVERIFY_WindowActions);
			this.dfVERIFY.TabIndex = 9;
			// 
			// cbVERIFY
			// 
			this.cbVERIFY.Name = "cbVERIFY";
			this.cbVERIFY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbVERIFY.Text = "Verified?";
			this.cbVERIFY.Location = new System.Drawing.Point(12, 189);
			this.cbVERIFY.Size = new System.Drawing.Size(80, 24);
			this.cbVERIFY.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbVERIFY_WindowActions);
			this.cbVERIFY.TabIndex = 10;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Date of Verification:";
			this.label3.Location = new System.Drawing.Point(134, 193);
			this.label3.Size = new System.Drawing.Size(117, 16);
			this.label3.TabIndex = 11;
			// 
			// dfVERIFYDATE
			// 
			this.dfVERIFYDATE.Name = "dfVERIFYDATE";
			this.dfVERIFYDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYDATE.Location = new System.Drawing.Point(254, 191);
			this.dfVERIFYDATE.Size = new System.Drawing.Size(66, 20);
			this.dfVERIFYDATE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfVERIFYDATE_WindowActions);
			this.dfVERIFYDATE.TabIndex = 12;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Result:";
			this.label4.Location = new System.Drawing.Point(12, 221);
			this.label4.Size = new System.Drawing.Size(40, 16);
			this.label4.TabIndex = 13;
			// 
			// dfVERIFYRESULT
			// 
			this.dfVERIFYRESULT.Name = "dfVERIFYRESULT";
			this.dfVERIFYRESULT.MaxLength = 10;
			this.dfVERIFYRESULT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYRESULT.Location = new System.Drawing.Point(56, 219);
			this.dfVERIFYRESULT.Size = new System.Drawing.Size(72, 20);
			this.dfVERIFYRESULT.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfVERIFYRESULT_WindowActions);
			this.dfVERIFYRESULT.TabIndex = 14;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "Next Verification:";
			this.label5.Location = new System.Drawing.Point(134, 221);
			this.label5.Size = new System.Drawing.Size(112, 16);
			this.label5.TabIndex = 15;
			// 
			// dfVERIFYNEXT
			// 
			this.dfVERIFYNEXT.Name = "dfVERIFYNEXT";
			this.dfVERIFYNEXT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYNEXT.Location = new System.Drawing.Point(254, 219);
			this.dfVERIFYNEXT.Size = new System.Drawing.Size(66, 20);
			this.dfVERIFYNEXT.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfVERIFYNEXT_WindowActions);
			this.dfVERIFYNEXT.TabIndex = 16;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label6.Text = "Notes:";
			this.label6.Location = new System.Drawing.Point(333, 174);
			this.label6.Size = new System.Drawing.Size(66, 16);
			this.label6.TabIndex = 17;
			// 
			// dfNOTES
			// 
			this.dfNOTES.Name = "dfNOTES";
			this.dfNOTES.MaxLength = 254;
			this.dfNOTES.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNOTES.Location = new System.Drawing.Point(333, 191);
			this.dfNOTES.Size = new System.Drawing.Size(167, 48);
			this.dfNOTES.TabIndex = 18;
			// 
			// clsDlgInfoFormProviders
			// 
			this.Name = "clsDlgInfoFormProviders";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsDlgInfoFormProviders_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
