// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Radio button. Responds to the AM_DisEnable msg.
	/// </summary>
	public class clsRadioEdit : clsRadio
	{
		#region Fields
		public SalBoolean bEditable = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsRadioEdit()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsRadioEdit(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// clsRadioEdit
			// 
			this.Name = "clsRadioEdit";
			this.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsRadioEdit_WindowActions);
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// clsRadioEdit WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.AM_DisEnable:
					this.clsRadioEdit_OnAM_DisEnable(sender, e);
					break;
				
				// On SAM_Create
				
				// Set bEditable = TRUE
				
				case pamc.ez32bit.Const.WM_LBUTTONDOWN:
					this.clsRadioEdit_OnWM_LBUTTONDOWN(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_LBUTTONDBLCLK:
					this.clsRadioEdit_OnWM_LBUTTONDBLCLK(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_CHAR:
					this.clsRadioEdit_OnWM_CHAR(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_SETFOCUS:
					this.clsRadioEdit_OnWM_SETFOCUS(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 1) 
			{
				// Call SalDisableWindow( hWndItem )
				this.bEditable = false;
			}
			else
			{
				// Call SalEnableWindow( hWndItem )
				this.bEditable = true;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_LBUTTONDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_OnWM_LBUTTONDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_LBUTTONDBLCLK event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_OnWM_LBUTTONDBLCLK(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
            //PPJ:FINAL:MHO - CA:0039 When WM_RBUTTONDOWN/WM_LBUTTONDBLCLK are handled, 0 has to be returned so that application captures mouse.
            e.Return = 0;
            #endregion
        }
		
		/// <summary>
		/// WM_CHAR event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_OnWM_CHAR(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_SETFOCUS event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsRadioEdit_OnWM_SETFOCUS(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bEditable)) 
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static clsRadioEdit FromHandle(SalWindowHandle handle)
		{
			return ((clsRadioEdit)SalWindow.FromHandle(handle, typeof(clsRadioEdit)));
		}
		#endregion
	}
}
