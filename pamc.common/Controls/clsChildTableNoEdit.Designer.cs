// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class clsChildTableNoEdit
	{
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// colSEQUENCE
			// 
			this.colSEQUENCE.Name = "colSEQUENCE";
			this.colSEQUENCE.Position = 1;
			// 
			// colROWID
			// 
			this.colROWID.Name = "colROWID";
			this.colROWID.Position = 2;
			// 
			// colCreateBy
			// 
			this.colCreateBy.Name = "colCreateBy";
			this.colCreateBy.Position = 3;
			// 
			// colCreateDate
			// 
			this.colCreateDate.Name = "colCreateDate";
			this.colCreateDate.Position = 4;
			// 
			// colLastChangeBy
			// 
			this.colLastChangeBy.Name = "colLastChangeBy";
			this.colLastChangeBy.Position = 5;
			// 
			// colLastChangeDate
			// 
			this.colLastChangeDate.Name = "colLastChangeDate";
			this.colLastChangeDate.Position = 6;
			// 
			// clsChildTableNoEdit
			// 
			this.Controls.Add(this.colSEQUENCE);
			this.Controls.Add(this.colROWID);
			this.Controls.Add(this.colCreateBy);
			this.Controls.Add(this.colCreateDate);
			this.Controls.Add(this.colLastChangeBy);
			this.Controls.Add(this.colLastChangeDate);
			this.Name = "clsChildTableNoEdit";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsChildTableNoEdit_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
