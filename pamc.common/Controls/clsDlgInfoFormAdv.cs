// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// clsDlgInfoForm dialog box w/ Next and Previous buttons
	/// </summary>
	public partial class clsDlgInfoFormAdv : clsDlgInfoForm
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsDlgInfoFormAdv()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsDlgInfoFormAdv(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// pbPrevious WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrevious_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbPrevious_OnSAM_Click(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_MOUSEMOVE:
					this.pbPrevious_OnWM_MOUSEMOVE(sender, e);
					break;
				
				case Const.AM_DisEnable:
					this.pbPrevious_OnAM_DisEnable(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrevious_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if ((pamc.price.Const.MODE_New == this.nFormMode) || (pamc.price.Const.MODE_Edit == this.nFormMode)) 
			{
				if (!(this.SendMessage(Const.CM_Save, 1, 0))) 
				{
					e.Return = false;
					return;
				}
			}
			// hWndTable will set the current table position to lParam, for example: TBLPOS_Last, TBLPOS_first, TBLPOS_Clear
			this.hWndTable.SendMessage(Const.CM_Skip, Const.SKIP_PREV, Sys.lParam);
			this.pbPrevious.SendMessage(Const.AM_DisEnable, Sys.wParam, clsChildTableNoEdit.FromHandle(this.hWndTable).GetRowPos());
			this.pbNext.SendMessage(Const.AM_DisEnable, Sys.wParam, clsChildTableNoEdit.FromHandle(this.hWndTable).GetRowPos());
			// Disable the Next and Previous buttons
			// Call SalMessageBox('Top of the list', '', MB_Ok)
			this.SendMessage(Const.CM_Populate, Sys.wParam, Sys.lParam);
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// WM_MOUSEMOVE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrevious_OnWM_MOUSEMOVE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetStatusBarText("Move to previous record");
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbPrevious_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.lParam == Const.TBLPOS_First) 
			{
				this.pbPrevious.DisableWindow();
			}
			else if ((Const.TBLPOS_Last == Sys.lParam) || (Const.TBLPOS_Records == Sys.lParam)) 
			{
				this.pbPrevious.EnableWindow();
			}
			#endregion
		}
		
		/// <summary>
		/// pbNext WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbNext_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbNext_OnSAM_Click(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_MOUSEMOVE:
					this.pbNext_OnWM_MOUSEMOVE(sender, e);
					break;
				
				case Const.AM_DisEnable:
					this.pbNext_OnAM_DisEnable(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbNext_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if ((pamc.price.Const.MODE_New == this.nFormMode) || (pamc.price.Const.MODE_Edit == this.nFormMode)) 
			{
				if (!(this.SendMessage(Const.CM_Save, 1, 0))) 
				{
					e.Return = false;
					return;
				}
			}
			// Disable the Next and Previous buttons
			this.hWndTable.SendMessage(Const.CM_Skip, Const.SKIP_NEXT, Sys.lParam);
			this.pbPrevious.SendMessage(Const.AM_DisEnable, Sys.wParam, clsChildTableNoEdit.FromHandle(this.hWndTable).GetRowPos());
			this.pbNext.SendMessage(Const.AM_DisEnable, Sys.wParam, clsChildTableNoEdit.FromHandle(this.hWndTable).GetRowPos());
			this.SendMessage(Const.CM_Populate, Sys.wParam, Sys.lParam);
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// WM_MOUSEMOVE event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbNext_OnWM_MOUSEMOVE(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetStatusBarText("Move to next record");
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbNext_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.lParam == Const.TBLPOS_Last) 
			{
				this.pbNext.DisableWindow();
			}
			else if ((Const.TBLPOS_Records == Sys.lParam) || (Const.TBLPOS_First == Sys.lParam)) 
			{
				this.pbNext.EnableWindow();
			}
			#endregion
		}
		#endregion
		
		#region Multiple Inheritance Operators
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsDlgInfoFormAdv to type clsInfoFormTable.
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsInfoFormTable(clsDlgInfoFormAdv self)
		{
			return self._clsInfoFormTable;
		}
		
		/// <summary>
		/// Multiple Inheritance: Cast operator from type clsInfoFormTable to type clsDlgInfoFormAdv.
		/// </summary>
		/// <param name="super"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static implicit operator clsDlgInfoFormAdv(clsInfoFormTable super)
		{
			return ((clsDlgInfoFormAdv)super._derived);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static clsDlgInfoFormAdv FromHandle(SalWindowHandle handle)
		{
			return ((clsDlgInfoFormAdv)SalWindow.FromHandle(handle, typeof(clsDlgInfoFormAdv)));
		}
		#endregion
	}
}
