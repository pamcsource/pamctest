// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class clsFrmMainModule
	{
		#region Window Controls
		// On SAM_Click
		// If wParam = 1 OR SalMessageBox( 'Are you really sure you want to leave this module?', 'Exit', MB_YesNo|MB_IconExclamation ) = IDYES
		// If CloseAllScreens( )
		// Call SalDestroyWindow( hWndForm )
		// Return TRUE
		// Else
		// Return FALSE
		public clsToolButton pbClose;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbClose = new pamc.common.clsToolButton();
			this.SuspendLayout();
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.AcceleratorKey = Keys.Escape;
			this.pbClose.Location = new System.Drawing.Point(342, 22);
			this.pbClose.Size = new System.Drawing.Size(38, 33);
			this.pbClose.ImageName = "close2.bmp";
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 0;
			// 
			// clsFrmMainModule
			// 
			this.Controls.Add(this.pbClose);
			this.Name = "clsFrmMainModule";
			this.ClientSize = new System.Drawing.Size(381, 223);
			this.BackColor = System.Drawing.Color.Teal;
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.ControlBox = false;
			this.Text = "(Untitled)";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsFrmMainModule_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
