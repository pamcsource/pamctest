// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Table column base class.
	/// </summary>
	public class clsCol : SalTableColumn
	{
		#region Fields
		public SalString strDBFieldName = "";
		public SalString strDBTableName = "";
		public SalString strWindowName = "";
		public SalBoolean bRequired = false;
		public SalNumber nIndex = 0;
		public SalWindowHandle hDfHandle = SalWindowHandle.Null;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsCol()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsCol(ISalWindow derived)
		{
			// Attach derived instance.
			this._derived = derived;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			// Attach actions handler to derived instance.
			this._derived.AttachMessageActions(this);
		}
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// clsCol
			// 
			this.Name = "clsCol";
			this.Title = "(Untitled)";
			this.Enabled = false;
			this.Format = "";
			this.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.clsCol_WindowActions);
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <param name="strFld"></param>
		/// <param name="strTbl"></param>
		/// <param name="strWin"></param>
		/// <param name="bReq"></param>
		/// <returns></returns>
		public SalNumber Initialize(SalString strFld, SalString strTbl, SalString strWin, SalBoolean bReq)
		{
			#region Actions
			using (new SalContext(this))
			{
				strDBFieldName = strFld;
				strDBTableName = strTbl;
				strWindowName = strWin;
				bRequired = bReq;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalString GetTbl()
		{
			#region Actions
			using (new SalContext(this))
			{
				return strDBTableName;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strTbl"></param>
		/// <returns></returns>
		public SalNumber SetTbl(SalString strTbl)
		{
			#region Actions
			using (new SalContext(this))
			{
				strDBTableName = strTbl;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalString GetField()
		{
			#region Actions
			using (new SalContext(this))
			{
				return strDBFieldName;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strFld"></param>
		/// <returns></returns>
		public SalNumber SetField(SalString strFld)
		{
			#region Actions
			using (new SalContext(this))
			{
				strDBFieldName = strFld;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalString GetWindow()
		{
			#region Actions
			using (new SalContext(this))
			{
				return strWindowName;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="strWin"></param>
		/// <returns></returns>
		public SalNumber SetWindow(SalString strWin)
		{
			#region Actions
			using (new SalContext(this))
			{
				strWindowName = strWin;
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalBoolean GetRequired()
		{
			#region Actions
			using (new SalContext(this))
			{
				return bRequired;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="bReq"></param>
		/// <returns></returns>
		public SalBoolean SetRequired(SalBoolean bReq)
		{
			#region Actions
			using (new SalContext(this))
			{
				bRequired = bReq;
			}

			return false;
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// clsCol WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.clsCol_OnSAM_Create(sender, e);
					break;
				
				case Const.AM_DisEnable:
					this.clsCol_OnAM_DisEnable(sender, e);
					break;
				
				case Const.CM_GetDBName:
					this.clsCol_OnCM_GetDBName(sender, e);
					break;
				
				case Const.CM_GetFieldName:
					this.clsCol_OnCM_GetFieldName(sender, e);
					break;
				
				case Const.CM_GetWindowName:
					this.clsCol_OnCM_GetWindowName(sender, e);
					break;
				
				case Const.CM_IsRequired:
					this.clsCol_OnCM_IsRequired(sender, e);
					break;
				
				case Const.CM_ColumnPos:
					this.clsCol_OnCM_ColumnPos(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_RBUTTONDOWN:
					this.clsCol_OnWM_RBUTTONDOWN(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_KEYDOWN:
					this.clsCol_OnWM_KEYDOWN(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Changed 2/20/96 - Don't send AM_Initialize and Set default values for DBFieldName, DBTableName, and strWindowName
			// 	first, then send message
			this.strDBFieldName = this.GetName();
			this.strDBFieldName = this.strDBFieldName.Mid(3, this.strDBFieldName.Length - 3).ToUpper();
			this.strDBTableName = Sys.hWndForm.GetName();
			this.strDBTableName = this.strDBTableName.Mid(3, this.strDBTableName.Length - 3);
			this.SendMessage(Const.CM_SetInstanceVars, 0, 0);
			this.strWindowName = this.GetName();
			// If strDBFieldName = ''
			// Set h1ColChild[nColCount] = hWndItem
			// Set strColArray[nColCount] = strDBFieldName
			// Set nColCount = nColCount + 1
			// If strDBTableName = ''
			// If strWindowName = ''
			#endregion
		}
		
		/// <summary>
		/// AM_DisEnable event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnAM_DisEnable(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == 1) 
			{
				this.SetColumnFlags(Sys.COL_Editable, false);
			}
			else
			{
				this.SetColumnFlags(Sys.COL_Editable, true);
			}
			#endregion
		}
		
		/// <summary>
		/// CM_GetDBName event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnCM_GetDBName(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Int.PushString(this.strDBTableName);
			#endregion
		}
		
		/// <summary>
		/// CM_GetFieldName event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnCM_GetFieldName(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Int.PushString(this.strDBFieldName);
			#endregion
		}
		
		/// <summary>
		/// CM_GetWindowName event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnCM_GetWindowName(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Int.PushString(this.strWindowName);
			#endregion
		}
		
		/// <summary>
		/// CM_IsRequired event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnCM_IsRequired(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// If bRequired = TRUE
			// Call PushString('1')
			// Else
			// Call PushString('0')
			e.Return = this.bRequired;
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_ColumnPos event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnCM_ColumnPos(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Int.PushString(this.nIndex.ToString(3));
			#endregion
		}
		
		/// <summary>
		/// WM_RBUTTONDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnWM_RBUTTONDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this == Sal.GetFocus()) 
			{
				if (((bool)Sal.GetFocus().SendMessage(Const.AM_Lookup, 0, 0)) && Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
				{
					Sys.hWndForm.SendMessage(Const.CM_DoLookup, 0, 0);
				}
			}
            //PPJ:FINAL:MHO - CA:0039 When WM_RBUTTONDOWN/WM_LBUTTONDBLCLK are handled, 0 has to be returned so that application captures mouse.
            e.Return = 0;
            #endregion
        }
		
		/// <summary>
		/// WM_KEYDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void clsCol_OnWM_KEYDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sys.wParam == Vis.VK_F2) 
			{
				if (((bool)Sal.GetFocus().SendMessage(Const.AM_Lookup, 0, 0)) && Sys.VALIDATE_Ok == Sal.SendValidateMsg()) 
				{
					Sys.hWndForm.SendMessage(Const.CM_DoLookup, 0, 0);
				}
                //PPJ:FINAL:MHO - CA:0038 When key action is overriden at WM_KEYDOWN, 0 has to be returned to stop default key action.
                e.Return = 0;
            }
			#endregion
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static clsCol FromHandle(SalWindowHandle handle)
		{
			return ((clsCol)SalWindow.FromHandle(handle, typeof(clsCol)));
		}
		#endregion
	}
}
