﻿using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	/// <summary>
	/// Base group box class
	/// </summary>
	public class FCSalGroupBox : SalGroupBox
	{
		#region Constructors/Destructors

		/// <summary>
		/// Default Constructor.
		/// </summary>
		public FCSalGroupBox()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// FCSalGroupBox
			// 
			this.UseCompatibleTextRendering = true;
		}
		#endregion

		#region System Methods/Properties

		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static FCSalGroupBox FromHandle(SalWindowHandle handle)
		{
			return ((FCSalGroupBox)SalWindow.FromHandle(handle, typeof(FCSalGroupBox)));
		}
		#endregion
	}
}
