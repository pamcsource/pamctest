// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class frmADJUST_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		// If NOT SalIsNull( hWndItem ) AND SqlLookup( 'Select CODE From ADJUST_CODES Where CODE = :dfCODE', nGInd )
		// Call ValidateErrorMsgBox( 'Adjustment Reason Code already exists, try again.' )
		// Return VALIDATE_Cancel
		public clsDataFieldEditKey dfCODE;
		// clsDataFieldEdit: dfDESCR
		// Message Actions
		// On SAM_Create
		// Call SetRequired( TRUE )
		// On SAM_Create
		// Call SetRequired( TRUE )
		public clsMLEdit mlDESCR;
		// clsDataField: dfSELECTABLE
		// Message Actions
		public clsCheckBoxAuto cbSELECTABLE;
		// clsDataField: dfEDITABLE
		// Message Actions
		public clsCheckBoxAuto cbEDITABLE;
		public clsCheckBoxAuto cbEXTERNAL;
		protected clsText label1;
		protected clsText label2;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfCODE = new pamc.common.clsDataFieldEditKey();
			this.mlDESCR = new pamc.common.clsMLEdit();
			this.cbSELECTABLE = new pamc.common.clsCheckBoxAuto();
			this.cbEDITABLE = new pamc.common.clsCheckBoxAuto();
			this.cbEXTERNAL = new pamc.common.clsCheckBoxAuto();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfCODE
			// 
			this.dfCODE.Name = "dfCODE";
			this.dfCODE.MaxLength = 7;
			this.dfCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCODE.Location = new System.Drawing.Point(143, 7);
			this.dfCODE.Size = new System.Drawing.Size(93, 20);
			this.dfCODE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCODE_WindowActions);
			this.dfCODE.TabIndex = 0;
			// 
			// mlDESCR
			// 
			this.mlDESCR.Name = "mlDESCR";
			this.mlDESCR.MaxLength = 100;
			this.mlDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.mlDESCR.Location = new System.Drawing.Point(143, 39);
			this.mlDESCR.Size = new System.Drawing.Size(344, 48);
			this.mlDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.mlDESCR_WindowActions);
			this.mlDESCR.TabIndex = 1;
			// 
			// cbSELECTABLE
			// 
			this.cbSELECTABLE.Name = "cbSELECTABLE";
			this.cbSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbSELECTABLE.Text = "Selectable";
			this.cbSELECTABLE.Location = new System.Drawing.Point(10, 71);
			this.cbSELECTABLE.Size = new System.Drawing.Size(112, 24);
			this.cbSELECTABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbSELECTABLE_WindowActions);
			this.cbSELECTABLE.TabIndex = 2;
			// 
			// cbEDITABLE
			// 
			this.cbEDITABLE.Name = "cbEDITABLE";
			this.cbEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbEDITABLE.Text = "Editable";
			this.cbEDITABLE.Location = new System.Drawing.Point(10, 95);
			this.cbEDITABLE.Size = new System.Drawing.Size(112, 24);
			this.cbEDITABLE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbEDITABLE_WindowActions);
			this.cbEDITABLE.TabIndex = 3;
			// 
			// cbEXTERNAL
			// 
			this.cbEXTERNAL.Name = "cbEXTERNAL";
			this.cbEXTERNAL.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbEXTERNAL.Text = "External";
			this.cbEXTERNAL.Location = new System.Drawing.Point(10, 119);
			this.cbEXTERNAL.Size = new System.Drawing.Size(112, 24);
			this.cbEXTERNAL.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.cbEXTERNAL_WindowActions);
			this.cbEXTERNAL.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(10, 11);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(10, 41);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 6;
			// 
			// frmADJUST_CODES
			// 
			this.Controls.Add(this.cbEXTERNAL);
			this.Controls.Add(this.cbEDITABLE);
			this.Controls.Add(this.cbSELECTABLE);
			this.Controls.Add(this.mlDESCR);
			this.Controls.Add(this.dfCODE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmADJUST_CODES";
			this.ClientSize = new System.Drawing.Size(520, 186);
			this.Text = "Adjustment Reason Code";
			this.Location = new System.Drawing.Point(48, 48);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmADJUST_CODES_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmADJUST_CODES == this) 
			{
				App.frmADJUST_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
