// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgPriceSummary
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDlgClose pbClose;
		protected SalFrame frame2;
		public clsDataFieldEditN dfAllowed;
		// On AM_UpdateSums
		// Call SalSendMsg( hWndItem, AM_GetValue, 0, 0 )
		public clsDataFieldEditN dfPaidByOther;
		// On AM_UpdateSums
		// Call SalSendMsg( hWndItem, AM_GetValue, 0, 0 )
		public clsDataFieldEditN dfAdjustSum;
		// On AM_UpdateSums
		// Call SalSendMsg( hWndItem, AM_GetValue, 0, 0 )
		public clsDataFieldEditN dfWithholdSum;
		public clsDataFieldEditN dfPreNet;
		// On AM_UpdateSums
		// Call SalSendMsg( hWndItem, AM_GetValue, 0, 0 )
		public clsDataFieldEditN dfAdjustSumWH;
		public clsDataFieldEditN dfNet;
		protected FCSalBackgroundText label1;
		protected FCSalBackgroundText label2;
		protected FCSalBackgroundText label3;
		protected FCSalBackgroundText label4;
		protected FCSalBackgroundText label5;
		protected FCSalBackgroundText label6;
		protected FCSalBackgroundText label7;
		protected SalLine line1;
		protected SalLine line2;
		protected SalLine line3;
		public SalPicture picAllowed;
		public SalPicture picPaidByOther;
		public SalPicture picAdjust;
		// Call PriceData.GetWithholdPercent( PriceData.nWHSet, dfPHCode, dfProcCode, dfFromDateSvc )
		public SalPicture picWithhold;
		public SalPicture picPreNet;
		public SalPicture picAdjustWH;
		public SalPicture picNet;
		protected SalFrame frame3;
		protected SalLine line4;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbClose = new pamc.common.clsDlgClose();
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.dfAllowed = new pamc.common.clsDataFieldEditN();
			this.dfPaidByOther = new pamc.common.clsDataFieldEditN();
			this.dfAdjustSum = new pamc.common.clsDataFieldEditN();
			this.dfWithholdSum = new pamc.common.clsDataFieldEditN();
			this.dfPreNet = new pamc.common.clsDataFieldEditN();
			this.dfAdjustSumWH = new pamc.common.clsDataFieldEditN();
			this.dfNet = new pamc.common.clsDataFieldEditN();
			this.label1 = new FCSalBackgroundText();
			this.label2 = new FCSalBackgroundText();
			this.label3 = new FCSalBackgroundText();
			this.label4 = new FCSalBackgroundText();
			this.label5 = new FCSalBackgroundText();
			this.label6 = new FCSalBackgroundText();
			this.label7 = new FCSalBackgroundText();
			this.line1 = new PPJ.Runtime.Windows.SalLine();
			this.line2 = new PPJ.Runtime.Windows.SalLine();
			this.line3 = new PPJ.Runtime.Windows.SalLine();
			this.picAllowed = new PPJ.Runtime.Windows.SalPicture();
			this.picPaidByOther = new PPJ.Runtime.Windows.SalPicture();
			this.picAdjust = new PPJ.Runtime.Windows.SalPicture();
			this.picWithhold = new PPJ.Runtime.Windows.SalPicture();
			this.picPreNet = new PPJ.Runtime.Windows.SalPicture();
			this.picAdjustWH = new PPJ.Runtime.Windows.SalPicture();
			this.picNet = new PPJ.Runtime.Windows.SalPicture();
			this.frame3 = new PPJ.Runtime.Windows.SalFrame();
			this.line4 = new PPJ.Runtime.Windows.SalLine();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			this.ToolBar.Create = false;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ClientArea.Controls.Add(this.dfNet);
			this.ClientArea.Controls.Add(this.dfAdjustSumWH);
			this.ClientArea.Controls.Add(this.dfPreNet);
			this.ClientArea.Controls.Add(this.dfWithholdSum);
			this.ClientArea.Controls.Add(this.dfAdjustSum);
			this.ClientArea.Controls.Add(this.dfPaidByOther);
			this.ClientArea.Controls.Add(this.dfAllowed);
			this.ClientArea.Controls.Add(this.pbClose);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label7);
			this.ClientArea.Controls.Add(this.label6);
			this.ClientArea.Controls.Add(this.label5);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.line4);
			this.ClientArea.Controls.Add(this.frame3);
			this.ClientArea.Controls.Add(this.picNet);
			this.ClientArea.Controls.Add(this.picAdjustWH);
			this.ClientArea.Controls.Add(this.picPreNet);
			this.ClientArea.Controls.Add(this.picWithhold);
			this.ClientArea.Controls.Add(this.picAdjust);
			this.ClientArea.Controls.Add(this.picPaidByOther);
			this.ClientArea.Controls.Add(this.picAllowed);
			this.ClientArea.Controls.Add(this.line3);
			this.ClientArea.Controls.Add(this.line2);
			this.ClientArea.Controls.Add(this.line1);
			this.ClientArea.Controls.Add(this.frame2);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Create = false;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Visible = false;
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Visible = false;
			this.frame1.Size = new System.Drawing.Size(301, 38);
			this.frame1.TabIndex = 2;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Visible = false;
			this.pbClose.Location = new System.Drawing.Point(184, -1);
			this.pbClose.Size = new System.Drawing.Size(78, 31);
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 3;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.Color.Gray;
			this.frame2.ForeColor = System.Drawing.Color.Teal;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Solid;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(7, 5);
			this.frame2.Size = new System.Drawing.Size(256, 220);
			this.frame2.TabIndex = 4;
			// 
			// dfAllowed
			// 
			this.dfAllowed.Name = "dfAllowed";
			this.dfAllowed.BackColor = System.Drawing.Color.Silver;
			this.dfAllowed.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfAllowed.ReadOnly = true;
			this.dfAllowed.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfAllowed.ForeColor = System.Drawing.Color.Green;
			this.dfAllowed.Location = new System.Drawing.Point(135, 17);
			this.dfAllowed.Size = new System.Drawing.Size(106, 16);
			this.dfAllowed.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfAllowed_WindowActions);
			this.dfAllowed.TabIndex = 5;
			// 
			// dfPaidByOther
			// 
			this.dfPaidByOther.Name = "dfPaidByOther";
			this.dfPaidByOther.BackColor = System.Drawing.Color.Silver;
			this.dfPaidByOther.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfPaidByOther.ReadOnly = true;
			this.dfPaidByOther.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfPaidByOther.ForeColor = System.Drawing.Color.Navy;
			this.dfPaidByOther.Location = new System.Drawing.Point(135, 46);
			this.dfPaidByOther.Size = new System.Drawing.Size(106, 16);
			this.dfPaidByOther.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPaidByOther_WindowActions);
			this.dfPaidByOther.TabIndex = 6;
			// 
			// dfAdjustSum
			// 
			this.dfAdjustSum.Name = "dfAdjustSum";
			this.dfAdjustSum.BackColor = System.Drawing.Color.Silver;
			this.dfAdjustSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfAdjustSum.ReadOnly = true;
			this.dfAdjustSum.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfAdjustSum.ForeColor = System.Drawing.Color.Navy;
			this.dfAdjustSum.Location = new System.Drawing.Point(135, 72);
			this.dfAdjustSum.Size = new System.Drawing.Size(106, 16);
			this.dfAdjustSum.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfAdjustSum_WindowActions);
			this.dfAdjustSum.TabIndex = 7;
			// 
			// dfWithholdSum
			// 
			this.dfWithholdSum.Name = "dfWithholdSum";
			this.dfWithholdSum.BackColor = System.Drawing.Color.Silver;
			this.dfWithholdSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfWithholdSum.ReadOnly = true;
			this.dfWithholdSum.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfWithholdSum.ForeColor = System.Drawing.Color.Navy;
			this.dfWithholdSum.Location = new System.Drawing.Point(135, 98);
			this.dfWithholdSum.Size = new System.Drawing.Size(106, 16);
			this.dfWithholdSum.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfWithholdSum_WindowActions);
			this.dfWithholdSum.TabIndex = 8;
			// 
			// dfPreNet
			// 
			this.dfPreNet.Name = "dfPreNet";
			this.dfPreNet.BackColor = System.Drawing.Color.Silver;
			this.dfPreNet.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfPreNet.ReadOnly = true;
			this.dfPreNet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfPreNet.Location = new System.Drawing.Point(135, 133);
			this.dfPreNet.Size = new System.Drawing.Size(106, 16);
			this.dfPreNet.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPreNet_WindowActions);
			this.dfPreNet.TabIndex = 9;
			// 
			// dfAdjustSumWH
			// 
			this.dfAdjustSumWH.Name = "dfAdjustSumWH";
			this.dfAdjustSumWH.BackColor = System.Drawing.Color.Silver;
			this.dfAdjustSumWH.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfAdjustSumWH.ReadOnly = true;
			this.dfAdjustSumWH.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfAdjustSumWH.ForeColor = System.Drawing.Color.Navy;
			this.dfAdjustSumWH.Location = new System.Drawing.Point(135, 161);
			this.dfAdjustSumWH.Size = new System.Drawing.Size(106, 16);
			this.dfAdjustSumWH.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfAdjustSumWH_WindowActions);
			this.dfAdjustSumWH.TabIndex = 10;
			// 
			// dfNet
			// 
			this.dfNet.Name = "dfNet";
			this.dfNet.BackColor = System.Drawing.Color.Silver;
			this.dfNet.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dfNet.ReadOnly = true;
			this.dfNet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfNet.Location = new System.Drawing.Point(135, 198);
			this.dfNet.Size = new System.Drawing.Size(106, 16);
			this.dfNet.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfNet_WindowActions);
			this.dfNet.TabIndex = 11;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.BackColor = System.Drawing.Color.Gray;
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Text = "Allowed Amount";
			this.label1.Location = new System.Drawing.Point(19, 16);
			this.label1.Size = new System.Drawing.Size(108, 18);
			this.label1.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.BackColor = System.Drawing.Color.Gray;
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Text = "$ Paid By Others";
			this.label2.Location = new System.Drawing.Point(19, 45);
			this.label2.Size = new System.Drawing.Size(108, 18);
			this.label2.TabIndex = 13;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.BackColor = System.Drawing.Color.Gray;
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Text = "Withhold";
			this.label3.Location = new System.Drawing.Point(19, 97);
			this.label3.Size = new System.Drawing.Size(108, 18);
			this.label3.TabIndex = 14;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.BackColor = System.Drawing.Color.Gray;
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.ForeColor = System.Drawing.Color.White;
			this.label4.Text = "Final Adjustment";
			this.label4.Location = new System.Drawing.Point(19, 160);
			this.label4.Size = new System.Drawing.Size(108, 16);
			this.label4.TabIndex = 15;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.BackColor = System.Drawing.Color.Gray;
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.ForeColor = System.Drawing.Color.White;
			this.label5.Text = "Adjustment(s)";
			this.label5.Location = new System.Drawing.Point(19, 72);
			this.label5.Size = new System.Drawing.Size(108, 16);
			this.label5.TabIndex = 16;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.BackColor = System.Drawing.Color.Gray;
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label6.ForeColor = System.Drawing.Color.White;
			this.label6.Text = "Subtotal";
			this.label6.Location = new System.Drawing.Point(19, 130);
			this.label6.Size = new System.Drawing.Size(108, 18);
			this.label6.TabIndex = 17;
			// 
			// label7
			// 
			this.label7.Name = "label7";
			this.label7.BackColor = System.Drawing.Color.Gray;
			this.label7.Font = new Font("Arial", 7.25f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline);
			this.label7.ForeColor = System.Drawing.Color.White;
			this.label7.Text = "Net Pay";
			this.label7.Location = new System.Drawing.Point(19, 196);
			this.label7.Size = new System.Drawing.Size(108, 19);
			this.label7.TabIndex = 18;
			// 
			// line1
			// 
			this.line1.Name = "line1";
			this.line1.LineStyle = PPJ.Runtime.Windows.LineStyle.Solid;
			this.line1.LineSize = 1;
			this.line1.Left = 133;
			this.line1.Top = 190;
			this.line1.Width = 115;
			this.line1.Height = 3;
			this.line1.LineType = PPJ.Runtime.Windows.LineType.Horizontal;
			this.line1.TabIndex = 19;
			// 
			// line2
			// 
			this.line2.Name = "line2";
			this.line2.LineStyle = PPJ.Runtime.Windows.LineStyle.Solid;
			this.line2.LineSize = 1;
			this.line2.Left = 133;
			this.line2.Top = 123;
			this.line2.Width = 115;
			this.line2.Height = 3;
			this.line2.LineType = PPJ.Runtime.Windows.LineType.Horizontal;
			this.line2.TabIndex = 20;
			// 
			// line3
			// 
			this.line3.Name = "line3";
			this.line3.LineStyle = PPJ.Runtime.Windows.LineStyle.Solid;
			this.line3.LineSize = 1;
			this.line3.Left = 133;
			this.line3.Top = 185;
			this.line3.Width = 115;
			this.line3.Height = 3;
			this.line3.LineType = PPJ.Runtime.Windows.LineType.Horizontal;
			this.line3.TabIndex = 21;
			// 
			// picAllowed
			// 
			this.picAllowed.Name = "picAllowed";
			this.picAllowed.BackColor = System.Drawing.Color.Silver;
			this.picAllowed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.picAllowed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picAllowed.ImageStorage = ImageStorage.External;
			this.picAllowed.Dock = System.Windows.Forms.DockStyle.None;
			this.picAllowed.Location = new System.Drawing.Point(133, 14);
			this.picAllowed.Size = new System.Drawing.Size(115, 22);
			this.picAllowed.TransparentColor = System.Drawing.Color.White;
			this.picAllowed.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picAllowed_WindowActions);
			this.picAllowed.TabIndex = 22;
			// 
			// picPaidByOther
			// 
			this.picPaidByOther.Name = "picPaidByOther";
			this.picPaidByOther.BackColor = System.Drawing.Color.Silver;
			this.picPaidByOther.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.picPaidByOther.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picPaidByOther.ImageStorage = ImageStorage.External;
			this.picPaidByOther.Dock = System.Windows.Forms.DockStyle.None;
			this.picPaidByOther.Location = new System.Drawing.Point(133, 44);
			this.picPaidByOther.Size = new System.Drawing.Size(115, 22);
			this.picPaidByOther.TransparentColor = System.Drawing.Color.White;
			this.picPaidByOther.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picPaidByOther_WindowActions);
			this.picPaidByOther.TabIndex = 23;
			// 
			// picAdjust
			// 
			this.picAdjust.Name = "picAdjust";
			this.picAdjust.BackColor = System.Drawing.Color.Silver;
			this.picAdjust.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.picAdjust.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picAdjust.ImageStorage = ImageStorage.External;
			this.picAdjust.Dock = System.Windows.Forms.DockStyle.None;
			this.picAdjust.Location = new System.Drawing.Point(133, 69);
			this.picAdjust.Size = new System.Drawing.Size(115, 22);
			this.picAdjust.TransparentColor = System.Drawing.Color.White;
			this.picAdjust.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picAdjust_WindowActions);
			this.picAdjust.TabIndex = 24;
			// 
			// picWithhold
			// 
			this.picWithhold.Name = "picWithhold";
			this.picWithhold.BackColor = System.Drawing.Color.Silver;
			this.picWithhold.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.picWithhold.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picWithhold.ImageStorage = ImageStorage.External;
			this.picWithhold.Dock = System.Windows.Forms.DockStyle.None;
			this.picWithhold.Location = new System.Drawing.Point(133, 95);
			this.picWithhold.Size = new System.Drawing.Size(115, 22);
			this.picWithhold.TransparentColor = System.Drawing.Color.White;
			this.picWithhold.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picWithhold_WindowActions);
			this.picWithhold.TabIndex = 25;
			// 
			// picPreNet
			// 
			this.picPreNet.Name = "picPreNet";
			this.picPreNet.BackColor = System.Drawing.Color.Silver;
			this.picPreNet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.picPreNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picPreNet.ImageStorage = ImageStorage.External;
			this.picPreNet.Dock = System.Windows.Forms.DockStyle.None;
			this.picPreNet.Location = new System.Drawing.Point(133, 129);
			this.picPreNet.Size = new System.Drawing.Size(115, 22);
			this.picPreNet.TransparentColor = System.Drawing.Color.White;
			this.picPreNet.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picPreNet_WindowActions);
			this.picPreNet.TabIndex = 26;
			// 
			// picAdjustWH
			// 
			this.picAdjustWH.Name = "picAdjustWH";
			this.picAdjustWH.BackColor = System.Drawing.Color.Silver;
			this.picAdjustWH.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.picAdjustWH.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picAdjustWH.ImageStorage = ImageStorage.External;
			this.picAdjustWH.Dock = System.Windows.Forms.DockStyle.None;
			this.picAdjustWH.Location = new System.Drawing.Point(133, 159);
			this.picAdjustWH.Size = new System.Drawing.Size(115, 22);
			this.picAdjustWH.TransparentColor = System.Drawing.Color.White;
			this.picAdjustWH.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picAdjustWH_WindowActions);
			this.picAdjustWH.TabIndex = 27;
			// 
			// picNet
			// 
			this.picNet.Name = "picNet";
			this.picNet.BackColor = System.Drawing.Color.Silver;
			this.picNet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.picNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picNet.ImageStorage = ImageStorage.External;
			this.picNet.Dock = System.Windows.Forms.DockStyle.None;
			this.picNet.Location = new System.Drawing.Point(134, 195);
			this.picNet.Size = new System.Drawing.Size(115, 22);
			this.picNet.TransparentColor = System.Drawing.Color.White;
			this.picNet.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.picNet_WindowActions);
			this.picNet.TabIndex = 28;
			// 
			// frame3
			// 
			this.frame3.Name = "frame3";
			this.frame3.BackColor = System.Drawing.Color.Silver;
			this.frame3.BorderStyle = PPJ.Runtime.Windows.BorderStyle.RaisedShadow;
			this.frame3.BorderSize = 1;
			this.frame3.Location = new System.Drawing.Point(133, 41);
			this.frame3.Size = new System.Drawing.Size(115, 79);
			this.frame3.TabIndex = 29;
			// 
			// line4
			// 
			this.line4.Name = "line4";
			this.line4.LineStyle = PPJ.Runtime.Windows.LineStyle.Solid;
			this.line4.LineSize = 1;
			this.line4.Left = 133;
			this.line4.Top = 154;
			this.line4.Width = 115;
			this.line4.Height = 3;
			this.line4.LineType = PPJ.Runtime.Windows.LineType.Horizontal;
			this.line4.TabIndex = 30;
			// 
			// dlgPriceSummary
			// 
			this.Name = "dlgPriceSummary";
			this.ClientSize = new System.Drawing.Size(269, 226);
			this.Text = "Service Price Summary";
			this.Location = new System.Drawing.Point(314, 88);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgPriceSummary_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgPriceSummary == this) 
			{
				App.dlgPriceSummary = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
