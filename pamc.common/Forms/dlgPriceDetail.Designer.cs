// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgPriceDetail
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDlgClose pbClose;
		protected SalFrame frame2;
		protected FCSalBackgroundText label1;
		protected FCSalBackgroundText label2;
		public clsDataFieldN dfBilled;
		public clsDataFieldN dfManDiscount;
		public clsDataFieldN dfTarrifReduct;
		public clsDataFieldN dfTRAllowed;
		public clsDataFieldN dfSchemeTarrif;
		public clsDataFieldN dfContractDiscount;
		public clsDataFieldN dfProviderDiscount;
		public clsDataFieldN dfCVAllowed;
		public clsDataFieldN dfContrval;
		public clsDataFieldN dfAdjust;
		public clsDataFieldN dfResponsable;
		public clsDataFieldN dfLevy;
		public clsDataFieldN dfCopayBase;
		public clsDataFieldN dfCopay;
		public clsDataFieldN dfDeductible;
		public clsDataFieldN dfApply_Benefits;
		public clsDataFieldN dfBenAvailable;
		public clsDataFieldN dfAdjustWH;
		public clsDataFieldN dfFromBenefits;
		public clsDataFieldN dfAvailableSavings;
		public clsDataFieldN dfSavingsAcc;
		public clsDataFieldN dfWithhold;
		public clsDataFieldN dfNet;
		protected FCSalBackgroundText label3;
		protected FCSalBackgroundText label4;
		protected FCSalBackgroundText label5;
		protected FCSalBackgroundText label6;
		protected FCSalBackgroundText label7;
		protected FCSalBackgroundText label8;
		protected FCSalBackgroundText label9;
		protected FCSalBackgroundText label10;
		protected FCSalBackgroundText label11;
		protected FCSalBackgroundText label12;
		protected FCSalBackgroundText label13;
		protected FCSalBackgroundText label14;
		protected FCSalBackgroundText label15;
		protected FCSalBackgroundText label16;
		protected FCSalBackgroundText label17;
		protected FCSalBackgroundText label18;
		protected FCSalBackgroundText label19;
		protected FCSalBackgroundText label20;
		protected FCSalBackgroundText label21;
		protected FCSalBackgroundText label22;
		protected FCSalBackgroundText label23;
		protected FCSalBackgroundText label24;
		protected FCSalBackgroundText label25;
		public clsDataFieldN dfFINALADJWH_ORIG;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbClose = new pamc.common.clsDlgClose();
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.label1 = new FCSalBackgroundText();
			this.label2 = new FCSalBackgroundText();
			this.dfBilled = new pamc.common.clsDataFieldN();
			this.dfManDiscount = new pamc.common.clsDataFieldN();
			this.dfTarrifReduct = new pamc.common.clsDataFieldN();
			this.dfTRAllowed = new pamc.common.clsDataFieldN();
			this.dfSchemeTarrif = new pamc.common.clsDataFieldN();
			this.dfContractDiscount = new pamc.common.clsDataFieldN();
			this.dfProviderDiscount = new pamc.common.clsDataFieldN();
			this.dfCVAllowed = new pamc.common.clsDataFieldN();
			this.dfContrval = new pamc.common.clsDataFieldN();
			this.dfAdjust = new pamc.common.clsDataFieldN();
			this.dfResponsable = new pamc.common.clsDataFieldN();
			this.dfLevy = new pamc.common.clsDataFieldN();
			this.dfCopayBase = new pamc.common.clsDataFieldN();
			this.dfCopay = new pamc.common.clsDataFieldN();
			this.dfDeductible = new pamc.common.clsDataFieldN();
			this.dfApply_Benefits = new pamc.common.clsDataFieldN();
			this.dfBenAvailable = new pamc.common.clsDataFieldN();
			this.dfAdjustWH = new pamc.common.clsDataFieldN();
			this.dfFromBenefits = new pamc.common.clsDataFieldN();
			this.dfAvailableSavings = new pamc.common.clsDataFieldN();
			this.dfSavingsAcc = new pamc.common.clsDataFieldN();
			this.dfWithhold = new pamc.common.clsDataFieldN();
			this.dfNet = new pamc.common.clsDataFieldN();
			this.label3 = new FCSalBackgroundText();
			this.label4 = new FCSalBackgroundText();
			this.label5 = new FCSalBackgroundText();
			this.label6 = new FCSalBackgroundText();
			this.label7 = new FCSalBackgroundText();
			this.label8 = new FCSalBackgroundText();
			this.label9 = new FCSalBackgroundText();
			this.label10 = new FCSalBackgroundText();
			this.label11 = new FCSalBackgroundText();
			this.label12 = new FCSalBackgroundText();
			this.label13 = new FCSalBackgroundText();
			this.label14 = new FCSalBackgroundText();
			this.label15 = new FCSalBackgroundText();
			this.label16 = new FCSalBackgroundText();
			this.label17 = new FCSalBackgroundText();
			this.label18 = new FCSalBackgroundText();
			this.label19 = new FCSalBackgroundText();
			this.label20 = new FCSalBackgroundText();
			this.label21 = new FCSalBackgroundText();
			this.label22 = new FCSalBackgroundText();
			this.label23 = new FCSalBackgroundText();
			this.label24 = new FCSalBackgroundText();
			this.label25 = new FCSalBackgroundText();
			this.dfFINALADJWH_ORIG = new pamc.common.clsDataFieldN();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			this.ToolBar.Create = false;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ClientArea.Controls.Add(this.dfFINALADJWH_ORIG);
			this.ClientArea.Controls.Add(this.dfNet);
			this.ClientArea.Controls.Add(this.dfWithhold);
			this.ClientArea.Controls.Add(this.dfSavingsAcc);
			this.ClientArea.Controls.Add(this.dfAvailableSavings);
			this.ClientArea.Controls.Add(this.dfFromBenefits);
			this.ClientArea.Controls.Add(this.dfAdjustWH);
			this.ClientArea.Controls.Add(this.dfBenAvailable);
			this.ClientArea.Controls.Add(this.dfApply_Benefits);
			this.ClientArea.Controls.Add(this.dfDeductible);
			this.ClientArea.Controls.Add(this.dfCopay);
			this.ClientArea.Controls.Add(this.dfCopayBase);
			this.ClientArea.Controls.Add(this.dfLevy);
			this.ClientArea.Controls.Add(this.dfResponsable);
			this.ClientArea.Controls.Add(this.dfAdjust);
			this.ClientArea.Controls.Add(this.dfContrval);
			this.ClientArea.Controls.Add(this.dfCVAllowed);
			this.ClientArea.Controls.Add(this.dfProviderDiscount);
			this.ClientArea.Controls.Add(this.dfContractDiscount);
			this.ClientArea.Controls.Add(this.dfSchemeTarrif);
			this.ClientArea.Controls.Add(this.dfTRAllowed);
			this.ClientArea.Controls.Add(this.dfTarrifReduct);
			this.ClientArea.Controls.Add(this.dfManDiscount);
			this.ClientArea.Controls.Add(this.dfBilled);
			this.ClientArea.Controls.Add(this.pbClose);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label25);
			this.ClientArea.Controls.Add(this.label24);
			this.ClientArea.Controls.Add(this.label23);
			this.ClientArea.Controls.Add(this.label22);
			this.ClientArea.Controls.Add(this.label21);
			this.ClientArea.Controls.Add(this.label20);
			this.ClientArea.Controls.Add(this.label19);
			this.ClientArea.Controls.Add(this.label18);
			this.ClientArea.Controls.Add(this.label17);
			this.ClientArea.Controls.Add(this.label16);
			this.ClientArea.Controls.Add(this.label15);
			this.ClientArea.Controls.Add(this.label14);
			this.ClientArea.Controls.Add(this.label13);
			this.ClientArea.Controls.Add(this.label12);
			this.ClientArea.Controls.Add(this.label11);
			this.ClientArea.Controls.Add(this.label10);
			this.ClientArea.Controls.Add(this.label9);
			this.ClientArea.Controls.Add(this.label8);
			this.ClientArea.Controls.Add(this.label7);
			this.ClientArea.Controls.Add(this.label6);
			this.ClientArea.Controls.Add(this.label5);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame2);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Create = false;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Visible = false;
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Visible = false;
			this.frame1.Size = new System.Drawing.Size(301, 38);
			this.frame1.TabIndex = 2;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Location = new System.Drawing.Point(488, 7);
			this.pbClose.Size = new System.Drawing.Size(78, 31);
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 3;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.Color.Silver;
			this.frame2.ForeColor = System.Drawing.Color.Gray;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(7, 7);
			this.frame2.Size = new System.Drawing.Size(424, 448);
			this.frame2.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label1.Text = ")";
			this.label1.Location = new System.Drawing.Point(408, 264);
			this.label1.Size = new System.Drawing.Size(8, 16);
			this.label1.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label2.Text = "- (";
			this.label2.Location = new System.Drawing.Point(327, 264);
			this.label2.Size = new System.Drawing.Size(16, 16);
			this.label2.TabIndex = 6;
			// 
			// dfBilled
			// 
			this.dfBilled.Name = "dfBilled";
			this.dfBilled.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfBilled.Location = new System.Drawing.Point(255, 23);
			this.dfBilled.Size = new System.Drawing.Size(64, 19);
			this.dfBilled.TabIndex = 7;
			// 
			// dfManDiscount
			// 
			this.dfManDiscount.Name = "dfManDiscount";
			this.dfManDiscount.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfManDiscount.Location = new System.Drawing.Point(255, 43);
			this.dfManDiscount.Size = new System.Drawing.Size(64, 19);
			this.dfManDiscount.TabIndex = 8;
			// 
			// dfTarrifReduct
			// 
			this.dfTarrifReduct.Name = "dfTarrifReduct";
			this.dfTarrifReduct.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTarrifReduct.Location = new System.Drawing.Point(255, 63);
			this.dfTarrifReduct.Size = new System.Drawing.Size(64, 19);
			this.dfTarrifReduct.TabIndex = 9;
			// 
			// dfTRAllowed
			// 
			this.dfTRAllowed.Name = "dfTRAllowed";
			this.dfTRAllowed.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTRAllowed.Location = new System.Drawing.Point(255, 83);
			this.dfTRAllowed.Size = new System.Drawing.Size(64, 19);
			this.dfTRAllowed.TabIndex = 10;
			// 
			// dfSchemeTarrif
			// 
			this.dfSchemeTarrif.Name = "dfSchemeTarrif";
			this.dfSchemeTarrif.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSchemeTarrif.Location = new System.Drawing.Point(255, 103);
			this.dfSchemeTarrif.Size = new System.Drawing.Size(64, 19);
			this.dfSchemeTarrif.TabIndex = 11;
			// 
			// dfContractDiscount
			// 
			this.dfContractDiscount.Name = "dfContractDiscount";
			this.dfContractDiscount.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfContractDiscount.Location = new System.Drawing.Point(255, 123);
			this.dfContractDiscount.Size = new System.Drawing.Size(64, 19);
			this.dfContractDiscount.TabIndex = 12;
			// 
			// dfProviderDiscount
			// 
			this.dfProviderDiscount.Name = "dfProviderDiscount";
			this.dfProviderDiscount.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfProviderDiscount.Location = new System.Drawing.Point(255, 143);
			this.dfProviderDiscount.Size = new System.Drawing.Size(64, 19);
			this.dfProviderDiscount.TabIndex = 13;
			// 
			// dfCVAllowed
			// 
			this.dfCVAllowed.Name = "dfCVAllowed";
			this.dfCVAllowed.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCVAllowed.Location = new System.Drawing.Point(255, 163);
			this.dfCVAllowed.Size = new System.Drawing.Size(64, 19);
			this.dfCVAllowed.TabIndex = 14;
			// 
			// dfContrval
			// 
			this.dfContrval.Name = "dfContrval";
			this.dfContrval.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfContrval.Location = new System.Drawing.Point(255, 183);
			this.dfContrval.Size = new System.Drawing.Size(64, 19);
			this.dfContrval.TabIndex = 15;
			// 
			// dfAdjust
			// 
			this.dfAdjust.Name = "dfAdjust";
			this.dfAdjust.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfAdjust.Location = new System.Drawing.Point(255, 203);
			this.dfAdjust.Size = new System.Drawing.Size(64, 19);
			this.dfAdjust.TabIndex = 16;
			// 
			// dfResponsable
			// 
			this.dfResponsable.Name = "dfResponsable";
			this.dfResponsable.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfResponsable.Location = new System.Drawing.Point(255, 223);
			this.dfResponsable.Size = new System.Drawing.Size(64, 19);
			this.dfResponsable.TabIndex = 17;
			// 
			// dfLevy
			// 
			this.dfLevy.Name = "dfLevy";
			this.dfLevy.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfLevy.Location = new System.Drawing.Point(255, 243);
			this.dfLevy.Size = new System.Drawing.Size(64, 19);
			this.dfLevy.TabIndex = 18;
			// 
			// dfCopayBase
			// 
			this.dfCopayBase.Name = "dfCopayBase";
			this.dfCopayBase.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCopayBase.Location = new System.Drawing.Point(255, 263);
			this.dfCopayBase.Size = new System.Drawing.Size(64, 19);
			this.dfCopayBase.TabIndex = 19;
			// 
			// dfCopay
			// 
			this.dfCopay.Name = "dfCopay";
			this.dfCopay.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCopay.Location = new System.Drawing.Point(255, 283);
			this.dfCopay.Size = new System.Drawing.Size(64, 19);
			this.dfCopay.TabIndex = 20;
			// 
			// dfDeductible
			// 
			this.dfDeductible.Name = "dfDeductible";
			this.dfDeductible.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDeductible.Location = new System.Drawing.Point(255, 303);
			this.dfDeductible.Size = new System.Drawing.Size(64, 19);
			this.dfDeductible.TabIndex = 21;
			// 
			// dfApply_Benefits
			// 
			this.dfApply_Benefits.Name = "dfApply_Benefits";
			this.dfApply_Benefits.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfApply_Benefits.Location = new System.Drawing.Point(255, 324);
			this.dfApply_Benefits.Size = new System.Drawing.Size(64, 19);
			this.dfApply_Benefits.TabIndex = 22;
			// 
			// dfBenAvailable
			// 
			this.dfBenAvailable.Name = "dfBenAvailable";
			this.dfBenAvailable.Visible = false;
			this.dfBenAvailable.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfBenAvailable.Location = new System.Drawing.Point(255, 407);
			this.dfBenAvailable.Size = new System.Drawing.Size(64, 19);
			this.dfBenAvailable.TabIndex = 23;
			// 
			// dfAdjustWH
			// 
			this.dfAdjustWH.Name = "dfAdjustWH";
			this.dfAdjustWH.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfAdjustWH.Location = new System.Drawing.Point(255, 344);
			this.dfAdjustWH.Size = new System.Drawing.Size(64, 19);
			this.dfAdjustWH.TabIndex = 24;
			// 
			// dfFromBenefits
			// 
			this.dfFromBenefits.Name = "dfFromBenefits";
			this.dfFromBenefits.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFromBenefits.Location = new System.Drawing.Point(255, 364);
			this.dfFromBenefits.Size = new System.Drawing.Size(64, 19);
			this.dfFromBenefits.TabIndex = 25;
			// 
			// dfAvailableSavings
			// 
			this.dfAvailableSavings.Name = "dfAvailableSavings";
			this.dfAvailableSavings.Visible = false;
			this.dfAvailableSavings.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfAvailableSavings.Location = new System.Drawing.Point(255, 479);
			this.dfAvailableSavings.Size = new System.Drawing.Size(64, 19);
			this.dfAvailableSavings.TabIndex = 26;
			// 
			// dfSavingsAcc
			// 
			this.dfSavingsAcc.Name = "dfSavingsAcc";
			this.dfSavingsAcc.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSavingsAcc.Location = new System.Drawing.Point(255, 384);
			this.dfSavingsAcc.Size = new System.Drawing.Size(64, 19);
			this.dfSavingsAcc.TabIndex = 27;
			// 
			// dfWithhold
			// 
			this.dfWithhold.Name = "dfWithhold";
			this.dfWithhold.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfWithhold.Location = new System.Drawing.Point(255, 404);
			this.dfWithhold.Size = new System.Drawing.Size(64, 19);
			this.dfWithhold.TabIndex = 28;
			// 
			// dfNet
			// 
			this.dfNet.Name = "dfNet";
			this.dfNet.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNet.Location = new System.Drawing.Point(255, 424);
			this.dfNet.Size = new System.Drawing.Size(64, 19);
			this.dfNet.TabIndex = 29;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label3.Text = "Billed";
			this.label3.Location = new System.Drawing.Point(23, 25);
			this.label3.Size = new System.Drawing.Size(224, 16);
			this.label3.TabIndex = 30;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label4.Text = "Member Manual Discount";
			this.label4.Location = new System.Drawing.Point(23, 44);
			this.label4.Size = new System.Drawing.Size(224, 16);
			this.label4.TabIndex = 31;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label5.Text = "Scheme Tariff";
			this.label5.Location = new System.Drawing.Point(23, 104);
			this.label5.Size = new System.Drawing.Size(224, 16);
			this.label5.TabIndex = 32;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label6.Text = "Contract Discount";
			this.label6.Location = new System.Drawing.Point(23, 124);
			this.label6.Size = new System.Drawing.Size(224, 16);
			this.label6.TabIndex = 33;
			// 
			// label7
			// 
			this.label7.Name = "label7";
			this.label7.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label7.Text = "Provider Manual Discount";
			this.label7.Location = new System.Drawing.Point(23, 144);
			this.label7.Size = new System.Drawing.Size(224, 16);
			this.label7.TabIndex = 34;
			// 
			// label8
			// 
			this.label8.Name = "label8";
			this.label8.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label8.Text = "Adjustments";
			this.label8.Location = new System.Drawing.Point(23, 204);
			this.label8.Size = new System.Drawing.Size(224, 16);
			this.label8.TabIndex = 35;
			// 
			// label9
			// 
			this.label9.Name = "label9";
			this.label9.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label9.Text = "Levy";
			this.label9.Location = new System.Drawing.Point(23, 244);
			this.label9.Size = new System.Drawing.Size(224, 16);
			this.label9.TabIndex = 36;
			// 
			// label10
			// 
			this.label10.Name = "label10";
			this.label10.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label10.Text = "Copay";
			this.label10.Location = new System.Drawing.Point(23, 284);
			this.label10.Size = new System.Drawing.Size(224, 16);
			this.label10.TabIndex = 37;
			// 
			// label11
			// 
			this.label11.Name = "label11";
			this.label11.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label11.Text = "Deductible";
			this.label11.Location = new System.Drawing.Point(23, 303);
			this.label11.Size = new System.Drawing.Size(224, 16);
			this.label11.TabIndex = 38;
			// 
			// label12
			// 
			this.label12.Name = "label12";
			this.label12.Visible = false;
			this.label12.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label12.Text = "Amount Available in Benefits";
			this.label12.Location = new System.Drawing.Point(23, 409);
			this.label12.Size = new System.Drawing.Size(224, 16);
			this.label12.TabIndex = 39;
			// 
			// label13
			// 
			this.label13.Name = "label13";
			this.label13.Visible = false;
			this.label13.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label13.Text = "Amount Available in Savings";
			this.label13.Location = new System.Drawing.Point(23, 481);
			this.label13.Size = new System.Drawing.Size(224, 16);
			this.label13.TabIndex = 40;
			// 
			// label14
			// 
			this.label14.Name = "label14";
			this.label14.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label14.Text = "Amount Paid from Savings Account";
			this.label14.Location = new System.Drawing.Point(23, 385);
			this.label14.Size = new System.Drawing.Size(224, 16);
			this.label14.TabIndex = 41;
			// 
			// label15
			// 
			this.label15.Name = "label15";
			this.label15.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label15.Text = "Withhold";
			this.label15.Location = new System.Drawing.Point(23, 404);
			this.label15.Size = new System.Drawing.Size(224, 16);
			this.label15.TabIndex = 42;
			// 
			// label16
			// 
			this.label16.Name = "label16";
			this.label16.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label16.Text = "Final Adjustment";
			this.label16.Location = new System.Drawing.Point(23, 345);
			this.label16.Size = new System.Drawing.Size(224, 16);
			this.label16.TabIndex = 43;
			// 
			// label17
			// 
			this.label17.Name = "label17";
			this.label17.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label17.Text = "Tariff Reduction";
			this.label17.Location = new System.Drawing.Point(23, 65);
			this.label17.Size = new System.Drawing.Size(224, 16);
			this.label17.TabIndex = 44;
			// 
			// label18
			// 
			this.label18.Name = "label18";
			this.label18.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label18.Text = "Tariff Allowed";
			this.label18.Location = new System.Drawing.Point(23, 84);
			this.label18.Size = new System.Drawing.Size(224, 16);
			this.label18.TabIndex = 45;
			// 
			// label19
			// 
			this.label19.Name = "label19";
			this.label19.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label19.Text = "Contract Value Allowed";
			this.label19.Location = new System.Drawing.Point(23, 164);
			this.label19.Size = new System.Drawing.Size(224, 16);
			this.label19.TabIndex = 46;
			// 
			// label20
			// 
			this.label20.Name = "label20";
			this.label20.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label20.Text = "Contract Value";
			this.label20.Location = new System.Drawing.Point(23, 184);
			this.label20.Size = new System.Drawing.Size(224, 16);
			this.label20.TabIndex = 47;
			// 
			// label21
			// 
			this.label21.Name = "label21";
			this.label21.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label21.Text = "Member / Scheme Responsibility";
			this.label21.Location = new System.Drawing.Point(23, 224);
			this.label21.Size = new System.Drawing.Size(224, 16);
			this.label21.TabIndex = 48;
			// 
			// label22
			// 
			this.label22.Name = "label22";
			this.label22.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label22.Text = "Co-Payment Base Amount";
			this.label22.Location = new System.Drawing.Point(23, 264);
			this.label22.Size = new System.Drawing.Size(224, 16);
			this.label22.TabIndex = 49;
			// 
			// label23
			// 
			this.label23.Name = "label23";
			this.label23.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label23.Text = "Amount Applicable to Benefits";
			this.label23.Location = new System.Drawing.Point(23, 324);
			this.label23.Size = new System.Drawing.Size(224, 16);
			this.label23.TabIndex = 50;
			// 
			// label24
			// 
			this.label24.Name = "label24";
			this.label24.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label24.Text = "Amount Deducted from Benefits";
			this.label24.Location = new System.Drawing.Point(23, 365);
			this.label24.Size = new System.Drawing.Size(224, 16);
			this.label24.TabIndex = 51;
			// 
			// label25
			// 
			this.label25.Name = "label25";
			this.label25.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label25.Text = "Net Paid";
			this.label25.Location = new System.Drawing.Point(23, 425);
			this.label25.Size = new System.Drawing.Size(224, 16);
			this.label25.TabIndex = 52;
			// 
			// dfFINALADJWH_ORIG
			// 
			this.dfFINALADJWH_ORIG.Name = "dfFINALADJWH_ORIG";
			this.dfFINALADJWH_ORIG.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFINALADJWH_ORIG.Location = new System.Drawing.Point(343, 263);
			this.dfFINALADJWH_ORIG.Size = new System.Drawing.Size(64, 19);
			this.dfFINALADJWH_ORIG.TabIndex = 53;
			// 
			// dlgPriceDetail
			// 
			this.Name = "dlgPriceDetail";
			this.ClientSize = new System.Drawing.Size(437, 461);
			this.Text = "Service Price Summary";
			this.Location = new System.Drawing.Point(260, 75);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgPriceDetail_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgPriceDetail == this) 
			{
				App.dlgPriceDetail = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
