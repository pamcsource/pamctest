// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	/// <param name="hWndTbl"></param>
	/// <param name="nMode"></param>
	public partial class fdlgPROV_LANGUAGE : clsDlgInfoFormProviders
	{
		#region Window Parameters
		public SalWindowHandle hWndTbl;
		public SalNumber nMode;
		#endregion
		
		#region Window Variables
		public SalString strWhere = "";
		public SalString strInsertValues = "";
		public SalString strPic = "";
		public SalString strText = "";
		public SalBoolean bNewSet = false;
		public SalBoolean bClearNotes = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public fdlgPROV_LANGUAGE(SalWindowHandle hWndTbl, SalNumber nMode)
		{
			// Assign global reference.
			App.fdlgPROV_LANGUAGE = this;
			// Window Parameters initialization.
			this.hWndTbl = hWndTbl;
			this.nMode = nMode;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalWindowHandle hWndTbl, SalNumber nMode)
		{
			fdlgPROV_LANGUAGE dlg = new fdlgPROV_LANGUAGE(hWndTbl, nMode);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static fdlgPROV_LANGUAGE FromHandle(SalWindowHandle handle)
		{
			return ((fdlgPROV_LANGUAGE)SalWindow.FromHandle(handle, typeof(fdlgPROV_LANGUAGE)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean LanguageValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SqlLocals.LanguageValidateEntryLocals locals = new SqlLocals.LanguageValidateEntryLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.strItem = strItem;
				locals.hWnd = hWnd;

				locals.strFieldName = locals.hWnd.GetName();
				if (locals.strFieldName == "dfLANGUAGE") 
				{
					pamc.price.Var.SqlInst.SqlPrep(pamc.price.Var.hSql, "Select  CODE, DESCR from LANGUAGE_CODES\r\n" +
						"	Where CODE = :strItem", ":hWndForm.dfLANGUAGE, :hWndForm.dfDescription");
					pamc.price.Var.hSql.Execute();
					locals.nInd = pamc.price.Var.hSql.FetchNext();
					if (locals.nInd == Sys.FETCH_EOF && locals.strItem != pamc.sqllib.Const.strNULL) 
					{
						return false;
					}
				}
				return true;
			}
			#endregion
		}
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean LanguageProcessLookup(SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strFieldName = "";
			// Number: nDisableItems[*]
			SalNumber nSelectableCol = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfLANGUAGE") 
				{

					// PPJ: Automatically generated temporary assignments. Properties canot be passed by reference.
					SalString temp1 = Sys.hWndForm.FindControl("dfLANGUAGE").Text;
					SalString temp2 = Sys.hWndForm.FindControl("dfDescription").Text;
					SalBoolean temp3 = Int.ProcessLookupLANGUAGE(0, ref temp1, ref temp2);
					Sys.hWndForm.FindControl("dfLANGUAGE").Text = temp1;
					Sys.hWndForm.FindControl("dfDescription").Text = temp2;

					if (temp3) 
					{
						return true;
					}
				}
				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// fdlgPROV_LANGUAGE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_LANGUAGE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.fdlgPROV_LANGUAGE_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Const.CM_DoLookup:
					this.fdlgPROV_LANGUAGE_OnCM_DoLookup(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.fdlgPROV_LANGUAGE_OnAM_Initialize(sender, e);
					break;
				
				case Const.CM_Populate:
					this.fdlgPROV_LANGUAGE_OnCM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_LANGUAGE_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.hWndTable = this.hWndTbl;
			this.nFormMode = this.nMode;
			this.strTypeTable = "PROV_LANGTYPES";
			this.strTypeTableName = "Language Types";
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_LANGUAGE_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.LanguageProcessLookup(Sal.GetFocus());
			this.ProcessLookup(Sal.GetFocus());
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_LANGUAGE_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(Const.AM_Initialize, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_LANGUAGE_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Const.CM_Populate, 0, 0);
			this.SendMessageToChildren(Const.AM_GetValue, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// dfLANGUAGE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfLANGUAGE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfLANGUAGE_OnSAM_Create(sender, e);
					break;
				
				case Const.AM_Lookup:
					this.dfLANGUAGE_OnAM_Lookup(sender, e);
					break;
				
				case Sys.SAM_Validate:
					this.dfLANGUAGE_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfLANGUAGE_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfLANGUAGE.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// AM_Lookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfLANGUAGE_OnAM_Lookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfLANGUAGE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			if (!(this.LanguageValidateEntry(this.dfLANGUAGE.Text, this.dfLANGUAGE))) 
			{
				if (!(this.LanguageProcessLookup(this.dfLANGUAGE))) 
				{
					e.Return = Sys.VALIDATE_Cancel;
					return;
				}
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfDescription WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDescription_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfDescription_OnCM_DoLookup(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.dfDescription_OnAM_Initialize(sender, e);
					break;
				
				case Const.AM_GetValue:
					this.dfDescription_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDescription_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select DESCR From LANGUAGE_CODES Where CODE = :dfLANGUAGE", ":dfDescription ", ref Var.nGInd))) 
			{
				this.dfDescription.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDescription_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfDescription.Text = pamc.sqllib.Const.strNULL;
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDescription_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select DESCR From LANGUAGE_CODES Where CODE = :dfLANGUAGE", ":dfDescription ", ref Var.nGInd))) 
			{
				this.dfDescription.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class LanguageValidateEntryLocals
			{
				public SalString strFieldName = "";
				public SalNumber nInd = 0;
				public SalNumber nItem = 0;
				public SalString strItem = "";
				public SalWindowHandle hWnd = SalWindowHandle.Null;
			}
		}
		#endregion
	}
}
