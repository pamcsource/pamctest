// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgCSM_DETAILS
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		protected clsText label1;
		public clsDataFieldDisabled dfUSER;
		public dlgCSM_DETAILS.tblCSM_DETAILTableWindow tblCSM_DETAIL;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new pamc.common.clsText();
			this.dfUSER = new pamc.common.clsDataFieldDisabled();
			this.tblCSM_DETAIL = (dlgCSM_DETAILS.tblCSM_DETAILTableWindow)CreateTableWindow(typeof(dlgCSM_DETAILS.tblCSM_DETAILTableWindow));
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.tblCSM_DETAIL);
			this.ClientArea.Controls.Add(this.dfUSER);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Visible = false;
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.Size = new System.Drawing.Size(587, 38);
			this.frame1.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Customer  Service Incident assigned to User :";
			this.label1.Location = new System.Drawing.Point(15, 49);
			this.label1.Size = new System.Drawing.Size(272, 16);
			this.label1.TabIndex = 3;
			// 
			// dfUSER
			// 
			this.dfUSER.Name = "dfUSER";
			this.dfUSER.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfUSER.Location = new System.Drawing.Point(287, 47);
			this.dfUSER.Size = new System.Drawing.Size(104, 19);
			this.dfUSER.TabIndex = 4;
			// 
			// tblCSM_DETAIL
			// 
			this.tblCSM_DETAIL.Name = "tblCSM_DETAIL";
			this.tblCSM_DETAIL.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.tblCSM_DETAIL.Location = new System.Drawing.Point(15, 79);
			this.tblCSM_DETAIL.Size = new System.Drawing.Size(416, 152);
			this.tblCSM_DETAIL.UseVisualStyles = true;
			// 
			// tblCSM_DETAIL.colCSINO
			// 
			this.tblCSM_DETAIL.colCSINO.Name = "colCSINO";
			this.tblCSM_DETAIL.colCSINO.Title = "Incident Number";
			this.tblCSM_DETAIL.colCSINO.Width = 136;
			this.tblCSM_DETAIL.colCSINO.Format = "";
			this.tblCSM_DETAIL.colCSINO.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.tblCSM_DETAIL.colCSINO.Position = 1;
			// 
			// tblCSM_DETAIL.colOPENDATE
			// 
			this.tblCSM_DETAIL.colOPENDATE.Name = "colOPENDATE";
			this.tblCSM_DETAIL.colOPENDATE.Title = "Date Assigned";
			this.tblCSM_DETAIL.colOPENDATE.Width = 112;
			this.tblCSM_DETAIL.colOPENDATE.DataType = PPJ.Runtime.Windows.DataType.DateTime;
			this.tblCSM_DETAIL.colOPENDATE.Format = "dd/MM/yyyy";
			this.tblCSM_DETAIL.colOPENDATE.Position = 2;
			// 
			// tblCSM_DETAIL.colLASTCHANGEBY
			// 
			this.tblCSM_DETAIL.colLASTCHANGEBY.Name = "colLASTCHANGEBY";
			this.tblCSM_DETAIL.colLASTCHANGEBY.Visible = false;
			this.tblCSM_DETAIL.colLASTCHANGEBY.Title = "Last Change By";
			this.tblCSM_DETAIL.colLASTCHANGEBY.Width = 34;
			this.tblCSM_DETAIL.colLASTCHANGEBY.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblCSM_DETAIL.colLASTCHANGEBY.Format = "";
			this.tblCSM_DETAIL.colLASTCHANGEBY.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.tblCSM_DETAIL.colLASTCHANGEBY.Position = 3;
			// 
			// tblCSM_DETAIL.colUSERNAME
			// 
			this.tblCSM_DETAIL.colUSERNAME.Name = "colUSERNAME";
			this.tblCSM_DETAIL.colUSERNAME.Title = "Assigned From";
			this.tblCSM_DETAIL.colUSERNAME.Width = 136;
			this.tblCSM_DETAIL.colUSERNAME.Format = "";
			this.tblCSM_DETAIL.colUSERNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.tblCSM_DETAIL.colUSERNAME.Position = 4;
			this.tblCSM_DETAIL.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblCSM_DETAIL_WindowActions);
			this.tblCSM_DETAIL.TabIndex = 5;
			// 
			// dlgCSM_DETAILS
			// 
			this.Name = "dlgCSM_DETAILS";
			this.ClientSize = new System.Drawing.Size(459, 272);
			this.Text = "Customer Service Incidents";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgCSM_DETAILS_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgCSM_DETAILS == this) 
			{
				App.dlgCSM_DETAILS = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tblCSM_DETAIL
		
		public partial class tblCSM_DETAILTableWindow
		{
			#region Window Controls
			public clsCol colCSINO;
			public clsCol colOPENDATE;
			public clsCol colLASTCHANGEBY;
			public clsCol colUSERNAME;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colCSINO = new pamc.common.clsCol();
				this.colOPENDATE = new pamc.common.clsCol();
				this.colLASTCHANGEBY = new pamc.common.clsCol();
				this.colUSERNAME = new pamc.common.clsCol();
				this.SuspendLayout();
				// 
				// colCSINO
				// 
				this.colCSINO.Name = "colCSINO";
				// 
				// colOPENDATE
				// 
				this.colOPENDATE.Name = "colOPENDATE";
				// 
				// colLASTCHANGEBY
				// 
				this.colLASTCHANGEBY.Name = "colLASTCHANGEBY";
				// 
				// colUSERNAME
				// 
				this.colUSERNAME.Name = "colUSERNAME";
				// 
				// tblCSM_DETAIL
				// 
				this.Controls.Add(this.colCSINO);
				this.Controls.Add(this.colOPENDATE);
				this.Controls.Add(this.colLASTCHANGEBY);
				this.Controls.Add(this.colUSERNAME);
				this.Name = "tblCSM_DETAIL";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
