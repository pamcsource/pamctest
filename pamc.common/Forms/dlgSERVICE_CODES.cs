// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	public partial class dlgSERVICE_CODES : clsWindowToDialog
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgSERVICE_CODES()
		{
			// Assign global reference.
			App.dlgSERVICE_CODES = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			dlgSERVICE_CODES dlg = new dlgSERVICE_CODES();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgSERVICE_CODES FromHandle(SalWindowHandle handle)
		{
			return ((dlgSERVICE_CODES)SalWindow.FromHandle(handle, typeof(dlgSERVICE_CODES)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dfSVCCAT WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCAT_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On SAM_Validate
				
				// If NOT ValidateEntry( MyValue, hWndItem )
				
				// If NOT ProcessLookup( hWndItem, LOOKUP_Edit )
				
				// Return VALIDATE_Cancel
				
				// Return VALIDATE_Ok
				
				case Const.AM_Lookup:
					this.dfSVCCAT_OnAM_Lookup(sender, e);
					break;
				
				// On WM_RBUTTONDOWN
				
				// If hWndItem = SalGetFocus()
				
				// Call ProcessLookup( hWndItem, LOOKUP_Edit )
			}
			#endregion
		}
		
		/// <summary>
		/// AM_Lookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCAT_OnAM_Lookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfSVCCATDESC WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCATDESC_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On CM_DoLookup
				
				// Call SqlExists( 'Select SVCCATDESC From _EZSVCCATS Into :dfSVCCATDESC Where SVCCAT = :dfSVCCAT', bGExists )
				
				// If NOT bGExists
				
				// Set MyValue = strNULL
				
				case Const.CM_DoLookup:
					this.dfSVCCATDESC_OnCM_DoLookup(sender, e);
					break;
				
				case Const.CM_NotSelect:
					this.dfSVCCATDESC_OnCM_NotSelect(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCATDESC_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select SVCCATDESC From SERVICE_CATS Where SVCCAT = :dfSVCCAT", ":dfSVCCATDESC ", ref Var.nGInd))) 
			{
				this.dfSVCCATDESC.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_NotSelect event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCATDESC_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// cbNEEDAUTHCB WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNEEDAUTHCB_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.AM_GetValue:
					this.cbNEEDAUTHCB_OnAM_GetValue(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.cbNEEDAUTHCB_OnAM_Initialize(sender, e);
					break;
				
				case Sys.SAM_Click:
					this.cbNEEDAUTHCB_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNEEDAUTHCB_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbNEEDAUTHCB.Checked = this.dfNEEDAUTH.Number;
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNEEDAUTHCB_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbNEEDAUTHCB.Checked = false;
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNEEDAUTHCB_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfNEEDAUTH.Number = this.cbNEEDAUTHCB.Checked;
			Int.RecordChanged();
			#endregion
		}
		#endregion
	}
}
