// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	public partial class dlgMEMB_WAIT : clsWindowToDialog
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgMEMB_WAIT()
		{
			// Assign global reference.
			App.dlgMEMB_WAIT = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			dlgMEMB_WAIT dlg = new dlgMEMB_WAIT();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgMEMB_WAIT FromHandle(SalWindowHandle handle)
		{
			return ((dlgMEMB_WAIT)SalWindow.FromHandle(handle, typeof(dlgMEMB_WAIT)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// cbPENDCB WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbPENDCB_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On AM_Initialize
				
				// If lParam = 0
				
				// Set MyValue = 0
				
				case Const.AM_GetValue:
					this.cbPENDCB_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbPENDCB_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbPENDCB.Checked = this.dfPEND.Number;
			#endregion
		}
		
		/// <summary>
		/// cbCOVEREDCB WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbCOVEREDCB_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On AM_Initialize
				
				// If lParam = 0
				
				// Set MyValue = 0
				
				case Const.AM_GetValue:
					this.cbCOVEREDCB_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbCOVEREDCB_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbCOVEREDCB.Checked = this.dfCOVERED.Number;
			#endregion
		}
		
		/// <summary>
		/// cbNOBENCB WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNOBENCB_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On AM_Initialize
				
				// If lParam = 0
				
				// Set MyValue = 0
				
				case Const.AM_GetValue:
					this.cbNOBENCB_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbNOBENCB_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbNOBENCB.Checked = this.dfNOBEN.Number;
			#endregion
		}
		
		/// <summary>
		/// cbTERMSTATUSCB WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbTERMSTATUSCB_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				// On AM_Initialize
				
				// If lParam = 0
				
				// Set MyValue = 0
				
				case Const.AM_GetValue:
					this.cbTERMSTATUSCB_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbTERMSTATUSCB_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.cbTERMSTATUSCB.Checked = this.dfTERMSTATUS.Number;
			#endregion
		}
		
		/// <summary>
		/// dfAdjCodeDesc WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfAdjCodeDesc_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfAdjCodeDesc_OnCM_DoLookup(sender, e);
					break;
				
				case Const.CM_NotSelect:
					this.dfAdjCodeDesc_OnCM_NotSelect(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfAdjCodeDesc_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select DESCR From  ADJUST_CODES Where CODE = :dfADJCODE", ":dfAdjCodeDesc ", ref Var.nGInd))) 
			{
				this.dfADJCODE.Text = pamc.sqllib.Const.strNULL;
				this.dfAdjCodeDesc.Text = pamc.sqllib.Const.strNULL;
				this.mlAdjCodeDesc.Text = pamc.sqllib.Const.strNULL;
			}
			else
			{
				this.mlAdjCodeDesc.Text = this.dfAdjCodeDesc.Text;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_NotSelect event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfAdjCodeDesc_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		#endregion
	}
}
