// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgFormat
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Toolbar control.
		/// </summary>
		protected SalFormToolBar ToolBar;
		
		/// <summary>
		/// Client area panel.
		/// </summary>
		protected SalFormClientArea ClientArea;
		
		/// <summary>
		/// StatusBar control.
		/// </summary>
		protected SalFormStatusBar StatusBar;
		#endregion
		
		
		#region Window Controls
		public clsDlgApply pbApply;
		public clsDlgClose pbClose;
		public SalPicture pic1;
		public FCSalRadioButton rbCurrent;
		public FCSalRadioButton rbEntire;
		protected clsText label1;
		public clsDataFieldEditN dfLength;
		protected clsText label2;
		protected clsText label3;
		public clsDataFieldEdit dfChar;
		protected clsText label4;
		public clsButtonText pbLeft;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ToolBar = new PPJ.Runtime.Windows.SalFormToolBar();
			this.ClientArea = new PPJ.Runtime.Windows.SalFormClientArea();
			this.StatusBar = new PPJ.Runtime.Windows.SalFormStatusBar();
			this.pbApply = new pamc.common.clsDlgApply();
			this.pbClose = new pamc.common.clsDlgClose();
			this.pic1 = new PPJ.Runtime.Windows.SalPicture();
			this.rbCurrent = new pamc.common.FCSalRadioButton();
			this.rbEntire = new pamc.common.FCSalRadioButton();
			this.label1 = new pamc.common.clsText();
			this.dfLength = new pamc.common.clsDataFieldEditN();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.dfChar = new pamc.common.clsDataFieldEdit();
			this.label4 = new pamc.common.clsText();
			this.pbLeft = new pamc.common.clsButtonText();
			this.ToolBar.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.BackColor = System.Drawing.Color.Gray;
			this.ToolBar.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.ToolBar.Height = 36;
			this.ToolBar.TabStop = true;
			this.ToolBar.Visible = true;
			this.ToolBar.Create = true;
			this.ToolBar.Controls.Add(this.pbClose);
			this.ToolBar.Controls.Add(this.pbApply);
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.pbLeft);
			this.ClientArea.Controls.Add(this.dfChar);
			this.ClientArea.Controls.Add(this.dfLength);
			this.ClientArea.Controls.Add(this.rbEntire);
			this.ClientArea.Controls.Add(this.rbCurrent);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.pic1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Visible = true;
			// 
			// pbApply
			// 
			this.pbApply.Name = "pbApply";
			this.pbApply.Location = new System.Drawing.Point(1, -1);
			this.pbApply.Size = new System.Drawing.Size(78, 35);
			this.pbApply.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbApply_WindowActions);
			this.pbApply.TabIndex = 0;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.Location = new System.Drawing.Point(327, -1);
			this.pbClose.Size = new System.Drawing.Size(78, 35);
			this.pbClose.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbClose_WindowActions);
			this.pbClose.TabIndex = 1;
			// 
			// pic1
			// 
			this.pic1.Name = "pic1";
			this.pic1.BackColor = System.Drawing.Color.Gray;
			this.pic1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pic1.ImageStorage = ImageStorage.External;
			this.pic1.Dock = System.Windows.Forms.DockStyle.None;
			this.pic1.Location = new System.Drawing.Point(7, 47);
			this.pic1.Size = new System.Drawing.Size(392, 56);
			this.pic1.TabIndex = 0;
			// 
			// rbCurrent
			// 
			this.rbCurrent.Name = "rbCurrent";
			this.rbCurrent.BackColor = System.Drawing.Color.Gray;
			this.rbCurrent.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbCurrent.ForeColor = System.Drawing.Color.White;
			this.rbCurrent.Text = "Currently selected rows";
			this.rbCurrent.Location = new System.Drawing.Point(151, 58);
			this.rbCurrent.Size = new System.Drawing.Size(160, 19);
			this.rbCurrent.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbCurrent_WindowActions);
			this.rbCurrent.TabIndex = 1;
			// 
			// rbEntire
			// 
			this.rbEntire.Name = "rbEntire";
			this.rbEntire.BackColor = System.Drawing.Color.Gray;
			this.rbEntire.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbEntire.ForeColor = System.Drawing.Color.White;
			this.rbEntire.Text = "Entire Table";
			this.rbEntire.Location = new System.Drawing.Point(151, 79);
			this.rbEntire.Size = new System.Drawing.Size(104, 19);
			this.rbEntire.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rbEntire_WindowActions);
			this.rbEntire.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Set to Length:";
			this.label1.Location = new System.Drawing.Point(7, 19);
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 3;
			// 
			// dfLength
			// 
			this.dfLength.Name = "dfLength";
			this.dfLength.MaxLength = 2;
			this.dfLength.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfLength.Format = "#0";
			this.dfLength.Location = new System.Drawing.Point(95, 15);
			this.dfLength.Size = new System.Drawing.Size(32, 20);
			this.dfLength.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfLength_WindowActions);
			this.dfLength.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.BackColor = System.Drawing.Color.Gray;
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Text = "Apply formatting to:";
			this.label2.Location = new System.Drawing.Point(23, 59);
			this.label2.Size = new System.Drawing.Size(120, 16);
			this.label2.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "and fill with Character:";
			this.label3.Location = new System.Drawing.Point(135, 19);
			this.label3.Size = new System.Drawing.Size(136, 16);
			this.label3.TabIndex = 6;
			// 
			// dfChar
			// 
			this.dfChar.Name = "dfChar";
			this.dfChar.MaxLength = 1;
			this.dfChar.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfChar.Location = new System.Drawing.Point(271, 15);
			this.dfChar.Size = new System.Drawing.Size(24, 20);
			this.dfChar.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfChar_WindowActions);
			this.dfChar.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "to the";
			this.label4.Location = new System.Drawing.Point(303, 19);
			this.label4.Size = new System.Drawing.Size(40, 16);
			this.label4.TabIndex = 8;
			// 
			// pbLeft
			// 
			this.pbLeft.Name = "pbLeft";
			this.pbLeft.BackColor = System.Drawing.Color.White;
			this.pbLeft.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbLeft.ForeColor = System.Drawing.Color.Black;
			this.pbLeft.Location = new System.Drawing.Point(343, 14);
			this.pbLeft.Size = new System.Drawing.Size(52, 23);
			this.pbLeft.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbLeft_WindowActions);
			this.pbLeft.TabIndex = 9;
			// 
			// dlgFormat
			// 
			this.Controls.Add(this.ClientArea);
			this.Controls.Add(this.ToolBar);
			this.Controls.Add(this.StatusBar);
			this.Name = "dlgFormat";
			this.ClientSize = new System.Drawing.Size(410, 162);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.BackColor = System.Drawing.Color.Silver;
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "Format Field";
			this.Location = new System.Drawing.Point(46, 90);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgFormat_WindowActions);
			this.ToolBar.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgFormat == this) 
			{
				App.dlgFormat = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
