// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Modal dialog box which allows you to enter FROM and TO DATES for
	/// a New Current record or a New History record. This is used by all screens
	/// that require from/todates with the exception of FeeSetAssignment which
	/// uses dlgNEWDATE1. This dialog box is used in conjunction with the
	/// Form Window and the Table Window.
	/// </summary>
	/// <param name="dtParamNEWFROMDATE"></param>
	/// <param name="dtParamNEWTODATE"></param>
	/// <param name="bCurrent"></param>
	/// <param name="dtBeforeToDate"></param>
	/// <param name="bGap"></param>
	public partial class dlgNEWDATE : SalDialogBox
	{
		#region Window Parameters
		public SalDateTime dtParamNEWFROMDATE;
		public SalDateTime dtParamNEWTODATE;
		public SalBoolean bCurrent;
		public SalDateTime dtBeforeToDate;
		public SalBoolean bGap;
		#endregion
		
		#region Window Variables
		public SalDateTime dtOldFromDate = SalDateTime.Null;
		public SalString strOldFromDate = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgNEWDATE(SalDateTime dtParamNEWFROMDATE, SalDateTime dtParamNEWTODATE, SalBoolean bCurrent, SalDateTime dtBeforeToDate, SalBoolean bGap)
		{
			// Assign global reference.
			App.dlgNEWDATE = this;
			// Window Parameters initialization.
			this.dtParamNEWFROMDATE = dtParamNEWFROMDATE;
			this.dtParamNEWTODATE = dtParamNEWTODATE;
			this.bCurrent = bCurrent;
			this.dtBeforeToDate = dtBeforeToDate;
			this.bGap = bGap;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, ref SalDateTime dtParamNEWFROMDATE, ref SalDateTime dtParamNEWTODATE, SalBoolean bCurrent, SalDateTime dtBeforeToDate, SalBoolean bGap)
		{
			dlgNEWDATE dlg = new dlgNEWDATE(dtParamNEWFROMDATE, dtParamNEWTODATE, bCurrent, dtBeforeToDate, bGap);
			SalNumber ret = dlg.ShowDialog(owner);
			dtParamNEWFROMDATE = dlg.dtParamNEWFROMDATE;
			dtParamNEWTODATE = dlg.dtParamNEWTODATE;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static dlgNEWDATE FromHandle(SalWindowHandle handle)
		{
			return ((dlgNEWDATE)SalWindow.FromHandle(handle, typeof(dlgNEWDATE)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgNEWDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgNEWDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgNEWDATE_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgNEWDATE_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.WaitCursor(false);
			if (this.bCurrent) 
			{
				this.SetText("New Current...");
				this.mlMESSAGE.Text = "Creating a New Current Record requires that you enter the From Date.";
			}
			else
			{
				this.SetText("Add History...");
				this.mlMESSAGE.Text = "Creating a New History Record requires that you enter the From Date.";
			}
			#endregion
		}
		
		/// <summary>
		/// dfNEWFROMDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNEWFROMDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfNEWFROMDATE_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNEWFROMDATE_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bCurrent) 
			{
				if (this.dtParamNEWTODATE != SalDateTime.Null) 
				{
					this.dfNEWFROMDATE.DateTime = this.dtParamNEWTODATE + 1;
					this.dfNEWFROMDATE.DisableWindow();
				}
				else
				{
					this.dfNEWFROMDATE.DateTime = SalDateTime.Current;
					if (this.dfNEWFROMDATE.DateTime <= this.dtParamNEWFROMDATE) 
					{
						this.dfNEWFROMDATE.DateTime = this.dtParamNEWFROMDATE + 1;
					}
				}
				this.dtOldFromDate = this.dtParamNEWFROMDATE;
			}
			else if (this.dtBeforeToDate != SalDateTime.Null) 
			{
				this.dfNEWFROMDATE.DateTime = this.dtParamNEWFROMDATE - 2;
			}
			#endregion
		}
		
		/// <summary>
		/// dfNEWTODATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNEWTODATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dfNEWTODATE_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfNEWTODATE_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bCurrent) && !(this.bGap)) 
			{
				this.dfNEWTODATE.DateTime = this.dtParamNEWFROMDATE - 1;
				this.dfNEWTODATE.DisableWindow();
			}
			else if (!(this.bCurrent) && this.bGap) 
			{
				this.dfNEWTODATE.DateTime = this.dtParamNEWFROMDATE - 1;
			}
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.IsNull(((SalDataField)Sys.hWndForm.FindControl("dfNEWFROMDATE")))) 
			{
				Sal.MessageBox("From Date is Required", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				Int.SetFocusToFirstEditableField(this);
				e.Return = false;
				return;
			}
			if (this.bCurrent) 
			{
				if (!(Sal.IsNull(((SalDataField)Sys.hWndForm.FindControl("dfNEWTODATE"))))) 
				{
					if (((SalDataField)Sys.hWndForm.FindControl("dfNEWFROMDATE")).DateTime > ((SalDataField)Sys.hWndForm.FindControl("dfNEWTODATE")).DateTime) 
					{
						Sal.MessageBox("From Date must be less than To Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
						Int.SetFocusToFirstEditableField(this);
						e.Return = false;
						return;
					}
				}
				if (((SalDataField)Sys.hWndForm.FindControl("dfNEWFROMDATE")).DateTime <= Sys.hWndForm.FindMember("dtOldFromDate").DateTime) 
				{
					Sal.MessageBox("The entered From Date must be greater than From Date of the most current record (" + this.dtOldFromDate.ToString("MM/dd/yyyy") + ").", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
					Int.SetFocusToFirstEditableField(this);
					e.Return = false;
					return;
				}
			}
			else
			{
				if (this.dfNEWFROMDATE.DateTime > this.dfNEWTODATE.DateTime) 
				{
					Sal.MessageBox("From Date must be less than To Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
					Int.SetFocusToFirstEditableField(this);
					e.Return = false;
					return;
				}
				if (this.dtBeforeToDate != SalDateTime.Null) 
				{
					if (this.bGap) 
					{
						if (this.dfNEWFROMDATE.DateTime <= this.dtBeforeToDate) 
						{
							Sal.MessageBox("The entered Date will overlap an existing record.", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
							Int.SetFocusToFirstEditableField(this);
							e.Return = false;
							return;
						}
					}
				}
				else
				{
					if (!(Int.FromAndToDates(Var.hWndMainTable, this.dfNEWFROMDATE.DateTime, this.dfNEWTODATE.DateTime, SalWindowHandle.Null, Const.HISTORY))) 
					{
						Int.SetFocusToFirstEditableField(this);
						e.Return = false;
						return;
					}
				}
			}
			this.dtParamNEWFROMDATE = this.dfNEWFROMDATE.DateTime;
			this.dtParamNEWTODATE = this.dfNEWTODATE.DateTime;
			this.EndDialog(1);
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		#endregion
	}
}
