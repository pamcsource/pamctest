// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class frmOUTCOME_CODES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private App.NamedMenus.menuFile menu_menuFile;
		private App.NamedMenus.menuRecordDefault menu_menuRecordDefault;
		private App.NamedMenus.menuView menu_menuView;
		private App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private App.NamedMenus.menuHelp menu_menuHelp;
		// On SAM_Validate
		// If NOT SalIsNull( hWndItem ) AND SqlLookup( 'Select CODE From OUTCOME_CODES Where CODE = :dfCODE', nGInd )
		// Call ValidateErrorMsgBox( 'Outcome Code already exists, try again.' )
		// Return VALIDATE_Cancel
		// Set MyValue = SalStrTrimX(MyValue)
		// Return VALIDATE_Ok
		public clsDataFieldEditKey dfCODE;
		public clsDefault df1;
		public clsDataFieldEdit dfDESCR;
		protected clsText label1;
		protected clsText label2;
		public clsDataFieldDisabled dfSELECTABLE;
		public clsDataFieldDisabled dfEDITABLE;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new App.NamedMenus.menuFile();
			this.menu_menuRecordDefault = new App.NamedMenus.menuRecordDefault();
			this.menu_menuView = new App.NamedMenus.menuView();
			this.menu_menuMDIWindows = new App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new App.NamedMenus.menuHelp();
			this.dfCODE = new pamc.common.clsDataFieldEditKey();
			this.df1 = new pamc.common.clsDefault();
			this.dfDESCR = new pamc.common.clsDataFieldEdit();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.dfSELECTABLE = new pamc.common.clsDataFieldDisabled();
			this.dfEDITABLE = new pamc.common.clsDataFieldDisabled();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecordDefault);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// dfCODE
			// 
			this.dfCODE.Name = "dfCODE";
			this.dfCODE.MaxLength = 2;
			this.dfCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCODE.Location = new System.Drawing.Point(146, 9);
			this.dfCODE.Size = new System.Drawing.Size(37, 20);
			this.dfCODE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCODE_WindowActions);
			this.dfCODE.TabIndex = 0;
			// 
			// df1
			// 
			this.df1.Name = "df1";
			this.df1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.df1.Location = new System.Drawing.Point(191, 12);
			this.df1.Size = new System.Drawing.Size(80, 16);
			this.df1.TabIndex = 1;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 30;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(146, 37);
			this.dfDESCR.Size = new System.Drawing.Size(224, 20);
			this.dfDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDESCR_WindowActions);
			this.dfDESCR.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Code:";
			this.label1.Location = new System.Drawing.Point(10, 11);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(10, 41);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 4;
			// 
			// dfSELECTABLE
			// 
			this.dfSELECTABLE.Name = "dfSELECTABLE";
			this.dfSELECTABLE.Visible = false;
			this.dfSELECTABLE.MaxLength = 1;
			this.dfSELECTABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfSELECTABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSELECTABLE.Format = "";
			this.dfSELECTABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfSELECTABLE.Location = new System.Drawing.Point(0, 0);
			this.dfSELECTABLE.Size = new System.Drawing.Size(66, 19);
			this.dfSELECTABLE.TabIndex = 5;
			// 
			// dfEDITABLE
			// 
			this.dfEDITABLE.Name = "dfEDITABLE";
			this.dfEDITABLE.Visible = false;
			this.dfEDITABLE.MaxLength = 1;
			this.dfEDITABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.dfEDITABLE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfEDITABLE.Format = "";
			this.dfEDITABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfEDITABLE.Location = new System.Drawing.Point(0, 0);
			this.dfEDITABLE.Size = new System.Drawing.Size(66, 19);
			this.dfEDITABLE.TabIndex = 6;
			// 
			// frmOUTCOME_CODES
			// 
			this.Controls.Add(this.dfEDITABLE);
			this.Controls.Add(this.dfSELECTABLE);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.df1);
			this.Controls.Add(this.dfCODE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmOUTCOME_CODES";
			this.ClientSize = new System.Drawing.Size(440, 37);
			this.Text = "Outcome Code";
			this.Location = new System.Drawing.Point(0, 0);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.frmOUTCOME_CODES_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.frmOUTCOME_CODES == this) 
			{
				App.frmOUTCOME_CODES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
