// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Used to lookup procedure codes in set tables
	/// </summary>
	/// <param name="strDf1"></param>
	/// <param name="strDf2"></param>
	/// <param name="strDf3"></param>
	/// <param name="strText1"></param>
	/// <param name="strText2"></param>
	/// <param name="strText3"></param>
	public partial class dlgLookup3 : SalDialogBox
	{
		#region Window Parameters
		public SalString strDf1;
		public SalString strDf2;
		public SalString strDf3;
		public SalString strText1;
		public SalString strText2;
		public SalString strText3;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgLookup3(SalString strDf1, SalString strDf2, SalString strDf3, SalString strText1, SalString strText2, SalString strText3)
		{
			// Assign global reference.
			App.dlgLookup3 = this;
			// Window Parameters initialization.
			this.strDf1 = strDf1;
			this.strDf2 = strDf2;
			this.strDf3 = strDf3;
			this.strText1 = strText1;
			this.strText2 = strText2;
			this.strText3 = strText3;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, ref SalString strDf1, ref SalString strDf2, ref SalString strDf3, SalString strText1, SalString strText2, SalString strText3)
		{
			dlgLookup3 dlg = new dlgLookup3(strDf1, strDf2, strDf3, strText1, strText2, strText3);
			SalNumber ret = dlg.ShowDialog(owner);
			strDf1 = dlg.strDf1;
			strDf2 = dlg.strDf2;
			strDf3 = dlg.strDf3;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static dlgLookup3 FromHandle(SalWindowHandle handle)
		{
			return ((dlgLookup3)SalWindow.FromHandle(handle, typeof(dlgLookup3)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgLookup3 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgLookup3_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgLookup3_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgLookup3_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfText1.Text = this.strText1;
			this.dfText2.Text = this.strText2;
			this.dfText3.Text = this.strText3;
			if (this.strDf3 == pamc.sqllib.Const.strNULL) 
			{
				Sal.DisableWindow(this.cb1);
			}
			this.cb1.Checked = false;
			this.df1.Text = this.strDf1;
			this.df2.Text = this.strDf2;
			// Set df3 = strDf3
			Int.SetPopupLoc(this);
			#endregion
		}
		
		/// <summary>
		/// df2 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void df2_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case pamc.ez32bit.Const.WM_CHAR:
					this.df2_OnWM_CHAR(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// WM_CHAR event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void df2_OnWM_CHAR(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Accept numeric digits, backspace, or period only. Negatives not accepted.
			if ((Sys.wParam >= Vis.VK_0 && Sys.wParam <= Vis.VK_9) || Sys.wParam == Vis.VK_Backspace) 
			{
			}
			else
			{
				e.Return = false;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// df3 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void df3_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.df3_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void df3_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.df3.IsEmpty()) && this.dfText3.Text != "Member ID") 
			{
				this.df3.Text = Int.ProcedurePad(this.df3.Text);
			}
			#endregion
		}
		
		/// <summary>
		/// cb1 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cb1_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.cb1_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cb1_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.cb1.Checked == true) 
			{
				this.df3.Text = this.strDf3;
			}
			else
			{
				this.df3.Clear();
			}
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.strDf1 = Vis.StrSubstitute(this.df1.Text, "\'", "\'\'");
			this.strDf2 = Vis.StrSubstitute(this.df2.Text, "\'", "\'\'");
			this.strDf3 = Vis.StrSubstitute(this.df3.Text, "\'", "\'\'");
			this.EndDialog(1);
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		#endregion
	}
}
