// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class tblCSM_SUBTYPES
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Accessories
		
		/// <summary>
		/// Window Main Menu control.
		/// </summary>
		private SalFormMainMenu MainMenu;
		#endregion
		
		
		#region Window Controls
		private App.NamedMenus.menuFile menu_menuFile;
		private App.NamedMenus.menuRecordDefault menu_menuRecordDefault;
		private App.NamedMenus.menuView menu_menuView;
		private App.NamedMenus.menuMDIWindows menu_menuMDIWindows;
		private App.NamedMenus.menuHelp menu_menuHelp;
		public clsColQueryKey colINCTYPE;
		public clsColQueryKey colSUBTYPE;
		public clsColQuery colDESCR;
		// clsColCB: colEDITABLE
		// List Values
		// Message Actions
		// clsColCB: colSELECTABLE
		// List Values
		// Message Actions
		public clsCol colDEFAULT_YN;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new PPJ.Runtime.Windows.SalFormMainMenu();
			this.menu_menuFile = new App.NamedMenus.menuFile();
			this.menu_menuRecordDefault = new App.NamedMenus.menuRecordDefault();
			this.menu_menuView = new App.NamedMenus.menuView();
			this.menu_menuMDIWindows = new App.NamedMenus.menuMDIWindows();
			this.menu_menuHelp = new App.NamedMenus.menuHelp();
			this.colINCTYPE = new pamc.common.clsColQueryKey();
			this.colSUBTYPE = new pamc.common.clsColQueryKey();
			this.colDESCR = new pamc.common.clsColQuery();
			this.colDEFAULT_YN = new pamc.common.clsCol();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.MenuItems.Add(this.menu_menuFile);
			this.MainMenu.MenuItems.Add(this.menu_menuRecordDefault);
			this.MainMenu.MenuItems.Add(this.menu_menuView);
			this.MainMenu.MenuItems.Add(this.menu_menuMDIWindows);
			this.MainMenu.MenuItems.Add(this.menu_menuHelp);
			// 
			// colROWID
			// 
			this.colROWID.Name = "colROWID";
			this.colROWID.TabIndex = 0;
			// 
			// colCreateBy
			// 
			this.colCreateBy.Name = "colCreateBy";
			this.colCreateBy.TabIndex = 1;
			// 
			// colCreateDate
			// 
			this.colCreateDate.Name = "colCreateDate";
			this.colCreateDate.TabIndex = 2;
			// 
			// colLastChangeBy
			// 
			this.colLastChangeBy.Name = "colLastChangeBy";
			this.colLastChangeBy.TabIndex = 3;
			// 
			// colLastChangeDate
			// 
			this.colLastChangeDate.Name = "colLastChangeDate";
			this.colLastChangeDate.TabIndex = 4;
			// 
			// colINCTYPE
			// 
			this.colINCTYPE.Name = "colINCTYPE";
			this.colINCTYPE.Title = "Incident \r\nType";
			this.colINCTYPE.MaxLength = 5;
			this.colINCTYPE.TabIndex = 5;
			// 
			// colSUBTYPE
			// 
			this.colSUBTYPE.Name = "colSUBTYPE";
			this.colSUBTYPE.Title = "Incident \r\nSubtype";
			this.colSUBTYPE.Width = 70;
			this.colSUBTYPE.MaxLength = 5;
			this.colSUBTYPE.TabIndex = 6;
			// 
			// colDESCR
			// 
			this.colDESCR.Name = "colDESCR";
			this.colDESCR.Title = "Description";
			this.colDESCR.Width = 227;
			this.colDESCR.MaxLength = 20;
			this.colDESCR.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.colDESCR_WindowActions);
			this.colDESCR.TabIndex = 7;
			// 
			// colDEFAULT_YN
			// 
			this.colDEFAULT_YN.Name = "colDEFAULT_YN";
			this.colDEFAULT_YN.Title = "Default";
			this.colDEFAULT_YN.Width = 64;
			this.colDEFAULT_YN.MaxLength = 1;
			this.colDEFAULT_YN.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.colDEFAULT_YN.Format = "";
			this.colDEFAULT_YN.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.colDEFAULT_YN.CellType = PPJ.Runtime.Windows.CellType.CheckBox;
			this.colDEFAULT_YN.CheckBox.CheckedValue = "1";
			this.colDEFAULT_YN.TabIndex = 8;
			// 
			// tblCSM_SUBTYPES
			// 
			this.Controls.Add(this.colROWID);
			this.Controls.Add(this.colCreateBy);
			this.Controls.Add(this.colCreateDate);
			this.Controls.Add(this.colLastChangeBy);
			this.Controls.Add(this.colLastChangeDate);
			this.Controls.Add(this.colINCTYPE);
			this.Controls.Add(this.colSUBTYPE);
			this.Controls.Add(this.colDESCR);
			this.Controls.Add(this.colDEFAULT_YN);
			this.Name = "tblCSM_SUBTYPES";
			this.Text = "Incident Subtypes";
			this.Location = new System.Drawing.Point(3, 6);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblCSM_SUBTYPES_WindowActions);
			this.Menu = this.MainMenu;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tblCSM_SUBTYPES == this) 
			{
				App.tblCSM_SUBTYPES = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
