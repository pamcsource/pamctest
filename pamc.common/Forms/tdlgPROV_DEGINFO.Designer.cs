// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class tdlgPROV_DEGINFO
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public tdlgPROV_DEGINFO.tbl1TableWindow tblPROV_DEGINFO;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tblPROV_DEGINFO = (tdlgPROV_DEGINFO.tbl1TableWindow)CreateTableWindow(typeof(tdlgPROV_DEGINFO.tbl1TableWindow));
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.tblPROV_DEGINFO);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbSave);
			this.ClientArea.Controls.Add(this.pbDelete);
			this.ClientArea.Controls.Add(this.pbEdit);
			this.ClientArea.Controls.Add(this.pbNew);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.frame2);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&A";
			this.label1.TabIndex = 1;
			// 
			// pbNew
			// 
			this.pbNew.Name = "pbNew";
			this.pbNew.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbNew.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Text = "&E";
			this.label2.TabIndex = 3;
			// 
			// pbEdit
			// 
			this.pbEdit.Name = "pbEdit";
			this.pbEdit.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbEdit.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Text = "&D";
			this.label3.TabIndex = 5;
			// 
			// pbDelete
			// 
			this.pbDelete.Name = "pbDelete";
			this.pbDelete.TabIndex = 6;
			// 
			// pbSave
			// 
			this.pbSave.Name = "pbSave";
			this.pbSave.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbSave.TabIndex = 7;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Text = "&T";
			this.label4.TabIndex = 9;
			// 
			// tblPROV_DEGINFO
			// 
			this.tblPROV_DEGINFO.Name = "tblPROV_DEGINFO";
			this.tblPROV_DEGINFO.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			// 
			// tblPROV_DEGINFO.colSEQUENCE
			// 
			this.tblPROV_DEGINFO.colSEQUENCE.Name = "colSEQUENCE";
			this.tblPROV_DEGINFO.colSEQUENCE.Position = 1;
			// 
			// tblPROV_DEGINFO.colROWID
			// 
			this.tblPROV_DEGINFO.colROWID.Name = "colROWID";
			this.tblPROV_DEGINFO.colROWID.Position = 2;
			// 
			// tblPROV_DEGINFO.colPROVID
			// 
			this.tblPROV_DEGINFO.colPROVID.Name = "colPROVID";
			this.tblPROV_DEGINFO.colPROVID.Position = 3;
			// 
			// tblPROV_DEGINFO.colTYPE
			// 
			this.tblPROV_DEGINFO.colTYPE.Name = "colTYPE";
			this.tblPROV_DEGINFO.colTYPE.Position = 4;
			// 
			// tblPROV_DEGINFO.colVERIFY
			// 
			this.tblPROV_DEGINFO.colVERIFY.Name = "colVERIFY";
			this.tblPROV_DEGINFO.colVERIFY.Position = 5;
			// 
			// tblPROV_DEGINFO.colVERIFYDATE
			// 
			this.tblPROV_DEGINFO.colVERIFYDATE.Name = "colVERIFYDATE";
			this.tblPROV_DEGINFO.colVERIFYDATE.Position = 6;
			// 
			// tblPROV_DEGINFO.colVERIFYRESULT
			// 
			this.tblPROV_DEGINFO.colVERIFYRESULT.Name = "colVERIFYRESULT";
			this.tblPROV_DEGINFO.colVERIFYRESULT.Position = 7;
			// 
			// tblPROV_DEGINFO.colVERIFYNEXT
			// 
			this.tblPROV_DEGINFO.colVERIFYNEXT.Name = "colVERIFYNEXT";
			this.tblPROV_DEGINFO.colVERIFYNEXT.Position = 8;
			// 
			// tblPROV_DEGINFO.colNOTES
			// 
			this.tblPROV_DEGINFO.colNOTES.Name = "colNOTES";
			this.tblPROV_DEGINFO.colNOTES.Position = 9;
			// 
			// tblPROV_DEGINFO.colCreateBy
			// 
			this.tblPROV_DEGINFO.colCreateBy.Name = "colCreateBy";
			this.tblPROV_DEGINFO.colCreateBy.Position = 10;
			// 
			// tblPROV_DEGINFO.colCreateDate
			// 
			this.tblPROV_DEGINFO.colCreateDate.Name = "colCreateDate";
			this.tblPROV_DEGINFO.colCreateDate.Position = 11;
			// 
			// tblPROV_DEGINFO.colLastChangeBy
			// 
			this.tblPROV_DEGINFO.colLastChangeBy.Name = "colLastChangeBy";
			this.tblPROV_DEGINFO.colLastChangeBy.Position = 12;
			// 
			// tblPROV_DEGINFO.colLastChangeDate
			// 
			this.tblPROV_DEGINFO.colLastChangeDate.Name = "colLastChangeDate";
			this.tblPROV_DEGINFO.colLastChangeDate.Position = 13;
			// 
			// tblPROV_DEGINFO.colDEGREE
			// 
			this.tblPROV_DEGINFO.colDEGREE.Name = "colDEGREE";
			this.tblPROV_DEGINFO.colDEGREE.Title = "Degree";
			this.tblPROV_DEGINFO.colDEGREE.Width = 87;
			this.tblPROV_DEGINFO.colDEGREE.MaxLength = 10;
			this.tblPROV_DEGINFO.colDEGREE.Position = 14;
			// 
			// tblPROV_DEGINFO.colYEARGRAD
			// 
			this.tblPROV_DEGINFO.colYEARGRAD.Name = "colYEARGRAD";
			this.tblPROV_DEGINFO.colYEARGRAD.Title = "Graduation\r\nYear";
			this.tblPROV_DEGINFO.colYEARGRAD.Width = 68;
			this.tblPROV_DEGINFO.colYEARGRAD.MaxLength = 4;
			this.tblPROV_DEGINFO.colYEARGRAD.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblPROV_DEGINFO.colYEARGRAD.Format = "0000";
			this.tblPROV_DEGINFO.colYEARGRAD.Position = 15;
			// 
			// tblPROV_DEGINFO.colSCHOOL
			// 
			this.tblPROV_DEGINFO.colSCHOOL.Name = "colSCHOOL";
			this.tblPROV_DEGINFO.colSCHOOL.Title = "School";
			this.tblPROV_DEGINFO.colSCHOOL.Width = 226;
			this.tblPROV_DEGINFO.colSCHOOL.MaxLength = 30;
			this.tblPROV_DEGINFO.colSCHOOL.Position = 16;
			// 
			// tblPROV_DEGINFO.colLOCATION
			// 
			this.tblPROV_DEGINFO.colLOCATION.Name = "colLOCATION";
			this.tblPROV_DEGINFO.colLOCATION.Title = "Location";
			this.tblPROV_DEGINFO.colLOCATION.Width = 160;
			this.tblPROV_DEGINFO.colLOCATION.MaxLength = 20;
			this.tblPROV_DEGINFO.colLOCATION.Position = 17;
			this.tblPROV_DEGINFO.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblPROV_DEGINFO_WindowActions);
			this.tblPROV_DEGINFO.TabIndex = 10;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.TabIndex = 11;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 12;
			// 
			// tdlgPROV_DEGINFO
			// 
			this.Name = "tdlgPROV_DEGINFO";
			this.Text = "Provider Degrees";
			this.Location = new System.Drawing.Point(6, 165);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tdlgPROV_DEGINFO_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tdlgPROV_DEGINFO == this) 
			{
				App.tdlgPROV_DEGINFO = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tblPROV_DEGINFO
		
		public partial class tbl1TableWindow
		{
			#region Window Controls
			public clsCol colDEGREE;
			public clsCol colYEARGRAD;
			public clsCol colSCHOOL;
			public clsCol colLOCATION;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colDEGREE = new pamc.common.clsCol();
				this.colYEARGRAD = new pamc.common.clsCol();
				this.colSCHOOL = new pamc.common.clsCol();
				this.colLOCATION = new pamc.common.clsCol();
				this.SuspendLayout();
				// 
				// colSEQUENCE
				// 
				this.colSEQUENCE.Name = "colSEQUENCE";
				// 
				// colROWID
				// 
				this.colROWID.Name = "colROWID";
				// 
				// colPROVID
				// 
				this.colPROVID.Name = "colPROVID";
				// 
				// colTYPE
				// 
				this.colTYPE.Name = "colTYPE";
				// 
				// colVERIFY
				// 
				this.colVERIFY.Name = "colVERIFY";
				// 
				// colVERIFYDATE
				// 
				this.colVERIFYDATE.Name = "colVERIFYDATE";
				// 
				// colVERIFYRESULT
				// 
				this.colVERIFYRESULT.Name = "colVERIFYRESULT";
				// 
				// colVERIFYNEXT
				// 
				this.colVERIFYNEXT.Name = "colVERIFYNEXT";
				// 
				// colNOTES
				// 
				this.colNOTES.Name = "colNOTES";
				// 
				// colCreateBy
				// 
				this.colCreateBy.Name = "colCreateBy";
				// 
				// colCreateDate
				// 
				this.colCreateDate.Name = "colCreateDate";
				// 
				// colLastChangeBy
				// 
				this.colLastChangeBy.Name = "colLastChangeBy";
				// 
				// colLastChangeDate
				// 
				this.colLastChangeDate.Name = "colLastChangeDate";
				// 
				// colDEGREE
				// 
				this.colDEGREE.Name = "colDEGREE";
				// 
				// colYEARGRAD
				// 
				this.colYEARGRAD.Name = "colYEARGRAD";
				// 
				// colSCHOOL
				// 
				this.colSCHOOL.Name = "colSCHOOL";
				// 
				// colLOCATION
				// 
				this.colLOCATION.Name = "colLOCATION";
				// 
				// tblPROV_DEGINFO
				// 
				this.Controls.Add(this.colSEQUENCE);
				this.Controls.Add(this.colROWID);
				this.Controls.Add(this.colPROVID);
				this.Controls.Add(this.colTYPE);
				this.Controls.Add(this.colVERIFY);
				this.Controls.Add(this.colVERIFYDATE);
				this.Controls.Add(this.colVERIFYRESULT);
				this.Controls.Add(this.colVERIFYNEXT);
				this.Controls.Add(this.colNOTES);
				this.Controls.Add(this.colCreateBy);
				this.Controls.Add(this.colCreateDate);
				this.Controls.Add(this.colLastChangeBy);
				this.Controls.Add(this.colLastChangeDate);
				this.Controls.Add(this.colDEGREE);
				this.Controls.Add(this.colYEARGRAD);
				this.Controls.Add(this.colSCHOOL);
				this.Controls.Add(this.colLOCATION);
				this.Name = "tblPROV_DEGINFO";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
