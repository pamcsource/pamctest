// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	public partial class tdlgPROV_ADDINFO : clsDlgInfoTableProviders
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public tdlgPROV_ADDINFO()
		{
			// Assign global reference.
			App.tdlgPROV_ADDINFO = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			tdlgPROV_ADDINFO dlg = new tdlgPROV_ADDINFO();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static tdlgPROV_ADDINFO FromHandle(SalWindowHandle handle)
		{
			return ((tdlgPROV_ADDINFO)SalWindow.FromHandle(handle, typeof(tdlgPROV_ADDINFO)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// tdlgPROV_ADDINFO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_ADDINFO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_Edit:
					this.tdlgPROV_ADDINFO_OnCM_Edit(sender, e);
					break;
				
				case Const.CM_New:
					this.tdlgPROV_ADDINFO_OnCM_New(sender, e);
					break;
				
				// On AM_Initialize
				
				// Call AddressTable.Initialize( hWndChildTbl )
				
				// On CM_CopyTableData
				
				// Call VendorTable.FillTable( hWndChildTbl )
				
				// On CM_SetInstanceVars
				
				// Call SetMinRows( 1 )
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Edit event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_ADDINFO_OnCM_Edit(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgPROV_ADDINFO.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_Edit);
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_New event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_ADDINFO_OnCM_New(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgPROV_ADDINFO.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_New);
			return;
			#endregion
		}
		
		/// <summary>
		/// tblPROV_ADDINFO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_ADDINFO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.tblPROV_ADDINFO_OnCM_SetInstanceVars(sender, e);
					break;
				
				// On SAM_CreateComplete
				
				// Call SalSendClassMessage( SAM_CreateComplete, wParam, lParam )
				
				// Call SalTblSetColumnTitle( colSTREET, SalStrProperX(ClientConfig.GetPROVADDRESS1()) )
				
				// Call SalTblSetColumnTitle( colSTREET2, SalStrProperX(ClientConfig.GetPROVADDRESS2()) )
				
				// Call SalTblSetColumnTitle( colSTREET3, SalStrProperX(ClientConfig.GetPROVADDRESS3()) )
				
				// Call SalTblSetColumnTitle( colCITY, SalStrProperX(ClientConfig.GetPROVADDRESS4()) )
				
				// Call SalTblSetColumnTitle( colSTREET4, SalStrProperX(ClientConfig.GetPROVADDRESS5()) )
				
				// Call SalTblSetColumnTitle( colZIP, SalStrProperX(ClientConfig.GetPROVPOSTAL()) )
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_ADDINFO_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblPROV_ADDINFO.strWhere = "PROVID = :hWndOwner.dfPROVID";
			this.tblPROV_ADDINFO.colSTREET.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVADDRESS1().ToProper());
			this.tblPROV_ADDINFO.colSTREET2.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVADDRESS2().ToProper());
			this.tblPROV_ADDINFO.colSTREET3.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVADDRESS3().ToProper());
			this.tblPROV_ADDINFO.colCITY.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVADDRESS4().ToProper());
			this.tblPROV_ADDINFO.colSTREET4.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVADDRESS5().ToProper());
			this.tblPROV_ADDINFO.colZIP.SetColumnTitle(pamc.price.Var.ClientConfig.GetPROVPOSTAL().ToProper());
			#endregion
		}
		#endregion
		
		#region tblPROV_ADDINFO
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public new partial class tbl1TableWindow : clsDlgInfoTableProviders.tbl1TableWindow
		{
			// reference to the container form.
			private tdlgPROV_ADDINFO _tdlgPROV_ADDINFO = null;
			
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tbl1TableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private tdlgPROV_ADDINFO tdlgPROV_ADDINFO
			{
				[DebuggerStepThrough]
				get
				{
					if (_tdlgPROV_ADDINFO == null) 
					{
						_tdlgPROV_ADDINFO = (tdlgPROV_ADDINFO)this.FindForm();
					}
					return _tdlgPROV_ADDINFO;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public new static tbl1TableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tbl1TableWindow)SalWindow.FromHandle(handle, typeof(tbl1TableWindow)));
			}
			#endregion
		}
		#endregion
	}
}
