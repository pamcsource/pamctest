// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	/// <param name="hWndTbl"></param>
	/// <param name="nMode"></param>
	public partial class fdlgPROV_SPECINFO : clsDlgInfoFormProviders
	{
		#region Window Parameters
		public SalWindowHandle hWndTbl;
		public SalNumber nMode;
		#endregion
		
		#region Window Variables
		public SalString strWhere = "";
		public SalString strInsertValues = "";
		public SalString strPic = "";
		public SalString strText = "";
		public SalBoolean bNewSet = false;
		public SalBoolean bClearNotes = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public fdlgPROV_SPECINFO(SalWindowHandle hWndTbl, SalNumber nMode)
		{
			// Assign global reference.
			App.fdlgPROV_SPECINFO = this;
			// Window Parameters initialization.
			this.hWndTbl = hWndTbl;
			this.nMode = nMode;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalWindowHandle hWndTbl, SalNumber nMode)
		{
			fdlgPROV_SPECINFO dlg = new fdlgPROV_SPECINFO(hWndTbl, nMode);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static fdlgPROV_SPECINFO FromHandle(SalWindowHandle handle)
		{
			return ((fdlgPROV_SPECINFO)SalWindow.FromHandle(handle, typeof(fdlgPROV_SPECINFO)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean SpecialtyValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strFieldName = "";
			SalNumber nInd = 0;
			SalNumber nItem = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				strItem = Vis.StrSubstitute(strItem, "\'", "\'\'");
				if (strFieldName == "dfSPECCODE") 
				{
					if (strItem == pamc.sqllib.Const.strNULL) 
					{
						dfSPECDESCR.Text = pamc.sqllib.Const.strNULL;
						return true;
					}
					return Int.SqlLookup("Select  CODE,DESCR from PROV_SPECCODES\r\n" +
						"	Where CODE = " + "\'" + strItem + "\'", ":hWndForm.dfSPECCODE,:hWndForm.dfSPECDESCR", ref nInd);
				}
				if (strFieldName == "dfCERTIFIED") 
				{
					if (strItem == pamc.sqllib.Const.strNULL) 
					{
						return true;
					}
					if (!(strItem == "N" || strItem == "Y" || strItem == "E")) 
					{
						pamc.ez32bit.Int.ValidateErrorMsgBox("Value must be either  Y = Yes, N = No or E = Eligible.");
						return false;
					}
					return true;
				}
			}

			return false;
			#endregion
		}
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean SpecialtyProcessLookup(SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strFieldName = "";
			SalArray<SalNumber> nDisableItems = new SalArray<SalNumber>();
			SalString str1 = "";
			SalString str2 = "";
			SalString str3 = "";
			SalBoolean bLookup = false;
			SalNumber nType = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfSPECCODE") 
				{
					strQuery = "Select Code = CODE, Description = DESCR from PROV_SPECCODES Order by 1 ";
					if (dlgLookupList.ModalDialog(this, strQuery, ref strResult, "Specialties", dfSPECCODE.Text, 0)) 
					{
						strResult.Tokenize("", Const.strTAB, strCols);
						dfSPECCODE.Text = strCols[0];
						dfSPECDESCR.Text = strCols[1];
						hWnd.SetModified(true);
						return true;
					}
				}
				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// fdlgPROV_SPECINFO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_SPECINFO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.fdlgPROV_SPECINFO_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Const.CM_DoLookup:
					this.fdlgPROV_SPECINFO_OnCM_DoLookup(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.fdlgPROV_SPECINFO_OnAM_Initialize(sender, e);
					break;
				
				case Const.CM_Populate:
					this.fdlgPROV_SPECINFO_OnCM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_SPECINFO_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.hWndTable = this.hWndTbl;
			this.nFormMode = this.nMode;
			this.strTypeTable = "PROV_SPECTYPES";
			this.strTypeTableName = "Specialty Types";
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_SPECINFO_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SpecialtyProcessLookup(Sal.GetFocus());
			this.ProcessLookup(Sal.GetFocus());
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_SPECINFO_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(Const.AM_Initialize, 0, 0);
			this.SendMessageToChildren(Const.CM_DoLookup, 0, 0);
			// Call SalSendMsgToChildren( hWndForm, CM_DoLookup, 0, 0 )
			// Set dfSEQUENCE = 0
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_SPECINFO_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Const.CM_Populate, 0, 0);
			this.SendMessageToChildren(Const.AM_GetValue, 0, 0);
			this.SendMessageToChildren(Const.CM_DoLookup, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// dfSPECCODE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECCODE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfSPECCODE_OnSAM_Validate(sender, e);
					break;
				
				case Const.AM_Lookup:
					this.dfSPECCODE_OnAM_Lookup(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_RBUTTONDOWN:
					this.dfSPECCODE_OnWM_RBUTTONDOWN(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.dfSPECCODE_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECCODE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			if (!(this.SpecialtyValidateEntry(this.dfSPECCODE.Text, this.dfSPECCODE))) 
			{
				if (!(this.SpecialtyProcessLookup(this.dfSPECCODE))) 
				{
					e.Return = Sys.VALIDATE_Cancel;
					return;
				}
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_Lookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECCODE_OnAM_Lookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// WM_RBUTTONDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECCODE_OnWM_RBUTTONDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfSPECCODE == Sal.GetFocus()) 
			{
				this.SpecialtyProcessLookup(this.dfSPECCODE);
			}
            //PPJ:FINAL:MHO - CA:0039 When WM_RBUTTONDOWN/WM_LBUTTONDBLCLK are handled, 0 has to be returned so that application captures mouse.
            e.Return = 0;
            #endregion
        }
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECCODE_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfSPECCODE.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// dfSPECDESCR WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECDESCR_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfSPECDESCR_OnCM_DoLookup(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSPECDESCR_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select DESCR From PROV_SPECCODES Where CODE = :dfSPECCODE", ":dfSPECDESCR ", ref Var.nGInd))) 
			{
				this.dfSPECDESCR.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		
		/// <summary>
		/// dfCERTIFIED WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTIFIED_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfCERTIFIED_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTIFIED_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			if (!(this.SpecialtyValidateEntry(this.dfCERTIFIED.Text, this.dfCERTIFIED))) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfCERTDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfCERTDATE_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTDATE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (!(this.dfCERTDATE.IsValidDateTime()) && !(this.dfCERTDATE.IsEmpty())) 
			{
				Sal.MessageBox("Invalid date !", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (this.dfCERTDATE.DateTime > this.dfCERTEXP.DateTime && !(this.dfCERTEXP.IsEmpty())) 
			{
				Sal.MessageBox("Certification Date must be before Expiration Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfCERTEXP WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTEXP_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfCERTEXP_OnSAM_Validate(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.dfCERTEXP_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTEXP_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (!(this.dfCERTEXP.IsValidDateTime()) && !(this.dfCERTEXP.IsEmpty())) 
			{
				Sal.MessageBox("Invalid date !", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (this.dfCERTEXP.DateTime < this.dfCERTDATE.DateTime && !(this.dfCERTEXP.IsEmpty())) 
			{
				Sal.MessageBox("Expiration Date must be greater than Certification Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfCERTEXP_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		#endregion
	}
}
