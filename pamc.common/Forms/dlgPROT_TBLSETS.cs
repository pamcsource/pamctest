// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	public partial class dlgPROT_TBLSETS : clsWindowToDialog
	{
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgPROT_TBLSETS()
		{
			// Assign global reference.
			App.dlgPROT_TBLSETS = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner)
		{
			dlgPROT_TBLSETS dlg = new dlgPROT_TBLSETS();
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static dlgPROT_TBLSETS FromHandle(SalWindowHandle handle)
		{
			return ((dlgPROT_TBLSETS)SalWindow.FromHandle(handle, typeof(dlgPROT_TBLSETS)));
		}
		#endregion
		
		#region Methods
		// Function: UpdateDescription
		// Description:
		// Returns
		// Parameters
		// Static Variables
		// Local variables
		// Sql Handle: hSqlDescr
		// String: strSql
		// String: strTblSet
		// String: strDescr
		// Number: nRow
		// Actions
		// Set strTblSet = dfPROT_TBLSET
		// Set strDescr = dfDESCR
		// Set strSql = 'Update PROT_TBLSETS Set DESCR = :strDescr Where PROT_TBLSET = :strTblSet'
		// If Connect(hSqlDescr)
		// If NOT SqlInst.SqlPrep( hSqlDescr, strSql ,'' )
		// Return FALSE
		// If NOT SqlExecute( hSqlDescr )
		// Return FALSE
		// Call SqlDisconnect(hSqlDescr)
		// Set nRow = TBL_MinRow
		// While SalTblFindNextRow( tblPROT_TBLSETS, nRow, 0, 0 )
		// If nRow >= 0
		// Call SalTblSetContext( tblPROT_TBLSETS, nRow )
		// If tblPROT_TBLSETS.colPROT_TBLSET = dfPROT_TBLSET
		// If tblPROT_TBLSETS.colDESCR != dfDESCR
		// Set tblPROT_TBLSETS.colDESCR = dfDESCR
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber UpdateCodeList()
		{
			#region Local Variables
			SqlLocals.UpdateCodeListLocals locals = new SqlLocals.UpdateCodeListLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{
				ml1.Text = pamc.sqllib.Const.strNULL;
				if (dfPROT_TBLSET.Text != pamc.sqllib.Const.strNULL) 
				{
					locals.strTblSet = dfPROT_TBLSET.Text;
					
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler707 = delegate(SalSqlHandle hSql)
					{
						dlgError.ModalDialog(this, pamc.price.Var.hSql, "Error reading Protocol Table Set Codes records from the database", "A SQL Database error occurred while reading records from the PROT_TBLSETS table.");
						return false;
					};
					#endregion

					if (!(pamc.price.Var.SqlInst.SqlRetr(pamc.price.Var.hSql, "EZCAP.Q515", ":strTblSet", ":ml1"))) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler707))) 
					{
						return false;
					}
					locals.nInd = pamc.price.Var.hSql.FetchNext(sqlErrorHandler707);
				}

				return 0;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgPROT_TBLSETS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgPROT_TBLSETS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgPROT_TBLSETS_OnSAM_Create(sender, e);
					break;
				
				case Const.CM_Populate:
					this.dlgPROT_TBLSETS_OnCM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgPROT_TBLSETS_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Create, Sys.wParam, Sys.lParam);
			this.pbAdd.DisableWindow();
			this.UpdateCodeList();
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgPROT_TBLSETS_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.UpdateCodeList();
			this.SendMessageToChildren(Const.CM_DoLookup, 0, 0);
			this.SendMessageToChildren(Const.AM_GetValue, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// dfDESCR WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDESCR_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfDESCR_OnCM_DoLookup(sender, e);
					break;
				
				// On SAM_Create
				
				// Call SetRequired( TRUE )
				
				case Const.CM_NotSelect:
					this.dfDESCR_OnCM_NotSelect(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDESCR_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select DESCR From  PROT_TBLSETS Where PROT_TBLSET = :dfPROT_TBLSET", ":dfDESCR ", ref Var.nGInd))) 
			{
				this.dfDESCR.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_NotSelect event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDESCR_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfSVCCODEDescr WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCODEDescr_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfSVCCODEDescr_OnCM_DoLookup(sender, e);
					break;
				
				case Const.CM_NotSelect:
					this.dfSVCCODEDescr_OnCM_NotSelect(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCODEDescr_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfPHCODE.Text == "M") 
			{
				if (this.dfSVCCODE.IsEmpty() || !(Int.SqlLookup("Select SVCDESC from MEDICINE_CODES Where SVCCODE = :dfSVCCODE AND PHCODE = :dfPHCODE", ":dfSVCCODEDescr", ref Var.nGInd))) 
				{
					this.dfSVCCODEDescr.Text = pamc.sqllib.Const.strNULL;
				}
			}
			else
			{
				if (this.dfSVCCODE.IsEmpty() || !(Int.SqlLookup("Select SVCDESC from SERVICE_CODES Where SVCCODE = :dfSVCCODE AND PHCODE = :dfPHCODE", ":dfSVCCODEDescr", ref Var.nGInd))) 
				{
					this.dfSVCCODEDescr.Text = pamc.sqllib.Const.strNULL;
				}
			}
			#endregion
		}
		
		/// <summary>
		/// CM_NotSelect event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSVCCODEDescr_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfSetDescr WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSetDescr_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfSetDescr_OnCM_DoLookup(sender, e);
					break;
				
				case Const.CM_NotSelect:
					this.dfSetDescr_OnCM_NotSelect(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSetDescr_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfPROT_SET.IsEmpty() || !(Int.SqlLookup("Select DESCR from PROT_SETS Where PROT_SET = :dfPROT_SET", ":dfSetDescr", ref Var.nGInd))) 
			{
				this.dfSetDescr.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_NotSelect event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfSetDescr_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class UpdateCodeListLocals
			{
				public SalString strDescr = "";
				public SalNumber nSet = 0;
				public SalString strTblSet = "";
				public SalNumber nInd = 0;
			}
		}
		#endregion
	}
}
