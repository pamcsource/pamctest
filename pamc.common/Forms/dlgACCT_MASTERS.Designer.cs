// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgACCT_MASTERS
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldEditKey dfACCOUNT;
		public clsDataFieldEditKey dfSUBACCOUNT;
		public clsDataFieldEdit dfDESCR;
		public clsDataFieldEdit dfTYPE;
		public clsDataFieldDisabled dfTYPEDESC;
		public clsDataFieldEditN dfACCTGROUP;
		protected clsTextWindowTo label1;
		protected clsTextWindowTo label2;
		protected clsTextWindowTo label3;
		protected clsTextWindowTo label4;
		protected clsTextWindowTo label5;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfACCOUNT = new pamc.common.clsDataFieldEditKey();
			this.dfSUBACCOUNT = new pamc.common.clsDataFieldEditKey();
			this.dfDESCR = new pamc.common.clsDataFieldEdit();
			this.dfTYPE = new pamc.common.clsDataFieldEdit();
			this.dfTYPEDESC = new pamc.common.clsDataFieldDisabled();
			this.dfACCTGROUP = new pamc.common.clsDataFieldEditN();
			this.label1 = new pamc.common.clsTextWindowTo();
			this.label2 = new pamc.common.clsTextWindowTo();
			this.label3 = new pamc.common.clsTextWindowTo();
			this.label4 = new pamc.common.clsTextWindowTo();
			this.label5 = new pamc.common.clsTextWindowTo();
			this.SuspendLayout();
			// 
			// dfACCOUNT
			// 
			this.dfACCOUNT.Name = "dfACCOUNT";
			this.dfACCOUNT.MaxLength = 4;
			this.dfACCOUNT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfACCOUNT.Location = new System.Drawing.Point(162, 7);
			this.dfACCOUNT.Size = new System.Drawing.Size(37, 20);
			this.dfACCOUNT.TabIndex = 0;
			// 
			// dfSUBACCOUNT
			// 
			this.dfSUBACCOUNT.Name = "dfSUBACCOUNT";
			this.dfSUBACCOUNT.MaxLength = 3;
			this.dfSUBACCOUNT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSUBACCOUNT.Location = new System.Drawing.Point(207, 7);
			this.dfSUBACCOUNT.Size = new System.Drawing.Size(30, 20);
			this.dfSUBACCOUNT.TabIndex = 1;
			// 
			// dfDESCR
			// 
			this.dfDESCR.Name = "dfDESCR";
			this.dfDESCR.MaxLength = 40;
			this.dfDESCR.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDESCR.Location = new System.Drawing.Point(162, 37);
			this.dfDESCR.Size = new System.Drawing.Size(291, 20);
			this.dfDESCR.TabIndex = 2;
			// 
			// dfTYPE
			// 
			this.dfTYPE.Name = "dfTYPE";
			this.dfTYPE.MaxLength = 2;
			this.dfTYPE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTYPE.Location = new System.Drawing.Point(162, 67);
			this.dfTYPE.Size = new System.Drawing.Size(31, 20);
			this.dfTYPE.TabIndex = 3;
			// 
			// dfTYPEDESC
			// 
			this.dfTYPEDESC.Name = "dfTYPEDESC";
			this.dfTYPEDESC.BackColor = System.Drawing.Color.Teal;
			this.dfTYPEDESC.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTYPEDESC.Location = new System.Drawing.Point(199, 70);
			this.dfTYPEDESC.Size = new System.Drawing.Size(254, 16);
			this.dfTYPEDESC.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfTYPEDESC_WindowActions);
			this.dfTYPEDESC.TabIndex = 4;
			// 
			// dfACCTGROUP
			// 
			this.dfACCTGROUP.Name = "dfACCTGROUP";
			this.dfACCTGROUP.MaxLength = 6;
			this.dfACCTGROUP.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfACCTGROUP.Format = "#0";
			this.dfACCTGROUP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
			this.dfACCTGROUP.Location = new System.Drawing.Point(162, 97);
			this.dfACCTGROUP.Size = new System.Drawing.Size(46, 20);
			this.dfACCTGROUP.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Account/Sub Account:";
			this.label1.Location = new System.Drawing.Point(26, 9);
			this.label1.Size = new System.Drawing.Size(133, 16);
			this.label1.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Description:";
			this.label2.Location = new System.Drawing.Point(26, 39);
			this.label2.Size = new System.Drawing.Size(89, 16);
			this.label2.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Type:";
			this.label3.Location = new System.Drawing.Point(26, 69);
			this.label3.Size = new System.Drawing.Size(89, 16);
			this.label3.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Group:";
			this.label4.Location = new System.Drawing.Point(26, 99);
			this.label4.Size = new System.Drawing.Size(63, 16);
			this.label4.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "-";
			this.label5.Location = new System.Drawing.Point(199, 9);
			this.label5.Size = new System.Drawing.Size(8, 16);
			this.label5.TabIndex = 10;
			// 
			// dlgACCT_MASTERS
			// 
			this.Controls.Add(this.dfACCTGROUP);
			this.Controls.Add(this.dfTYPEDESC);
			this.Controls.Add(this.dfTYPE);
			this.Controls.Add(this.dfDESCR);
			this.Controls.Add(this.dfSUBACCOUNT);
			this.Controls.Add(this.dfACCOUNT);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "dlgACCT_MASTERS";
			this.ClientSize = new System.Drawing.Size(474, 133);
			this.Text = "Account Number";
			this.Location = new System.Drawing.Point(45, 65);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgACCT_MASTERS == this) 
			{
				App.dlgACCT_MASTERS = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
