// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	/// <param name="VendorTable"></param>
	/// <param name="bUseTableData"></param>
	/// <param name="bUpdateBureau"></param>
	public partial class tdlgPROV_VENDINFO : clsDlgInfoTableProviders
	{
		#region Window Parameters
		public clsTableData VendorTable;
		public SalBoolean bUseTableData;
		public SalBoolean bUpdateBureau;
		#endregion
		
		#region Window Variables
		public SalNumber nCurrentRow = 0;
		public SalBoolean bPrimaryFound = false;
		public SalString strPrimaryVENDORID = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public tdlgPROV_VENDINFO(clsTableData VendorTable, SalBoolean bUseTableData, SalBoolean bUpdateBureau)
		{
			// Assign global reference.
			App.tdlgPROV_VENDINFO = this;
			// Window Parameters initialization.
			this.VendorTable = VendorTable;
			this.bUseTableData = bUseTableData;
			this.bUpdateBureau = bUpdateBureau;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, clsTableData VendorTable, SalBoolean bUseTableData, ref SalBoolean bUpdateBureau)
		{
			tdlgPROV_VENDINFO dlg = new tdlgPROV_VENDINFO(VendorTable, bUseTableData, bUpdateBureau);
			SalNumber ret = dlg.ShowDialog(owner);
			bUpdateBureau = dlg.bUpdateBureau;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static tdlgPROV_VENDINFO FromHandle(SalWindowHandle handle)
		{
			return ((tdlgPROV_VENDINFO)SalWindow.FromHandle(handle, typeof(tdlgPROV_VENDINFO)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// tdlgPROV_VENDINFO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_Edit:
					this.tdlgPROV_VENDINFO_OnCM_Edit(sender, e);
					break;
				
				case Const.CM_New:
					this.tdlgPROV_VENDINFO_OnCM_New(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.tdlgPROV_VENDINFO_OnAM_Initialize(sender, e);
					break;
				
				case Const.CM_CopyTableData:
					this.tdlgPROV_VENDINFO_OnCM_CopyTableData(sender, e);
					break;
				
				case Const.CM_SetInstanceVars:
					this.tdlgPROV_VENDINFO_OnCM_SetInstanceVars(sender, e);
					break;
				
				// On SAM_Destroy
				
				// If SalTblAnyRows( tblPROV_VENDINFO, 0, 0 )
				
				// If SalTblFetchRow( tblPROV_VENDINFO, 0 ) = 1
				
				// Set frmPROV_MASTERS.dfVENDOR = tblPROV_VENDINFO.colVENDOR
				
				// Else
				
				// Set frmPROV_MASTERS.dfVENDOR = ''
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Edit event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_OnCM_Edit(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgPROV_VENDINFO.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_Edit);
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_New event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_OnCM_New(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = fdlgPROV_VENDINFO.ModalDialog(this, this.hWndChildTbl, pamc.price.Const.MODE_New);
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bUseTableData) 
			{
				this.VendorTable.Initialize(this.hWndChildTbl);
			}
			#endregion
		}
		
		/// <summary>
		/// CM_CopyTableData event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_OnCM_CopyTableData(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.VendorTable.FillTable(this.hWndChildTbl);
			if (this.tblPROV_VENDINFO.FetchRow(0) == 1) 
			{
				App.frmPROV_MASTERS.dfVENDOR.Text = this.tblPROV_VENDINFO.colVENDOR.Text;
			}
			else
			{
				App.frmPROV_MASTERS.dfVENDOR.Text = " ";
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tdlgPROV_VENDINFO_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SetUseTableData(this.bUseTableData);
			this.SetMinRows(1);
			#endregion
		}
		
		/// <summary>
		/// tblPROV_VENDINFO WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_VENDINFO_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.tblPROV_VENDINFO_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Const.CM_Populate:
					this.tblPROV_VENDINFO_OnCM_Populate(sender, e);
					break;
				
				case Sys.SAM_FetchRow:
					this.tblPROV_VENDINFO_OnSAM_FetchRow(sender, e);
					break;
				
				case Const.CM_Save:
					this.tblPROV_VENDINFO_OnCM_Save(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_VENDINFO_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblPROV_VENDINFO.strWhere = "PROVID = :hWndOwner.dfPROVID";
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_VENDINFO_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.bUseTableData)) 
			{
				e.Return = Sal.SendClassMessage(Const.CM_Populate, Sys.wParam, Sys.lParam);
				return;
			}
			this.tblPROV_VENDINFO.SetRowCount(this.VendorTable.GetRows());
			e.Return = this.VendorTable.PopulateAllRows(this.tblPROV_VENDINFO);
			return;
			#endregion
		}
		
		/// <summary>
		/// SAM_FetchRow event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_VENDINFO_OnSAM_FetchRow(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.bUseTableData) 
			{
				e.Return = this.VendorTable.PopulateRow(Sys.lParam);
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Save event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblPROV_VENDINFO_OnCM_Save(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblPROV_VENDINFO.bTmp = Sal.SendClassMessage(Const.CM_Save, 0, 0);
			this.bPrimaryFound = 0;
			this.nCurrentRow = Sys.TBL_MinRow;
			while (true)
			{
				if (this.tblPROV_VENDINFO.FindNextRow(ref this.nCurrentRow, 0, 0)) 
				{
					this.tblPROV_VENDINFO.SetContextRow(this.nCurrentRow);
					if (this.tblPROV_VENDINFO.colTYPE.Text == "PRIMARY") 
					{
						App.frmPROV_MASTERS.dfVENDOR.Text = this.tblPROV_VENDINFO.colVENDOR.Text;
						this.bPrimaryFound = 1;
						// Update Bureau
						break;
					}
					// if no primary found still update vendorid
					App.frmPROV_MASTERS.dfVENDOR.Text = this.tblPROV_VENDINFO.colVENDOR.Text;
				}
				else
				{
					break;
				}
			}
			// Update Bureau even  if there is no Primary Vendor
			this.bUpdateBureau = 1;
			// If SalTblFetchRow( tblPROV_VENDINFO, 0 ) = 1
			// Set frmPROV_MASTERS.dfVENDOR = tblPROV_VENDINFO.colVENDOR
			e.Return = this.tblPROV_VENDINFO.bTmp;
			return;
			#endregion
		}
		#endregion
		
		#region tblPROV_VENDINFO
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public new partial class tbl1TableWindow : clsDlgInfoTableProviders.tbl1TableWindow
		{
			// reference to the container form.
			private tdlgPROV_VENDINFO _tdlgPROV_VENDINFO = null;
			
			
			#region Window Variables
			public SalBoolean bTmp = false;
			#endregion
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tbl1TableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private tdlgPROV_VENDINFO tdlgPROV_VENDINFO
			{
				[DebuggerStepThrough]
				get
				{
					if (_tdlgPROV_VENDINFO == null) 
					{
						_tdlgPROV_VENDINFO = (tdlgPROV_VENDINFO)this.FindForm();
					}
					return _tdlgPROV_VENDINFO;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public new static tbl1TableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tbl1TableWindow)SalWindow.FromHandle(handle, typeof(tbl1TableWindow)));
			}
			#endregion
		}
		#endregion
	}
}
