// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class tdlgPROV_IDINFO
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public tdlgPROV_IDINFO.tbl1TableWindow tblPROV_IDINFO;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tblPROV_IDINFO = (tdlgPROV_IDINFO.tbl1TableWindow)CreateTableWindow(typeof(tdlgPROV_IDINFO.tbl1TableWindow));
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.tblPROV_IDINFO);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbSave);
			this.ClientArea.Controls.Add(this.pbDelete);
			this.ClientArea.Controls.Add(this.pbEdit);
			this.ClientArea.Controls.Add(this.pbNew);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.frame2);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&A";
			this.label1.TabIndex = 1;
			// 
			// pbNew
			// 
			this.pbNew.Name = "pbNew";
			this.pbNew.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbNew.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Text = "&E";
			this.label2.TabIndex = 3;
			// 
			// pbEdit
			// 
			this.pbEdit.Name = "pbEdit";
			this.pbEdit.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbEdit.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Text = "&D";
			this.label3.TabIndex = 5;
			// 
			// pbDelete
			// 
			this.pbDelete.Name = "pbDelete";
			this.pbDelete.TabIndex = 6;
			// 
			// pbSave
			// 
			this.pbSave.Name = "pbSave";
			this.pbSave.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbSave.TabIndex = 7;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Text = "&T";
			this.label4.TabIndex = 9;
			// 
			// tblPROV_IDINFO
			// 
			this.tblPROV_IDINFO.Name = "tblPROV_IDINFO";
			this.tblPROV_IDINFO.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			// 
			// tblPROV_IDINFO.colSEQUENCE
			// 
			this.tblPROV_IDINFO.colSEQUENCE.Name = "colSEQUENCE";
			this.tblPROV_IDINFO.colSEQUENCE.Position = 1;
			// 
			// tblPROV_IDINFO.colROWID
			// 
			this.tblPROV_IDINFO.colROWID.Name = "colROWID";
			this.tblPROV_IDINFO.colROWID.Position = 2;
			// 
			// tblPROV_IDINFO.colPROVID
			// 
			this.tblPROV_IDINFO.colPROVID.Name = "colPROVID";
			this.tblPROV_IDINFO.colPROVID.Position = 3;
			// 
			// tblPROV_IDINFO.colTYPE
			// 
			this.tblPROV_IDINFO.colTYPE.Name = "colTYPE";
			this.tblPROV_IDINFO.colTYPE.Position = 4;
			// 
			// tblPROV_IDINFO.colVERIFY
			// 
			this.tblPROV_IDINFO.colVERIFY.Name = "colVERIFY";
			this.tblPROV_IDINFO.colVERIFY.Position = 5;
			// 
			// tblPROV_IDINFO.colVERIFYDATE
			// 
			this.tblPROV_IDINFO.colVERIFYDATE.Name = "colVERIFYDATE";
			this.tblPROV_IDINFO.colVERIFYDATE.Position = 6;
			// 
			// tblPROV_IDINFO.colVERIFYRESULT
			// 
			this.tblPROV_IDINFO.colVERIFYRESULT.Name = "colVERIFYRESULT";
			this.tblPROV_IDINFO.colVERIFYRESULT.Position = 7;
			// 
			// tblPROV_IDINFO.colVERIFYNEXT
			// 
			this.tblPROV_IDINFO.colVERIFYNEXT.Name = "colVERIFYNEXT";
			this.tblPROV_IDINFO.colVERIFYNEXT.Position = 8;
			// 
			// tblPROV_IDINFO.colNOTES
			// 
			this.tblPROV_IDINFO.colNOTES.Name = "colNOTES";
			this.tblPROV_IDINFO.colNOTES.Width = 116;
			this.tblPROV_IDINFO.colNOTES.Position = 9;
			// 
			// tblPROV_IDINFO.colCreateBy
			// 
			this.tblPROV_IDINFO.colCreateBy.Name = "colCreateBy";
			this.tblPROV_IDINFO.colCreateBy.Position = 10;
			// 
			// tblPROV_IDINFO.colCreateDate
			// 
			this.tblPROV_IDINFO.colCreateDate.Name = "colCreateDate";
			this.tblPROV_IDINFO.colCreateDate.Position = 11;
			// 
			// tblPROV_IDINFO.colLastChangeBy
			// 
			this.tblPROV_IDINFO.colLastChangeBy.Name = "colLastChangeBy";
			this.tblPROV_IDINFO.colLastChangeBy.Width = 94;
			this.tblPROV_IDINFO.colLastChangeBy.Position = 12;
			// 
			// tblPROV_IDINFO.colLastChangeDate
			// 
			this.tblPROV_IDINFO.colLastChangeDate.Name = "colLastChangeDate";
			this.tblPROV_IDINFO.colLastChangeDate.Position = 13;
			// 
			// tblPROV_IDINFO.colOUTSIDEID
			// 
			this.tblPROV_IDINFO.colOUTSIDEID.Name = "colOUTSIDEID";
			this.tblPROV_IDINFO.colOUTSIDEID.Title = "Outside\r\nID";
			this.tblPROV_IDINFO.colOUTSIDEID.Width = 86;
			this.tblPROV_IDINFO.colOUTSIDEID.MaxLength = 30;
			this.tblPROV_IDINFO.colOUTSIDEID.Position = 14;
			// 
			// tblPROV_IDINFO.colEDITABLE
			// 
			this.tblPROV_IDINFO.colEDITABLE.Name = "colEDITABLE";
			this.tblPROV_IDINFO.colEDITABLE.Visible = false;
			this.tblPROV_IDINFO.colEDITABLE.Title = "Outside\r\nID";
			this.tblPROV_IDINFO.colEDITABLE.Width = 140;
			this.tblPROV_IDINFO.colEDITABLE.MaxLength = 1;
			this.tblPROV_IDINFO.colEDITABLE.DataType = PPJ.Runtime.Windows.DataType.Number;
			this.tblPROV_IDINFO.colEDITABLE.Format = "";
			this.tblPROV_IDINFO.colEDITABLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.tblPROV_IDINFO.colEDITABLE.Position = 15;
			// 
			// tblPROV_IDINFO.colRECORDDATE
			// 
			this.tblPROV_IDINFO.colRECORDDATE.Name = "colRECORDDATE";
			this.tblPROV_IDINFO.colRECORDDATE.Title = "Date\r\nRecorded";
			this.tblPROV_IDINFO.colRECORDDATE.Width = 68;
			this.tblPROV_IDINFO.colRECORDDATE.DataType = PPJ.Runtime.Windows.DataType.DateTime;
			this.tblPROV_IDINFO.colRECORDDATE.Format = "dd/MM/yyyy";
			this.tblPROV_IDINFO.colRECORDDATE.Position = 16;
			this.tblPROV_IDINFO.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tblPROV_IDINFO_WindowActions);
			this.tblPROV_IDINFO.TabIndex = 10;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.TabIndex = 11;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 12;
			// 
			// tdlgPROV_IDINFO
			// 
			this.Name = "tdlgPROV_IDINFO";
			this.Text = "Provider Outside Identifications";
			this.Location = new System.Drawing.Point(6, 165);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.tdlgPROV_IDINFO_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.tdlgPROV_IDINFO == this) 
			{
				App.tdlgPROV_IDINFO = null;
			}
			base.Dispose(disposing);
		}
		#endregion
		
		#region tblPROV_IDINFO
		
		public partial class tbl1TableWindow
		{
			#region Window Controls
			// 01/27/98 MP - Changed data length of colOUTSIDEID field from 10 to 30.
			public clsCol colOUTSIDEID;
			public clsCol colEDITABLE;
			public clsCol colRECORDDATE;
			#endregion
			
			#region Windows Form Designer generated code
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.colOUTSIDEID = new pamc.common.clsCol();
				this.colEDITABLE = new pamc.common.clsCol();
				this.colRECORDDATE = new pamc.common.clsCol();
				this.SuspendLayout();
				// 
				// colSEQUENCE
				// 
				this.colSEQUENCE.Name = "colSEQUENCE";
				// 
				// colROWID
				// 
				this.colROWID.Name = "colROWID";
				// 
				// colPROVID
				// 
				this.colPROVID.Name = "colPROVID";
				// 
				// colTYPE
				// 
				this.colTYPE.Name = "colTYPE";
				// 
				// colVERIFY
				// 
				this.colVERIFY.Name = "colVERIFY";
				// 
				// colVERIFYDATE
				// 
				this.colVERIFYDATE.Name = "colVERIFYDATE";
				// 
				// colVERIFYRESULT
				// 
				this.colVERIFYRESULT.Name = "colVERIFYRESULT";
				// 
				// colVERIFYNEXT
				// 
				this.colVERIFYNEXT.Name = "colVERIFYNEXT";
				// 
				// colNOTES
				// 
				this.colNOTES.Name = "colNOTES";
				// 
				// colCreateBy
				// 
				this.colCreateBy.Name = "colCreateBy";
				// 
				// colCreateDate
				// 
				this.colCreateDate.Name = "colCreateDate";
				// 
				// colLastChangeBy
				// 
				this.colLastChangeBy.Name = "colLastChangeBy";
				// 
				// colLastChangeDate
				// 
				this.colLastChangeDate.Name = "colLastChangeDate";
				// 
				// colOUTSIDEID
				// 
				this.colOUTSIDEID.Name = "colOUTSIDEID";
				// 
				// colEDITABLE
				// 
				this.colEDITABLE.Name = "colEDITABLE";
				// 
				// colRECORDDATE
				// 
				this.colRECORDDATE.Name = "colRECORDDATE";
				// 
				// tblPROV_IDINFO
				// 
				this.Controls.Add(this.colSEQUENCE);
				this.Controls.Add(this.colROWID);
				this.Controls.Add(this.colPROVID);
				this.Controls.Add(this.colTYPE);
				this.Controls.Add(this.colVERIFY);
				this.Controls.Add(this.colVERIFYDATE);
				this.Controls.Add(this.colVERIFYRESULT);
				this.Controls.Add(this.colVERIFYNEXT);
				this.Controls.Add(this.colNOTES);
				this.Controls.Add(this.colCreateBy);
				this.Controls.Add(this.colCreateDate);
				this.Controls.Add(this.colLastChangeBy);
				this.Controls.Add(this.colLastChangeDate);
				this.Controls.Add(this.colOUTSIDEID);
				this.Controls.Add(this.colEDITABLE);
				this.Controls.Add(this.colRECORDDATE);
				this.Name = "tblPROV_IDINFO";
				this.ResumeLayout(false);
			}
			#endregion
		}
		#endregion
	}
}
