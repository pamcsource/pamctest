// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Dialog box for adding/editing/viewing adjustments. New guy.
	/// </summary>
	/// <param name="bEditable"></param>
	/// <param name="nTotal"></param>
	/// <param name="strCode"></param>
	/// <param name="strItemNo"></param>
	/// <param name="nTblRowID"></param>
	/// <param name="strType"></param>
	/// <param name="Adjustments"></param>
	public partial class dlgAdjust2 : SalDialogBox
	{
		#region Window Parameters
		public SalBoolean bEditable;
		public SalNumber nTotal;
		public SalString strCode;
		public SalString strItemNo;
		public SalNumber nTblRowID;
		public SalString strType;
		public clsAdjustDetails Adjustments;
		#endregion
		
		#region Window Variables
		public SalNumber nStartRow = 0;
		public SalNumber nTets = 0;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public dlgAdjust2(SalBoolean bEditable, SalNumber nTotal, SalString strCode, SalString strItemNo, SalNumber nTblRowID, SalString strType, clsAdjustDetails Adjustments)
		{
			// Assign global reference.
			App.dlgAdjust2 = this;
			// Window Parameters initialization.
			this.bEditable = bEditable;
			this.nTotal = nTotal;
			this.strCode = strCode;
			this.strItemNo = strItemNo;
			this.nTblRowID = nTblRowID;
			this.strType = strType;
			this.Adjustments = Adjustments;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalBoolean bEditable, ref SalNumber nTotal, ref SalString strCode, SalString strItemNo, SalNumber nTblRowID, SalString strType, clsAdjustDetails Adjustments)
		{
			dlgAdjust2 dlg = new dlgAdjust2(bEditable, nTotal, strCode, strItemNo, nTblRowID, strType, Adjustments);
			SalNumber ret = dlg.ShowDialog(owner);
			nTotal = dlg.nTotal;
			strCode = dlg.strCode;
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static dlgAdjust2 FromHandle(SalWindowHandle handle)
		{
			return ((dlgAdjust2)SalWindow.FromHandle(handle, typeof(dlgAdjust2)));
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// dlgAdjust2 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgAdjust2_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.dlgAdjust2_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dlgAdjust2_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Int.SetPopupLoc(this);
			if (this.bEditable) 
			{
				Int.EnableAllFields(this);
				this.tblTEMP_ADJUSTS.SendMessage(Const.AM_DisEnable, 0, 0);
			}
			else
			{
				Int.DisableAllFields(this);
				this.tblTEMP_ADJUSTS.SendMessage(Const.AM_DisEnable, 1, 0);
			}
			#endregion
		}
		
		/// <summary>
		/// tblTEMP_ADJUSTS WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.tblTEMP_ADJUSTS_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Const.CM_DoLookup:
					this.tblTEMP_ADJUSTS_OnCM_DoLookup(sender, e);
					break;
				
				case Const.AM_ValidateInput:
					this.tblTEMP_ADJUSTS_OnAM_ValidateInput(sender, e);
					break;
				
				case Const.CM_Populate:
					this.tblTEMP_ADJUSTS_OnCM_Populate(sender, e);
					break;
				
				case Const.CM_Save:
					this.tblTEMP_ADJUSTS_OnCM_Save(sender, e);
					break;
				
				case Sys.SAM_FetchRow:
					this.tblTEMP_ADJUSTS_OnSAM_FetchRow(sender, e);
					break;
				
				case Const.CM_AddRow:
					this.tblTEMP_ADJUSTS_OnCM_AddRow(sender, e);
					break;
				
				case Const.AM_DeleteRows:
					this.tblTEMP_ADJUSTS_OnAM_DeleteRows(sender, e);
					break;
				
				case Const.CM_Delete:
					this.tblTEMP_ADJUSTS_OnCM_Delete(sender, e);
					break;
				
				case Const.CM_CreateFormIsComplete:
					this.tblTEMP_ADJUSTS_OnCM_CreateFormIsComplete(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblTEMP_ADJUSTS.SetRowLimit(Const.ITEM_MaxAdjustRows);
			this.tblTEMP_ADJUSTS.DefineRowHeader("", 32, Sys.TBL_RowHdr_Visible, this.tblTEMP_ADJUSTS.colSEQUENCE);
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblTEMP_ADJUSTS.ProcessLookup(Sal.GetFocus(), Const.LOOKUP_Edit);
			#endregion
		}
		
		/// <summary>
		/// AM_ValidateInput event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnAM_ValidateInput(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblTEMP_ADJUSTS.DeleteBlankRows();
			if (!(Int.CheckRowsOnForm(this))) 
			{
				e.Return = Const.VALIDATE_NotOK;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblTEMP_ADJUSTS.SetTableRange(this.Adjustments);
			this.dfTotal.SendMessage(Const.AM_GetValue, 0, 0);
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// CM_Save event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_Save(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.tblTEMP_ADJUSTS.nRowCount == 0) 
			{
				this.Adjustments.nRows = 0;
				e.Return = true;
				return;
			}
			else
			{
				this.tblTEMP_ADJUSTS.SetAllRows(this.Adjustments);
				e.Return = true;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_FetchRow event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnSAM_FetchRow(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.Adjustments.AdjustDetails[Sys.lParam].nSequence > 0) 
			{
				this.tblTEMP_ADJUSTS.colSEQUENCE.Number = this.Adjustments.AdjustDetails[Sys.lParam].nSequence;
				this.tblTEMP_ADJUSTS.colAdjust.Number = this.Adjustments.AdjustDetails[Sys.lParam].nAdjust;
				this.tblTEMP_ADJUSTS.colAdjCode.Text = this.Adjustments.AdjustDetails[Sys.lParam].strAdjCode;
				this.tblTEMP_ADJUSTS.colComments.Text = this.Adjustments.AdjustDetails[Sys.lParam].strComments;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_AddRow event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_AddRow(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if ((this.tblTEMP_ADJUSTS.colAdjust.IsEmpty() || this.tblTEMP_ADJUSTS.colAdjCode.IsEmpty()) && this.tblTEMP_ADJUSTS.AnyRows(0, 0)) 
			{
				Sal.MessageBox("Before inserting a new row you must enter information in the first two columns", "Unable to insert new row", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = false;
				return;
			}
			e.Return = Sal.SendClassMessage(Const.CM_AddRow, Sys.wParam, Sys.lParam);
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_DeleteRows event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnAM_DeleteRows(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// If SalStrTrimX( tblTEMP_ADJUSTS.colAdjCode ) != 'GBX'
			// Call SalSendClassMessage( AM_DeleteRows, wParam, lParam )
			// Call SalSendMsg( dfTotal, AM_GetValue, 0, 0 )
			Sal.SendClassMessage(Const.AM_DeleteRows, Sys.wParam, Sys.lParam);
			this.dfTotal.SendMessage(Const.AM_GetValue, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_Delete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_Delete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Const.CM_Delete, Sys.wParam, Sys.lParam);
			this.dfTotal.SendMessage(Const.AM_GetValue, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// CM_CreateFormIsComplete event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tblTEMP_ADJUSTS_OnCM_CreateFormIsComplete(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.tblTEMP_ADJUSTS.KillFocusRow();
			this.tblTEMP_ADJUSTS.SendMessage(Const.CM_Populate, 0, 0);
			Int.WaitMsg(Sys.hWndMDI, false, pamc.sqllib.Const.strNULL);
			this.tblTEMP_ADJUSTS.colComments.DisableWindow();
			#endregion
		}
		
		/// <summary>
		/// dfTotal WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTotal_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.AM_GetValue:
					this.dfTotal_OnAM_GetValue(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// AM_GetValue event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTotal_OnAM_GetValue(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(this.tblTEMP_ADJUSTS.AnyRows(0, 0))) 
			{
				this.dfTotal.Number = 0.00m;
			}
			else
			{
				this.dfTotal.Number = this.tblTEMP_ADJUSTS.ColumnSum(this.tblTEMP_ADJUSTS.colAdjust.GetColumnID(), 0, 0);
			}
			if (this.dfTotal.Number < 0 && !(this.dfTotal.IsEmpty())) 
			{
				this.dfTotal.SetWindowColor(Sys.COLOR_IndexWindowText, Sys.COLOR_Red);
			}
			else
			{
				this.dfTotal.SetWindowColor(Sys.COLOR_IndexWindowText, Sys.COLOR_DarkBlue);
			}
			#endregion
		}
		
		/// <summary>
		/// pbOk WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbOk_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbOk_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.tblTEMP_ADJUSTS.SendMessage(Const.AM_ValidateInput, 0, 0) == Const.VALIDATE_NotOK) 
			{
				e.Return = false;
				return;
			}
			this.tblTEMP_ADJUSTS.SendMessage(Const.CM_Save, 0, 0);
			this.dfTotal.SendMessage(Const.AM_GetValue, 0, 0);
			this.nTotal = this.dfTotal.Number;
			if (this.tblTEMP_ADJUSTS.nRowCount > 1) 
			{
				this.strCode = pamc.price.Const.ADJUST_More;
			}
			else
			{
				this.nStartRow = Sys.TBL_MinRow;
				if (this.tblTEMP_ADJUSTS.FindNextRow(ref this.nStartRow, 0, 0)) 
				{
					this.tblTEMP_ADJUSTS.SetContextRow(this.nStartRow);
					this.strCode = this.tblTEMP_ADJUSTS.colAdjCode.Text;
				}
				else
				{
					this.strCode = "";
				}
			}
			this.EndDialog(1);
			#endregion
		}
		
		/// <summary>
		/// pbCancel WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pbCancel_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pbCancel_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.EndDialog(0);
			#endregion
		}
		#endregion
		
		#region tblTEMP_ADJUSTS
		
		/// <summary>
		/// Child Table Window implementation.
		/// </summary>
		public partial class tblTEMP_ADJUSTSTableWindow : clsChildTable
		{
			// reference to the container form.
			private dlgAdjust2 _dlgAdjust2 = null;
			
			
			#region Window Variables
			public SalString strWhere = "";
			#endregion
			
			#region Constructors/Destructors
			
			/// <summary>
			/// Default Constructor.
			/// </summary>
			public tblTEMP_ADJUSTSTableWindow()
			{
				// This call is required by the Windows Form Designer.
				InitializeComponent();
			}
			#endregion
			
			#region System Methods/Properties
			
			/// <summary>
			/// Parent form.
			/// </summary>
			private dlgAdjust2 dlgAdjust2
			{
				[DebuggerStepThrough]
				get
				{
					if (_dlgAdjust2 == null) 
					{
						_dlgAdjust2 = (dlgAdjust2)this.FindForm();
					}
					return _dlgAdjust2;
				}
			}
			
			/// <summary>
			/// Returns the object instance associated with the window handle.
			/// </summary>
			/// <param name="handle"></param>
			/// <returns></returns>
			[DebuggerStepThrough]
			public new static tblTEMP_ADJUSTSTableWindow FromHandle(SalWindowHandle handle)
			{
				return ((tblTEMP_ADJUSTSTableWindow)SalWindow.FromHandle(handle, typeof(tblTEMP_ADJUSTSTableWindow)));
			}
			#endregion
			
			#region Methods
			
			/// <summary>
			/// </summary>
			/// <returns></returns>
			public SalNumber DeleteBlankRows()
			{
				#region Local Variables
				SalNumber nRow = 0;
				SalNumber nSequence = 0;
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					nRow = Sys.TBL_MinRow;
					nSequence = 1;
					while (hWndTbl.FindNextRow(ref nRow, 0, 0)) 
					{
						hWndTbl.SetContextRow(nRow);
						if (this.colAdjCode.IsEmpty() && this.colAdjust.Number == 0) 
						{
							this.DeleteRow(nRow, Sys.TBL_Adjust);
							nRowCount = nRowCount - 1;
						}
						else
						{
							this.colSEQUENCE.Number = nSequence;
							nSequence = nSequence + 1;
						}
					}
				}

				return 0;
				#endregion
			}
			
			/// <summary>
			/// This function processes lookups for all data fields on this form.
			/// </summary>
			/// <param name="hWnd"></param>
			/// <param name="nType"></param>
			/// <returns></returns>
			public SalBoolean ProcessLookup(SalWindowHandle hWnd, SalNumber nType)
			{
				#region Local Variables
				SalString strResult = "";
				SalArray<SalString> strCols = new SalArray<SalString>();
				SalString strQuery = "";
				SalString strFieldName = "";
				SalString str1 = "";
				SalString str2 = "";
				SalString str3 = "";
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					strFieldName = Int.GetItemNameX(hWnd);
					if (strFieldName == "colAdjCode") 
					{
						// this part needs to be replaced to accomodate the new Lookup (enhancement arl3224)
						// Set strQuery = 'Select Code=CODE,Description=DESCR, SELECTABLE from ADJUST_CODES Order by 1'
						// If SalModalDialog( dlgLookupList, hWndForm, strQuery, strResult, 'Adjustment Reason Codes',tblTEMP_ADJUSTS.colAdjCode,3)
						// Call SalStrTokenize( strResult, '', strTAB, strCols )
						// Set tblTEMP_ADJUSTS.colAdjCode = strCols[0]
						// Set tblTEMP_ADJUSTS.colComments = strCols[1]
						// Return TRUE
						// new code for enhancement arl3224:
						str2 = this.colAdjCode.Text;
						if (Int.ProcessLookupADJ(nType, ref str2, ref str3, "User")) 
						{
							this.colAdjCode.Text = str2;
							this.colComments.Text = str3;
							hWnd.SetModified(true);
							return true;
						}
						// end of new code for enhancement arl3224
					}
					return false;
				}
				#endregion
			}
			
			/// <summary>
			/// This function is called from the SAM_Validate msg for each data field.
			/// If strItem = strNULL then it will clear all of the affected entries,
			/// else it will set them to the proper looked-up values.
			/// </summary>
			/// <param name="strItem"></param>
			/// <param name="hWnd"></param>
			/// <returns></returns>
			public SalBoolean ValidateEntry(SalString strItem, SalWindowHandle hWnd)
			{
				#region Local Variables
				SalArray<SalString> strData = new SalArray<SalString>();
				SalString strFieldName = "";
				SalNumber nInd = 0;
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					strFieldName = Int.GetItemNameX(hWnd);
					if (strFieldName == "colAdjCode") 
					{
						if (strItem == pamc.sqllib.Const.strNULL) 
						{
							this.colComments.Text = pamc.sqllib.Const.strNULL;
							return true;
						}
						return Int.SqlLookup("Select CODE,DESCR from ADJUST_CODES\r\n" +
							"	Where CODE = " + "\'" + strItem + "\'" + "  AND SELECTABLE = 1", ":hWndForm.colAdjCode, :hWndForm.colComments", ref nInd);
					}
					return true;
				}
				#endregion
			}
			
			/// <summary>
			/// </summary>
			/// <param name="_Adjusts"></param>
			/// <returns></returns>
			public SalNumber SetTableRange(clsAdjustDetails _Adjusts)
			{
				#region Local Variables
				SalNumber nMin = 0;
				SalNumber nMax = 0;
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					this.ResetTable();
					nMax = _Adjusts.GetRowNumber();
					if (nMax > 0) 
					{
						this.SetRange(nMin, nMax - 1);
						this.nRowCount = nMax;
					}
				}

				return 0;
				#endregion
			}
			
			/// <summary>
			/// </summary>
			/// <param name="lcAdjustments"></param>
			/// <returns></returns>
			public SalNumber SetAllRows(clsAdjustDetails lcAdjustments)
			{
				#region Local Variables
				SalNumber nStartRow = 0;
				SalNumber n = 0;
				#endregion
				
				#region Actions
				using (new SalContext(this))
				{
					nStartRow = Sys.TBL_MinRow;
					while (this.FindNextRow(ref nStartRow, 0, 0)) 
					{
						this.SetContextRow(nStartRow);
						lcAdjustments.AdjustDetails[n].nSequence = this.colSEQUENCE.Number;
						lcAdjustments.AdjustDetails[n].nAdjust = this.colAdjust.Number;
						lcAdjustments.AdjustDetails[n].strAdjCode = this.colAdjCode.Text;
						lcAdjustments.AdjustDetails[n].strComments = this.colComments.Text;
						n = n + 1;
					}
					lcAdjustments.nRows = n;
				}

				return 0;
				#endregion
			}
			#endregion
			
			#region Window Actions
			
			/// <summary>
			/// colAdjust WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjust_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Sys.SAM_Validate:
						this.colAdjust_OnSAM_Validate(sender, e);
						break;
					
					case Const.CM_SetInstanceVars:
						this.colAdjust_OnCM_SetInstanceVars(sender, e);
						break;
					
					case Sys.SAM_SetFocus:
						this.colAdjust_OnSAM_SetFocus(sender, e);
						break;
					
					// On WM_CHAR
					
					// Accept numeric digits, backspace, period or negative only.
					
					// If (wParam >= VK_0 AND wParam <= VK_9) OR wParam = VK_Backspace OR wParam = 46 or wParam = 45
					
					// Else
					
					// Return FALSE
					
					// On SAM_SetFocus
					
					// Call SalDisableWindow( pbLookup )
				}
				#endregion
			}
			
			/// <summary>
			/// SAM_Validate event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjust_OnSAM_Validate(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				// Set df1 = SalTblColumnSum( hWndForm, 3, 0, 0 )
				if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
				{
					e.Return = Sys.VALIDATE_Cancel;
					return;
				}
				this.dlgAdjust2.dfTotal.SendMessage(Const.AM_GetValue, 0, 0);
				if (this.colAdjust.IsEmpty() || this.colAdjust.Number == 0) 
				{
					this.colAdjCode.Text = pamc.sqllib.Const.strNULL;
					this.colComments.Text = pamc.sqllib.Const.strNULL;
				}
				#endregion
			}
			
			/// <summary>
			/// CM_SetInstanceVars event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjust_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colAdjust.SetNegative(true);
				#endregion
			}
			
			/// <summary>
			/// SAM_SetFocus event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjust_OnSAM_SetFocus(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				// If SalStrTrimX( colAdjCode ) = 'GBX'
				// Call SalDisableWindow( colAdjust )
				#endregion
			}
			
			/// <summary>
			/// colAdjCode WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjCode_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					// On SAM_Validate
					
					// Set bExists = TRUE
					
					// If NOT SalIsNull( hWndItem )
					
					// Call SqlExists('Select DESCR from ADJUST_CODES into :strDesc Where CODE = :colAdjCode ', bExists )
					
					// If NOT bExists
					
					// If NOT dlgCLAIM_DETAILS.ProcessLookup( hWndItem, LOOKUP_Edit )
					
					// Return VALIDATE_Cancel
					
					// Else
					
					// Set colComments = strDesc
					
					// Else
					
					// Set colComments = strNULL
					
					// Return VALIDATE_Ok
					
					case Sys.SAM_Validate:
						this.colAdjCode_OnSAM_Validate(sender, e);
						break;
					
					case Const.AM_Empty:
						this.colAdjCode_OnAM_Empty(sender, e);
						break;
					
					case Const.AM_Lookup:
						this.colAdjCode_OnAM_Lookup(sender, e);
						break;
					
					// On SAM_SetFocus
					
					// Call SalEnableWindow( pbLookup )
					
					// Set strFieldName = 'colAdjCode'
					
					// Set hWndField = hWndItem
					
					case Sys.SAM_SetFocus:
						this.colAdjCode_OnSAM_SetFocus(sender, e);
						break;
					
					// On WM_RBUTTONDOWN
					
					// Call dlgCLAIM_DETAILS.ProcessLookup( hWndItem, LOOKUP_Edit )
				}
				#endregion
			}
			
			/// <summary>
			/// SAM_Validate event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjCode_OnSAM_Validate(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colAdjCode.Text = Vis.StrTrim(this.colAdjCode.Text);
				if (!(this.ValidateEntry(this.colAdjCode.Text, this.colAdjCode))) 
				{
					if (!(this.ProcessLookup(this.colAdjCode, Const.LOOKUP_Edit))) 
					{
						e.Return = Sys.VALIDATE_Cancel;
						return;
					}
				}
				e.Return = Sys.VALIDATE_Ok;
				return;
				#endregion
			}
			
			/// <summary>
			/// AM_Empty event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjCode_OnAM_Empty(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				if (this.colAdjCode.IsEmpty() && this.colAdjust.Number >= 0) 
				{
					e.Return = Const.DATA_MISSING;
					return;
				}
				e.Return = 0;
				return;
				#endregion
			}
			
			/// <summary>
			/// AM_Lookup event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjCode_OnAM_Lookup(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// SAM_SetFocus event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colAdjCode_OnSAM_SetFocus(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				// If SalStrTrimX( colAdjCode ) = 'GBX'
				// Call SalDisableWindow( colAdjCode )
				#endregion
			}
			
			/// <summary>
			/// colItemNo WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colItemNo_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.AM_Initialize:
						this.colItemNo_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colItemNo_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colItemNo.Text = this.dlgAdjust2.strItemNo;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colDetailRowKey WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colDetailRowKey_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.AM_Initialize:
						this.colDetailRowKey_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colDetailRowKey_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colDetailRowKey.Number = this.dlgAdjust2.nTblRowID;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colFlag WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colFlag_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.AM_Initialize:
						this.colFlag_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colFlag_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colFlag.Number = 0;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colOpID WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colOpID_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.AM_Initialize:
						this.colOpID_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colOpID_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colOpID.Number = Var.nOpID;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colCreateBy WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateBy_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.CM_NotInsert:
						this.colCreateBy_OnCM_NotInsert(sender, e);
						break;
					
					case Const.CM_NotSelect:
						this.colCreateBy_OnCM_NotSelect(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// CM_NotInsert event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateBy_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// CM_NotSelect event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateBy_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colCreateDate WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateDate_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.CM_NotInsert:
						this.colCreateDate_OnCM_NotInsert(sender, e);
						break;
					
					case Const.CM_NotSelect:
						this.colCreateDate_OnCM_NotSelect(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// CM_NotInsert event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateDate_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// CM_NotSelect event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colCreateDate_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colLastChangeBy WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeBy_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.CM_NotInsert:
						this.colLastChangeBy_OnCM_NotInsert(sender, e);
						break;
					
					case Const.CM_NotSelect:
						this.colLastChangeBy_OnCM_NotSelect(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// CM_NotInsert event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeBy_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// CM_NotSelect event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeBy_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colLastChangeDate WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeDate_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.CM_NotInsert:
						this.colLastChangeDate_OnCM_NotInsert(sender, e);
						break;
					
					case Const.CM_NotSelect:
						this.colLastChangeDate_OnCM_NotSelect(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// CM_NotInsert event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeDate_OnCM_NotInsert(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// CM_NotSelect event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colLastChangeDate_OnCM_NotSelect(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				e.Return = true;
				return;
				#endregion
			}
			
			/// <summary>
			/// colType WindowActions Handler
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colType_WindowActions(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				switch (e.ActionType)
				{
					case Const.AM_Initialize:
						this.colType_OnAM_Initialize(sender, e);
						break;
				}
				#endregion
			}
			
			/// <summary>
			/// AM_Initialize event handler.
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			private void colType_OnAM_Initialize(object sender, WindowActionsEventArgs e)
			{
				#region Actions
				e.Handled = true;
				this.colType.Text = this.dlgAdjust2.strType;
				e.Return = true;
				return;
				#endregion
			}
			#endregion
		}
		#endregion
	}
}
