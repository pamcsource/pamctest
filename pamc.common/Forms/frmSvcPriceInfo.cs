// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	public partial class frmSvcPriceInfo : SalFormWindow
	{
		#region Window Variables
		public SalNumber nInd = 0;
		public SalNumber nBenset = 0;
		public SalNumber nTrackset = 0;
		public SalNumber nAmtPct = 0;
		public SalNumber nCopayAmt = 0;
		public SalNumber nCopayPct = 0;
		public SalBoolean bLfLimitYN = false;
		public SalBoolean bYrLimitYN = false;
		public SalNumber nIMemb = 0;
		public SalNumber nISubsc = 0;
		public SalBoolean bOOP = false;
		public SalString strModifBasis = "";
		public SalNumber nAdjust = 0;
		public SalString strBasis = "";
		public SalNumber nRate1 = 0;
		public SalNumber nRate2 = 0;
		public SalNumber nDays = 0;
		public SalNumber nPercent = 0;
		public SalNumber nProfPercent = 0;
		public SalNumber nTechPercent = 0;
		public SalBoolean bCapitated = false;
		public clsMemberUtiliz MemberUtiliz = new clsMemberUtiliz();
		public clsSubscUtiliz SubscUtiliz = new clsSubscUtiliz();
		public clsMemberQty MemberQty = new clsMemberQty();
		public clsFamilyQty FamilyQty = new clsFamilyQty();
		public clsPriceData PriceData = new clsPriceData();
		public clsGlobal GlobalQty = new clsGlobal();
		public SalString strMembname = "";
		public SalString strMembid = "";
		public SalString strHpcode = "";
		public SalString strOpt = "";
		public SalNumber nCobCB = 0;
		public SalString strPCP = "";
		public SalString strSUBSSN = "";
		public SalNumber nSavingsAcc = 0;
		public SalNumber nTrackS = 0;
		public SalDateTime dtNow = SalDateTime.Null;
		public SalNumber nLBIdx = 0;
		public SalString strRecv = "";
		public SalString strBenType = "";
		public SalNumber nGlobal = 0;
		public SalArray<SalNumber> nGlobalArray = new SalArray<SalNumber>();
		public SalNumber nProrataAmount = 0;
		public SalNumber nProrataQty = 0;
		public SalString strPRCode = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public frmSvcPriceInfo()
		{
			// Assign global reference.
			App.frmSvcPriceInfo = this;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the form window.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static frmSvcPriceInfo CreateWindow(Control owner)
		{
			frmSvcPriceInfo frm = new frmSvcPriceInfo();
			frm.Show(owner);
			return frm;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static frmSvcPriceInfo FromHandle(SalWindowHandle handle)
		{
			return ((frmSvcPriceInfo)SalWindow.FromHandle(handle, typeof(frmSvcPriceInfo)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// </summary>
		/// <param name="strItem"></param>
		/// <returns></returns>
		public SalNumber SetMData(SalString strItem)
		{
			#region Local Variables
			SqlLocals.SetMDataLocals locals = new SqlLocals.SetMDataLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.strItem = strItem;

				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler648 = delegate(SalSqlHandle hSql)
				{
					dlgError.ModalDialog(this, pamc.price.Var.hSql, "Database Access Error", "An error occurred while trying to get member information");
					return false;
				};
				#endregion

				if (!(pamc.price.Var.hSql.Retrieve("EZCAP.Q032", ":strItem", ":strMembname,:strMembid,:strHpcode,:strOpt,:nCobCB,:strPCP,:strSUBSSN,:nSavingsAcc", sqlErrorHandler648))) 
				{
					return false;
				}
				if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler648))) 
				{
					return false;
				}
				if (pamc.price.Var.hSql.FetchNext(ref locals.nInd, sqlErrorHandler648)) 
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <returns></returns>
		public SalNumber ClearAll()
		{
			#region Actions
			using (new SalContext(this))
			{
				dfYrLimit.Text = "NONE";
				dfLfLimit.Text = "NONE";
				dfYrDolLimit.Text = "NONE";
				dfLfDolLimit.Text = "NONE";
				dfFYrLimit.Text = "NONE";
				dfFLfLimit.Text = "NONE";
				dfFYrDolLimit.Text = "NONE";
				dfFLfDolLimit.Text = "NONE";
				dfGlobalDolLimit.Text = "NONE";
				dfYrBal.Text = "N/A";
				dfLfBal.Text = "N/A";
				dfYrDolBal.Text = "N/A";
				dfLfDolBal.Text = "N/A";
				dfFYrBal.Text = "N/A";
				dfFLfBal.Text = "N/A";
				dfFYrDolBal.Text = "N/A";
				dfFLfDolBal.Text = "N/A";
				dfGlobalDolBal.Text = "N/A";
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="nTrackSet"></param>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public SalNumber SetGlobals(SalNumber nTrackSet, SalDateTime dtDate)
		{
			#region Local Variables
			SqlLocals.SetGlobalsLocals locals = new SqlLocals.SetGlobalsLocals();
			#endregion
			
			#region Actions
			using (new SalContext(this, locals))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.nTrackSet = nTrackSet;
				locals.dtDate = dtDate;

				locals.nCount = 0;
				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler649 = delegate(SalSqlHandle hSql)
				{
					dlgError.ModalDialog(this, pamc.price.Var.hSql, "Database Access Error", "An error occurred while trying to get member information");
					return false;
				};
				#endregion

				if (!(pamc.price.Var.hSql.Retrieve("EZCAP.Q138", ":nTrackSet,:dtDate", ":nGlobalArray[nCount]", sqlErrorHandler649))) 
				{
					return false;
				}
				if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler649))) 
				{
					return false;
				}
				while (pamc.price.Var.hSql.FetchNext(ref nInd, sqlErrorHandler649)) 
				{
					locals.nCount = locals.nCount + 1;
				}

				return 0;
			}
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="_GlobalArray"></param>
		/// <param name="_Value"></param>
		/// <returns></returns>
		public SalBoolean SearchArray(SalArray<SalNumber> _GlobalArray, SalNumber _Value)
		{
			#region Local Variables
			SalNumber nUpper = 0;
			SalNumber nCount = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				nCount = 0;
				nUpper = _GlobalArray.GetUpperBound(1);
				while (nCount <= nUpper) 
				{
					nCount = nCount + 1;
					if (_GlobalArray[nCount] == _Value) 
					{
						return true;
					}
				}
				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// frmSvcPriceInfo WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmSvcPriceInfo_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Create:
					this.frmSvcPriceInfo_OnSAM_Create(sender, e);
					break;
				
				case Const.AM_Populate:
					this.frmSvcPriceInfo_OnAM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmSvcPriceInfo_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.CenterWindow();
			this.dfDate.DateTime = SalDateTime.Current;
			#endregion
		}
		
		/// <summary>
		/// AM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmSvcPriceInfo_OnAM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.ClearAll();
			if (this.SetMData(this.dfMembID.Text)) 
			{
				this.MemberQty.Initialize(this.dfMembID.Text, this.strHpcode);
				this.FamilyQty.Initialize(this.strSUBSSN, this.strHpcode);
				this.GlobalQty.Initialize(this.strSUBSSN, this.strHpcode);
				this.dtNow = SalDateTime.Current;
				if (pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, "Select TRACKSET \r\n" +
					"	From BEN_OPTIONS\r\n" +
					"	Where HPCODE = :strHpcode AND OPT = :strOpt AND\r\n" +
					"	:dfDate Between FROMDATE AND @nullvalue( TODATE, \'31/12/9999\' )", ":nTrackS")) 
				{
					this.nInd = pamc.price.Var.hSql.FetchNext();
					this.SetGlobals(this.nTrackS, this.dfDate.DateTime);
					if (this.nTrackS > 0) 
					{
						if (pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, "Select Global  From Bentrack_tables\r\n" +
							"	Where TRACKSET = :nTrackS AND Bentype = :strBenType AND\r\n" +
							"	:dfDate Between FROMDATE AND @nullvalue( TODATE, \'31/12/9999\' )", ":nGlobal")) 
						{
							this.nInd = pamc.price.Var.hSql.FetchNext();
							this.PriceData.bGYrDolLimit = false;
							this.PriceData.bFLfDolLimit = false;
							this.PriceData.bFYrDolLimit = false;
							this.PriceData.bFLfLimit = false;
							this.PriceData.bFYrLimit = false;
							this.PriceData.bLfDolLimit = false;
							this.PriceData.bYrDolLimit = false;
							this.PriceData.bLfLimit = false;
							this.PriceData.bYrLimit = false;
							this.PriceData.GetServiceLimits(this.nTrackS, this.strBenType.ToNumber(), this.dfDate.DateTime, this.dfMembID.Text);
							if (this.strBenType.Trim().Length == 1) 
							{
								this.strPRCode = "PR0" + this.strBenType.Trim() + "      ";
							}
							else
							{
								this.strPRCode = "PR" + this.strBenType.Trim() + "      ";
							}
							pamc.price.Var.hSql.Retrieve("EZCAP.Q164", ":dfMembID,:strPRCode,:dfDate", ":nProrataAmount,:nProrataQty");
							pamc.price.Var.hSql.Execute();
							this.nInd = pamc.price.Var.hSql.FetchNext();
							if (!(this.PriceData.bYrLimit)) 
							{
								this.dfYrLimit.Text = "NONE";
							}
							else
							{
								this.dfYrLimit.Text = this.PriceData.nYrLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataQty.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nYrLimit - this.nProrataQty).ToString(0);
							}
							if (!(this.PriceData.bLfLimit)) 
							{
								this.dfLfLimit.Text = "NONE";
							}
							else
							{
								this.dfLfLimit.Text = this.PriceData.nLfLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataQty.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nLfLimit - this.nProrataQty).ToString(0);
							}
							if (!(this.PriceData.bYrDolLimit)) 
							{
								this.dfYrDolLimit.Text = "NONE";
							}
							else
							{
								this.dfYrDolLimit.Text = this.PriceData.nYrDolLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataAmount.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nYrDolLimit - this.nProrataAmount).ToString(0);
							}
							if (!(this.PriceData.bLfDolLimit)) 
							{
								this.dfLfDolLimit.Text = "NONE";
							}
							else
							{
								this.dfLfDolLimit.Text = this.PriceData.nLfDolLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataAmount.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nLfDolLimit - this.nProrataAmount).ToString(0);
							}
							if (!(this.PriceData.bFYrLimit)) 
							{
								this.dfFYrLimit.Text = "NONE";
							}
							else
							{
								this.dfFYrLimit.Text = this.PriceData.nFYrLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataQty.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nFYrLimit - this.nProrataQty).ToString(0);
							}
							if (!(this.PriceData.bFLfLimit)) 
							{
								this.dfFLfLimit.Text = "NONE";
							}
							else
							{
								this.dfFLfLimit.Text = this.PriceData.nFLfLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataQty.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nFLfLimit - this.nProrataQty).ToString(0);
							}
							if (!(this.PriceData.bFYrDolLimit)) 
							{
								this.dfFYrDolLimit.Text = "NONE";
							}
							else
							{
								this.dfFYrDolLimit.Text = this.PriceData.nFYrDolLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataAmount.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nFYrDolLimit - this.nProrataAmount).ToString(0);
							}
							if (!(this.PriceData.bFLfDolLimit)) 
							{
								this.dfFLfDolLimit.Text = "NONE";
							}
							else
							{
								this.dfFLfDolLimit.Text = this.PriceData.nFLfDolLimit.ToString(0);
								this.dfProrataAmount.Text = this.nProrataAmount.ToString(0);
								this.dfProrataServLimit.Text = (this.PriceData.nFLfDolLimit - this.nProrataAmount).ToString(0);
							}
							if (!(this.PriceData.bGYrDolLimit)) 
							{
								this.dfGlobalDolLimit.Text = "NONE";
							}
							else
							{
								this.dfGlobalDolLimit.Text = this.PriceData.nGYrDolLimit.ToString(0);
								// Set dfProrataAmount = SalNumberToStrX( nProrataAmount, 0 )
								// Set dfProrataServLimit = SalNumberToStrX( PriceData.nGYrDolLimit - nProrataAmount, 0 )
							}
							if (((this.nProrataAmount == 0) || (this.nProrataAmount == SalNumber.Null)) && ((this.nProrataQty == 0) || (this.nProrataQty == SalNumber.Null))) 
							{
								this.dfProrataAmount.Text = "NONE";
								this.dfProrataServLimit.Text = "NONE";
							}
							this.dfYrAccum.Number = this.MemberQty.GetQtyForYr(this.dfDate.DateTime.Year(), this.strBenType.ToNumber());
							this.dfLfAccum.Number = this.MemberQty.GetQtyForType(this.strBenType.ToNumber());
							this.dfYrDolAccum.Number = this.MemberQty.GetDolForYr(this.dfDate.DateTime.Year(), this.strBenType.ToNumber());
							this.dfLfDolAccum.Number = this.MemberQty.GetDolForType(this.strBenType.ToNumber());
							this.dfFYrAccum.Number = this.FamilyQty.GetQtyForFYr(this.dfDate.DateTime.Year(), this.strBenType.ToNumber());
							this.dfFLfAccum.Number = this.FamilyQty.GetQtyForFType(this.strBenType.ToNumber());
							this.dfFYrDolAccum.Number = this.FamilyQty.GetDolForFYr(this.dfDate.DateTime.Year(), this.strBenType.ToNumber());
							this.dfFLfDolAccum.Number = this.FamilyQty.GetDolForFType(this.strBenType.ToNumber());
							if (this.SearchArray(this.nGlobalArray, this.strBenType.ToNumber())) 
							{
								this.dfYrDolAccum.Number = this.dfFYrDolAccum.Number;
								this.dfLfDolAccum.Number = this.dfFLfDolAccum.Number;
								this.dfGlobalDolLimit.Text = this.PriceData.nYrDolLimit.ToString(0);
							}
							if (this.nGlobal > 0) 
							{
								this.dfGlobalDolAccum.Number = this.GlobalQty.GetDolForGYr(this.dfDate.DateTime.Year(), this.nGlobal);
							}
							else
							{
								this.dfGlobalDolAccum.Number = this.GlobalQty.GetDolForGYr(this.dfDate.DateTime.Year(), this.strBenType.ToNumber());
							}
							if (this.dfYrLimit.Text != "NONE") 
							{
								this.dfYrBal.Text = (((SalString)this.dfYrLimit.Text).ToNumber() - this.dfYrAccum.Number).ToString(0);
							}
							else
							{
								this.dfYrBal.Text = "N/A";
							}
							if (this.dfLfLimit.Text != "NONE") 
							{
								this.dfLfBal.Text = (((SalString)this.dfLfLimit.Text).ToNumber() - this.dfLfAccum.Number).ToString(0);
							}
							else
							{
								this.dfLfBal.Text = "N/A";
							}
							if (this.dfYrDolLimit.Text != "NONE") 
							{
								this.dfYrDolBal.Text = (((SalString)this.dfYrDolLimit.Text).ToNumber() - this.dfYrDolAccum.Number).ToString(2);
							}
							else
							{
								this.dfYrDolBal.Text = "N/A";
							}
							if (this.dfLfDolLimit.Text != "NONE") 
							{
								this.dfLfDolBal.Text = (((SalString)this.dfLfDolLimit.Text).ToNumber() - this.dfLfDolAccum.Number).ToString(2);
							}
							else
							{
								this.dfLfDolBal.Text = "N/A";
							}
							if (this.dfFYrLimit.Text != "NONE") 
							{
								this.dfFYrBal.Text = (((SalString)this.dfFYrLimit.Text).ToNumber() - this.dfFYrAccum.Number).ToString(0);
							}
							else
							{
								this.dfFYrBal.Text = "N/A";
							}
							if (this.dfFLfLimit.Text != "NONE") 
							{
								this.dfFLfBal.Text = (((SalString)this.dfFLfLimit.Text).ToNumber() - this.dfFLfAccum.Number).ToString(0);
							}
							else
							{
								this.dfFLfBal.Text = "N/A";
							}
							if (this.dfFYrDolLimit.Text != "NONE") 
							{
								this.dfFYrDolBal.Text = (((SalString)this.dfFYrDolLimit.Text).ToNumber() - this.dfFYrDolAccum.Number).ToString(2);
							}
							else
							{
								this.dfFYrDolBal.Text = "N/A";
							}
							if (this.dfFLfDolLimit.Text != "NONE") 
							{
								this.dfFLfDolBal.Text = (((SalString)this.dfFLfDolLimit.Text).ToNumber() - this.dfFLfDolAccum.Number).ToString(2);
							}
							else
							{
								this.dfFLfDolBal.Text = "N/A";
							}
							if (this.dfGlobalDolLimit.Text != "NONE") 
							{
								this.dfGlobalDolBal.Text = (((SalString)this.dfGlobalDolLimit.Text).ToNumber() - this.dfGlobalDolAccum.Number).ToString(2);
							}
							else
							{
								this.dfGlobalDolBal.Text = "N/A";
							}
						}
					}
				}
			}
			#endregion
		}
		
		/// <summary>
		/// dfDate WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDate_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfDate_OnSAM_Validate(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.dfDate_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDate_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (this.dfDate.DateTime > SalDateTime.Current) 
			{
				pamc.ez32bit.Int.ValidateErrorMsgBox("Date Received cannot be later than today");
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfDate_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfDate.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// pb2 WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pb2_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Click:
					this.pb2_OnSAM_Click(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Click event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pb2_OnSAM_Click(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			// Call SalListPopulate( lbTCodes, hSql, 'Select u.Bentype,c.Descr,sum(u.Dollar),sum(u.Qty),u.benyear from memb_utils u,Bentrack_codes c Where u.Membid = \''||dfMembID||'\' AND u.BENYEAR >= 1999 AND c.Code = u.bentype group by 1,2,5' )
			this.lbTCodes.PopulateList(pamc.price.Var.hSql, "Select Code,Descr from Bentrack_codes  order by Code");
			this.strRecv = this.lbTCodes.GetListItemText(0);
			this.strBenType = this.strRecv.Left(this.strRecv.Scan("	")).Trim();
			Sal.WaitCursor(true);
			// Call SalSendMsgToChildren( hWndForm, AM_DisEnable, 1 , 0 )
			this.SendMessage(Const.AM_Populate, 0, 0);
			Sal.WaitCursor(false);
			#endregion
		}
		
		/// <summary>
		/// lbTCodes WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lbTCodes_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_DoubleClick:
					this.lbTCodes_OnSAM_DoubleClick(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_DoubleClick event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lbTCodes_OnSAM_DoubleClick(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.nLBIdx = this.lbTCodes.GetListSelectedIndex();
			this.strRecv = this.lbTCodes.GetListItemText(this.nLBIdx);
			this.strBenType = this.strRecv.Left(this.strRecv.Scan("	")).Trim();
			Sal.WaitCursor(true);
			// Call SalSendMsgToChildren( hWndForm, AM_DisEnable, 1 , 0 )
			this.SendMessage(Const.AM_Populate, 0, 0);
			Sal.WaitCursor(false);
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class SetMDataLocals
			{
				public SalNumber nInd = 0;
				public SalString strPCP = "";
				public SalNumber nTest = 0;
				public SalString strItem = "";
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class SetGlobalsLocals
			{
				public SalNumber nCount = 0;
				public SalNumber nTrackSet = 0;
				public SalDateTime dtDate = SalDateTime.Null;
			}
		}
		#endregion
	}
}
