// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgDefaultQuery
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public FCSalRadioButton rb1;
		public FCSalRadioButton rb2;
		protected FCSalGroupBox groupBox1;
		// Set strQueryID = strOpID||'CLD00'
		public SalPushbutton pbModify;
		public clsDlgOk pbOk;
		public clsDlgCancel pbCancel;
		protected SalFrame frame1;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.rb1 = new pamc.common.FCSalRadioButton();
			this.rb2 = new pamc.common.FCSalRadioButton();
			this.groupBox1 = new FCSalGroupBox();
			this.pbModify = new PPJ.Runtime.Windows.SalPushbutton();
			this.pbOk = new pamc.common.clsDlgOk();
			this.pbCancel = new pamc.common.clsDlgCancel();
			this.frame1 = new PPJ.Runtime.Windows.SalFrame();
			this.SuspendLayout();
			// 
			// rb1
			// 
			this.rb1.Name = "rb1";
			this.rb1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rb1.Text = "Stored &Query";
			this.rb1.Location = new System.Drawing.Point(19, 27);
			this.rb1.Size = new System.Drawing.Size(112, 24);
			this.rb1.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rb1_WindowActions);
			this.rb1.TabIndex = 0;
			// 
			// rb2
			// 
			this.rb2.Name = "rb2";
			this.rb2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rb2.Text = "&None";
			this.rb2.Location = new System.Drawing.Point(19, 55);
			this.rb2.Size = new System.Drawing.Size(112, 24);
			this.rb2.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.rb2_WindowActions);
			this.rb2.TabIndex = 1;
			// 
			// groupBox1
			// 
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox1.ForeColor = System.Drawing.Color.Teal;
			this.groupBox1.Text = "Intitial View";
			this.groupBox1.Location = new System.Drawing.Point(7, 11);
			this.groupBox1.Size = new System.Drawing.Size(219, 72);
			this.groupBox1.TabIndex = 2;
			// 
			// pbModify
			// 
			this.pbModify.Name = "pbModify";
			this.pbModify.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbModify.Text = "&Modify...";
			this.pbModify.Location = new System.Drawing.Point(137, 26);
			this.pbModify.Size = new System.Drawing.Size(77, 24);
			this.pbModify.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbModify_WindowActions);
			this.pbModify.TabIndex = 3;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Location = new System.Drawing.Point(257, 4);
			this.pbOk.Size = new System.Drawing.Size(78, 33);
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 4;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.Location = new System.Drawing.Point(257, 40);
			this.pbCancel.Size = new System.Drawing.Size(78, 33);
			this.pbCancel.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbCancel_WindowActions);
			this.pbCancel.TabIndex = 5;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.BackColor = System.Drawing.Color.Gray;
			this.frame1.BorderStyle = PPJ.Runtime.Windows.BorderStyle.Etched;
			this.frame1.BorderSize = 1;
			this.frame1.Location = new System.Drawing.Point(253, -8);
			this.frame1.Size = new System.Drawing.Size(92, 118);
			this.frame1.TabIndex = 6;
			// 
			// dlgDefaultQuery
			// 
			this.Controls.Add(this.pbCancel);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.pbModify);
			this.Controls.Add(this.rb2);
			this.Controls.Add(this.rb1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.frame1);
			this.Name = "dlgDefaultQuery";
			this.ClientSize = new System.Drawing.Size(338, 95);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.BackColor = System.Drawing.Color.Silver;
			this.Font = new Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
			this.Text = "Preferences";
			this.Location = new System.Drawing.Point(126, 168);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgDefaultQuery_WindowActions);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgDefaultQuery == this) 
			{
				App.dlgDefaultQuery = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
