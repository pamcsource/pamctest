// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class dlgPROV_OutStatus
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldD dfFLAGGEDDATE;
		public clsDataFieldD dfOUTLDDATE;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.dfLOADDT);
			this.ClientArea.Controls.Add(this.dfOUTLOADDT);
			this.ClientArea.Controls.Add(this.dfXPDATE);
			this.ClientArea.Controls.Add(this.rbNone);
			this.ClientArea.Controls.Add(this.rbDelete);
			this.ClientArea.Controls.Add(this.rbChange);
			this.ClientArea.Controls.Add(this.rbAdd);
			this.ClientArea.Controls.Add(this.rbNotReady);
			this.ClientArea.Controls.Add(this.rbReady);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.groupBox2);
			this.ClientArea.Controls.Add(this.groupBox1);
			this.ClientArea.Controls.Add(this.frame1);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.pbOk_WindowActions);
			this.pbOk.TabIndex = 0;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 1;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 2;
			// 
			// rbReady
			// 
			this.rbReady.Name = "rbReady";
			this.rbReady.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbReady.TabIndex = 3;
			// 
			// rbNotReady
			// 
			this.rbNotReady.Name = "rbNotReady";
			this.rbNotReady.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbNotReady.TabIndex = 4;
			// 
			// groupBox1
			// 
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox1.Text = "Outload Status";
			this.groupBox1.TabIndex = 5;
			// 
			// rbAdd
			// 
			this.rbAdd.Name = "rbAdd";
			this.rbAdd.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbAdd.TabIndex = 6;
			// 
			// rbChange
			// 
			this.rbChange.Name = "rbChange";
			this.rbChange.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbChange.TabIndex = 7;
			// 
			// rbDelete
			// 
			this.rbDelete.Name = "rbDelete";
			this.rbDelete.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbDelete.TabIndex = 8;
			// 
			// rbNone
			// 
			this.rbNone.Name = "rbNone";
			this.rbNone.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.rbNone.TabIndex = 9;
			// 
			// groupBox2
			// 
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.groupBox2.Text = "Transaction Code";
			this.groupBox2.TabIndex = 10;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Date Flagged:";
			this.label1.TabIndex = 11;
			// 
			// dfXPDATE
			// 
			this.dfXPDATE.Name = "dfXPDATE";
			this.dfXPDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfXPDATE.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Date Last Outloaded:";
			this.label2.TabIndex = 13;
			// 
			// dfOUTLOADDT
			// 
			this.dfOUTLOADDT.Name = "dfOUTLOADDT";
			this.dfOUTLOADDT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfOUTLOADDT.TabIndex = 14;
			// 
			// dfLOADDT
			// 
			this.dfLOADDT.Name = "dfLOADDT";
			this.dfLOADDT.Visible = false;
			this.dfLOADDT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfLOADDT.Location = new System.Drawing.Point(317, 212);
			this.dfLOADDT.TabIndex = 15;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Visible = false;
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Date of Last Batch Inload:";
			this.label3.TabIndex = 16;
			// 
			// dlgPROV_OutStatus
			// 
			this.Name = "dlgPROV_OutStatus";
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dlgPROV_OutStatus_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		
		/// <summary>
		/// Enhanced component initialization method - used to initialize
		/// inherited controls. This method is not supported by the designer.
		/// </summary>
		private void InitializeComponentEx()
		{
			this.dfFLAGGEDDATE = base.dfXPDATE;
			this.dfOUTLDDATE = base.dfOUTLOADDT;
			// 
			// dfFLAGGEDDATE
			// 
			this.dfFLAGGEDDATE.Name = "dfFLAGGEDDATE";
			// 
			// dfOUTLDDATE
			// 
			this.dfOUTLDDATE.Name = "dfOUTLDDATE";
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgPROV_OutStatus == this) 
			{
				App.dlgPROV_OutStatus = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
