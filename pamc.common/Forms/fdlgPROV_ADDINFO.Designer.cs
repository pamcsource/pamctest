// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	public partial class fdlgPROV_ADDINFO
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		// Frame
		protected SalFrame frame2;
		protected clsText label7;
		public clsDataFieldEdit dfSTREET;
		protected clsText label8;
		public clsDataFieldEdit dfSTREET2;
		protected clsText label9;
		public clsDataFieldEdit dfSTREET3;
		protected clsText label10;
		public clsDataFieldEdit dfCITY;
		protected clsText label11;
		public clsDataFieldEdit dfSTREET4;
		public clsDataFieldEdit dfSTATE;
		public clsDataFieldEdit df16;
		public clsDataFieldEdit df17;
		public clsDataFieldEdit df18;
		protected clsText label12;
		public clsDataFieldEdit dfZIP;
		public clsDataFieldEdit dfCOUNTY;
		public clsDataFieldEdit dfPHONECODE;
		public clsDataFieldEdit dfPHONE;
		public clsDataFieldEdit dfFAXCODE;
		public clsDataFieldEdit dfFAX;
		// clsText: Street:
		protected clsText label13;
		// clsText: Zip:
		protected clsText label14;
		protected clsText label15;
		protected clsText label16;
		protected SalFrame frame3;
		public clsDataFieldDisabled dfDescription;
		protected clsText label17;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.frame2 = new PPJ.Runtime.Windows.SalFrame();
			this.label7 = new pamc.common.clsText();
			this.dfSTREET = new pamc.common.clsDataFieldEdit();
			this.label8 = new pamc.common.clsText();
			this.dfSTREET2 = new pamc.common.clsDataFieldEdit();
			this.label9 = new pamc.common.clsText();
			this.dfSTREET3 = new pamc.common.clsDataFieldEdit();
			this.label10 = new pamc.common.clsText();
			this.dfCITY = new pamc.common.clsDataFieldEdit();
			this.label11 = new pamc.common.clsText();
			this.dfSTREET4 = new pamc.common.clsDataFieldEdit();
			this.dfSTATE = new pamc.common.clsDataFieldEdit();
			this.df16 = new pamc.common.clsDataFieldEdit();
			this.df17 = new pamc.common.clsDataFieldEdit();
			this.df18 = new pamc.common.clsDataFieldEdit();
			this.label12 = new pamc.common.clsText();
			this.dfZIP = new pamc.common.clsDataFieldEdit();
			this.dfCOUNTY = new pamc.common.clsDataFieldEdit();
			this.dfPHONECODE = new pamc.common.clsDataFieldEdit();
			this.dfPHONE = new pamc.common.clsDataFieldEdit();
			this.dfFAXCODE = new pamc.common.clsDataFieldEdit();
			this.dfFAX = new pamc.common.clsDataFieldEdit();
			this.label13 = new pamc.common.clsText();
			this.label14 = new pamc.common.clsText();
			this.label15 = new pamc.common.clsText();
			this.label16 = new pamc.common.clsText();
			this.frame3 = new PPJ.Runtime.Windows.SalFrame();
			this.dfDescription = new pamc.common.clsDataFieldDisabled();
			this.label17 = new pamc.common.clsText();
			this.ClientArea.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolBar
			// 
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.TabStop = true;
			// 
			// ClientArea
			// 
			this.ClientArea.Name = "ClientArea";
			this.ClientArea.AutoScroll = false;
			this.ClientArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.ClientArea.Controls.Add(this.dfNOTES);
			this.ClientArea.Controls.Add(this.dfVERIFYNEXT);
			this.ClientArea.Controls.Add(this.dfVERIFYRESULT);
			this.ClientArea.Controls.Add(this.dfVERIFYDATE);
			this.ClientArea.Controls.Add(this.cbVERIFY);
			this.ClientArea.Controls.Add(this.dfVERIFY);
			this.ClientArea.Controls.Add(this.dfDescription);
			this.ClientArea.Controls.Add(this.dfFAX);
			this.ClientArea.Controls.Add(this.dfFAXCODE);
			this.ClientArea.Controls.Add(this.dfPHONE);
			this.ClientArea.Controls.Add(this.dfPHONECODE);
			this.ClientArea.Controls.Add(this.dfCOUNTY);
			this.ClientArea.Controls.Add(this.dfZIP);
			this.ClientArea.Controls.Add(this.dfPROVID);
			this.ClientArea.Controls.Add(this.df18);
			this.ClientArea.Controls.Add(this.df17);
			this.ClientArea.Controls.Add(this.df16);
			this.ClientArea.Controls.Add(this.dfSTATE);
			this.ClientArea.Controls.Add(this.dfSTREET4);
			this.ClientArea.Controls.Add(this.dfCITY);
			this.ClientArea.Controls.Add(this.dfSTREET3);
			this.ClientArea.Controls.Add(this.dfSTREET2);
			this.ClientArea.Controls.Add(this.dfSTREET);
			this.ClientArea.Controls.Add(this.dfTYPE);
			this.ClientArea.Controls.Add(this.pbCancel);
			this.ClientArea.Controls.Add(this.dfSEQUENCE);
			this.ClientArea.Controls.Add(this.pbNew);
			this.ClientArea.Controls.Add(this.pbOk);
			this.ClientArea.Controls.Add(this.label17);
			this.ClientArea.Controls.Add(this.label6);
			this.ClientArea.Controls.Add(this.label5);
			this.ClientArea.Controls.Add(this.label4);
			this.ClientArea.Controls.Add(this.label3);
			this.ClientArea.Controls.Add(this.label16);
			this.ClientArea.Controls.Add(this.label15);
			this.ClientArea.Controls.Add(this.label14);
			this.ClientArea.Controls.Add(this.label13);
			this.ClientArea.Controls.Add(this.label12);
			this.ClientArea.Controls.Add(this.label11);
			this.ClientArea.Controls.Add(this.label10);
			this.ClientArea.Controls.Add(this.label9);
			this.ClientArea.Controls.Add(this.label8);
			this.ClientArea.Controls.Add(this.label7);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.label1);
			this.ClientArea.Controls.Add(this.frame3);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.frame2);
			// 
			// StatusBar
			// 
			this.StatusBar.Name = "StatusBar";
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.BackColor = System.Drawing.Color.Teal;
			this.frame2.BorderStyle = PPJ.Runtime.Windows.BorderStyle.DropShadow;
			this.frame2.BorderSize = 1;
			this.frame2.Location = new System.Drawing.Point(11, 65);
			this.frame2.Size = new System.Drawing.Size(308, 246);
			this.frame2.TabIndex = 0;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Text = "&A";
			this.label1.TabIndex = 2;
			// 
			// pbNew
			// 
			this.pbNew.Name = "pbNew";
			this.pbNew.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.pbNew.TabIndex = 3;
			// 
			// dfSEQUENCE
			// 
			this.dfSEQUENCE.Name = "dfSEQUENCE";
			this.dfSEQUENCE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfSEQUENCE.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Sequence No:";
			this.label2.TabIndex = 5;
			// 
			// pbCancel
			// 
			this.pbCancel.Name = "pbCancel";
			this.pbCancel.TabIndex = 6;
			// 
			// dfTYPE
			// 
			this.dfTYPE.Name = "dfTYPE";
			this.dfTYPE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfTYPE.Location = new System.Drawing.Point(436, 41);
			this.dfTYPE.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.Name = "label7";
			this.label7.BackColor = System.Drawing.Color.Teal;
			this.label7.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label7.ForeColor = System.Drawing.Color.White;
			this.label7.Text = "Address 1:";
			this.label7.Location = new System.Drawing.Point(21, 77);
			this.label7.Size = new System.Drawing.Size(66, 16);
			this.label7.TabIndex = 8;
			// 
			// dfSTREET
			// 
			this.dfSTREET.Name = "dfSTREET";
			this.dfSTREET.MaxLength = 30;
			this.dfSTREET.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTREET.Location = new System.Drawing.Point(87, 73);
			this.dfSTREET.Size = new System.Drawing.Size(225, 20);
			this.dfSTREET.TabIndex = 9;
			// 
			// label8
			// 
			this.label8.Name = "label8";
			this.label8.BackColor = System.Drawing.Color.Teal;
			this.label8.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label8.ForeColor = System.Drawing.Color.White;
			this.label8.Text = "Address 2:";
			this.label8.Location = new System.Drawing.Point(21, 109);
			this.label8.Size = new System.Drawing.Size(66, 16);
			this.label8.TabIndex = 10;
			// 
			// dfSTREET2
			// 
			this.dfSTREET2.Name = "dfSTREET2";
			this.dfSTREET2.MaxLength = 30;
			this.dfSTREET2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTREET2.Location = new System.Drawing.Point(87, 105);
			this.dfSTREET2.Size = new System.Drawing.Size(225, 20);
			this.dfSTREET2.TabIndex = 11;
			// 
			// label9
			// 
			this.label9.Name = "label9";
			this.label9.BackColor = System.Drawing.Color.Teal;
			this.label9.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label9.ForeColor = System.Drawing.Color.White;
			this.label9.Text = "Address 3:";
			this.label9.Location = new System.Drawing.Point(21, 141);
			this.label9.Size = new System.Drawing.Size(66, 16);
			this.label9.TabIndex = 12;
			// 
			// dfSTREET3
			// 
			this.dfSTREET3.Name = "dfSTREET3";
			this.dfSTREET3.MaxLength = 30;
			this.dfSTREET3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTREET3.Location = new System.Drawing.Point(87, 137);
			this.dfSTREET3.Size = new System.Drawing.Size(225, 20);
			this.dfSTREET3.TabIndex = 13;
			// 
			// label10
			// 
			this.label10.Name = "label10";
			this.label10.BackColor = System.Drawing.Color.Teal;
			this.label10.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label10.ForeColor = System.Drawing.Color.White;
			this.label10.Text = "Address 4:";
			this.label10.Location = new System.Drawing.Point(21, 176);
			this.label10.Size = new System.Drawing.Size(66, 16);
			this.label10.TabIndex = 14;
			// 
			// dfCITY
			// 
			this.dfCITY.Name = "dfCITY";
			this.dfCITY.MaxLength = 30;
			this.dfCITY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCITY.Location = new System.Drawing.Point(87, 170);
			this.dfCITY.Size = new System.Drawing.Size(225, 20);
			this.dfCITY.TabIndex = 15;
			// 
			// label11
			// 
			this.label11.Name = "label11";
			this.label11.BackColor = System.Drawing.Color.Teal;
			this.label11.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label11.ForeColor = System.Drawing.Color.White;
			this.label11.Text = "Address 5:";
			this.label11.Location = new System.Drawing.Point(21, 205);
			this.label11.Size = new System.Drawing.Size(66, 16);
			this.label11.TabIndex = 16;
			// 
			// dfSTREET4
			// 
			this.dfSTREET4.Name = "dfSTREET4";
			this.dfSTREET4.MaxLength = 30;
			this.dfSTREET4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTREET4.Location = new System.Drawing.Point(87, 204);
			this.dfSTREET4.Size = new System.Drawing.Size(225, 20);
			this.dfSTREET4.TabIndex = 17;
			// 
			// dfSTATE
			// 
			this.dfSTATE.Name = "dfSTATE";
			this.dfSTATE.MaxLength = 2;
			this.dfSTATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfSTATE.Location = new System.Drawing.Point(88, 236);
			this.dfSTATE.Size = new System.Drawing.Size(29, 20);
			this.dfSTATE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfSTATE_WindowActions);
			this.dfSTATE.TabIndex = 18;
			// 
			// df16
			// 
			this.df16.Name = "df16";
			this.df16.Visible = false;
			this.df16.MaxLength = 30;
			this.df16.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.df16.Location = new System.Drawing.Point(71, 71);
			this.df16.Size = new System.Drawing.Size(225, 20);
			this.df16.TabIndex = 19;
			// 
			// df17
			// 
			this.df17.Name = "df17";
			this.df17.Visible = false;
			this.df17.MaxLength = 30;
			this.df17.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.df17.Location = new System.Drawing.Point(69, 100);
			this.df17.Size = new System.Drawing.Size(225, 20);
			this.df17.TabIndex = 20;
			// 
			// df18
			// 
			this.df18.Name = "df18";
			this.df18.Visible = false;
			this.df18.MaxLength = 2;
			this.df18.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.df18.Location = new System.Drawing.Point(76, 128);
			this.df18.Size = new System.Drawing.Size(36, 20);
			this.df18.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.df18_WindowActions);
			this.df18.TabIndex = 21;
			// 
			// dfPROVID
			// 
			this.dfPROVID.Name = "dfPROVID";
			this.dfPROVID.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPROVID.TabIndex = 22;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 23;
			// 
			// label12
			// 
			this.label12.Name = "label12";
			this.label12.BackColor = System.Drawing.Color.Teal;
			this.label12.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label12.ForeColor = System.Drawing.Color.White;
			this.label12.Text = "Postal Code:";
			this.label12.Location = new System.Drawing.Point(141, 240);
			this.label12.Size = new System.Drawing.Size(74, 16);
			this.label12.TabIndex = 24;
			// 
			// dfZIP
			// 
			this.dfZIP.Name = "dfZIP";
			this.dfZIP.MaxLength = 10;
			this.dfZIP.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfZIP.Location = new System.Drawing.Point(224, 236);
			this.dfZIP.Size = new System.Drawing.Size(84, 20);
			this.dfZIP.EditMask = "99999-9999";
			this.dfZIP.TabIndex = 25;
			// 
			// dfCOUNTY
			// 
			this.dfCOUNTY.Name = "dfCOUNTY";
			this.dfCOUNTY.MaxLength = 8;
			this.dfCOUNTY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfCOUNTY.Location = new System.Drawing.Point(87, 263);
			this.dfCOUNTY.Size = new System.Drawing.Size(85, 20);
			this.dfCOUNTY.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfCOUNTY_WindowActions);
			this.dfCOUNTY.TabIndex = 26;
			// 
			// dfPHONECODE
			// 
			this.dfPHONECODE.Name = "dfPHONECODE";
			this.dfPHONECODE.MaxLength = 5;
			this.dfPHONECODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPHONECODE.Location = new System.Drawing.Point(346, 95);
			this.dfPHONECODE.Size = new System.Drawing.Size(48, 20);
			this.dfPHONECODE.EditMask = "99999";
			this.dfPHONECODE.TabIndex = 27;
			// 
			// dfPHONE
			// 
			this.dfPHONE.Name = "dfPHONE";
			this.dfPHONE.MaxLength = 7;
			this.dfPHONE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfPHONE.Format = "";
			this.dfPHONE.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfPHONE.Location = new System.Drawing.Point(402, 95);
			this.dfPHONE.Size = new System.Drawing.Size(97, 20);
			this.dfPHONE.EditMask = "9999999";
			this.dfPHONE.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfPHONE_WindowActions);
			this.dfPHONE.TabIndex = 28;
			// 
			// dfFAXCODE
			// 
			this.dfFAXCODE.Name = "dfFAXCODE";
			this.dfFAXCODE.MaxLength = 5;
			this.dfFAXCODE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFAXCODE.Location = new System.Drawing.Point(346, 151);
			this.dfFAXCODE.Size = new System.Drawing.Size(48, 20);
			this.dfFAXCODE.EditMask = "99999";
			this.dfFAXCODE.TabIndex = 29;
			// 
			// dfFAX
			// 
			this.dfFAX.Name = "dfFAX";
			this.dfFAX.MaxLength = 7;
			this.dfFAX.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfFAX.Format = "";
			this.dfFAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.dfFAX.Location = new System.Drawing.Point(403, 151);
			this.dfFAX.Size = new System.Drawing.Size(97, 20);
			this.dfFAX.EditMask = "9999999";
			this.dfFAX.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfFAX_WindowActions);
			this.dfFAX.TabIndex = 30;
			// 
			// label13
			// 
			this.label13.Name = "label13";
			this.label13.BackColor = System.Drawing.Color.Teal;
			this.label13.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label13.ForeColor = System.Drawing.Color.White;
			this.label13.Text = "State:";
			this.label13.Location = new System.Drawing.Point(21, 240);
			this.label13.Size = new System.Drawing.Size(39, 16);
			this.label13.TabIndex = 31;
			// 
			// label14
			// 
			this.label14.Name = "label14";
			this.label14.BackColor = System.Drawing.Color.Teal;
			this.label14.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label14.ForeColor = System.Drawing.Color.White;
			this.label14.Text = "Fax:";
			this.label14.Location = new System.Drawing.Point(347, 134);
			this.label14.Size = new System.Drawing.Size(28, 16);
			this.label14.TabIndex = 32;
			// 
			// label15
			// 
			this.label15.Name = "label15";
			this.label15.BackColor = System.Drawing.Color.Teal;
			this.label15.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label15.ForeColor = System.Drawing.Color.White;
			this.label15.Text = "County:";
			this.label15.Location = new System.Drawing.Point(19, 267);
			this.label15.Size = new System.Drawing.Size(45, 16);
			this.label15.TabIndex = 33;
			// 
			// label16
			// 
			this.label16.Name = "label16";
			this.label16.BackColor = System.Drawing.Color.Teal;
			this.label16.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label16.ForeColor = System.Drawing.Color.White;
			this.label16.Text = "Phone:";
			this.label16.Location = new System.Drawing.Point(346, 74);
			this.label16.Size = new System.Drawing.Size(43, 16);
			this.label16.TabIndex = 34;
			// 
			// frame3
			// 
			this.frame3.Name = "frame3";
			this.frame3.BackColor = System.Drawing.Color.Teal;
			this.frame3.BorderStyle = PPJ.Runtime.Windows.BorderStyle.DropShadow;
			this.frame3.BorderSize = 1;
			this.frame3.Location = new System.Drawing.Point(335, 65);
			this.frame3.Size = new System.Drawing.Size(184, 126);
			this.frame3.TabIndex = 35;
			// 
			// dfDescription
			// 
			this.dfDescription.Name = "dfDescription";
			this.dfDescription.BackColor = System.Drawing.Color.Teal;
			this.dfDescription.MaxLength = 30;
			this.dfDescription.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfDescription.Location = new System.Drawing.Point(15, 289);
			this.dfDescription.Size = new System.Drawing.Size(289, 19);
			this.dfDescription.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.dfDescription_WindowActions);
			this.dfDescription.TabIndex = 36;
			// 
			// dfVERIFY
			// 
			this.dfVERIFY.Name = "dfVERIFY";
			this.dfVERIFY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFY.Location = new System.Drawing.Point(256, 295);
			this.dfVERIFY.TabIndex = 37;
			// 
			// cbVERIFY
			// 
			this.cbVERIFY.Name = "cbVERIFY";
			this.cbVERIFY.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.cbVERIFY.Location = new System.Drawing.Point(14, 317);
			this.cbVERIFY.TabIndex = 38;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Date of Verification:";
			this.label3.Location = new System.Drawing.Point(136, 321);
			this.label3.TabIndex = 39;
			// 
			// dfVERIFYDATE
			// 
			this.dfVERIFYDATE.Name = "dfVERIFYDATE";
			this.dfVERIFYDATE.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYDATE.Location = new System.Drawing.Point(248, 319);
			this.dfVERIFYDATE.TabIndex = 40;
			// 
			// label4
			// 
			this.label4.Name = "label4";
			this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label4.Text = "Result:";
			this.label4.Location = new System.Drawing.Point(14, 349);
			this.label4.TabIndex = 41;
			// 
			// dfVERIFYRESULT
			// 
			this.dfVERIFYRESULT.Name = "dfVERIFYRESULT";
			this.dfVERIFYRESULT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYRESULT.Location = new System.Drawing.Point(58, 347);
			this.dfVERIFYRESULT.TabIndex = 42;
			// 
			// label5
			// 
			this.label5.Name = "label5";
			this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label5.Text = "Next Verification:";
			this.label5.Location = new System.Drawing.Point(136, 349);
			this.label5.TabIndex = 43;
			// 
			// dfVERIFYNEXT
			// 
			this.dfVERIFYNEXT.Name = "dfVERIFYNEXT";
			this.dfVERIFYNEXT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfVERIFYNEXT.Location = new System.Drawing.Point(248, 347);
			this.dfVERIFYNEXT.TabIndex = 44;
			// 
			// label6
			// 
			this.label6.Name = "label6";
			this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label6.Text = "Notes:";
			this.label6.Location = new System.Drawing.Point(335, 198);
			this.label6.TabIndex = 45;
			// 
			// dfNOTES
			// 
			this.dfNOTES.Name = "dfNOTES";
			this.dfNOTES.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular);
			this.dfNOTES.Location = new System.Drawing.Point(335, 223);
			this.dfNOTES.Size = new System.Drawing.Size(183, 144);
			this.dfNOTES.TabIndex = 46;
			// 
			// label17
			// 
			this.label17.Name = "label17";
			this.label17.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label17.Text = "Address Type:";
			this.label17.Location = new System.Drawing.Point(328, 43);
			this.label17.Size = new System.Drawing.Size(84, 16);
			this.label17.TabIndex = 47;
			// 
			// fdlgPROV_ADDINFO
			// 
			this.Name = "fdlgPROV_ADDINFO";
			this.ClientSize = new System.Drawing.Size(537, 393);
			this.Text = "Provider Address";
			this.Location = new System.Drawing.Point(256, 295);
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.WindowActions += new PPJ.Runtime.Windows.WindowActionsEventHandler(this.fdlgPROV_ADDINFO_WindowActions);
			this.ClientArea.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.fdlgPROV_ADDINFO == this) 
			{
				App.fdlgPROV_ADDINFO = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
