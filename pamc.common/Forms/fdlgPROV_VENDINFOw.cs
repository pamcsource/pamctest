// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// </summary>
	/// <param name="hWndTbl"></param>
	/// <param name="nMode"></param>
	public partial class fdlgPROV_VENDINFOw : clsDlgInfoFormProviders
	{
		#region Window Parameters
		public SalWindowHandle hWndTbl;
		public SalNumber nMode;
		#endregion
		
		#region Window Variables
		public SalString strWhere = "";
		public SalString strInsertValues = "";
		public SalString strPic = "";
		public SalString strText = "";
		public SalBoolean bNewSet = false;
		public SalBoolean bClearNotes = false;
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public fdlgPROV_VENDINFOw(SalWindowHandle hWndTbl, SalNumber nMode)
		{
			// Assign global reference.
			App.fdlgPROV_VENDINFOw = this;
			// Window Parameters initialization.
			this.hWndTbl = hWndTbl;
			this.nMode = nMode;
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Shows the modal dialog.
		/// </summary>
		/// <param name="owner"></param>
		/// <returns></returns>
		public static SalNumber ModalDialog(Control owner, SalWindowHandle hWndTbl, SalNumber nMode)
		{
			fdlgPROV_VENDINFOw dlg = new fdlgPROV_VENDINFOw(hWndTbl, nMode);
			SalNumber ret = dlg.ShowDialog(owner);
			return ret;
		}
		
		/// <summary>
		/// Returns the object instance associated with the window handle.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public new static fdlgPROV_VENDINFOw FromHandle(SalWindowHandle handle)
		{
			return ((fdlgPROV_VENDINFOw)SalWindow.FromHandle(handle, typeof(fdlgPROV_VENDINFOw)));
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// This function is called from the SAM_Validate msg for each data field.
		/// The value and the handle of the field to check is passed as parameters.
		/// If strItem = strNULL then it will clear all of the affected entries,
		/// else it will set them to the proper looked-up values.
		/// </summary>
		/// <param name="strItem"></param>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean VendorValidateEntry(SalString strItem, SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strFieldName = "";
			SalNumber nInd = 0;
			SalNumber nItem = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				strItem = Vis.StrSubstitute(strItem, "\'", "\'\'");
				if (strFieldName == "dfVENDOR") 
				{
					if (strItem == pamc.sqllib.Const.strNULL) 
					{
						dfVENDORNAME.Text = pamc.sqllib.Const.strNULL;
						return true;
					}
					return Int.SqlLookup("Select  VENDORID,VENDORNM from VEND_MASTERS\r\n" +
						"	Where VENDORID = " + "\'" + strItem + "\'", ":hWndForm.dfVENDOR,:hWndForm.dfVENDORNAME", ref nInd);
				}
			}

			return false;
			#endregion
		}
		
		/// <summary>
		/// This function processes lookups for all data fields on this form.
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		public SalBoolean VendorProcessLookup(SalWindowHandle hWnd)
		{
			#region Local Variables
			SalString strResult = "";
			SalArray<SalString> strCols = new SalArray<SalString>();
			SalString strQuery = "";
			SalString strFieldName = "";
			SalArray<SalNumber> nDisableItems = new SalArray<SalNumber>();
			SalString str1 = "";
			SalString str2 = "";
			SalString str3 = "";
			SalBoolean bLookup = false;
			SalNumber nType = 0;
			#endregion
			
			#region Actions
			using (new SalContext(this))
			{
				strFieldName = hWnd.GetName();
				if (strFieldName == "dfVENDOR") 
				{

					// PPJ: Automatically generated temporary assignments. Properties canot be passed by reference.
					SalString temp1 = Sys.hWndForm.FindControl("dfVENDOR").Text;
					SalString temp2 = Sys.hWndForm.FindControl("dfVENDORNAME").Text;
					SalBoolean temp3 = Int.ProcessLookupVENDOR(nType, ref temp1, ref temp2);
					Sys.hWndForm.FindControl("dfVENDOR").Text = temp1;
					Sys.hWndForm.FindControl("dfVENDORNAME").Text = temp2;

					return temp3;
				}
				return false;
			}
			#endregion
		}
		#endregion
		
		#region Window Actions
		
		/// <summary>
		/// fdlgPROV_VENDINFOw WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_VENDINFOw_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_SetInstanceVars:
					this.fdlgPROV_VENDINFOw_OnCM_SetInstanceVars(sender, e);
					break;
				
				case Const.CM_DoLookup:
					this.fdlgPROV_VENDINFOw_OnCM_DoLookup(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.fdlgPROV_VENDINFOw_OnAM_Initialize(sender, e);
					break;
				
				case Const.CM_Populate:
					this.fdlgPROV_VENDINFOw_OnCM_Populate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_SetInstanceVars event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_VENDINFOw_OnCM_SetInstanceVars(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.hWndTable = this.hWndTbl;
			this.nFormMode = this.nMode;
			this.strTypeTable = "PROV_VENDTYPES";
			this.strTypeTableName = "Vendor Types";
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_VENDINFOw_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.VendorProcessLookup(Sal.GetFocus());
			this.ProcessLookup(Sal.GetFocus());
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_VENDINFOw_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.SendMessageToChildren(Const.AM_Initialize, 0, 0);
			this.SendMessageToChildren(Const.CM_DoLookup, 0, 0);
			// Call SalSendMsgToChildren( hWndForm, CM_DoLookup, 0, 0 )
			// Set dfSEQUENCE = 0
			#endregion
		}
		
		/// <summary>
		/// CM_Populate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fdlgPROV_VENDINFOw_OnCM_Populate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Const.CM_Populate, 0, 0);
			this.SendMessageToChildren(Const.AM_GetValue, 0, 0);
			this.SendMessageToChildren(Const.CM_DoLookup, 0, 0);
			#endregion
		}
		
		/// <summary>
		/// dfVENDOR WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDOR_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfVENDOR_OnSAM_Validate(sender, e);
					break;
				
				case Const.AM_Lookup:
					this.dfVENDOR_OnAM_Lookup(sender, e);
					break;
				
				case pamc.ez32bit.Const.WM_RBUTTONDOWN:
					this.dfVENDOR_OnWM_RBUTTONDOWN(sender, e);
					break;
				
				case Sys.SAM_Create:
					this.dfVENDOR_OnSAM_Create(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDOR_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam);
			if (!(this.VendorValidateEntry(this.dfVENDOR.Text, this.dfVENDOR))) 
			{
				if (!(this.VendorProcessLookup(this.dfVENDOR))) 
				{
					e.Return = Sys.VALIDATE_Cancel;
					return;
				}
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_Lookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDOR_OnAM_Lookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			e.Return = true;
			return;
			#endregion
		}
		
		/// <summary>
		/// WM_RBUTTONDOWN event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDOR_OnWM_RBUTTONDOWN(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (this.dfVENDOR == Sal.GetFocus()) 
			{
				this.VendorProcessLookup(this.dfVENDOR);
			}
            //PPJ:FINAL:MHO - CA:0039 When WM_RBUTTONDOWN/WM_LBUTTONDBLCLK are handled, 0 has to be returned so that application captures mouse.
            e.Return = 0;
            #endregion
        }
		
		/// <summary>
		/// SAM_Create event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDOR_OnSAM_Create(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			this.dfVENDOR.SetRequired(true);
			#endregion
		}
		
		/// <summary>
		/// dfFROMDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfFROMDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfFROMDATE_OnSAM_Validate(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfFROMDATE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (!(this.dfFROMDATE.IsValidDateTime()) && !(this.dfFROMDATE.IsEmpty())) 
			{
				Sal.MessageBox("Invalid date !", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (this.dfFROMDATE.DateTime > this.dfTHRUDATE.DateTime && !(this.dfTHRUDATE.IsEmpty())) 
			{
				Sal.MessageBox("From Date must be less than Through Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// dfTHRUDATE WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTHRUDATE_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Sys.SAM_Validate:
					this.dfTHRUDATE_OnSAM_Validate(sender, e);
					break;
				
				case Const.AM_Initialize:
					this.dfTHRUDATE_OnAM_Initialize(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// SAM_Validate event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTHRUDATE_OnSAM_Validate(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (Sal.SendClassMessage(Sys.SAM_Validate, Sys.wParam, Sys.lParam) == Sys.VALIDATE_Cancel) 
			{
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (!(this.dfTHRUDATE.IsValidDateTime()) && !(this.dfTHRUDATE.IsEmpty())) 
			{
				Sal.MessageBox("Invalid date !", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			if (this.dfTHRUDATE.DateTime < this.dfFROMDATE.DateTime && !(this.dfTHRUDATE.IsEmpty())) 
			{
				Sal.MessageBox("Through Date must be greater than From Date", "Validate", (Sys.MB_Ok | Sys.MB_IconStop));
				e.Return = Sys.VALIDATE_Cancel;
				return;
			}
			e.Return = Sys.VALIDATE_Ok;
			return;
			#endregion
		}
		
		/// <summary>
		/// AM_Initialize event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfTHRUDATE_OnAM_Initialize(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			#endregion
		}
		
		/// <summary>
		/// dfVENDORNAME WindowActions Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDORNAME_WindowActions(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			switch (e.ActionType)
			{
				case Const.CM_DoLookup:
					this.dfVENDORNAME_OnCM_DoLookup(sender, e);
					break;
			}
			#endregion
		}
		
		/// <summary>
		/// CM_DoLookup event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dfVENDORNAME_OnCM_DoLookup(object sender, WindowActionsEventArgs e)
		{
			#region Actions
			e.Handled = true;
			if (!(Int.SqlLookup("Select VENDORNM From VEND_MASTERS Where VENDORID = :dfVENDOR", ":dfVENDORNAME ", ref Var.nGInd))) 
			{
				this.dfVENDORNAME.Text = pamc.sqllib.Const.strNULL;
			}
			#endregion
		}
		#endregion
	}
}
