// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Defines a record of type CLAIM_MASTERS
	/// </summary>
	public class clsClaimMasterR : SalFunctionalClass
	{
		#region Fields
		public SalString strClaimNo = "";
		public SalString strAuthNo = "";
		public SalString strProvId = "";
		public SalString strVendor = "";
		public SalString strMembId = "";
		public SalString strMembName = "";
		public SalNumber nNumEn = 0;
		public SalString strRefProvId = "";
		public SalDateTime dtDateRecd = SalDateTime.Null;
		public SalDateTime dtDatePaid = SalDateTime.Null;
		public SalNumber nChPrefix = 0;
		public SalNumber nNbrUnits = 0;
		public SalString strHpCode = "";
		public SalString strOpt = "";
		public SalString strSpec = "";
		public SalString strStatus = "";
		public SalString strPlaceSvc = "";
		public SalString strOutcome = "";
		public SalNumber nBilled = 0;
		public SalNumber nContrVal = 0;
		public SalNumber nNet = 0;
		public SalString strCaseNo = "";
		public SalNumber nBatch = 0;
		public SalString strProvClaim = "";
		public SalString strEdiRef = "";
		public SalDateTime dtXPDate = SalDateTime.Null;
		public SalString strXPStatus = "";
		public SalString strXPTran = "";
		public SalDateTime dtOutLoadDt = SalDateTime.Null;
		public SalNumber nPcpEn = 0;
		public SalNumber nCob = 0;
		public SalDateTime dtDateFrom = SalDateTime.Null;
		public SalNumber nCreateBy = 0;
		public SalDateTime dtCreateDate = SalDateTime.Null;
		public SalNumber nLastChangeBy = 0;
		public SalDateTime dtLastChangeDate = SalDateTime.Null;
		public SalDateTime dtDateTo = SalDateTime.Null;
		public SalString strSubAcct = "";
		public SalString strAcct = "";
		public SalString strP0 = "";
		public SalString strP1 = "";
		public SalString strP2 = "";
		public SalString strP3 = "";
		public SalString strP4 = "";
		public SalString strP5 = "";
		public SalString strP6 = "";
		public SalString strP7 = "";
		public SalString strP8 = "";
		public SalString strP9 = "";
		public SalString strPHCode = "";
		public SalNumber nCheck = 0;
		public SalString strHRId = "";
		public SalDateTime dtAdmDate = SalDateTime.Null;
		public SalNumber nAdmHour = 0;
		public SalString strAdmHour = "";
		public SalString strAdmType = "";
		public SalString strAdmSource = "";
		public SalDateTime dtDSchDate = SalDateTime.Null;
		public SalNumber nDSchHour = 0;
		public SalString strDSchHour = "";
		public SalNumber nInPatDays = 0;
		public SalString strAdProvId = "";
		public SalString strAtProvId = "";
		public SalString strHosPat = "";
		public SalString strProvPat = "";
		public SalString strMedRecNo = "";
		public SalString strBillType = "";
		public SalString strProc1 = "";
		public SalDateTime dtProcDate1 = SalDateTime.Null;
		public SalString strProc2 = "";
		public SalDateTime dtProcDate2 = SalDateTime.Null;
		public SalString strProc3 = "";
		public SalDateTime dtProcDate3 = SalDateTime.Null;
		public SalString strProc4 = "";
		public SalDateTime dtProcDate4 = SalDateTime.Null;
		public SalString strCondCode1 = "";
		public SalString strCondCode2 = "";
		public SalString strCondCode3 = "";
		public SalString strCondCode4 = "";
		public SalString strCondCode5 = "";
		public SalString strCondCode6 = "";
		public SalString strCondCode7 = "";
		public SalString strSUBSSN = "";
		public SalNumber nSavingsAcc = 0;
		public SalString strClaimType = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsClaimMasterR(){ }
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsClaimMasterR(object derived)
		{
			// Attach derived instance.
			this._derived = derived;
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Clears all the instance variables
		/// </summary>
		/// <returns></returns>
		public SalNumber Initialize()
		{
			#region Actions
			strClaimNo = SalString.Null;
			strAuthNo = SalString.Null;
			strProvId = "99999999999999999999";
			strVendor = "99999999999999999999";
			strMembId = SalString.Null;
			strMembName = SalString.Null;
			nNumEn = SalNumber.Null;
			strRefProvId = SalString.Null;
			dtDateRecd = SalDateTime.Null;
			dtDatePaid = SalDateTime.Null;
			nChPrefix = 999;
			nNbrUnits = 0;
			strHpCode = SalString.Null;
			strOpt = SalString.Null;
			strSpec = "14";
			strStatus = "9";
			strPlaceSvc = "11";
			strOutcome = "1";
			nBilled = SalNumber.Null;
			nContrVal = SalNumber.Null;
			nNet = SalNumber.Null;
			strCaseNo = SalString.Null;
			nBatch = SalNumber.Null;
			strProvClaim = SalString.Null;
			strEdiRef = SalString.Null;
			dtXPDate = SalDateTime.Null;
			strXPStatus = SalString.Null;
			strXPTran = SalString.Null;
			dtOutLoadDt = SalDateTime.Null;
			nPcpEn = SalNumber.Null;
			nCob = SalNumber.Null;
			dtDateFrom = SalDateTime.Null;
			nCreateBy = SalNumber.Null;
			dtCreateDate = SalDateTime.Null;
			nLastChangeBy = SalNumber.Null;
			dtLastChangeDate = SalDateTime.Null;
			dtDateTo = SalDateTime.Null;
			strSubAcct = "999";
			strAcct = "5000";
			strP0 = SalString.Null;
			strP1 = SalString.Null;
			strP2 = SalString.Null;
			strP3 = SalString.Null;
			strP4 = SalString.Null;
			strP5 = SalString.Null;
			strP6 = SalString.Null;
			strP7 = SalString.Null;
			strP8 = SalString.Null;
			strP9 = SalString.Null;
			strPHCode = "P";
			nCheck = 999999;
			strHRId = SalString.Null;
			dtAdmDate = SalDateTime.Null;
			nAdmHour = SalNumber.Null;
			strAdmHour = SalString.Null;
			strAdmType = SalString.Null;
			strAdmSource = SalString.Null;
			dtDSchDate = SalDateTime.Null;
			nDSchHour = SalNumber.Null;
			strDSchHour = SalString.Null;
			nInPatDays = SalNumber.Null;
			strAdProvId = SalString.Null;
			strAtProvId = SalString.Null;
			strHosPat = SalString.Null;
			strMedRecNo = SalString.Null;
			strBillType = SalString.Null;
			strProc1 = SalString.Null;
			dtProcDate1 = SalDateTime.Null;
			strProc2 = SalString.Null;
			dtProcDate2 = SalDateTime.Null;
			strProc3 = SalString.Null;
			dtProcDate3 = SalDateTime.Null;
			strProc4 = SalString.Null;
			dtProcDate4 = SalDateTime.Null;
			strCondCode1 = SalString.Null;
			strCondCode2 = SalString.Null;
			strCondCode3 = SalString.Null;
			strCondCode4 = SalString.Null;
			strCondCode5 = SalString.Null;
			strCondCode6 = SalString.Null;
			strCondCode7 = SalString.Null;
			strSUBSSN = SalString.Null;
			nSavingsAcc = SalNumber.Null;
			strClaimType = "P";

			return 0;
			#endregion
		}
		
		/// <summary>
		/// </summary>
		/// <param name="PendData"></param>
		/// <returns></returns>
		public SalNumber CopyPendArray(clsPendData PendData)
		{
			#region Actions
			strP0 = PendData.strPendArray[0];
			strP1 = PendData.strPendArray[1];
			strP2 = PendData.strPendArray[2];
			strP3 = PendData.strPendArray[3];
			strP4 = PendData.strPendArray[4];
			strP5 = PendData.strPendArray[5];
			strP6 = PendData.strPendArray[6];
			strP7 = PendData.strPendArray[7];
			strP8 = PendData.strPendArray[8];
			strP9 = PendData.strPendArray[9];

			return 0;
			#endregion
		}
		#endregion
	}
}
