// <ppj name="pamc.common" date="05.09.2018 14:02:05" id="523D76E80A4B007060A2D46797920F8D013BE936"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.common
{
	
	/// <summary>
	/// Data structure to hold table window data.
	/// </summary>
	public class clsTableData : SalFunctionalClass
	{
		#region Fields
		public SalString strTableName = "";
		public SalArray<SalString> strColNames = new SalArray<SalString>();
		public SalArray<SalWindowHandle> hWndCol = new SalArray<SalWindowHandle>();
		public SalArray<SalNumber> nColType = new SalArray<SalNumber>();
		public SalArray<SalNumber> nColExc = new SalArray<SalNumber>();
		public SalString strInsert = "";
		public SalArray<SalString> strValArr = new SalArray<SalString>();
		public SalArray<clsRow> strValues = new SalArray<clsRow>();
		public SalNumber nRows = 0;
		public SalNumber nCols = 0;
		public SalBoolean bInitialized = false;
		public SalString strNewIns = "";
		#endregion
		
		#region Constructors/Destructors
		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public clsTableData(){ }
		
		/// <summary>
		/// Multiple Inheritance: Constructor.
		/// </summary>
		/// <param name="derived"></param>
		public clsTableData(object derived)
		{
			// Attach derived instance.
			this._derived = derived;
		}
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Returns the database table name for this instance
		/// </summary>
		/// <returns></returns>
		public SalString GetTableName()
		{
			#region Actions
			return strTableName;
			#endregion
		}
		
		/// <summary>
		/// Returns number of rows in data structure
		/// </summary>
		/// <returns></returns>
		public SalNumber GetRows()
		{
			#region Actions
			return nRows;
			#endregion
		}
		
		/// <summary>
		/// Copies row data from table window to clsDataTable structure
		/// </summary>
		/// <returns></returns>
		public SalNumber AddRow()
		{
			#region Local Variables
			SalNumber i = 0;
			#endregion
			
			#region Actions
			while (i < nCols) 
			{
				if (!(hWndCol[i].GetTextUnmasked(ref strValues[nRows].strColData.GetArray(i)[i]))) 
				{
					strValues[nRows].strColData.GetArray(i)[i] = hWndCol[i].GetText(254);
				}
				i = i + 1;
			}
			nRows = nRows + 1;

			return 0;
			#endregion
		}
		
		/// <summary>
		/// Copies row data from clsDataTable structure to table window
		/// </summary>
		/// <param name="nRow"></param>
		/// <returns></returns>
		public SalNumber PopulateRow(SalNumber nRow)
		{
			#region Local Variables
			SalNumber i = 0;
			#endregion
			
			#region Actions
			while (i < nCols) 
			{
				hWndCol[i].SetText(strValues[nRow].strColData[i], false);
				// Call SalSetWindowText( hWndCol[i], strValues[nRow].strColData[i] )
				i = i + 1;
			}
			return Sys.TBL_RowFetched;
			#endregion
		}
		
		/// <summary>
		/// Populates all rows in the table window with data from clsTableData structure.
		/// Table window must process SAM_FetchRow msgs and call PopulateRow() to
		/// copy data from clsTableData structure to table window row.
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <returns></returns>
		public SalNumber PopulateAllRows(SalWindowHandle hWndTbl)
		{
			#region Local Variables
			SalNumber i = 0;
			#endregion
			
			#region Actions
			hWndTbl.ResetTable();
			hWndTbl.SetRange(0, nRows - 1);
			i = 0;
			// Fetch all rows
			while (i < nRows) 
			{
				hWndTbl.FetchRow(i);
				i = i + 1;
			}
			return nRows;
			#endregion
		}
		
		/// <summary>
		/// Sets strTableName, strColNames, hWndCol, strInsert, nCols instance variables.
		/// Call this function to initialize data structure for a given table window.
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <returns></returns>
		public SalNumber Initialize(SalWindowHandle hWndTbl)
		{
			#region Local Variables
			SalString sCol = "";
			SalString sColumn = "";
			SalString sValues = "";
			SalNumber j = 0;
			SalNumber k = 0;
			SalWindowHandle hWndChild = SalWindowHandle.Null;
			#endregion
			
			#region Actions
			sCol = pamc.sqllib.Const.strNULL;
			sColumn = pamc.sqllib.Const.strNULL;
			sValues = pamc.sqllib.Const.strNULL;
			j = 0;
			k = 1;
			strTableName = hWndTbl.GetName();
			strTableName = strTableName.Mid(3, strTableName.Length - 3);
			hWndChild = hWndTbl.GetFirstChild(Sys.TYPE_TableColumn);
			while (hWndChild != SalWindowHandle.Null) 
			{
				if (clsCol.FromHandle(hWndChild).strDBTableName == strTableName && !(hWndChild.SendMessage(Const.CM_NotInsert, 0, 0))) 
				{
					sCol = clsCol.FromHandle(hWndChild).strDBFieldName.ToUpper();
					if (sCol != pamc.sqllib.Const.strNULL) 
					{
						if (sColumn == pamc.sqllib.Const.strNULL) 
						{
							sColumn = sCol;
						}
						else
						{
							sColumn = sColumn + "," + sCol;
						}
						if (sValues == pamc.sqllib.Const.strNULL) 
						{
							sValues = ":strValues[i].strColData[" + j.ToString(0) + "]";
							// Set sValues = strValues[i].strColData[j]
							// Set sValues =  '\'' || strValues[i].strColData[ j ] || '\''   -- Dosent work has a '' Value
						}
						else
						{
							// Set sValues = sValues|| ', ' ||  '\'' ||  strValues[i].strColData[ j ]   || '\''     -- Dosent work has a '' Value
							sValues = sValues + ", :strValues[i].strColData[" + j.ToString(0) + "]";
						}
					}
				}
				else
				{
					nColExc[k] = j;
					k = k + 1;
				}
				strColNames[j] = sCol;
				nColType[j] = hWndChild.GetDataType();
				hWndCol[j] = hWndChild;
				hWndChild = hWndChild.GetNextChild(Sys.TYPE_TableColumn);
				j = j + 1;
			}
			strInsert = "Insert into \r\n	" + strTableName + " \r\n	( " + sColumn + ") \r\n	values (" + sValues + ")";
			nCols = j;
			return true;
			#endregion
		}
		
		/// <summary>
		/// Copies all rows from table window to clsDataTable structure.
		/// </summary>
		/// <param name="hWndTbl"></param>
		/// <returns></returns>
		public SalNumber FillTable(SalWindowHandle hWndTbl)
		{
			#region Local Variables
			SalNumber nRow = 0;
			#endregion
			
			#region Actions
			nRow = Sys.TBL_MinRow;
			nRows = 0;
			while (hWndTbl.FindNextRow(ref nRow, 0, 0)) 
			{
				hWndTbl.SetContextRow(nRow);
				AddRow();
			}

			return 0;
			#endregion
		}
		
		/// <summary>
		/// Sets the number of table rows to be 0.  Effectively clears the table.
		/// </summary>
		/// <returns></returns>
		public SalNumber ResetTable()
		{
			#region Actions
			nRows = 0;

			return 0;
			#endregion
		}
		
		/// <summary>
		/// Sets the column value of all rows.
		/// </summary>
		/// <param name="strColName"></param>
		/// <param name="strVal"></param>
		/// <returns></returns>
		public SalNumber SetColumnValues(SalString strColName, SalString strVal)
		{
			#region Local Variables
			SalNumber i = 0;
			SalNumber nIndex = 0;
			#endregion
			
			#region Actions
			nIndex = GetColumnNo(strColName);
			if (nIndex < 0) 
			{
				return false;
			}
			while (i < nRows) 
			{
				strValues[i].strColData[nIndex] = strVal;
				i = i + 1;
			}
			return true;
			#endregion
		}
		
		/// <summary>
		/// Gets the column number for a given column name
		/// </summary>
		/// <param name="strColName"></param>
		/// <returns></returns>
		public SalNumber GetColumnNo(SalString strColName)
		{
			#region Local Variables
			SalNumber i = 0;
			#endregion
			
			#region Actions
			while (i < nCols) 
			{
				if (strColNames[i] == strColName) 
				{
					return i;
				}
				i = i + 1;
			}
			return -1;
			#endregion
		}
		
		/// <summary>
		/// Performs SQL insert statement for all rows in clsDataTable structure.
		/// </summary>
		/// <returns></returns>
		public SalNumber DoInserts()
		{
			#region Local Variables
			SqlLocals.DoInsertsLocals locals = new SqlLocals.DoInsertsLocals();
			#endregion
			
			#region Actions
			using (new SqlContext(locals, this))
			{
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler146 = delegate(SalSqlHandle hSql)
				{
					pamc.price.Var.hSql.GetError(ref locals.nErr, ref locals.strErr);
					dlgError.ModalDialog(Sys.hWndForm, pamc.price.Var.hSql, "Error Saving Detail Records", "An error occurred while trying to save the detail records in the table");
					return false;
				};
				#endregion

				while (locals.i < nRows) 
				{
					#region WhenSqlError
					WhenSqlErrorHandler sqlErrorHandler147 = delegate(SalSqlHandle hSql)
					{
						pamc.price.Var.hSql.GetError(ref locals.nErr, ref locals.strErr);
						dlgError.ModalDialog(Sys.hWndForm, pamc.price.Var.hSql, "Error Saving Detail Records", "An error occurred while trying to save the detail records in the table");
						return false;
					};
					#endregion

					locals.nUpperCountDim2 = strValues[locals.i].strColData.GetUpperBound(1);
					locals.nExcCountDim = nColExc.GetUpperBound(1);
					strValArr.SetUpperBound(1, -1);
					locals.k = 0;
					locals.l = 0;
					locals.j = 1;
					while (locals.k <= locals.nUpperCountDim2) 
					{
						if (locals.nExcCountDim > 0) 
						{
							while (locals.j <= locals.nExcCountDim) 
							{
								if (nColExc[locals.j] != locals.k) 
								{
									if (nColType[locals.k] == 2) 
									{
										pamc.sqllib.Int.FrmDateTime(strValues[locals.i].strColData[locals.k], ref strValues[locals.i].strColData.GetArray(locals.k)[locals.k]);
										pamc.price.Var.SqlInst.FrmDate(strValues[locals.i].strColData[locals.k], ref strValues[locals.i].strColData.GetArray(locals.k)[locals.k]);
									}
									strValArr[locals.l] = strValues[locals.i].strColData[locals.k];
									locals.l = locals.l + 1;
								}
								locals.j = locals.j + 1;
								locals.k = locals.k + 1;
							}
							locals.j = 1;
						}
						else
						{
							if (nColType[locals.k] == 2) 
							{
								strValues[locals.i].strColData[locals.k] = strValues[locals.i].strColData[locals.k].Left(10);
							}
							strValArr[locals.l] = strValues[locals.i].strColData[locals.k];
							locals.l = locals.l + 1;
							locals.k = locals.k + 1;
						}
					}
					pamc.price.Var.SqlInst.SQLArray2Values(strInsert, strValArr, nColType, ref strNewIns);
					if (!(pamc.price.Var.SqlInst.SqlPrep(pamc.price.Var.hSql, strNewIns, ""))) 
					{
						return false;
					}
					if (!(pamc.price.Var.hSql.Execute(sqlErrorHandler147))) 
					{
						return false;
					}
					locals.i = locals.i + 1;
				}
				return true;
			}
			#endregion
		}
		
		/// <summary>
		/// Deletes all current DB table rows for the given where clause and
		/// performs SQL insert statement for all rows in clsDataTable structure.
		/// </summary>
		/// <param name="strWhere"></param>
		/// <returns></returns>
		public SalNumber DoDeletesAndInserts(SalString strWhere)
		{
			#region Local Variables
			SqlLocals.DoDeletesAndInsertsLocals locals = new SqlLocals.DoDeletesAndInsertsLocals();
			#endregion
			
			#region Actions
			using (new SqlContext(locals, this))
			{

				// PPJ: Assign parameters to the locals instance.
				locals.strWhere = strWhere;

				
				#region WhenSqlError
				WhenSqlErrorHandler sqlErrorHandler148 = delegate(SalSqlHandle hSql)
				{
					dlgError.ModalDialog(Sys.hWndForm, pamc.price.Var.hSql, "Error Saving Detail Records", "An error occurred while trying to save the detail records in the table");
					return false;
				};
				#endregion

				locals.strDelete = "Delete From " + strTableName + " Where " + locals.strWhere;
				if (!(pamc.price.Var.SqlInst.SqlPrepAndExec(pamc.price.Var.hSql, locals.strDelete, ""))) 
				{
					return false;
				}
				return DoInserts();
			}
			#endregion
		}
		#endregion
		
		#region SqlLocals
		
		/// <summary>
		/// Container class used to group the inner classes that contain
		/// the local variables that have been extracted from methods that use sql calls.
		/// </summary>
		private class SqlLocals
		{
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class DoInsertsLocals
			{
				public SalNumber i = 0;
				public SalNumber nErr = 0;
				public SalString strErr = "";
				public SalString strTest = "";
				public SalNumber j = 0;
				public SalNumber k = 0;
				public SalNumber l = 0;
				public SalNumber nUpperCountDim2 = 0;
				public SalNumber nExcCountDim = 0;
			}
			
			/// <summary>
			/// Contains the local variables that have been extracted from the
			/// method that uses sql calls and might need to access local bind variables.
			/// </summary>
			public class DoDeletesAndInsertsLocals
			{
				public SalString strDelete = "";
				public SalString strWhere = "";
			}
		}
		#endregion
	}
}
