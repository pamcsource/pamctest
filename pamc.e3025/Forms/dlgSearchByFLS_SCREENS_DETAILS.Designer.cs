// <ppj name="pamc.e3025" date="07.09.2018 10:36:32" id="375CF598AEC20AE8CD94576E6C72ED58481E72E4"/>
// ======================================================================================================
// This code was generated by the Ice Porter(tm) Tool version 4.6.1.0
// Ice Porter is part of The Porting Project (PPJ) by Ice Tea Group, LLC.
// The generated code is not guaranteed to be accurate and to compile without
// manual modifications.
// 
// ICE TEA GROUP LLC SHALL IN NO EVENT BE LIABLE FOR ANY DAMAGES WHATSOEVER
// (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
// INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER LOSS OF ANY KIND)
// ARISING OUT OF THE USE OR INABILITY TO USE THE GENERATED CODE, WHETHER
// DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR OTHERWISE, REGARDLESS
// OF THE FORM OF ACTION, EVEN IF ICE TEA GROUP LLC HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
// =====================================================================================================
using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using pamc.common;
using pamc.ez32bit;
using pamc.price;
using pamc.sqllib;
using PPJ.Runtime;
using PPJ.Runtime.Sql;
using PPJ.Runtime.Vis;
using PPJ.Runtime.Windows;
using PPJ.Runtime.Windows.QO;

namespace pamc.e3025
{
	
	public partial class dlgSearchByFLS_SCREENS_DETAILS
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Window Controls
		public clsDataFieldSearch dfPROFILE_ID;
		public clsDataFieldSearch dfSCREEN_OBJ_NAME;
		public clsDataFieldSearch dfFIELD_OBJ_NAME;
		protected clsText label1;
		protected clsText label2;
		protected clsText label3;
		#endregion
		
		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dfPROFILE_ID = new pamc.common.clsDataFieldSearch();
			this.dfSCREEN_OBJ_NAME = new pamc.common.clsDataFieldSearch();
			this.dfFIELD_OBJ_NAME = new pamc.common.clsDataFieldSearch();
			this.label1 = new pamc.common.clsText();
			this.label2 = new pamc.common.clsText();
			this.label3 = new pamc.common.clsText();
			this.SuspendLayout();
			// 
			// dfPROFILE_ID
			// 
			this.dfPROFILE_ID.Name = "dfPROFILE_ID";
			this.dfPROFILE_ID.Location = new System.Drawing.Point(152, 56);
			this.dfPROFILE_ID.Size = new System.Drawing.Size(66, 24);
			this.dfPROFILE_ID.TabIndex = 0;
			// 
			// dfSCREEN_OBJ_NAME
			// 
			this.dfSCREEN_OBJ_NAME.Name = "dfSCREEN_OBJ_NAME";
			this.dfSCREEN_OBJ_NAME.Location = new System.Drawing.Point(152, 88);
			this.dfSCREEN_OBJ_NAME.Size = new System.Drawing.Size(255, 24);
			this.dfSCREEN_OBJ_NAME.TabIndex = 1;
			// 
			// dfFIELD_OBJ_NAME
			// 
			this.dfFIELD_OBJ_NAME.Name = "dfFIELD_OBJ_NAME";
			this.dfFIELD_OBJ_NAME.Location = new System.Drawing.Point(152, 128);
			this.dfFIELD_OBJ_NAME.Size = new System.Drawing.Size(255, 24);
			this.dfFIELD_OBJ_NAME.TabIndex = 2;
			// 
			// pbOk
			// 
			this.pbOk.Name = "pbOk";
			this.pbOk.Location = new System.Drawing.Point(-5, 2);
			this.pbOk.TabIndex = 3;
			// 
			// pbOrderBy
			// 
			this.pbOrderBy.Name = "pbOrderBy";
			this.pbOrderBy.TabIndex = 4;
			// 
			// frame1
			// 
			this.frame1.Name = "frame1";
			this.frame1.TabIndex = 5;
			// 
			// pbCount
			// 
			this.pbCount.Name = "pbCount";
			this.pbCount.TabIndex = 6;
			// 
			// dfCOUNT
			// 
			this.dfCOUNT.Name = "dfCOUNT";
			this.dfCOUNT.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.dfCOUNT.TabIndex = 7;
			// 
			// pbClose
			// 
			this.pbClose.Name = "pbClose";
			this.pbClose.TabIndex = 8;
			// 
			// frame2
			// 
			this.frame2.Name = "frame2";
			this.frame2.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.Name = "label1";
			this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label1.Text = "Profile ID:";
			this.label1.Location = new System.Drawing.Point(15, 57);
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.Name = "label2";
			this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label2.Text = "Screen  Name:";
			this.label2.Location = new System.Drawing.Point(15, 89);
			this.label2.Size = new System.Drawing.Size(128, 16);
			this.label2.TabIndex = 11;
			// 
			// label3
			// 
			this.label3.Name = "label3";
			this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold);
			this.label3.Text = "Field  Name:";
			this.label3.Location = new System.Drawing.Point(15, 129);
			this.label3.Size = new System.Drawing.Size(128, 16);
			this.label3.TabIndex = 12;
			// 
			// dlgSearchByFLS_SCREENS_DETAILS
			// 
			this.Controls.Add(this.pbClose);
			this.Controls.Add(this.dfCOUNT);
			this.Controls.Add(this.pbCount);
			this.Controls.Add(this.pbOrderBy);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.dfFIELD_OBJ_NAME);
			this.Controls.Add(this.dfSCREEN_OBJ_NAME);
			this.Controls.Add(this.dfPROFILE_ID);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.frame2);
			this.Controls.Add(this.frame1);
			this.Name = "dlgSearchByFLS_SCREENS_DETAILS";
			this.ClientSize = new System.Drawing.Size(439, 174);
			this.ResumeLayout(false);
		}
		#endregion
		
		#region System Methods/Properties
		
		/// <summary>
		/// Release global reference.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) 
			{
				components.Dispose();
			}
			if (App.dlgSearchByFLS_SCREENS_DETAILS == this) 
			{
				App.dlgSearchByFLS_SCREENS_DETAILS = null;
			}
			base.Dispose(disposing);
		}
		#endregion
	}
}
